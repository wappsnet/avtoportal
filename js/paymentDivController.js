mainModule.controller('paymentController', ['$scope', '$http' ,
    function ($scope, $http) {

        $scope.enabled = false;
        $scope.announcementId = -1;
        $scope.announcementType = -1;

        $scope.init = function(){
            $scope.type = 1;
        };

        $scope.addToUrgent = function(announcementId, announcementType){
            $scope.announcementId = announcementId;
            $scope.announcementType = announcementType;
            $scope.type = 1;
            $scope.fadeIn();
        };

        $scope.addToHomeTop = function(announcementId, announcementType){
            $scope.announcementId = announcementId;
            $scope.announcementType = announcementType;
            $scope.type = 2;
            $scope.fadeIn();
        };

        $scope.addToListTop = function(announcementId, announcementType){
            $scope.announcementId = announcementId;
            $scope.announcementType = announcementType;
            $scope.type = 3;
            $scope.fadeIn();
        };

        $scope.isUrgent = function(){
            return $scope.type == 1;
        };

        $scope.isHomeTop = function(){
            return $scope.type == 2;
        };

        $scope.isListTop = function(){
            return $scope.type == 3;
        };

        $scope.close = function($event){
            $event.preventDefault();
            $scope.fadeOut();
        };

        $scope.accept = function($event){
            $event.preventDefault();
            if($scope.isUrgent()){
                $scope.acceptUrgent();
            }else if($scope.isHomeTop()){
                $scope.acceptHomeTop();
            }else if($scope.isListTop()){
                $scope.acceptListTop();
            }
            $scope.fadeOut();
        };

        $scope.acceptUrgent = function(){
            $http({
                method: 'POST',
                data: "id="+$scope.announcementId+"&formType="+$scope.announcementType,
                url: 'user/addToUrgent',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function () {
                });
        };

        $scope.acceptHomeTop = function(){
            $http({
                method: 'POST',
                data: "id="+$scope.announcementId+"&formType="+$scope.announcementType,
                url: 'user/addToHomeTop',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function () {
                });
        };

        $scope.acceptListTop = function(){
            $http({
                method: 'POST',
                data: "id="+$scope.announcementId+"&formType="+$scope.announcementType,
                url: 'user/addToListTop',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function () {
                });
        };

        $scope.fadeIn = function(){
            $scope.enabled = true;
        };

        $scope.fadeOut = function(){
            $scope.enabled = false;
        };

    }
]);