function initSlider(){
    var ul_left = parseInt($('#slider_ul').css('left'));
    slideLeft('#slider_ul', ul_left);
    var rightSlide = false;
    var leftSlide = true;
    $('#slider_div').hover(function () {
        $('#slider_ul').stop(true, false)
    }, function () {
        slideLeft('#slider_ul', ul_left, 0.05, rightSlide);
        leftSlide = true;
        rightSlide = false;
    });
    var leftArrow = $('#slider_left');
    leftArrow.mousedown(function () {
        slideLeft('#slider_ul', ul_left, 0.2, rightSlide);
        leftSlide = true;
        rightSlide = false;
    });
    leftArrow.mouseup(function () {
        $('#slider_ul').stop(true, false)
    });
    leftArrow.mouseout(function () {
        $('#slider_ul').stop(true, false)
    });
    var rightArrow = $('#slider_right');
    rightArrow.mousedown(function () {
        slideRight('#slider_ul', ul_left, 0.2, leftSlide);
        leftSlide = false;
        rightSlide = true;
    });
    rightArrow.mouseup(function () {
        $('#slider_ul').stop(true, false)
    });
    rightArrow.mouseout(function () {
        $('#slider_ul').stop(true, false)
    });
};

function slideLeft(ulId, initial_left, speed, reversed) {
    var item_width = $(ulId + ' li').outerWidth();

    var left = parseInt($(ulId).css('left'));
    var path_to_go = reversed ? Math.abs(initial_left - left) : item_width - (Math.abs(left - initial_left));
    var left_indent = initial_left - item_width;
    var speed_calculated = ((typeof speed == 'undefined') ? 0.05 : speed);
    $(ulId).animate({'left': '-=' + path_to_go},
        {
            duration: parseFloat(path_to_go / speed_calculated),
            easing: 'linear',
            complete: function () {
                if(!reversed){
                    $(ulId + ' li:last').after($(ulId + ' li:first'));
                }
                $(ulId).css('left', initial_left);
                slideLeft(ulId, initial_left, speed);
            }
        })
};

function slideRight(ulId, initial_left, speed, reversed) {
    var item_width = $(ulId + ' li').outerWidth();

    var left = parseInt($(ulId).css('left'));
    var path_to_go = reversed ? Math.abs(left - initial_left) : item_width - (Math.abs(initial_left - left));
    var left_indent = item_width + initial_left;
    var speed_calculated = ((typeof speed == 'undefined') ? 0.05 : speed);
    $(ulId).animate({'left': '+=' + path_to_go},
        {
            duration: parseFloat(path_to_go / speed_calculated),
            easing: 'linear',
            complete: function () {
                if(!reversed){
                    $(ulId + ' li:first').before($(ulId + ' li:last'));
                }
                $(ulId).css('left', initial_left);
                slideRight(ulId, initial_left, speed);
            }
        }
    )
};