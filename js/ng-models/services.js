mainModule.factory('Group', ['$resource', function ($resource) {
    return new $resource('rest/groups/:id',
        {id: '@id'},
        {});
}]);

mainModule.factory('Services', ['$resource', function ($resource) {
    return new $resource('rest/services/:id',
        {id: '@id'},
        {});
}]);

mainModule.factory('Location', ['$resource', function ($resource) {
    return new $resource('rest/location/:id',
        {id: '@id'},
        {});
}]);

mainModule.factory('ArmLocation', ['$resource', function ($resource) {
    return new $resource('rest/arm_location/:id',
        {id: '@id'},
        {});
}]);



mainModule.factory('carAnnouncements', ['$resource', function ($resource) {
    return new $resource('rest/car_announcements/:id',
        {id: '@id'},
        {});
}]);

mainModule.factory('transportationAnnouncements', ['$resource', function ($resource) {
    return new $resource('rest/transportation_announcements/:id',
        {id: '@id'},
        {});
}]);

mainModule.factory('specCarAnnouncements', ['$resource', function ($resource) {
    return new $resource('rest/spec_car_announcements/:id',
        {id: '@id'},
        {});
}]);

mainModule.factory('tourismAnnouncements', ['$resource', function ($resource) {
    return new $resource('rest/tourism_announcements/:id',
        {id: '@id'},
        {});
}]);



mainModule.factory('ruralAnnouncements', ['$resource', function ($resource) {
    return new $resource('rest/rural_announcements/:id',
        {id: '@id'},
        {});
}]);


mainModule.factory('carRentAnnouncements', ['$resource', function ($resource) {
    return new $resource('rest/car_rent_announcements/:id',
        {id: '@id'},
        {});
}]);


mainModule.factory('jobAnnouncements', ['$resource', function ($resource) {
    return new $resource('rest/job_announcements/:id',
        {id: '@id'},
        {});
}]);


mainModule.factory('taxiAnnouncements', ['$resource', function ($resource) {
    return new $resource('rest/taxi_announcements/:id',
        {id: '@id'},
        {});
}]);

mainModule.factory('specCarAnnouncements', ['$resource', function ($resource) {
    return new $resource('rest/specCar_announcements/:id',
        {id: '@id'},
        {});
}]);


mainModule.factory('autoServiceAnnouncements', ['$resource', function ($resource) {
    return new $resource('rest/auto_service/:id',
        {id: '@id'},
        {});
}]);



mainModule.factory('evacuatorAnnouncements', ['$resource', function ($resource) {
    return new $resource('rest/evacuator_announcements/:id',
        {id: '@id'},
        {});
}]);


mainModule.factory('shippingAnnouncements', ['$resource', function ($resource) {
    return new $resource('rest/shipping_announcements/:id',
        {id: '@id'},
        {});
}]);

mainModule.factory('spareParts', ['$resource', function ($resource) {
    return new $resource('rest/spareParts/:id',
        {id: '@id'},
        {});
}]);

mainModule.factory('wheels', ['$resource', function ($resource) {
    return new $resource('rest/wheels/:id',
        {id: '@id'},
        {});
}]);

mainModule.factory('news', ['$resource', function ($resource) {
    return new $resource('rest/news/:id',
        {id: '@id'},
        {});
}]);


mainModule.factory('phoneType', ['$resource', function ($resource) {
    return new $resource('rest/phoneType/:id',
        {id: '@id'},
        {});
}]);



mainModule.factory('appa', ['$resource', function ($resource) {
    return new $resource('rest/appa/:id',
        {id: '@id'},
        {});
}]);

mainModule.factory('autoschool', ['$resource', function ($resource) {
    return new $resource('rest/autoschool/:id',
        {id: '@id'},
        {});
}]);

mainModule.factory('techObserve', ['$resource', function ($resource) {
    return new $resource('rest/techObserve/:id',
        {id: '@id'},
        {});
}]);

mainModule.factory('taxService', ['$resource', function ($resource) {
    return new $resource('rest/taxService/:id',
        {id: '@id'},
        {});
}]);



mainModule.factory('banks', ['$resource', function ($resource) {
    return new $resource('rest/banks/:id',
        {id: '@id'},
        {});
}]);

mainModule.factory('appaAutoTypes', ['$resource', function ($resource) {
    return new $resource('rest/appaAutoTypes/:id',
        {id: '@id'},
        {});
}]);

mainModule.factory('appaAutoUsages', ['$resource', function ($resource) {
    return new $resource('rest/appaAutoUsages/:id',
        {id: '@id'},
        {});
}]);

mainModule.factory('appaContractPeriods', ['$resource', function ($resource) {
    return new $resource('rest/appaContractPeriods/:id',
        {id: '@id'},
        {});
}]);

mainModule.factory('sortingOptions', ['$resource', function ($resource) {
    return new $resource('rest/sortingOptions/:id',
        {id: '@id'},
        {});
}]);


mainModule.factory('banks', ['$resource', function ($resource) {
    return new $resource('rest/banks/:id',
        {id: '@id'},
        {});
}]);

mainModule.factory('isRegistered',function($http, $q){
        return{
            apiPath:'site/authenticateUser',
            getIsRegistered: function(){
                //Creating a deferred object
                var deferred = $q.defer();

                //Calling Web API to fetch shopping cart items
                $http.get(this.apiPath).success(function(data){
                    //Passing data to deferred's resolve function on successful completion
                    deferred.resolve(data.registered);
                }).error(function(){

                        //Sending a friendly error message in case of failure
                        deferred.reject("An error occured while fetching items");
                    });

                //Returning the promise object
                return deferred.promise;
            }
        }
});

mainModule.factory('isAdmin',function($http, $q){
    return{
        apiPath:'site/isAdmin',
        getIsAdmin: function(){
            //Creating a deferred object
            var deferred = $q.defer();

            //Calling Web API to fetch shopping cart items
            $http.get(this.apiPath).success(function(data){
                //Passing data to deferred's resolve function on successful completion
                deferred.resolve(data.isAdmin);
            }).error(function(){

                    //Sending a friendly error message in case of failure
                    deferred.reject("An error occured while fetching items");
                });

            //Returning the promise object
            return deferred.promise;
        }
    }
});


mainModule.service('uploadService', [ '$http', function ($http) {
    var uploads = {
        images: [
            {src: 'images/icon1.png', id: 'upl_img_1', dataId: '-55', isUploaded: false, title: '', orderId: 1},
            {src: 'images/icon2.png', id: 'upl_img_2', dataId: '-55', isUploaded: false, title: '', orderId: 2},
            {src: 'images/icon3.png', id: 'upl_img_3', dataId: '-55', isUploaded: false, title: '', orderId: 3},
            {src: 'images/icon4.png', id: 'upl_img_4', dataId: '-55', isUploaded: false, title: '', orderId: 4},
            {src: 'images/icon5.png', id: 'upl_img_5', dataId: '-55', isUploaded: false, title: '', orderId: 5},
            {src: 'images/icon6.png', id: 'upl_img_6', dataId: '-55', isUploaded: false, title: '', orderId: 6},
            {src: 'images/icon7.png', id: 'upl_img_7', dataId: '-55', isUploaded: false, title: '', orderId: 7},
            {src: 'images/icon8.png', id: 'upl_img_8', dataId: '-55', isUploaded: false, title: '', orderId: 8},
            {src: 'images/icon9.png', id: 'upl_img_9', dataId: '-55', isUploaded: false, title: '', orderId: 9}
        ],
        imagesToDelete: [],
        imagesToSave: [],
        uploadedImages: function (parametr, data) {
            $http.get('site/loadUploads?formType=' + parametr)
                .success(function (data) {
                    return data;
                });

            return data;
        }};
    var dealerUploads = {
        images: [
            {src: 'images/icon1.png', id: '1upl_img_1', dataId: '-55', isUploaded: false, title: '', orderId: 1},
            {src: 'images/icon2.png', id: '1upl_img_2', dataId: '-55', isUploaded: false, title: '', orderId: 2},
            {src: 'images/icon3.png', id: '1upl_img_3', dataId: '-55', isUploaded: false, title: '', orderId: 3},
            {src: 'images/icon4.png', id: '1upl_img_4', dataId: '-55', isUploaded: false, title: '', orderId: 4},
            {src: 'images/icon5.png', id: '1upl_img_5', dataId: '-55', isUploaded: false, title: '', orderId: 5},
            {src: 'images/icon6.png', id: '1upl_img_6', dataId: '-55', isUploaded: false, title: '', orderId: 6},
            {src: 'images/icon7.png', id: '1upl_img_7', dataId: '-55', isUploaded: false, title: '', orderId: 7},
            {src: 'images/icon8.png', id: '1upl_img_8', dataId: '-55', isUploaded: false, title: '', orderId: 8},
            {src: 'images/icon9.png', id: '1upl_img_9', dataId: '-55', isUploaded: false, title: '', orderId: 9}
        ],
        imagesToDelete: [],
        imagesToSave: [],
        uploadedImages: function (parametr, data) {
            $http.get('site/loadUploads?formType=' + parametr)
                .success(function (data) {
                    return data;
                });

            return data;
        }};
    return {
        getUploads: function () {
            return uploads;
        },

        getDealerUploads: function () {
            return dealerUploads;
        }
    };
}]);

mainModule.factory('immediateAutos', ['$resource', function ($resource) {
    return new $resource('site/immediateAutos/',
        {id: '@id'},
        {});
}]);

//for answers

mainModule.filter('truncate', function () {
    return function (text, length, end) {
        if (isNaN(length))
            length = 10;
        if(!text)
            return '';
        if (end === undefined)
            end = "...";

        if (text.length <= length || text.length - end.length <= length) {
            return text;
        }
        else {
            return String(text).substring(0, length-end.length) + end;
        }

    };
});

mainModule.factory('GroupAnswerAuto', ['$resource', function ($resource) {
    return new $resource('rest/CarProperty/:id',
        {id: '@id' , ann_id : '@ann_id' },
        {});
}]);

mainModule.factory('GroupAnswerAutoRent', ['$resource', function ($resource) {
    return new $resource('rest/CarRentProperties/:id',
        {id: '@id' , ann_id : '@ann_id' },
        {});
}]);

mainModule.factory('GroupAnswerSparePart', ['$resource', function ($resource) {
    return new $resource('rest/SparePartProperty/:id',
        {id: '@id' , ann_id : '@ann_id' },
        {});
}]);

mainModule.factory('GroupAnswerSparePartWheel', ['$resource', function ($resource) {
    return new $resource('rest/WheelsProperties/:id',
        {id: '@id' , ann_id : '@ann_id' },
        {});
}]);

mainModule.factory('GroupAnswerTaxi', ['$resource', function ($resource) {
    return new $resource('rest/TaxiProperties/:id',
        {id: '@id' , ann_id : '@ann_id' },
        {});
}]);

mainModule.factory('GroupAnswerEvacuator', ['$resource', function ($resource) {
    return new $resource('rest/EvacuatorProperties/:id',
        {id: '@id' , ann_id : '@ann_id' },
        {});
}]);

mainModule.factory('GroupAnswerShipping', ['$resource', function ($resource) {
    return new $resource('rest/ShippingProperties/:id',
        {id: '@id' , ann_id : '@ann_id' },
        {});
}]);

mainModule.factory('GroupAnswerTransportation', ['$resource', function ($resource) {
    return new $resource('rest/TransportationProperties/:id',
        {id: '@id' , ann_id : '@ann_id' },
        {});
}]);

mainModule.factory('GroupAnswerTourism', ['$resource', function ($resource) {
    return new $resource('rest/TourismProperties/:id',
        {id: '@id' , ann_id : '@ann_id' },
        {});
}]);

mainModule.factory('GroupAnswerRuraltech', ['$resource', function ($resource) {
    return new $resource('rest/RuraltechProperties/:id',
        {id: '@id' , ann_id : '@ann_id' },
        {});
}]);

mainModule.factory('GroupAnswerJob', ['$resource', function ($resource) {
    return new $resource('rest/JobProperties/:id',
        {id: '@id' , ann_id : '@ann_id' },
        {});
}]);

mainModule.factory('GroupAnswerSpecCar', ['$resource', function ($resource) {
    return new $resource('rest/SpecCarProperties/:id',
        {id: '@id' , ann_id : '@ann_id' },
        {});
}]);

mainModule.factory('GroupAnswerAutoService', ['$resource', function ($resource) {
    return new $resource('rest/AutoServiceProperties/:id',
        {id: '@id' , ann_id : '@ann_id' },
        {});
}]);

mainModule.factory('AutoModelGroup', ['$resource', function ($resource) {
    return new $resource('rest/ModelGroups/:id',
        {id: '@id' , ann_id : '@ann_id' },
        {});
}]);

mainModule.factory('RoadPoliceAddresses', ['$resource', function ($resource) {
    return new $resource('rest/RoadPoliceAddresses/:id',
        {id: '@id' , ann_id : '@ann_id' },
        {});
}]);

mainModule.factory('RoadPolicePenalties', ['$resource', function ($resource) {
    return new $resource('rest/RoadPolicePenalties/:id',
        {id: '@id' , ann_id : '@ann_id' },
        {});
}]);

mainModule.service('filters', function ($http, $angularCacheFactory,$q) {
    $angularCacheFactory('dataCache', {
        maxAge: 1800000, // Items added to this cache expire after 30 minutes.
        cacheFlushInterval: 3600000, // This cache will clear itself every hour.
        deleteOnExpire: 'aggressive', // Items will be deleted from this cache right when they expire.
        storageMode: 'localStorage'
    });

    return {
            getFilterData: function () {
                var deferred = $q.defer(),
                    start = new Date().getTime();

            $http.get('dAOClassifiers/loadCarAnnouncementFilters', {
                cache: $angularCacheFactory.get('dataCache')
            }).success(function (data) {
                    console.log('time taken for request: ' + (new Date().getTime() - start) + 'ms');
                    deferred.resolve(data);

                });
            return deferred.promise;
        }
    };
});

mainModule.service('groupByID', function ($http, $angularCacheFactory,$q) {

    $angularCacheFactory('groupCache', {
        maxAge: 1800000, // Items added to this cache expire after 30 minutes.
        cacheFlushInterval: 3600000, // This cache will clear itself every hour.
        deleteOnExpire: 'aggressive', // Items will be deleted from this cache right when they expire.
        storageMode: 'localStorage'
    });

    return {
        getGroupById: function (id) {
            var deferred = $q.defer();
            $http.get('utility/groupByID/?group_id=' + id, {
                cache: $angularCacheFactory.get('groupCache')
            }).success(function (data) {
                    deferred.resolve(data);
                });
            return deferred.promise;
        }
    };
});


mainModule.service('groupAnswersByID', function ($http,$q) {
    return {
        getGroupAnswersById: function (group_id , ann_id , model_name) {
            var deferred = $q.defer();
            $http({
                method: 'post',
                url: 'utility/answerGroupID',
                data: 'group_id=' + group_id + '&ann_id=' + ann_id + '&model=' + model_name,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                    deferred.resolve(data);
                });
            return deferred.promise;
        }
    };
});

mainModule.service('groupAnswersByIDLookup', function ($http,$q) {
    return {
        getGroupAnswersById: function (group_id , ann_id , model_name) {
            var deferred = $q.defer();
            $http({
                method: 'post',
                url: 'utility/answerGroupID',
                data: 'group_id=' + group_id + '&ann_id=' + ann_id + '&model=' + model_name,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                    var props = data.properties;
                    for(var i = 0 ; i < props.length ; i++){
                        if(props[i].c_property_type_id == 2 && props[i].lookup_items != null){
                            for(var j = 0; j < props[i].lookup_items.length ; j++){
                                if( props[i].lookup_items[j].id == props[i].temp_value){
                                    props[i].temp_value = props[i].lookup_items[j]
                                }
                            }
                        }
                    }
                    deferred.resolve(data);
                });
            return deferred.promise;
        }
    };
});






mainModule.filter('unsafe', function($sce) {
    return function(val) {
        return $sce.trustAsHtml(val);
    };
});



mainModule.service('pagingProperties', function ($rootScope) {

    var pageSizeOptions = [
        {"id": 10},
        {"id": 15},
        {"id": 20},
        {"id": 25},
        {"id": 30},
        {"id": 35},
        {"id": 40}
    ];
    var currentPage = 0;
    var pageSize = 10;
    var numberOfPages = 0;

    return {

        getPageSizeOptions : function(){
            return pageSizeOptions;
        },

        setCurrentPage : function (currentPageSet) {
            currentPage = currentPageSet;
        },

        getCurrentPage : function () {
            return currentPage;
        },

        setPageSize : function (pageSizeSet) {
            pageSize = pageSizeSet;
            this.loadNumberOfPages();
        },

        getPageSize : function () {
            return pageSize;
        },

        loadNumberOfPages : function () {
            numberOfPages = Math.ceil($rootScope.announcements.length / pageSize);
        },

        getNumberOfPages : function(){
            return numberOfPages;
        }
    };
});




