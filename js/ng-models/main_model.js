var mainModule = angular.module('main', ['ngResource','google-maps','jmdobry.angular-cache','cgBusy']);

mainModule.run(function($rootScope) {
    // you can inject any instance here
});

mainModule.constant('onlyNumberPattern', /^[0-9]+$/);

mainModule.filter('startFrom', function () {
    return function (input, start) {
        if(input){
            start = +start; //parse to int
            return input.slice(start);
        }
        return input;

    }
});

mainModule.directive('calendar', function () {
    return {
        require: 'ngModel',
        link: function (scope, el, attr, ngModel) {
            $(el).datepicker({
                inline: true,
                showOtherMonths: true,
                dateFormat: 'yy-mm-dd',
                dayNamesMin: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
                onSelect: function (dateText) {
                    scope.$apply(function () {
                        ngModel.$setViewValue(dateText);
                    });
                }
            });
        }
    };
})
//mainModule.config(['$httpProvider', function ($httpProvider) {
//    var $http,
//        interceptor = ['$q', '$injector', function ($q, $injector) {
//            var error;
//
//            function success(response) {
//                // get $http via $injector because of circular dependency problem
//                $http = $http || $injector.get('$http');
//                if($http.pendingRequests.length < 1) {
//                    $("body").css("overflow", "auto");
//                    $('#loadingWidget').hide();
//                }
//                return response;
//            }
//
//
//            function error(response) {
//                $("body").css("overflow", "auto");
//                // get $http via $injector because of circular dependency problem
//                $http = $http || $injector.get('$http');
//                if($http.pendingRequests.length < 1) {
//                    $('#loadingWidget').hide();
//                }
//                return $q.reject(response);
//            }
//
//            return function (promise) {
//                $("body").css("overflow", "hidden");
//                $('#loadingWidget').height($( document ).height());
//                $('#loadingWidget').width($('#loadingWidget').parent().width());
//                $('#loadingWidget').show();
//                return promise.then(success, error);
//            }
//        }];
//
//    $httpProvider.responseInterceptors.push(interceptor);
//}]);


var dates =  {compare:function(a,b) {
    // Compare two dates (could be of any type supported by the convert
    // function above) and returns:
    //  -1 : if a < b
    //   0 : if a = b
    //   1 : if a > b
    // NaN : if a or b is an illegal date
    // NOTE: The code inside isFinite does an assignment (=).
    return (
        isFinite(a=this.convert(a).valueOf()) &&
            isFinite(b=this.convert(b).valueOf()) ?
            (a>b)-(a<b) :
            NaN
        );
    },
    convert:function(d) {
        // Converts the date in d to a date-object. The input can be:
        //   a date object: returned without modification
        //  an array      : Interpreted as [year,month,day]. NOTE: month is 0-11.
        //   a number     : Interpreted as number of milliseconds
        //                  since 1 Jan 1970 (a timestamp)
        //   a string     : Any format supported by the javascript engine, like
        //                  "YYYY/MM/DD", "MM/DD/YYYY", "Jan 31 2009" etc.
        //  an object     : Interpreted as an object with year, month and date
        //                  attributes.  **NOTE** month is 0-11.
        return (
            d.constructor === Date ? d :
                d.constructor === Array ? new Date(d[0],d[1],d[2]) :
                    d.constructor === Number ? new Date(d) :
                        d.constructor === String ? new Date(d) :
                            typeof d === "object" ? new Date(d.year,d.month,d.date) :
                                NaN
            );
    },
     formatDate:function (formatDate, formatString)
        {
            var yyyy = formatDate.getFullYear();
            var yy = yyyy.toString().substring(2);
            var m = formatDate.getMonth() + 1;
            var mm = m < 10 ? "0" + m : m;
            var d = formatDate.getDate();
            var dd = d < 10 ? "0" + d : d;

            var h = formatDate.getHours();
            var hh = h < 10 ? "0" + h : h;
            var n = formatDate.getMinutes();
            var nn = n < 10 ? "0" + n : n;
            var s = formatDate.getSeconds();
            var ss = s < 10 ? "0" + s : s;

            formatString = formatString.replace(/yyyy/i, yyyy);
            formatString = formatString.replace(/yy/i, yy);
            formatString = formatString.replace(/mm/i, mm);
            formatString = formatString.replace(/m/i, m);
            formatString = formatString.replace(/dd/i, dd);
            formatString = formatString.replace(/d/i, d);
            formatString = formatString.replace(/hh/i, hh);
            formatString = formatString.replace(/h/i, h);
            formatString = formatString.replace(/nn/i, nn);
            formatString = formatString.replace(/n/i, n);
            formatString = formatString.replace(/ss/i, ss);
            formatString = formatString.replace(/s/i, s);

            return formatString;
        }
}

