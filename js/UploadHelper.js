/**
 * Created with JetBrains PhpStorm.
 * User: Yerem
 * Date: 6/19/13
 * Time: 12:48 AM
 * To change this template use File | Settings | File Templates.
 */
function doAjaxUpload(imageId, fileId, optional) {
    if(!optional){
        $("#loading").fadeIn();
    }else{
        $("#loading1").fadeIn();
    }

    $.ajaxFileUpload
    (
        {
            url: 'upload/upload',
            secureuri: false,
            fileElementId: fileId,
            dataType: 'json',
            data: {name: 'logan', id: 'id'},
            success: function (data, status) {
                $('#qqfile').change(function () {
                    $('#path').val($(this).val());
                });
                if (typeof(data.error) != 'undefined') {
                    if (data.error != '') {
                        if(!optional){
                            $("#loading").fadeOut();
                        }else{
                            $("#loading1").fadeOut();
                        }

                        //alert(data.error);
                    } else {
                        $("#"+imageId+"").attr("src", data.imagePath);
                        $("#"+imageId+"").closest("a").attr("data-id", data.id);
                        $("#"+imageId+"").attr("alt", data.title);
                        var scope = angular.element("#"+imageId+"").scope();
                        scope.image.title = data.title;
                        scope.image.src = data.imagePath;
                        scope.image.dataId = data.id;
                        scope.image.isUploaded = true;
                        scope.imagesToSave.push({id: scope.image.dataId, title: scope.image.title, src: scope.image.src, orderId: scope.image.orderId});
                        scope.$digest();
                        if(!optional){
                            $("#loading").fadeOut();
                        }else{
                            $("#loading1").fadeOut();
                        }
                    }
                }
            },
            error: function (data, status, e) {
                //alert(e);
            }
        }
    )
}