/**
 * Created with JetBrains PhpStorm.
 * User: Edgar
 * Date: 1/12/14
 * Time: 11:44 PM
 * To change this template use File | Settings | File Templates.
 */

mainModule.controller('autoServicesFromController', ['autoServiceAnnouncements', '$scope', '$http' , '$rootScope', 'sortingOptions',
    function (autoServiceAnnouncements, $scope, $http, $rootScope, sortingOptions) {

        $scope.init = function(){
            var loc_id = $('#loc_id').val();
            var location = $('#location').val();

            if(location == 1){
                $http({
                    method: 'POST',
                    url: 'autoService/getAutoServicesByPlace',
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    data: 'loc_id=' + loc_id
                }).success(function (data) {
                        $scope.services = data;
                        for(var i = 0 ; i < $scope.services.length ;i++){
                            $scope.markers.push({latitude:$scope.services[i].latitude , longitude : $scope.services[i].longitude });
                        }
                    }).then(function (data) {
                    });
            }else{

                $http({
                    method: 'POST',
                    url: 'autoService/getAutoServicesBySubPlace',
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    data: 'subLoc_id=' + loc_id
                }).success(function (data) {
                        $scope.services = data;
                        for(var i = 0 ; i < $scope.services.length ;i++){
                            $scope.markers.push({latitude:$scope.services[i].latitude , longitude : $scope.services[i].longitude });
                        }
                    }).then(function (data) {
                    });
            }
        };

        $scope.services = [];

        angular.extend($scope, {
            center: {
                latitude: 40.17316305031736, // initial map center latitude
                longitude: 44.70839843750002 // initial map center longitude
            },
            markers: [], // an array of markers,
            zoom: 8
        });


        $scope.toDetailedInfo = function (id) {
            var href = $('#url_helper').find("a").attr("href");
            if (href != undefined) {
                href += "?id=" + id;
                window.location = href;
            }
        };

    }]);