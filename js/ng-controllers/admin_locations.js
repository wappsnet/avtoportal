/**
 * Created with JetBrains PhpStorm.
 * User: Edgar
 * Date: 9/14/13
 * Time: 1:09 AM
 * To change this template use File | Settings | File Templates.
 */
mainModule.controller('admin_location_controller', ['$scope', '$http',
    function($scope, $http) {

        $scope.init = function(){
            $http({
                method: 'POST',
                url: 'default/listLocations',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).then(function(response){
                    $scope.locationItems = response.data;
                });

            $http({
                method: 'POST',
                url: 'default/listSubLocations',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).then(function(response){
                    $scope.subLocationItems = response.data;
                });
        }

        $scope.locationItems = [];
        $scope.subLocationItems = [];

        $scope.deleteSubLoc = function(index , itemID){
            $http({
                method: 'POST',
                url: 'default/deleteSubLocItem',
                data: 'itemID=' + itemID ,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function(data){
                    $($scope.subLocationItems).splice(index, 1);
                    $scope.init();
                });
        }

        $scope.deleteLoc = function(index , itemID){
            $http({
                method: 'POST',
                url: 'default/deleteLocItem',
                data: 'itemID=' + itemID ,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function(data){
                    $( $scope.locationItems).splice(index, 1);
                    $scope.init();
                });
        }

        $scope.editLocItem = function (itemID) {
            var href = $('#url_helper_loc').find("a").attr("href");
            if (href != undefined) {
                href += "?location_id=" + itemID;
                window.location = href;
            }
        };

        $scope.editSubLocItem = function (itemID) {
            var href = $('#url_helper_subloc').find("a").attr("href");
            if (href != undefined) {
                href += "?subLocation_id=" + itemID;
                window.location = href;
            }
        };



    }]);
