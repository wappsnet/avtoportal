mainModule.controller('officialDealers_controller', ['$scope', '$http' ,
    function($scope, $http ) {


        $scope.initIt = function(){
            var dealer_id = $('#is_official').val();
            if(dealer_id == '1'){
                $http.get('site/getOfficialDealers')
                    .success(function (data) {
                        $scope.dealers = data;

                        for(var i = 0 ; i < $scope.dealers.length ; i++){
                            $scope.markers.push({ latitude :$scope.dealers[i].latitude ,longitude : $scope.dealers[i].longitude});
                        }

                    });
            }else{
                $http.get('site/getUniversalDealers')
                    .success(function (data) {
                        $scope.dealers = data;

                        for(var i = 0 ; i < $scope.dealers.length ; i++){
                            $scope.markers.push({ latitude :$scope.dealers[i].latitude ,longitude : $scope.dealers[i].longitude});
                        }

                    });
            }

        };

        $scope.goToDealer = function (index) {
            var dealer = $scope.dealers[index];
            window.location = "dealerCarAnnouncements?dealerId="+dealer.id;
        };

        angular.extend($scope, {
            center: {
                latitude: 40.17316305031736, // initial map center latitude
                longitude: 44.70839843750002 // initial map center longitude
            },
            markers: [], // an array of markers,
            zoom: 8
        });




    }]);

