mainModule.controller('appa_view_controller', ['$scope', '$http','appa',
    function($scope, $http, appa) {

        $scope.init = function(){
            var appa_id = parseInt($('#appa_id').html());
            $scope.promise = $http({
                method: 'POST',
                url: 'googleLocations/appaMarkers',
                data: 'appa_id=' + appa_id,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).then(function(response){
                    $scope.markers = response.data;
                });

            $scope.appa_item = appa.get({id:appa_id});

        }


        angular.extend($scope, {
            center: {
                latitude: 40.17316305031736, // initial map center latitude
                longitude: 44.70839843750002 // initial map center longitude
            },
            markers: [], // an array of markers,
            zoom: 8
        });


    }]);
