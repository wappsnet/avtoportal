mainModule.controller('ruralDetailedController',
    function ($scope, $rootScope, $controller) {
        $controller('baseDetailedController', {$scope: $scope});

        $scope.initIt = function () {
            $scope.superInit('ruralTechDetailed');
            $scope.accept_url = 'ruralFilterHelper/accept';
            $scope.listing_url = "listRuralAnnouncementsAll";
            $scope.delete_url = 'ruralFilterHelper/delete';
            $scope.filter_item = "rural_filters";
        };

    });
