
mainModule.controller('autoServicesController', ['autoServiceAnnouncements', '$scope', '$http' , '$rootScope', 'sortingOptions',
    function (autoServiceAnnouncements, $scope, $http, $rootScope, sortingOptions) {

      $scope.init = function(){
          $http({
              method: 'POST',
              url: 'site/sortingOptions',
              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
          }).success(function (data) {
                  $scope.sortingOptions = data;
                  $scope.sortingOption = data[0];
              });

          $http({
              method: 'POST',
              url: 'autoService/getAutoServices',
              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
          }).success(function (data) {
                  $scope.services = data;
                  for(var i = 0 ; i < $scope.services.length ;i++){
                      $scope.markers.push({latitude:$scope.services[i].latitude , longitude : $scope.services[i].longitude });
                  }
                  $scope.loadNumberOfPages();
              }).then(function (data) {
              });
      };

        $scope.services = [];

        angular.extend($scope, {
            center: {
                latitude: 40.17316305031736, // initial map center latitude
                longitude: 44.70839843750002 // initial map center longitude
            },
            markers: [], // an array of markers,
            zoom: 8
        });


        $scope.toDetailedInfo = function (id) {
            var href = $('#url_helper').find("a").attr("href");
            if (href != undefined) {
                href += "?id=" + id;
                window.location = href;
            }
        };

        /****************** PAGING **********************/
        $scope.currentPage = 0;
        $scope.pageSize = 10;

        $scope.pageSizeOptions = [
            {"id":10},
            {"id":15},
            {"id":20},
            {"id":25},
            {"id":30},
            {"id":35},
            {"id":40}
        ];

        $scope.pageSizeTMP = $scope.pageSizeOptions[0];

        $scope.setPageSize = function(pageSize){
            $scope.pageSize = pageSize;
            $scope.loadNumberOfPages();
        };

        $scope.loadNumberOfPages = function () {
            $scope.numberOfPages = Math.ceil($scope.services.length / $scope.pageSize);
        };
        /****************** PAGING **********************/

        $scope.sort = function(){

            if($scope.sortingOption.id != "7"){
                var comparator = $scope.getComparatorByType();
                var list = $scope.services;
                list.sort(comparator);
                $scope.services = list;
            }
        };

        $scope.getComparatorByType = function(){
            var type = $scope.sortingOption.id;
            switch (type){
                case '1':
                    return yearComparatorReverse;
                case '2':
                    return yearComparator;
                case '3':
                    return priceComparatorReverse;
                case '4':
                    return priceComparator;
                case '5':
                    return createdComparatorReverse;
                case '6':
                    return createdComparator;
            }
        };

        function priceComparatorReverse(aObj,bObj) {
            var a = parseInt($scope.getCurrencyValues(aObj.price, aObj.rate_id));
            var b = parseInt($scope.getCurrencyValues(bObj.price, bObj.rate_id));
            if (a < b)
                return 1;
            if (a > b)
                return -1;
            return 0;
        };

        function priceComparator(aObj,bObj) {
            var a = parseInt($scope.getCurrencyValues(aObj.price, aObj.rate_id));
            var b = parseInt($scope.getCurrencyValues(bObj.price, bObj.rate_id));
            if (a < b)
                return -1;
            if (a > b)
                return 1;
            return 0;
        };



        /* ======================== Accept and Delete actions   ======================================  */

        $scope.acceptItem = function(id, $event){
            $event.preventDefault();
            $http({
                method: 'POST',
                url: 'autoService/acceptAndRefresh',
                data: 'id='+id,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                    $scope.services = data.announcements;
                    $scope.loadNumberOfPages();
                }).then(function (data) {
                });
        };

        $scope.deleteItem = function(id, $event){
            $event.preventDefault();
            $http({
                method: 'POST',
                url: 'autoService/deleteAndRefresh',
                data: 'id='+id,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                    $scope.services= data.announcements;
                    $scope.loadNumberOfPages();
                }).then(function (data) {
                });
        };



    }]);