/**
 * Created with JetBrains PhpStorm.
 * User: Yerem
 * Date: 6/18/13
 * Time: 11:47 PM
 * To change this template use File | Settings | File Templates.
 */
mainModule.controller('bannerUploadController', ['$scope', '$http', function ($scope, $http) {

    $scope.isPosition1Img = true;
    $scope.isPosition2Img = true;
    $scope.isPosition3Img = true;
    $scope.isPosition4Img = true;
    $scope.isPosition5Img = true;
    $scope.isPosition6Img = true;

    $scope.urlPosition1 = '';
    $scope.urlPosition2 = '';
    $scope.urlPosition3 = '';
    $scope.urlPosition4 = '';
    $scope.urlPosition5 = '';
    $scope.urlPosition6 = '';

    $scope.showSecondTab = false;


    $scope.init = function () {
        $http.get('bannerUpload/getBanners')
            .success(function (data) {
                for(var i = 0; i < data.length; i ++){
                    if(data[i].positionId == 1){
                        $scope.position1 = data[i].file_name;
                    }else if(data[i].positionId == 2){
                        $scope.position2 = data[i].file_name;
                    }else if(data[i].positionId == 3){
                        $scope.position3 = data[i].file_name;
                    }else if(data[i].positionId == 4){
                        $scope.position4 = data[i].file_name;
                    }else if(data[i].positionId == 5){
                        $scope.position5 = data[i].file_name;
                    }else if(data[i].positionId == 6){
                        $scope.position6 = data[i].file_name;
                    }

                }
            });
    };

    $scope.init();

    $scope.uploadPosition1 = function () {
        $scope.upload(1, $scope.urlPosition1);
    };

    $scope.uploadPosition2 = function () {
        $scope.upload(2, $scope.urlPosition2);
    };

    $scope.uploadPosition3 = function () {
        $scope.upload(3, $scope.urlPosition3);
    };

    $scope.uploadPosition4 = function () {
        $scope.upload(4, $scope.urlPosition4);
    };

    $scope.uploadPosition5 = function () {
        $scope.upload(5, $scope.urlPosition5);
    };

    $scope.uploadPosition6 = function () {
        $scope.upload(6, $scope.urlPosition6);
    };

    $scope.upload = function (positionId, url) {
        $("#loading").fadeIn();
        $.ajaxFileUpload
        (
            {
                url: 'bannerUpload/upload',
                secureuri: false,
                fileElementId: 'qqfile',
                dataType: 'json',
                data: {name: 'logan', id: positionId, url:url},
                success: function (data, status) {
                    if (typeof(data.error) != 'undefined') {
                        if (data.error != '') {
                            $("#loading").fadeOut();
                            //alert(data.error);
                        } else {
                            if (data.isFlash) {
                                var parent = $('embed#flash_position_' + positionId).parent();
                                var newElement = "<embed src='" + data.imagePath + "' type='application/x-shockwave-flash' pluginspage='http://www.macromedia.com/go/getflashplayer' id='flash_position_"+positionId+"'>";
                                $('embed#flash_position_' + positionId).remove();
                                parent.append(newElement);
                                $("#img_position_" + positionId).attr("src", "");
                                $("#img_position_" + positionId).attr("style", "display:none");
                            } else {
                                $("#url_position_" + positionId).attr("href", data.url);
                                $("#img_position_" + positionId).attr("src", data.imagePath);
                                $("#img_position_" + positionId).attr("style", "display:block");
                                var parent = $('embed#flash_position_' + positionId).parent();
                                var newElement = "<embed src='' type='application/x-shockwave-flash' pluginspage='http://www.macromedia.com/go/getflashplayer' id='flash_position_"+positionId+"' style='display:none'>";
                                $('embed#flash_position_' + positionId).remove();
                                parent.append(newElement);
                            }
                            $("#loading").fadeOut();
                        }
                    }
                },
                error: function (data, status, e) {
                    //alert(e);
                }
            }
        )
    };

}]);