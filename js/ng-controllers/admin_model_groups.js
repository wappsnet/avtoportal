mainModule.controller('auto_model_groups', ['$scope', '$http',
    function($scope, $http ) {

        $scope.init = function(){
            $http({
                method: 'POST',
                url: 'default/listModelGroups',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).then(function(response){
                    $scope.model_groups = response.data;

                });
        }

        $scope.model_groups = [];

        $scope.deleteItem = function(index , itemID){
            $http({
                method: 'POST',
                url: 'default/deleteModelGroupItem',
                data: 'itemID=' + itemID ,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function(data){
                    $($scope.model_groups).splice(index, 1);
                    $scope.init();
                });
        }

        $scope.editItem = function (itemID) {
            var element = $('#url_helper');
            var href = $('#url_helper').find("a").attr("href");
            if (href != undefined) {
                href += "?group_id=" + itemID;
                window.location = href;
            }
        };



    }]);
