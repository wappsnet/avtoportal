mainModule.controller('autoServiceDetailed_Controller', ['$scope', 'autoServiceAnnouncements', '$rootScope','$http',
    function ($scope, autoServiceAnnouncements, $rootScope, $http) {

        $scope.initIt = function () {
            var car_id = parseInt($('#car_id').html());
            $scope.promise = $scope.announcement = autoServiceAnnouncements.get({id: car_id}, function () {
                var dateParts = $scope.announcement.created.split('-');
                var subDateParts = dateParts[2].split(" ");
                var timeParts = subDateParts[1].split(":");
                $scope.announcement.created = new Date(dateParts[0], dateParts[1] , subDateParts[0] , timeParts[0] , timeParts[1] , timeParts[2]);

                $scope.markers.push({latitude:$scope.announcement.latitude , longitude : $scope.announcement.longitude });
            });

            $http({
                method: 'POST',
                url: 'site/selectedServices',
                data: "announcement_id="+car_id,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                    $scope.services = data;
                }).then(function (data) {
                });


        };

        angular.extend($scope, {
            center: {
                latitude: 39.97316305031736, // initial map center latitude
                longitude: 45.10839843750002 // initial map center longitude
            },
            markers: [], // an array of markers,
            services:[],
            zoom: 7
        });


        $(window).on('beforeunload', function () {
            var car = {"car_id": $scope.announcement.id, "car_name": $scope.announcement.car_name, "image_name":$scope.announcement.main_image, "price":$scope.announcement.price, "action":"ruralTechDetailed"};
            var lastViewed = JSON.parse(sessionStorage.getItem("last_viewed"));
            if (!lastViewed) {
                lastViewed = [];
            }
            for (var i = 0; i < lastViewed.length; i++) {
                if (lastViewed[i].car_id == car.car_id) {
                    lastViewed.splice(i, 1);
                }
            }
            if (lastViewed.length == $rootScope.LAST_VIEWED_MAX_SIZE) {
                lastViewed.pop();
            }
            lastViewed.push(car);
            sessionStorage.setItem("last_viewed", JSON.stringify(lastViewed));

        });


        /*========================Accept Delete Actions===================*/

        $scope.acceptItem = function(id, $event){
            $event.preventDefault();
            $http({
                method: 'POST',
                url: 'ruralFilterHelper/accept',
                data: "id="+id,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                    window.location = "listCarAnnouncements";
                }).then(function (data) {
                });

        };

        $scope.deleteItem = function(id, $event){
            $event.preventDefault();
            $http({
                method: 'POST',
                url: 'ruralFilterHelper/delete',
                data: "id="+id,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                    window.location = "listCarAnnouncements";
                }).then(function (data) {
                });

        };

        /*========================END Accept Delete Actions===================*/

        $scope.addToRememberedList = function(car_id, car_name, price, image_name){

            var car = {"car_id": car_id, "car_name": car_name, "image_name":image_name, "price":price, "action": "ruralTechDetailed"};
            var remembered = JSON.parse(sessionStorage.getItem("remembered"));
            if (!remembered) {
                remembered = [];
            }
            for (var i = 0; i < remembered.length; i++) {
                if (remembered[i].car_id == car.car_id && remembered[i].action == car.action) {
                    remembered.splice(i, 1);
                }
            }
            if (remembered.length == $rootScope.LAST_VIEWED_MAX_SIZE) {
                remembered.pop();
            }
            remembered.push(car);
            sessionStorage.setItem("remembered", JSON.stringify(remembered));
        };




    }]);
