mainModule.controller('ruralAnnouncementsController',
    function ($scope, $controller, sortingOptions) {
        $controller('listTemplateBaseController', {$scope: $scope});

        $scope.init = function () {
            $scope.showFilterByID = true;
            var searchUrl = 'ruralFilterHelper/getAllFilters';
            var rememberedListCache = 'rural_filters';
            var detailedUrl = 'ruralTechDetailed';
            var acceptUrl = 'ruralFilterHelper/acceptAndRefresh';
            var deleteUrl = 'ruralFilterHelper/deleteAndRefresh';
            $scope.initBaseController(searchUrl, rememberedListCache, detailedUrl, acceptUrl, deleteUrl);
            $scope.sortingOptions = sortingOptions.query(function (data) {
                $scope.sortingOption = data[0];
            });
        };


    });