mainModule.controller('auto_school_admin_controller', ['$scope', '$http',
    function($scope, $http ) {

        $scope.init = function(){
            $http({
                method: 'POST',
                url: 'default/listAutoSchools',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).then(function(response){
                    $scope.school_items = response.data;

                });
        }

        $scope.appa_items = [];

        $scope.deleteItem = function(index , itemID){
            $http({
                method: 'POST',
                url: 'default/deleteAutoSchoolItem',
                data: 'itemID=' + itemID ,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function(data){
                    $($scope.school_items).splice(index, 1);
                    $scope.init();
                });
        }

        $scope.editItem = function (itemID) {
            var element = $('#url_helper');
            var href = $('#url_helper').find("a").attr("href");
            if (href != undefined) {
                href += "?school_id=" + itemID;
                window.location = href;
            }
        };



    }]);
