mainModule.controller('transportationDetailedController',
    function ($scope, $rootScope, $controller) {
        $controller('baseDetailedController', {$scope: $scope});

        $scope.initIt = function () {
            $scope.superInit('transDetailed');
            $scope.accept_url = 'transportFilterHelper/accept';
            $scope.listing_url = "listTransportationAnnouncements";
            $scope.delete_url = 'transportFilterHelper/delete';
            $scope.filter_item = "transport_filters";
        };

    });
