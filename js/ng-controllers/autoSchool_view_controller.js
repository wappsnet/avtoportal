mainModule.controller('autoschool_view_controller', ['$scope', '$http','autoschool',
    function($scope, $http, autoschool) {

        $scope.init = function(){
            var autoschool_id = parseInt($('#autoSchool_id').html());
            $http({
                method: 'POST',
                url: 'googleLocations/autoSchoolMarkers',
                data: 'autoschool_id=' + autoschool_id,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).then(function(response){
                    $scope.markers = response.data;
                });

            $scope.autoschool_item = autoschool.get({id:autoschool_id});

        }


        angular.extend($scope, {
            center: {
                latitude: 40.17316305031736, // initial map center latitude
                longitude: 44.70839843750002 // initial map center longitude
            },
            markers: [], // an array of markers,
            zoom: 8
        });


    }]);
