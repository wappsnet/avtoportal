/**
 * Created with JetBrains PhpStorm.
 * User: Edgar
 * Date: 12/1/13
 * Time: 9:54 PM
 * To change this template use File | Settings | File Templates.
 */
mainModule.controller('customs_view_controller', ['$scope', '$http','taxService',
    function($scope, $http, taxService) {

        $scope.init = function(){
            //  var trafficPolice_id = parseInt($('#trafficPolice_id').html());
            $scope.promise = $http({
                method: 'POST',
                url: 'googleLocations/taxServiceMarkers',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).then(function(response){
                    $scope.markers = response.data;
                });
            $scope.tax_item = taxService.get({id:1});
        }

        angular.extend($scope, {
            center: {
                latitude: 40.17316305031736, // initial map center latitude
                longitude: 44.70839843750002 // initial map center longitude
            },
            markers: [], // an array of markers,
            zoom: 8
        });


    }]);
