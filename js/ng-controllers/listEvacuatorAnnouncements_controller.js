mainModule.controller('evacuatorController',
    function ($scope, $controller) {
        $controller('listTemplateBaseController', {$scope: $scope});

        $scope.init = function () {
            $scope.showFilterByID = true;
            var searchUrl = 'evacuatorFilterHelper/getAllFilters';
            var rememberedListCache = 'evacuator_filters';
            var detailedUrl = 'evacuatorDetailed';
            var acceptUrl = 'evacuatorFilterHelper/acceptAndRefresh';
            var deleteUrl = 'evacuatorFilterHelper/deleteAndRefresh';
            $scope.initBaseController(searchUrl, rememberedListCache, detailedUrl, acceptUrl, deleteUrl);
            $http({
                method: 'POST',
                url: 'site/sortingOptions',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                    $scope.sortingOptions = data;
                    $scope.sortingOption = data[0];
                });
        };


    });

