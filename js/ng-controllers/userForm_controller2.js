mainModule.controller('registrationController', ['$http', '$scope','phoneType', 'uploadService', function($http, $scope, phoneType, uploadService){

    $scope.images = uploadService.getDealerUploads().images;
    $scope.imagesToSave = uploadService.getDealerUploads().imagesToSave;
    $scope.imagesToDelete = uploadService.getUploads().imagesToDelete;

    $scope.submitted = false;
    $scope.isPassRepeaterPressed = false;
    $scope.emailValidationMessage = "";

    $scope.phoneTypes = [];
    $scope.user = {
        isPersonal : 'true',
        isOfficial: 'false',
        email: '',
        password : '',
        confirm_password: '',
        name: '',
        showName : false,
        organisation : '',
        address : '',
        site: '',
        phoneType: '',
        phone: '',
        secondPhoneType : '',
        secondPhone : '',
        code: '',
        isAgree: false,
        image_title : '',
        phone1_id :'',
        dealer_markers: []
    };

    $scope.isDealer = function(){
        if( $scope.user.isPersonal == 'false'){
            return 'true';
        }
        return 'false';
    };

    angular.extend($scope, {
        center: {
            latitude: 55.730203356051, // initial map center latitude
            longitude: 37.622680664062 // initial map center longitude
        },
        zoom: 8
    });

    $scope.toShow = function(){
        return $scope.user.isOfficial;
    };

    $scope.getUploadImages = function(){
        var uploadImages = $scope.images;
        var resArr = [];
        for(var i = 0; i < uploadImages.length; i++ ){
            var image = uploadImages[i];
            if(image.isUploaded){
                resArr.push(image);
            }
        }
        return resArr;
    };




    $scope.init = function(){
        $scope.editMode = $('#editMode').val();
        if($scope.editMode){
            $scope.initEdit();
        }else{
            $scope.phoneTypes =  phoneType.query(function(data){
                $scope.user.phoneType = data[0];
                $scope.user.secondPhoneType = data[0];
            });
        }
    };

    $scope.initEdit = function(){
        $http({
            method: 'POST',
            url: 'user/loadUser',
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(data){
            $scope.isAdmin = data.isAdmin;
            $scope.user = data.user;
            $scope.user.image_title = data.image_title;
            $scope.$$childHead.images[0].title = $scope.user.image_title;
            $scope.$$childHead.images[0].src = "uploads/"+$scope.user.image_title;
            $scope.$$childHead.images[0].dataId = 5;

            $scope.user.isPersonal = data.user.user_roles_id == 1;
            $scope.user.isOfficial = data.user.user_roles_id == 2;

            $scope.user.organisation = data.user.organisation_name;
            $scope.user.address = data.user.organisation_address;
            $scope.user.site = data.user.web_site;
            $scope.user.name = data.user.user_name;
            $scope.user.showName = !data.user.name_is_hidden;

            $scope.user.phone = data.userPhone.phone_number;
            $scope.user.phoneType = data.userPhoneType;
            $scope.user.secondPhone = data.userSecondPhone.phone_number;
            $scope.user.secondPhoneType = data.userSecondPhoneType;

            $scope.user.phone1_id = data.user.phone1_id;

            $scope.user.password = '';
            $scope.phoneTypes =  phoneType.query(function(data){
                $scope.user.phoneType = data[$scope.user.phoneType.id-1];

                $scope.user.secondPhoneType = data[$scope.user.secondPhoneType.id-1];
            });
        });
    };


    $scope.isPressed = function(){
        $scope.isPassRepeaterPressed = true;
    };

    $scope.submit = function(form){
        $scope.submitted = true;
        $scope.user.image_title = $scope.$$childHead.images[0].title;
        $scope.emailValidationMessage = "";
        if(!$scope.editMode){

            $http({
                method: 'POST',
                url: 'user/validateCaptcha',
                data: 'data=' + angular.toJson($scope.user.code),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function(data){
                var result  = JSON.parse(data);
                $scope.showCaptchaError = !result;
                if(result && form.$valid){
                    $http({
                        method: 'POST',
                        url: 'user/ValidateEmail',
                        data: 'email=' + $scope.user.email,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                    }).success(function(data){
                        if(data != 0){
                            $scope.emailValidationMessage = "Этот e mail уже зарегистрирован."
                        }else{
                            $http({
                                method: 'POST',
                                url: 'user/insert',
                                data: 'data=' + angular.toJson($scope.user) + '&images=' + angular.toJson($scope.getUploadImages()),
                                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                            }).success(function(){
                                angular.forEach($scope.$$childHead.images, function (image) {
                                    if (image.isUploaded && image.dataId == -1) {
                                        image.dataId = 5;
                                    }
                                });
                                angular.forEach(uploadService.getDealerUploads().images, function (image) {
                                    if (image.isUploaded && image.dataId == -1) {
                                        image.dataId = 5;
                                    }
                                });
                                window.location = document.getElementById('redirectIT').href;
                            });
                        }
                    });
                }
            });
        }else{
            if(form.$valid){
                $http({
                    method: 'POST',
                    url: 'user/update',
                    data: 'data=' + angular.toJson($scope.user) + '&images=' + angular.toJson($scope.getUploadImages()),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function(){
                    angular.forEach($scope.$$childHead.images, function (image) {
                        if (image.isUploaded && image.dataId == -1) {
                            image.dataId = 5;
                        }
                    });
                    angular.forEach(uploadService.getDealerUploads().images, function (image) {
                        if (image.isUploaded && image.dataId == -1) {
                            image.dataId = 5;
                        }
                    });
                    window.location = document.getElementById('redirectIT').href;
                });
            }
        }
    }

}]);