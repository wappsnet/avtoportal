/**
 * Created with JetBrains PhpStorm.
 * User: Yerem
 * Date: 6/18/13
 * Time: 11:47 PM
 * To change this template use File | Settings | File Templates.
 */
mainModule.controller('uploadController', ['$scope', '$http', 'uploadService', function ($scope, $http, uploadService) {
    $scope.formType = $('#formType').val();

    $scope.init = function(type){
        if(type==1){
            /* already uploaded images(case when form is edited and has uploaded images)*/
            $scope.uploads = uploadService.getDealerUploads().uploadedImages($scope.formType);
            /* default images */
            $scope.images = uploadService.getDealerUploads().images;
            /* images for save to db*/
            $scope.imagesToSave = uploadService.getDealerUploads().imagesToSave;
            /* images to delete from db*/
            $scope.imagesToDelete = uploadService.getDealerUploads().imagesToDelete;
        }else if(type==0){
            /* already uploaded images(case when form is edited and has uploaded images)*/
            $scope.uploads = uploadService.getUploads().uploadedImages($scope.formType);
            /* default images */
            $scope.images = uploadService.getUploads().images;
            /* images for save to db*/
            $scope.imagesToSave = uploadService.getUploads().imagesToSave;
            /* images to delete from db*/
            $scope.imagesToDelete = uploadService.getUploads().imagesToDelete;
        }
    };





    //boolean flag for logo upload
    $scope.showLogoUpload = true;

    $scope.upload = function () {
        var img = null;
        var progress = true;
        angular.forEach($scope.images, function (image) {
            if (!image.isUploaded && progress) {
                img = image;
                progress = false;
            }
        });
        if(img){
            doAjaxUpload(img.id, 'qqfile');
        }

        angular.element('#path').val('');
        $scope.showLogoUpload = false;
    };

    $scope.uploadDealer = function () {
        var img = null;
        var progress = true;
        angular.forEach($scope.images, function (image) {
            if (!image.isUploaded && progress) {
                img = image;
                progress = false;
            }
        });
        if(img){
            //doAjaxUpload(img.id, 'qqfile1', 'optional');
            $("#loading1").fadeIn();
            $.ajaxFileUpload
            (
                {
                    url: 'upload/upload',
                    secureuri: false,
                    fileElementId: 'qqfile1',
                    dataType: 'json',
                    data: {name: 'logan', id: 'id'},
                    success: function (data, status) {
                        $('#qqfile1').change(function () {
                            $('#path_logo').val($(this).val());
                        });
                        if (typeof(data.error) != 'undefined') {
                            if (data.error != '') {
                                $("#loading1").fadeOut();
                                //alert(data.error);
                            } else {

                                $('#logo_upload').hide();
                                $("#"+img.id+"").attr("src", data.imagePath);
                                $("#"+img.id+"").closest("a").attr("data-id", data.id);
                                $("#"+img.id+"").attr("alt", data.title);
                                var scope = angular.element("#"+img.id+"").scope();
                                scope.image.title = data.title;
                                scope.image.src = data.imagePath;
                                scope.image.dataId = data.id;
                                scope.image.isUploaded = true;
                                scope.imagesToSave.push({id: scope.image.dataId, title: scope.image.title, src: scope.image.src, orderId: scope.image.orderId});
                                scope.$digest();
                                $("#loading1").fadeOut();
                            }
                        }
                    },
                    error: function (data, status, e) {
                        //alert(e);
                    }
                }
            )
        }
    };

    $scope.removeImg = function (index) {
        var imageToRemove = $scope.images[index];
        if (imageToRemove.dataId != -55) {
            $scope.imagesToDelete.push({id: imageToRemove.dataId, title: imageToRemove.title, src: imageToRemove.src});
            angular.forEach($scope.imagesToSave, function (image) {
                if (image.title == imageToRemove.title) {
                    $scope.imagesToSave.splice(index, 1);
                }
            });
            imageToRemove.src = 'images/icon' + (index + 1) + '.png ';
            imageToRemove.dataId = -55;
            imageToRemove.isUploaded = false;
            imageToRemove.title = '';
        }
        $('#qqfile1').change(function () {
            $('#path_logo').val($(this).val());
        });
        $('#path_logo').val('');
        $('#logo_upload').show();
    };

    $(window).on('beforeunload', function () {
        var arraysOfNames = [];
        angular.forEach($scope.images, function (image) {
            if (image.isUploaded && image.dataId == -1) {
                arraysOfNames.push(image.title);
            }
        });

        angular.forEach($scope.imagesToDelete, function (image) {
            if (image.id == -1) {
                arraysOfNames.push(image.title);
            }
        });

        if (arraysOfNames.length !== 0) {
            $.ajax({
                type: "POST",
                url: "upload/cleanUpOnExit",
                async: false,
                data: {ids: arraysOfNames},
                error: function (xhr) {
                    //alert("failure" + xhr.readyState + this.url)
                }
            });
        }
    });


}]);