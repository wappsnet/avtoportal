/**
 * Created with JetBrains PhpStorm.
 * User: Edgar
 * Date: 7/26/13
 * Time: 12:35 AM
 * To change this template use File | Settings | File Templates.
 */
mainModule.controller('news-controller', ['$scope', '$http','news',
    function($scope, $http, news) {


        $scope.init = function(){
           $scope.news =  news.query(function(news){
               for(var i = 0 ;i < news.length ; i++){
                   var dateParts = news[i].created.split('-');
                   var subDateParts = dateParts[2].split(" ");
                   var timeParts = subDateParts[1].split(":");
                   news[i].created = new Date(dateParts[0], dateParts[1] , subDateParts[0] , timeParts[0] , timeParts[1] , timeParts[2]);
               }
               $scope.news = news;
               $scope.loadNumberOfPages();
           });
        };

        $scope.toDetailedInfo = function (id, $event) {
            var href = $('#href_news').find('a').attr("href");
            if (href != undefined) {
                href += "?id=" + id;
                window.location = href;
            }
        };


        /****************** PAGING **********************/
        $scope.currentPage = 0;
        $scope.pageSize = 10;

        $scope.pageSizeOptions = [
            {"id":10},
            {"id":15},
            {"id":20},
            {"id":25},
            {"id":30},
            {"id":35},
            {"id":40}
        ];

        $scope.pageSizeTMP = $scope.pageSizeOptions[0];

        $scope.setPageSize = function(pageSize){
            $scope.pageSize = pageSize;
            $scope.loadNumberOfPages();
        };

        $scope.loadNumberOfPages = function () {
            $scope.numberOfPages = Math.ceil($scope.news.length / $scope.pageSize);
        };
        /****************** PAGING **********************/

        $scope.secureText = function (html)
        {
            var tmp = document.createElement("DIV");
            tmp.innerHTML = html;
            return tmp.textContent || tmp.innerText || "";
        };


    }]);
