mainModule.controller('need_reg_login', ['$http', '$scope','$rootScope',
    function($http, $scope,$rootScope){
        $scope.registered = false;
        $scope.userName = '';
        $scope.showValidationMessage = false;

        $scope.email = '';
        $scope.password = '';

        $scope.goToForgotPassword = function(){
            window.location = "forgotPassword";
        };


        $scope.login = function(url){
            $http({
                method: 'POST',
                url: url,
                data : {
                    email : $scope.email,
                    password: $scope.password
                },
                headers: {'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'}
            }).
                success(function(data, status) {
                    if(data == 'error'){
                        $scope.showValidationMessage = true;
                        $scope.password = '';
                    }else{
                        $scope.status = status;
                        $scope.data = data;
                        $scope.registered = true;
                        $scope.userName = data.username;
                        window.location = document.getElementById('redirectIT_home').href;}
                }).
                error(function(data, status) {
                    $scope.data = data || "Request failed";
                    $scope.status = status;
                    //alert(status);
                });
        };



    }
]);
