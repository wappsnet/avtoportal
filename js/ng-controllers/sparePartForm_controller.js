/**
 * Created with JetBrains PhpStorm.
 * User: Edgar
 * Date: 7/14/13
 * Time: 9:40 PM
 * To change this template use File | Settings | File Templates.
 */
mainModule.controller('spare-controller',
    function($scope, groupByID , $q, $controller, groupAnswersByID, groupAnswersByIDLookup) {
        $controller('baseFormController', {$scope: $scope});

        $scope.init = function(){
            $scope.superInit();
            var isEdit = $('#editMode').val();

            if(isEdit == 1){
                $scope.editInit();
            }else{
                $scope.httpCalls.push(  groupByID.getGroupById(16).then(function (data){
                        $scope.spareProps = data;
                    })
                );
                $scope.httpCalls.push(  groupByID.getGroupById(19).then(function (data){
                        $scope.term = data;
                    })
                );
                $scope.promise = $q.all($scope.httpCalls);
            }
        }


        $scope.submit = function(form){
            $scope.submitted = true;
            if(form.$valid){
                if($scope.isAdmin){
                    $scope.groups = [];
                    $scope.groups.push($scope.spareProps, $scope.term, $scope.groupUserinfo);
                }else{
                    $scope.groups.push($scope.spareProps, $scope.term);
                }
                $scope.formSubmit('sparePartForm/insert');
            }else{
                if($("#announcement_form :input.ng-invalid")){
                    $('html, body').animate({
                        scrollTop: $("#announcement_form :input.ng-invalid").first().offset().top - 50}, 1000);
                }
            }
        };

        $scope.editInit = function(){
            var announcement_id = $('#announcement_id').val();
            $scope.announcementToEdit = announcement_id;

            $scope.ordinaryData(announcement_id , 'SparePartAnnouncement', 'SparePartPhotos');

            $scope.httpCalls.push(
                groupAnswersByIDLookup.getGroupAnswersById(16,  announcement_id , 'SparePartProperty').then(function (data){
                    $scope.spareProps = data;
                })
            );

            $scope.httpCalls.push(
                groupAnswersByIDLookup.getGroupAnswersById(19,announcement_id, 'SparePartProperty').then(function (data){
                    $scope.term = data;
                })
            );

            $scope.promise = $q.all($scope.httpCalls);
        }

});
