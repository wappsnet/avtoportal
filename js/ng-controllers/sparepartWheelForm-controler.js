mainModule.controller('spare-wheel-controller',
    function($scope, groupByID , $q, $controller, groupAnswersByID, groupAnswersByIDLookup) {
        $controller('baseFormController', {$scope: $scope});

        $scope.init = function(){
            $scope.superInit();
            var isEdit = $('#editMode').val();

            if(isEdit == 1){
                $scope.editInit();
            }else{
                $scope.httpCalls.push(  groupByID.getGroupById(8).then(function (data){
                        $scope.spareProps = data;
                    })
                );
                $scope.httpCalls.push(  groupByID.getGroupById(19).then(function (data){
                        $scope.term = data;
                    })
                );
                $scope.promise = $q.all($scope.httpCalls);
            }
        }


        $scope.submit = function(form){
            $scope.submitted = true;
            if(form.$valid){
                $scope.groups = [];
                if($scope.isAdmin){
                    $scope.groups.push($scope.spareProps, $scope.term, $scope.groupUserinfo);
                }else{
                    $scope.groups.push($scope.spareProps, $scope.term);
                }
                $scope.formSubmit('sparePartWheel/insert');
            }else{
                if($("#announcement_form :input.ng-invalid")){
                    $('html, body').animate({
                        scrollTop: $("#announcement_form :input.ng-invalid").first().offset().top - 50}, 1000);
                }
            }
        }

        $scope.editInit = function(){
            var announcement_id = $('#announcement_id').val();
            $scope.announcementToEdit = announcement_id;

            $scope.ordinaryData(announcement_id , 'WheelsAnnouncement', 'WheelsPhoto');

            $scope.httpCalls.push(
                groupAnswersByIDLookup.getGroupAnswersById(8,  announcement_id , 'WheelsProperties').then(function (data){
                    $scope.spareProps = data;
                })
            );
            $scope.httpCalls.push(
                groupAnswersByIDLookup.getGroupAnswersById(19,announcement_id, 'WheelsProperties').then(function (data){
                    $scope.term = data;
                })
            );

            $scope.promise = $q.all($scope.httpCalls);

        }

    });
