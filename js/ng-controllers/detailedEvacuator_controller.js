mainModule.controller('evacuatorDetailedController',
    function ($scope, $rootScope, $controller) {
        $controller('baseDetailedController', {$scope: $scope});

        $scope.initIt = function () {
            $scope.superInit('evacDetailed');
            $scope.accept_url = 'evacuatorFilterHelper/accept';
            $scope.listing_url = "listEvacuatorAnnouncements";
            $scope.delete_url = 'evacuatorFilterHelper/delete';
            $scope.filter_item = "evacuator_filters";
        };

    });
