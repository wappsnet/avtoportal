mainModule.controller('google_controller', ['$scope', '$http',
    function($scope, $http) {


        $scope.currentAddress = "";
        $scope.currentInfoWindow = "";

        $scope.editMode = false;
        $scope.editIndex = -1;

        $scope.init = function(){
                var location_type = parseInt($('#location_type').html());
            var item_id = parseInt($('#item_id').html());

            if(location_type && item_id){

                $http({
                    method: 'POST',
                    url: 'default/adminMarkers',
                    data: 'item_id=' + item_id + '&location_type=' + location_type,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).then(function(response){
                        $scope.markers = response.data;
                    });
            }

        }


        angular.extend($scope, {
            center: {
                latitude: 39.97316305031736, // initial map center latitude
                longitude: 45.60839843750002 // initial map center longitude
            },
            markers: [], // an array of markers,
            zoom: 8,// the zoom level
            geocoder: new google.maps.Geocoder()
        });

        $scope.deleteMarker = function(idx){
            $scope.markers.splice(idx, 1);
        }



        $scope.codeAddress = function () {
            $scope.editMode = false;
            $scope.geocoder.geocode( { 'address': $scope.currentAddress}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    var lat = parseFloat(results[0].geometry.location.lat());
                    var long = parseFloat (results[0].geometry.location.lng());
                    var markerCenter = {latitude:  lat, longitude: long};
                    $scope.markers.push({addressName:$scope.currentAddress , infoWindow : "<div style='padding:5px'>" +  $scope.currentInfoWindow  + "</div>", latitude :lat ,longitude : long });
                    document.getElementById('google_markers').value = angular.toJson($scope.markers);
                    $scope.center = markerCenter;
                    $scope.$apply();
                   // marker = null;
                    $scope.currentAddress = null;
                    $scope.currentInfoWindow = null;

                } else {
                    alert('Geocode was not successful for the following reason: ' + status);
                }
            });
        }

        $scope.editMarkerInfo = function(){
            if($scope.editMode){

                if($scope.editIndex >= 0 &&  $scope.editIndex < $scope.markers.length){
                    $scope.markers[$scope.editIndex].infoWindow = "<div style='padding:5px'>" +  $scope.currentInfoWindow  + "</div>";
                    document.getElementById('google_markers').value = angular.toJson($scope.markers);
                    $scope.currentAddress = null;
                    $scope.currentInfoWindow = null;
                    $scope.editMode = false;
                  //  $scope.center = $scope.markers[$scope.editIndex];
                }

            }
        }

        $scope.prepareEditMarkerInfo = function(idx){
            $scope.currentAddress = $scope.markers[idx].addressName;
            $scope.currentInfoWindow = $scope.markers[idx].infoWindow;

            $scope.editMode = true;
            $scope.editIndex = idx;

        }

    }]);
