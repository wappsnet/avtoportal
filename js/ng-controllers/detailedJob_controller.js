mainModule.controller('jobDetailedController',
    function ($scope, $rootScope, $controller) {
        $controller('baseDetailedController', {$scope: $scope});

        $scope.initIt = function () {
            $scope.superInit('jobDetailed');
            $scope.accept_url = 'jobFilterHelper/accept';
            $scope.listing_url = "listJobAnnouncementsAll";
            $scope.delete_url = 'jobFilterHelper/delete';
            $scope.filter_item = "job_filters";
        };

    });
