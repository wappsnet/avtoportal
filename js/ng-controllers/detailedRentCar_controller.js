mainModule.controller('carRentDetailedController',
    function ($scope, $rootScope, $controller) {
        $controller('baseDetailedController', {$scope: $scope});

        $scope.initIt = function () {
            $scope.superInit('carRentDetailed');
            $scope.accept_url = 'rentCarFilterHelper/accept';
            $scope.listing_url = "listRentCarAnnouncements";
            $scope.delete_url = 'rentCarFilterHelper/delete';
            $scope.filter_item = "rent_car_filters";
        };

    });
