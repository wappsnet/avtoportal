mainModule.controller('dealerAnnouncements',
    function ($scope, $http, sortingOptions, $rootScope, $controller) {

        $controller('listTemplateBaseController', {$scope: $scope});

        $scope.map_view = 'false';
        $scope.gallery_view = 'true';

        $scope.init = function () {
            var dealer_id = $('#dealer_id').val();
            $scope.showFilterByID = true;
            var searchUrl = 'carFilterHelper/getAllFilters';
            var rememberedListCache = 'car_filters';
            var detailedUrl = 'carDetailed';
            var acceptUrl = 'carFilterHelper/acceptAndRefresh';
            var deleteUrl = 'carFilterHelper/deleteAndRefresh';
            //$scope.initBaseController(searchUrl, rememberedListCache, detailedUrl, acceptUrl, deleteUrl);

            $scope.sortingOptions = sortingOptions.query(function (data) {
                $scope.sortingOption = data[0];
            });
            $http({
                method: 'POST',
                url: 'site/getDealerCarAnnouncements',
                data: 'dealerId=' + dealer_id,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                    $rootScope.announcements = data;
                    $scope.pagingProperties.loadNumberOfPages();
                });

            $http({
                method: 'POST',
                url: 'site/getDealerMarker',
                data: 'dealerId=' + dealer_id,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                    $scope.markers.push(data);
                });
        };

        $scope.switchToMap = function () {
            $scope.map_view = 'true';
            $scope.gallery_view = 'false';
        };

        $scope.switchToGallery = function () {
            $scope.map_view = 'false';
            $scope.gallery_view = 'true';
        };

        angular.extend($scope, {
            center: {
                latitude: 40.183333000000000000, // initial map center latitude
                longitude: 44.816666999999984000 // initial map center longitude
            },
            markers: [],
            zoom: 8
        });


    });


