mainModule.controller('myAnnouncementsController', ['$scope', '$http',
    function ($scope, $http) {

        $scope.init = function () {
            var showAll = $('#showAllAnnouncements').val();
            $http({
                method: 'POST',
                url: 'site/loadMyAnnouncements',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                    $scope.announcements = data;

                    $http({
                        method: 'POST',
                        url: 'site/getTerms',
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                    }).success(function (data) {
                            $scope.terms = data;
                            $scope.currentTerm = data[0];

                            for (var i = 0; i < $scope.announcements.length; i++) {
                                $scope.announcements[i].currentTerm = data[0];
                            }
                        });
                });
        };

        $scope.getDuration = function (term_id, date_created) {
            var days = 0;
            if (term_id == 57) {
                days = 14;
            } else if (term_id == 58) {
                days = 28;
            } else if (term_id == 59) {
                days = 56;
            }
            var date = new Date(date_created);
            var to = new Date();
            to.setDate(date.getDate() + days);
            return dates.formatDate(to, "yyyy-mm-dd");
        };

        $scope.deleteAnnouncement = function (id, formType, index, $event) {
            if (confirm('Вы уверены, что хотите удалить заявление')) {
                $event.preventDefault();
                $http({
                    method: 'POST',
                    data: "id=" + id + "&formType=" + formType,
                    url: 'user/deleteAnnouncement',
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function (data) {
                        $scope.announcements.splice(index, 1);
                    })
            }
        };

        $scope.checkDate = function (announcement) {
            $http({
                method: 'POST',
                data: "id=" + announcement.id + "&formType=" + announcement.announcement_type_id + "&term_id=" + announcement.currentTerm.value,
                url: 'site/addValidDate',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                    $scope.init();
                });
        };


        $scope.addToUrgent = function (announcementId, announcementType) {
            $scope.$$childHead.addToUrgent(announcementId, announcementType);
        };

        $scope.addToHomeTop = function (announcementId, announcementType) {
            $scope.$$childHead.addToHomeTop(announcementId, announcementType);
        };

        $scope.addToListTop = function (announcementId, announcementType) {
            $scope.$$childHead.addToListTop(announcementId, announcementType);
        };


    }]);
