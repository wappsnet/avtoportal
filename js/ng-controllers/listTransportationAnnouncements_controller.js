mainModule.controller('transportationAnnouncements_controller',
    function ($scope, $http, $controller) {

        $controller('listTemplateBaseController', {$scope: $scope});

        $scope.init = function () {
            $scope.showFilterByID = true;
            var searchUrl = 'transportFilterHelper/getAllFilters';
            var rememberedListCache = 'transport_filters';
            var detailedUrl = 'transportationDetailed';
            var acceptUrl = 'transportFilterHelper/acceptAndRefresh';
            var deleteUrl = 'transportFilterHelper/deleteAndRefresh';
            $scope.initBaseController(searchUrl, rememberedListCache, detailedUrl, acceptUrl, deleteUrl);
            $http({
                method: 'POST',
                url: 'site/sortingOptions',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                    $scope.sortingOptions = data;
                    $scope.sortingOption = data[0];
                });
        };


    });