mainModule.controller('loginController', ['$http', '$scope','$rootScope',
    function($http, $scope,$rootScope){
        $scope.registered = false;
        $scope.userName = '';
        $scope.showValidationMessage = false;

        $scope.email = '';
        $scope.password = '';

        $scope.init = function(){
            $scope.authenticateUser();
            $rootScope.initRates();
            $scope.initMessagesMap();
            $scope.announcement_size();

        };

        $scope.authenticateUser = function(){
            $http({
                method: 'POST',
                url: 'site/authenticateUser',
                headers: {'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'}
            }).
                success(function(data, status) {
                    $scope.userName = data.user_name;
                    $scope.registered = data.registered;
                    //alert(status);
                }).
                error(function(data, status) {
                    $scope.data = data || "Request failed";
                    $scope.status = status;
                    //alert(status);
                });
        };

        $scope.announcement_size = function(){
            $http({
                method: 'POST',
                url: 'site/loadMyAnnouncements',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                    $scope.announcement_size = data.length;
                });
        };


        $scope.login = function(url){
            $http({
                method: 'POST',
                url: url,
                data : {
                    email : $scope.email,
                    password: $scope.password
                },
                headers: {'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'}
            }).
                success(function(data, status) {
                    if(data == 'error'){
                        $scope.showValidationMessage = true;
                        $scope.password = '';
                    }else{
                        $scope.showValidationMessage2 = true;
                        $scope.status = status;
                        $scope.data = data;
                        $scope.registered = true;
                        $scope.userName = data.username;
                        }
                }).
                error(function(data, status) {
                    $scope.data = data || "Request failed";
                    $scope.status = status;
                    //alert(status);
                });
        };

        $scope.logout = function(url){
            $http({
                method: 'POST',
                url: url,
                headers: {'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'}
            }).
                success(function() {
                    window.location = document.getElementById('redirectIT_home').href;
                    //alert(status);
                }).
                error(function(data, status) {
                    $scope.data = data || "Request failed";
                    $scope.status = status;
                    //alert(status);
                });
        };

        $rootScope.LAST_VIEWED_MAX_SIZE = 15;
        $rootScope.lastViewdItems = JSON.parse(sessionStorage.getItem("last_viewed")) == null ? [] : JSON.parse(sessionStorage.getItem("last_viewed"));
        $rootScope.rememberedItems = JSON.parse(sessionStorage.getItem("remembered")) == null ? [] : JSON.parse(sessionStorage.getItem("remembered"));


        $rootScope.switchToRemembered = function(){
            $('#remembered').css('color', '#fff');
            $('#remembered').css('text-decoration', 'underline');
            $('#remembered').css('background', '#856fb5');
            $('#lastView').css('color', '#000');
            $('#lastView').css('text-decoration', '');
            $('#lastView').css('background', '#f1f1f1');
            $('#carusel_2').show();
            $('#carusel_1').hide();
            $('#tab_id').val('false');
        };

        $rootScope.switchToLastViewed = function(){
            $('#lastView').css('color', '#fff');
            $('#lastView').css('text-decoration', 'underline');
            $('#lastView').css('background', '#856fb5');
            $('#remembered').css('color', '#000');
            $('#remembered').css('text-decoration', '');
            $('#remembered').css('background', '#f1f1f1');
            $('#carusel_2').hide();
            $('#carusel_1').show();
            $('#tab_id').val('true');
        };


        $rootScope.getRememberedItemsSize = function(){
            return $rootScope.rememberedItems.length;
        };


        $rootScope.removeRememberedItem = function(index, $event){
            $rootScope.rememberedItems.splice(index, 1);
            sessionStorage.setItem("remembered", JSON.stringify($rootScope.rememberedItems));
            $event.preventDefault();
        };

        $rootScope.removeFromLastViewed = function (index) {
            var lastViewed = window.sessionStorage.getItem("last_viewed");
            lastViewed.splice(index, 1);
            sessionStorage.setItem("last_viewed", lastViewed);
        };


        /****************** CURRENCY RATES **********************/
        $rootScope.rate_to_usd = 0;
        $rootScope.rate_to_euro = 0;
        $rootScope.rate_to_rub = 0;

        $rootScope.getCurrencyLabel = function(rate_id){
            if(rate_id == 1){
                return "RUR";
            }else if(rate_id == 2){
                return "USD"
            }else if(rate_id == 3){
                return "EURO";
            }
            return null;
        };

        $rootScope.initRates = function () {
            $http({
                method: 'POST',
                url: 'site/getRates',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                    for (var i = 0; i < data.length; i++) {
                        if (data[i].rate == "EUR") {
                            $rootScope.rate_to_euro = data[i].value;
                        } else if (data[i].rate == "USD") {
                            $rootScope.rate_to_usd = data[i].value;
                        } else if (data[i].rate == "RUB") {
                            $rootScope.rate_to_rub = data[i].value;
                        }
                    }
                });
        };

        $rootScope.getCurrencyValues = function(amount, rate_id){
            var usd = 0;
            var euro = 0;
            var rur = 0;
            if(rate_id == 1){
                usd =  amount / $rootScope.rate_to_usd;
                euro =  amount / $rootScope.rate_to_euro;
                return " " + usd.toFixed(2) + " USD" + ", " + euro.toFixed(2) + " EURO";

            }else if(rate_id == 2){
                rur =  amount * $rootScope.rate_to_usd;
                euro =  amount * $rootScope.rate_to_usd / $rootScope.rate_to_euro;
                return " " + rur.toFixed(2) + " RUR" + ", " + euro.toFixed(2) + " EURO";

            }else if(rate_id == 3){
                rur =  amount * $rootScope.rate_to_euro;
                usd =  amount * $rootScope.rate_to_euro/ $rootScope.rate_to_usd;
                return " " + rur.toFixed(2) + " RUR" + ", " + usd.toFixed(2) + " USD";
            }
            return null;
        };

        $rootScope.getCurrencyValueFirst = function(amount, rate_id){
            var usd = 0;
            var euro = 0;
            var rur = 0;
            if(rate_id == 1){
                usd =  amount / $rootScope.rate_to_usd;
                euro =  amount / $rootScope.rate_to_euro;
                return " " + usd.toFixed(2) + " USD";

            }else if(rate_id == 2){
                rur =  amount * $rootScope.rate_to_usd;
                euro =  amount * $rootScope.rate_to_usd / $rootScope.rate_to_euro;
                return " " + rur.toFixed(2) + " RUR";

            }else if(rate_id == 3){
                rur =  amount * $rootScope.rate_to_euro;
                usd =  amount * $rootScope.rate_to_euro/ $rootScope.rate_to_usd;
                return " " + rur.toFixed(2) + " RUR";
            }
            return null;
        };

        $rootScope.getCurrencyValueSecond = function(amount, rate_id){
            var usd = 0;
            var euro = 0;
            var rur = 0;
            if(rate_id == 1){
                usd =  amount / $rootScope.rate_to_usd;
                euro =  amount / $rootScope.rate_to_euro;
                return euro.toFixed(2) + " EURO";

            }else if(rate_id == 2){
                rur =  amount * $rootScope.rate_to_usd;
                euro =  amount * $rootScope.rate_to_usd / $rootScope.rate_to_euro;
                return euro.toFixed(2) + " EURO";

            }else if(rate_id == 3){
                rur =  amount * $rootScope.rate_to_euro;
                usd =  amount * $rootScope.rate_to_euro/ $rootScope.rate_to_usd;
                return usd.toFixed(2) + " USD";
            }
            return null;
        };

        $rootScope.getCurrencyValueThird = function(amount, rate_id){
            var usd = 0;
            var euro = 0;
            var rub = 0;
            if(rate_id == 1){
                rub =  amount;
                return rub.toFixed(2) + " RUB";

            }else if(rate_id == 2){
                rub =  amount * $rootScope.rate_to_usd;
                return rub.toFixed(2) + " RUB";

            }else if(rate_id == 3){
                rub =  amount * $rootScope.rate_to_euro;
                return usd.toFixed(2) + " RUB";
            }
            return null;
        };

        /****************** CURRENCY RATES **********************/

        $scope.goToForgotPassword = function(){
            window.location = "forgotPassword";
        };


        $scope.constructFilters = function (filterName, childName, filterValue, filter_condition, filter_type) {
            var filters = [];
            filters.push({"filter_name": filterName, "child_name":childName, "filter_value": filterValue, "filter_condition": filter_condition, "filter_type" : filter_type});
            window.sessionStorage.setItem("car_filters", JSON.stringify(filters));
        };

        $scope.toList = function(filterName, propertyId){
            $scope.constructFilters($rootScope.messageMap['14'], $rootScope.messageMap['15'], filterName, "t.mark_id =" + propertyId, 1);
            window.location = "listCarAnnouncements";
        };

        $scope.initMessagesMap = function(){
            $http({
                method: 'POST',
                url: 'dAOClassifiers/initMessageMap',
                headers: {'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'}
            }).
                success(function(data, status) {
                  $rootScope.messageMap = data;
                }).
                error(function(data, status) {
                    //alert(status);
                });
        };

        $rootScope.renderWriteToSeller = false;

        $rootScope.showWriteToSeller = function ($event) {
            $event.preventDefault();
            $rootScope.renderWriteToSeller = true;
        };

        $rootScope.hideWriteToSeller = function ($event) {
            $event.preventDefault();
            $rootScope.renderWriteToSeller = false;
        };

        $rootScope.sendMail = function (form) {
            var code = '';
            var userMail = $scope.$$childHead.announcement.userInfo.email;
            if($scope.user && $scope.user.code){
                code = $scope.user.code;
            }
            $http({
                method: 'POST',
                url: 'user/validateCaptcha',
                data: 'data=' + angular.toJson(code),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                    var result = JSON.parse(data);
                    $scope.showCaptchaError = !result;
                    if (result && form.$valid) {
                        $http({
                            method: 'POST',
                            url: 'site/sendMailToSeller',
                            data: 'data=' + angular.toJson($scope.user) + '&userMail='+userMail,
                            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                        }).success(function () {
                                $scope.mailSended = true;
                            });
                    }
                });
        };

        $rootScope.thumbImages = [];

        $rootScope.fillImages = function (images){
            $rootScope.thumbImages = images.slice(0);
            var size = images.length;
            for(var tmp = size; tmp < 10; tmp ++){
                $rootScope.thumbImages.push($scope.getDefaultImage());
            }
        };

        $rootScope.getDefaultImage = function (){
            return {"announcement_id":"-1", "id":"-1", "name":"no_image.jpg", "order_id":"1"};
        };

        $rootScope.onImgHover = function ($event, image) {
            if(image.announcement_id != -1){
                var src = '';
                var element = $event.target;
                if(element.tagName == 'DIV'){
                    src = $("img", element).prop("src");
                }else{
                    src = element.src;
                }
                angular.element('#detailed_main_image').attr('src', src);
                angular.element.attr('href', src);
            }
        };
    }
]);


mainModule.controller('baseFormController',
    function($scope, $http, Location, uploadService, isRegistered, isAdmin, groupByID ) {

        $scope.isAdmin;
        $scope.description;
        $scope.location;
        $scope.subLocation;
        $scope.subLocations;
        $scope.optional_location;
        $scope.submitted = false;

        $scope.promise;
        $scope.httpCalls = [];

        $scope.groups = [];

        $scope.top_id;

        $scope.announcementToEdit = null;

        $scope.superInit = function(){
            $scope.httpCalls = [];
            $scope.groups = [];

            $scope.httpCalls.push( isAdmin.getIsAdmin().then(function(data){
                    $scope.isAdmin = data;
                    if($scope.isAdmin){
                        groupByID.getGroupById(10).then(function (data){
                            $scope.groupUserinfo = data;
                        });
                    }
                })
            );

            $scope.httpCalls.push( isRegistered.getIsRegistered().then(function(data){
                    $scope.registered = data;
                })
            );



            $scope.locations = Location.query(
                function(data){
                    if(data.length > 0){
                        $scope.location = data[0];
                        $scope.subLocations = $scope.location.regions;
                    }
                }
            );

            $scope.imagesToSave = uploadService.getUploads().imagesToSave;
            $scope.imagesToDelete = uploadService.getUploads().imagesToDelete;
        };

        $scope.formSubmit = function(url){
            $http({
                method: 'POST',
                url: url,
                data: 'data=' + angular.toJson($scope.autoToJson()) + '&uploadSave=' + angular.toJson($scope.imagesToSave) +
                    '&uploadDelete=' + angular.toJson($scope.imagesToDelete) + '&announcementToEdit=' + $scope.announcementToEdit,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function(){
                    angular.forEach(uploadService.getUploads().images, function (image) {
                        if (image.isUploaded && image.dataId == -1) {
                            image.dataId = 5;
                        }
                    });
                    window.location = document.getElementById('redirectIT').href;
                });
        };

        $scope.autoToJson = function(){
            var rate_id = $('#rate_id').val();
            var jsonResult;
            var i, j, group_length, property_length;

            group_length =  $scope.groups.length;
            jsonResult = '{"groups":[';
            for(i = 0 ;i < group_length ; i++){
                jsonResult += '{';
                jsonResult += '"id":' + $scope.groups[i].id + ',';
                jsonResult += '"properties":[';
                property_length = $scope.groups[i].properties.length;
                for(j = 0; j < property_length ; j++){
                    jsonResult += '{';
                    jsonResult += '"id":' + $scope.groups[i].properties[j].id + ',';
                    jsonResult += '"type_id":' + $scope.groups[i].properties[j].c_property_type_id + ',';
                    if($scope.groups[i].properties[j].c_property_type_id == 2){
                        var propertyTmp = $scope.groups[i].properties[j].temp_value;
                        jsonResult += '"value":{'
                            + '"id":' + (propertyTmp == null ? 'null' : propertyTmp.id) + ','
                            + '"name":' + (propertyTmp == null ? 'null' : propertyTmp.name != null ? '"' + propertyTmp.name.replace(/\"/g,'') + '"' : 'null' )
//                            + '"name_rus":' + (propertyTmp == null ? 'null' : propertyTmp.name_rus != null ? '"' + propertyTmp.name_rus.replace(/\"/g,'') + '"' : 'null' ) + ','
//                            + '"name_geo":' + (propertyTmp == null ? 'null' : propertyTmp.name_geo != null ? '"' + propertyTmp.name_geo.replace(/\"/g,'') + '"' : 'null')  + ','
//                            + '"name_arm":' + (propertyTmp == null ? 'null' : propertyTmp.name_arm != null ? '"' + propertyTmp.name_arm.replace(/\"/g,'') + '"' : 'null')
                            + '}';
                    }else{
                        jsonResult += '"value":' +
                            ($scope.groups[i].properties[j].temp_value == undefined ||  $scope.groups[i].properties[j].temp_value == null ? '"null"' :  '"' +$scope.groups[i].properties[j].temp_value  + '"') ;
                    }
                    jsonResult += '}' + (j != property_length -1 ? ',' : '');
                }
                jsonResult += ']}' + (i != group_length -1 ? ',' : '');
            }
            jsonResult += '],';
            jsonResult += '"description":' + ($scope.description == undefined || $scope.description == null ? '"null"' : '"' + $scope.description.replace(/\r?\n/g, '\\n')  + '"') + ',';
            jsonResult += '"location":' + ($scope.location == undefined || $scope.location == null ? '"null"' : '"' + $scope.location.id + '"' ) +',';
            jsonResult += '"subLocation":' + ($scope.subLocation == undefined || $scope.subLocation == null ? '"null"' :'"' + $scope.subLocation.id + '"' ) + ',' ;
            jsonResult += '"top_id":' + ($scope.top_id == undefined || $scope.top_id == null ? "0" :'"' + $scope.top_id + '"' ) + ',' ;
            jsonResult += '"rate_id":' + ('"' + rate_id + '"' ) + ',' ;
            jsonResult += '"optional_location":' + ($scope.optional_location == undefined || $scope.optional_location == null? '"null"' : '"' + $scope.optional_location  + '"')  ;
            jsonResult += '}';
            //console.log(jsonResult);
            return  JSON.parse(jsonResult);
        };

        $scope.toShowSubLocations = function(){
            return $scope.subLocations != null && $scope.subLocations.length != 0;
        };

        $scope.countryChange = function(){
            $scope.subLocations = $scope.location.regions;
        };

        $scope.ordinaryData = function(ann_id , model, imageModel){
            var promise = $http({
                method: 'post',
                url: 'utility/loadItemByID',
                data: 'ann_id=' + ann_id +'&model=' + model + '&imageModel=' + imageModel ,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function(response){
                    $scope.description = response.description;
                    var i,j , image;
                    $scope.locations = Location.query(function(locations){
                        for(var i = 0; i < locations.length ; i++){
                            if(locations[i].id == response.c_location_id){
                                $scope.location = $scope.locations[i];
                                $scope.subLocations = $scope.location.regions;
                            }
                        }
                        if($scope.subLocations != null){
                            for(var i = 0; i < $scope.subLocations.length ; i++){
                                if($scope.subLocations[i].id == response.c_sub_location_id){
                                    $scope.subLocation = $scope.subLocations[i];
                                }
                            }
                        }
                    });

                    $scope.optional_location =  response.optional_location;

                    var uploadImages = uploadService.getUploads().images;
                    for(i = 0 ; i < response.images.length ; i++){
                        image =  response.images[i];

                        for(j = 0; j <  uploadImages.length ; j++){
                            if(uploadImages[j].orderId == image.order_id){
                                uploadImages[j].src = 'uploads/' + image.name;
                                uploadImages[j].dataId = image.id;
                                uploadImages[j].isUploaded = true;
                            }
                        }
                    }
                });
            $scope.httpCalls.push(promise);

        };

        $scope.checkPrices = function(index , isCheck){
            if(index == 3){
                var inputs = $(".descrition :text");
                if(isCheck){
                    for(var i = 0 ; i < inputs.length ; i++){
                        $(inputs[i]).prop('disabled', true);
                    }
                    $scope.groupThree.properties[0].validation = 2;
                }else{
                    for(var i = 0 ; i < inputs.length ; i++){
                        $(inputs[i]).prop('disabled', false);
                    }
                    $scope.groupThree.properties[0].validation = 4;
                }
            }
        };
    });

mainModule.controller('baseDetailedController',
    function($scope, $rootScope, $http) {

        $scope.detailedUrl;
        $scope.announcement_id;
        $scope.promise;
        $scope.announcement;
        $scope.currentPosition = 0;

        $scope.accept_url;
        $scope.listing_url;
        $scope.delete_url;
        $scope.filter_item;


        $scope.superInit = function(detailed_url){
            $scope.announcement_id = parseInt($('#car_id').html());
            $scope.detailedUrl = detailed_url;
            $scope.initRemembered();

            $scope.promise = $http({
                method: 'POST',
                url: 'detailed/' + $scope.detailedUrl ,
                data: "car_id=" + $scope.announcement_id,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                    $scope.announcement  = data;
                    var dateParts = $scope.announcement.created.split('-');
                    var subDateParts = dateParts[2].split(" ");
                    var timeParts = subDateParts[1].split(":");
                    $scope.announcement.created = new Date(dateParts[0], dateParts[1] , subDateParts[0] , timeParts[0] , timeParts[1] , timeParts[2]);
                    $rootScope.fillImages($scope.announcement.images);

            });
        }


        $scope.next = function($event){
            $event.preventDefault();
            if($scope.currentPosition < $scope.announcement.images.length/2-1){
                $scope.currentPosition = $scope.currentPosition+2;
                angular.element('#multizoom2').attr('src', 'uploads/' + $scope.announcement.images[ $scope.currentPosition].name);
            }
        };

        $scope.prev = function($event){
            $event.preventDefault();
            if($scope.currentPosition > 1){
                $scope.currentPosition = $scope.currentPosition-2;
                angular.element('#multizoom2').attr('src', 'uploads/' + $scope.announcement.images[ $scope.currentPosition].name);
            }
        };



        $(window).on('beforeunload', function () {
            var car = {"car_id": $scope.announcement.id, "car_name": $scope.announcement.announcement_name, "image_name":$scope.announcement.main_image, "price":$scope.announcement.payment_type?$scope.announcement.payment_type+' ':''+ $scope.announcement.price, "action":$scope.detailedUrl};
            var lastViewed = JSON.parse(sessionStorage.getItem("last_viewed"));
            if (!lastViewed) {
                lastViewed = [];
            }
            for (var i = 0; i < lastViewed.length; i++) {
                if (lastViewed[i].car_id == car.car_id) {
                    lastViewed.splice(i, 1);
                }
            }
            if (lastViewed.length == $rootScope.LAST_VIEWED_MAX_SIZE) {
                lastViewed.pop();
            }
            lastViewed.push(car);
            sessionStorage.setItem("last_viewed", JSON.stringify(lastViewed));

        });



        $scope.initRemembered = function(){
            var remembered = JSON.parse(sessionStorage.getItem("remembered"));
            $scope.remembered = false;
            if (!remembered) {
                remembered = [];
            }
            for (var i = 0; i < remembered.length; i++) {
                if (remembered[i].car_id == $scope.announcement_id && remembered[i].action == $scope.detailedUrl ) {
                    $scope.remembered = true;
                    return;
                }
            }
        };

        $scope.acceptItem = function(id, $event){
            $event.preventDefault();
            $http({
                method: 'POST',
                url: $scope.accept_url,
                data: "id="+id,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                    window.location = $scope.listing_url;
            });
        };


        $scope.deleteItem = function(id, $event){
            $event.preventDefault();
            $http({
                method: 'POST',
                url: $scope.delete_url,
                data: "id="+id,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                    window.location = $scope.listing_url;
                })
        };

        $scope.addToRememberedList = function(car_id, car_name, price, image_name){
            var car = {"car_id": car_id, "car_name": car_name, "image_name":image_name, "price":price, "action": $scope.detailedUrl};
            var remembered = JSON.parse(sessionStorage.getItem("remembered"));
            if (!remembered) {
                remembered = [];
            }
            for (var i = 0; i < remembered.length; i++) {
                if (remembered[i].car_id == car.car_id && remembered[i].action == car.action) {
                    remembered.splice(i, 1);
                }
            }
            if (remembered.length == $rootScope.LAST_VIEWED_MAX_SIZE) {
                remembered.pop();
            }
            remembered.push(car);
            sessionStorage.setItem("remembered", JSON.stringify(remembered));
            $scope.initRemembered();
        };

        $scope.removeFromRememberedList = function (car_id) {
            var remembered = JSON.parse(sessionStorage.getItem("remembered"));
            if (!remembered) {
                remembered = [];
            }
            for (var i = 0; i < remembered.length; i++) {
                if (remembered[i].car_id == car_id && remembered[i].action == $scope.detailedUrl) {
                    remembered.splice(i, 1);
                }
            }
            sessionStorage.setItem("remembered", JSON.stringify(remembered));
            $scope.initRemembered();
        };

        $scope.constructFilters = function (filterName, childName, filterValue, filter_condition, filter_type) {
            var filters = JSON.parse(window.sessionStorage.getItem($scope.filter_item));
            if (!filters) {
                filters = [];
            }
            filters.push({"filter_name": filterName, "child_name": childName, "filter_value": filterValue, "filter_condition": filter_condition, "filter_type": filter_type});
            window.sessionStorage.setItem($scope.filter_item, JSON.stringify(filters));
        };

        $scope.findUserAnnouncements = function ($event) {
            $event.preventDefault();
            if ($scope.announcement.user_id != -55) {
                $scope.constructFilters("User All Announcements", "", "Phone Number is " + $scope.announcement.userInfo.phone1, "(t.user_id =" + $scope.announcement.user_id + ")", 1);
            } else {
                $scope.constructFilters("User All Announcements", "", "Phone Number is " + $scope.announcement.userInfo.phone1, "(t.unregistered_user_id =" + $scope.announcement.unregistered_user_id + ")", 1);
            }

            window.location = $scope.listing_url;
        };

        $scope.addToUrgent = function(announcementId, announcementType){
            $scope.$$childHead.addToUrgent(announcementId, announcementType);
        };

        $scope.addToHomeTop = function(announcementId, announcementType){
            $scope.$$childHead.addToHomeTop(announcementId, announcementType);
        };

        $scope.addToListTop = function(announcementId, announcementType){
            $scope.$$childHead.addToListTop(announcementId, announcementType);
        };


    });

mainModule.controller('listTemplateBaseController',
    function ($scope, $http, $rootScope, onlyNumberPattern, pagingProperties) {

        $scope.pagingProperties = pagingProperties;

        $rootScope.onlyNumbers = onlyNumberPattern;
        $rootScope.showFilterByID = true;
        $rootScope.showAdditionalDiv = false;
        $rootScope.showInLargeItems = false;

        $scope.initBaseController = function (searchUrl, rememberedListCache, detailedUrl, acceptUrl, deleteUrl) {
            $rootScope.searchUrl = searchUrl;
            $rootScope.rememberedListCache = rememberedListCache;
            $rootScope.detailedUrl = detailedUrl;
            $rootScope.acceptUrl = acceptUrl;
            $rootScope.deleteUrl = deleteUrl;
            $scope.search();
        };

        /****************** VIEW MODE **********************/


        $scope.switchViewToLarge = function () {
            $scope.showInLargeItems = true;
        };

        $scope.switchViewToList = function () {
            $scope.showInLargeItems = false;
        };
        /****************** VIEW MODE **********************/


        /****************** Sorting **********************/
        $scope.sort = function () {
            if ($scope.sortingOption.id != "7") {
                var comparator = $scope.getComparatorByType();
                var list = $rootScope.announcements;
                list.sort(comparator);
                $rootScope.announcements = list;
            }
        };

        $scope.getComparatorByType = function () {
            var type = $scope.sortingOption.id;
            switch (type) {
                case '1':
                    return yearComparatorReverse;
                case '2':
                    return yearComparator;
                case '3':
                    return priceComparatorReverse;
                case '4':
                    return priceComparator;
                case '5':
                    return createdComparatorReverse;
                case '6':
                    return createdComparator;
                default :
                    return '-1';
            }
        };

        function priceComparatorReverse(aObj, bObj) {
            var a = parseInt($scope.getCurrencyValues(aObj.price, aObj.rate_id));
            var b = parseInt($scope.getCurrencyValues(bObj.price, bObj.rate_id));
            if (a < b)
                return 1;
            if (a > b)
                return -1;
            return 0;
        };

        function priceComparator(aObj, bObj) {
            var a = parseInt($scope.getCurrencyValues(aObj.price, aObj.rate_id));
            var b = parseInt($scope.getCurrencyValues(bObj.price, bObj.rate_id));
            if (a < b)
                return -1;
            if (a > b)
                return 1;
            return 0;
        };

        function yearComparator(a, b) {
            var a = parseInt(a.year);
            var b = parseInt(b.year);
            if (a < b)
                return -1;
            if (a > b)
                return 1;
            return 0;
        };

        function yearComparatorReverse(a, b) {
            var a = parseInt(a.year);
            var b = parseInt(b.year);
            if (a < b)
                return 1;
            if (a > b)
                return -1;
            return 0;
        };

        function createdComparator(a, b) {
            var a = new Date(a.created);
            var b = new Date(b.created);
            return -(dates.compare(a, b));
        };

        function createdComparatorReverse(a, b) {
            var a = new Date(a.created);
            var b = new Date(b.created);
            return dates.compare(a, b);
        };

        /****************** Sorting **********************/


        /****************** Searching **********************/
        $scope.search = function () {
            $rootScope.promise = $http({
                method: 'POST',
                url: $rootScope.searchUrl,
                data: 'data=' + angular.toJson(JSON.parse(window.sessionStorage.getItem($rootScope.rememberedListCache))),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                    $rootScope.announcements = data.announcements;
                    for (var i = 0; i < $rootScope.announcements.length; i++) {
                        var dateParts = $rootScope.announcements[i].created.split('-');
                        var subDateParts = dateParts[2].split(" ");
                        var timeParts = subDateParts[1].split(":");
                        $rootScope.announcements[i].created = new Date(dateParts[0], dateParts[1], subDateParts[0], timeParts[0], timeParts[1], timeParts[2]);

                        if ($scope.isAnnouncementRemembered($rootScope.announcements[i].id)) {
                            $rootScope.announcements[i].remembered = true;
                        } else {
                            $rootScope.announcements[i].remembered = false;
                        }
                    }
                    $rootScope.filters = data.filters;
                    $rootScope.addedFilters = JSON.parse(window.sessionStorage.getItem($rootScope.rememberedListCache));
                    $scope.isAdmin = data.isAdmin;
                    $scope.pagingProperties.loadNumberOfPages();
                    $scope.pagingProperties.setCurrentPage(0);
                }).then(function (data) {
                });
        };


        $scope.isAnnouncementRemembered = function (car_id) {
            var remembered = JSON.parse(sessionStorage.getItem("remembered"));
            $scope.remembered = false;
            if (!remembered) {
                remembered = [];
            }
            for (var i = 0; i < remembered.length; i++) {
                if (remembered[i].car_id == car_id && remembered[i].action == $rootScope.detailedUrl) {
                    return true;
                }
            }
            return false;
        };

        $scope.constructFilters = function (filterName, childName, filterValue, filter_condition, filter_type) {
            var filters = JSON.parse(window.sessionStorage.getItem($rootScope.rememberedListCache));
            if (!filters) {
                filters = [];
            }
            filters.push({"filter_name": filterName, "child_name": childName, "filter_value": filterValue, "filter_condition": filter_condition, "filter_type": filter_type});
            window.sessionStorage.setItem($rootScope.rememberedListCache, JSON.stringify(filters));
        };

        $scope.addFilter = function (filterName, childName, filterValue, filter_condition, filter_type, $event) {
            $scope.showFilterByIDError = false;
            $scope.searchId = "";
            $event.preventDefault();
            $scope.constructFilters(filterName, childName, filterValue, filter_condition, filter_type);
            $scope.search();
            $('html, body').animate({ scrollTop: 0}, 1000);
        };


        $scope.searchByType = 'ID';
        $scope.addByIdFilter = function ($event) {
            if ($scope.searchId !== undefined) {
                if($scope.searchByType == 'Phone'){
                    $scope.addFilter($rootScope.messageMap['81'], "", $scope.searchId, "user_phone.phone_number =" + $scope.searchId + " or unregistered_users.phone_1 = " + $scope.searchId, 1,$event);
                }else if($scope.searchByType == 'ID'){
                    $scope.addFilter('ID', "", "ID = " + $scope.searchId, "t.id =" + $scope.searchId, 1, $event);
                }
                $scope.showFilterByID = false;
            } else {
                $event.preventDefault();
                $scope.showFilterByIDError = true;
            }
        };

        $scope.removeFilter = function (index) {
            var filters = JSON.parse(window.sessionStorage.getItem($rootScope.rememberedListCache));
            var childName = filters[index].child_name;
            var childElement = null;
            if (filters[index].filter_name == 'ID' || filters[index].filter_name == $rootScope.messageMap['81']) {
                $scope.showFilterByID = true;
                $scope.showFilterByIDError = false;
                $scope.searchId = "";
            }
            if (childName) {
                for (var i = 0; i < filters.length; i++) {
                    if (filters[i].filter_name == childName) {
                        childElement = filters[i];
                        break;
                    }
                }
                var position = $.inArray(childElement, filters);
                if (position != -1) {
                    filters.splice(position, 1);
                }
            }
            filters.splice(index, 1);
            window.sessionStorage.setItem($rootScope.rememberedListCache, JSON.stringify(filters));
            $scope.search();
        };


        $scope.toDetailedInfo = function (id) {
            var href = $('#url_helper').find("a").attr("href");
            if (href != undefined) {
                href += "?id=" + id;
                window.location = href;
            }
        };


        /****************** Remember list **********************/
        $scope.addToRememberedList = function (car_id, car_name, price, image_name, index, $event) {
            var car = {"car_id": car_id, "car_name": car_name, "image_name": image_name, "price": price, "action": $rootScope.detailedUrl};
            var remembered = JSON.parse(sessionStorage.getItem("remembered"));
            if (!remembered) {
                remembered = [];
            }
            for (var i = 0; i < remembered.length; i++) {
                if (remembered[i].car_id == car.car_id && remembered[i].action == car.action) {
                    remembered.splice(i, 1);
                }
            }
            if (remembered.length == $rootScope.LAST_VIEWED_MAX_SIZE) {
                remembered.pop();
            }
            remembered.push(car);
            sessionStorage.setItem("remembered", JSON.stringify(remembered));
            $rootScope.announcements[index].remembered = true;
            $event.stopImmediatePropagation();

        };

        $scope.removeFromRememberedList = function (car_id, index, $event) {
            var remembered = JSON.parse(sessionStorage.getItem("remembered"));
            if (!remembered) {
                remembered = [];
            }
            for (var i = 0; i < remembered.length; i++) {
                if (remembered[i].car_id == car_id && remembered[i].action == $rootScope.rememberedListCache) {
                    remembered.splice(i, 1);
                }
            }
            sessionStorage.setItem("remembered", JSON.stringify(remembered));
            $rootScope.announcements[index].remembered = false;
            $event.stopImmediatePropagation();
        };


        $scope.additional = function (filterName, $event) {
            $('#additional_div').css('left', $event.pageX);      // <<< use pageX and pageY
            $('#additional_div').css('top', $event.pageY);
            $('#additional_div').css('display', 'inline');
            $('#additional_div').css('position', 'absolute');
            $('#additional_div').css('border', '2px solid black');
            $('#additional_div').css('z-index', "100");
            $('#additional_div').css('background-color', "#cfd1cf");
            $('#bolcker').css('opacity', "0.4");
            $('#bolcker').show();
            $event.preventDefault();
            $.each($rootScope.filters, function (i, item) {
                if (item.filter_name == filterName) {
                    $scope.additionalFilter = item;
                }
            });
            $scope.showAdditionalDiv = true;
        };

        $scope.hideAddition = function () {
            $('#bolcker').hide();
            $('#additional_div').hide();
        };

        $scope.makeSearch = function () {
            var filters = $scope.additionalFilter.filters;
            for (var i = 0; i < filters.length; i++) {
                if (filters[i].checked) {
                    $scope.constructFilters($scope.additionalFilter.filter_name, $scope.additionalFilter.child_name, filters[i].name, filters[i].filter_condition, 2);
                }
            }
            $scope.search();
            $scope.hideAddition();
        };


        /* ======================== Accept and Delete actions   ======================================  */

        $scope.acceptItem = function (id, $event) {
            $event.preventDefault();
            $http({
                method: 'POST',
                url: $scope.acceptUrl,
                data: 'data=' + angular.toJson(JSON.parse(window.sessionStorage.getItem($rootScope.rememberedListCache))) + "&id=" + id,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                    $rootScope.announcements = data.announcements;
                    $scope.pagingProperties.loadNumberOfPages();
                    $rootScope.filters = data.filters;
                    $rootScope.addedFilters = JSON.parse(window.sessionStorage.getItem($rootScope.rememberedListCache));
                }).then(function (data) {
                });
        };

        $scope.deleteItem = function (id, $event) {
            $event.preventDefault();
            $http({
                method: 'POST',
                url: $scope.deleteUrl,
                data: 'data=' + angular.toJson(JSON.parse(window.sessionStorage.getItem($rootScope.rememberedListCache))) + "&id=" + id,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                    $rootScope.announcements = data.announcements;
                    $scope.pagingProperties.loadNumberOfPages();
                    $rootScope.filters = data.filters;
                    $rootScope.addedFilters = JSON.parse(window.sessionStorage.getItem($rootScope.rememberedListCache));
                });
        };

        $scope.showFirstBanner = function($index){
            var firstPlace = Math.round($scope.pagingProperties.getPageSize() / 3)-1;
            if($index == firstPlace){
                return true;
            }
            return false;
        };

        $scope.showSecondBanner = function($index){
            var secondPlace = Math.round($scope.pagingProperties.getPageSize() / 3)*2-1;
            if($index == secondPlace){
                return true;
            }
            return false;
        };

        /* ======================== END Accept and Delete actions   ======================================  */

    }
);
