mainModule.controller('tourismController',
    function($scope, groupByID , $q, $controller, groupAnswersByID, groupAnswersByIDLookup) {
        $controller('baseFormController', {$scope: $scope});

        $scope.init = function(){
            $scope.superInit();
            var isEdit = $('#editMode').val();

            if(isEdit == 1){
                $scope.editInit();
            }else{
                $scope.httpCalls.push(  groupByID.getGroupById(21).then(function (data){
                        $scope.groupOne = data;
                    })
                );
                $scope.httpCalls.push(  groupByID.getGroupById(12).then(function (data){
                        $scope.groupTwo = data;
                    })
                );
                $scope.httpCalls.push(  groupByID.getGroupById(19).then(function (data){
                        $scope.term = data;
                    })
                );
                $scope.promise = $q.all($scope.httpCalls);
            }
        }


        $scope.submit = function(form){
            $scope.submitted = true;
            if(form.$valid){
                $scope.groups = [];
                if($scope.isAdmin){
                    $scope.groups.push($scope.groupOne, $scope.term, $scope.groupTwo, $scope.groupUserinfo);
                }else{
                    $scope.groups.push($scope.groupOne, $scope.term, $scope.groupTwo);
                }
                $scope.formSubmit('tourismForm/insert');
            }else{
                if($("#announcement_form :input.ng-invalid")){
                    $('html, body').animate({
                        scrollTop: $("#announcement_form :input.ng-invalid").first().offset().top - 50}, 1000);
                }

            }
        }


        $scope.editInit = function(){
            var announcement_id = $('#announcement_id').val();
            $scope.announcementToEdit = announcement_id;
            $scope.ordinaryData(announcement_id , 'TouristAnnouncement', 'TourismPhotos');

            $scope.httpCalls.push(
                groupAnswersByIDLookup.getGroupAnswersById(21,  announcement_id , 'TourismProperties').then(function (data){
                    $scope.groupOne = data;
                })
            );
            $scope.httpCalls.push(
                groupAnswersByIDLookup.getGroupAnswersById(12,  announcement_id , 'TourismProperties').then(function (data){
                    $scope.groupTwo = data;
                })
            );
            $scope.httpCalls.push(
                groupAnswersByIDLookup.getGroupAnswersById(19,announcement_id, 'TourismProperties').then(function (data){
                    $scope.term = data;
                })
            );
            $scope.promise = $q.all($scope.httpCalls);
        }


    });
