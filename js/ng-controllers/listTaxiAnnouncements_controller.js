mainModule.controller('taxiAnnouncementsController',
    function ($scope, $http, $controller) {

        $controller('listTemplateBaseController', {$scope: $scope});

        $scope.init = function () {
            $scope.showFilterByID = true;
            var searchUrl = 'taxiFilterHelper/getAllFilters';
            var rememberedListCache = 'taxi_filters';
            var detailedUrl = 'taxiDetailed';
            var acceptUrl = 'taxiFilterHelper/acceptAndRefresh';
            var deleteUrl = 'taxiFilterHelper/deleteAndRefresh';
            $scope.initBaseController(searchUrl, rememberedListCache, detailedUrl, acceptUrl, deleteUrl);
            $http({
                method: 'POST',
                url: 'site/sortingOptions',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                    $scope.sortingOptions = data;
                    $scope.sortingOption = data[0];
                });
        };


    });