mainModule.controller('forgotPasswordController', ['$scope', '$http',
    function ($scope, $http) {

        $scope.email = "";

        $scope.submit = function(form){
            if(form.$valid){
                $http({
                    method: 'POST',
                    url: 'site/requestNewPassword',
                    data: 'data=' + angular.toJson($scope.email),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function(){
                        $scope.requestSended = true;
                    });
            }else{

            }
        }

        $scope.goToIndex = function(){
            window.location = "index";
        };





    }
]);