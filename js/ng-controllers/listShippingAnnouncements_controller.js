mainModule.controller('shippingAnnouncementsController',
    function ($scope, $http, $controller) {

        $controller('listTemplateBaseController', {$scope: $scope});

        $scope.init = function () {
            $scope.showFilterByID = true;
            var searchUrl = 'shippingFilterHelper/getAllFilters';
            var rememberedListCache = 'shipping_filters';
            var detailedUrl = 'shippingDetailed';
            var acceptUrl = 'shippingFilterHelper/acceptAndRefresh';
            var deleteUrl = 'shippingFilterHelper/deleteAndRefresh';
            $scope.initBaseController(searchUrl, rememberedListCache, detailedUrl, acceptUrl, deleteUrl);
            $http({
                method: 'POST',
                url: 'site/sortingOptions',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                    $scope.sortingOptions = data;
                    $scope.sortingOption = data[0];
                });
        };


    });