/**
 * Created with JetBrains PhpStorm.
 * User: Edgar
 * Date: 9/14/13
 * Time: 1:09 AM
 * To change this template use File | Settings | File Templates.
 */
mainModule.controller('items_controller', ['$scope', '$http',
    function($scope, $http) {

        $scope.getCarItemsByID = function( ){
            var item_type = $('#item_type').val();
            $http({
                method: 'POST',
                url: 'default/carLookupItemsByID',
                data: 'announcement_type=' + item_type ,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).then(function(response){
                    $scope.carItems = response.data;
                    $($scope.carItems).splice(0, 1);
                });
        }

        $scope.init = function(){
            $scope.getCarItemsByID();

        }

        $scope.deleteItem = function(index , items , itemID , is_service){
            if(is_service == 0){
                $http({
                    method: 'POST',
                    url: 'default/deleteLookupItem',
                    data: 'itemID=' + itemID ,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function(data){
                        $(items).splice(index, 1);
                        $scope.init();
                    });
            }else{
                $http({
                    method: 'POST',
                    url: 'default/deleteServiceItem',
                    data: 'itemID=' + itemID ,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function(data){
                        $(items).splice(index, 1);
                        $scope.init();
                    });
            }

        }

        $scope.editItem = function (itemID , is_service) {
            var href;
            if(is_service == 0){
                href = $('#url_helper').find("a").attr("href");
                if (href != undefined) {
                    href += "?lookup_prop_id=" + itemID;
                    window.location = href;
                }
            }else{
                href = $('#url_helper_service').find("a").attr("href");
                if (href != undefined) {
                    href += "?service_id=" + itemID;
                    window.location = href;
                }
            }


        };





    }]);
