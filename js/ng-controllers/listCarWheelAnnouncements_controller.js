mainModule.controller('carWheelAnnouncementsController',
    function ($scope, $http, $controller) {

        $controller('listTemplateBaseController', {$scope: $scope});

        $scope.init = function () {
            $scope.showFilterByID = true;
            var searchUrl = 'wheelFilterHelper/getAllFilters';
            var rememberedListCache = 'wheel_filters';
            var detailedUrl = 'wheelDetailed';
            var acceptUrl = 'wheelFilterHelper/acceptAndRefresh';
            var deleteUrl = 'wheelFilterHelper/deleteAndRefresh';
            $scope.initBaseController(searchUrl, rememberedListCache, detailedUrl, acceptUrl, deleteUrl);
            $http({
                method: 'POST',
                url: 'site/sortingOptions',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                    $scope.sortingOptions = data;
                    $scope.sortingOption = data[0];
                });
        };


    });
