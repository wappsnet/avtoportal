mainModule.controller('wheelDetailedController',
    function ($scope, $rootScope, $controller) {
        $controller('baseDetailedController', {$scope: $scope});

        $scope.initIt = function () {
            $scope.superInit('wheelDetailed');
            $scope.accept_url = 'wheelFilterHelper/accept';
            $scope.listing_url = "listCarWheelAnnouncementsAll";
            $scope.delete_url = 'wheelFilterHelper/delete';
            $scope.filter_item = "wheel_filters";

        };
    });
