mainModule.directive('prpinput', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        scope: { property: '=' },
        link: function(scope, elm, attrs, ctrl){
            var regex;
            if(scope.property.validation == 2 || scope.property.validation == 4){
                regex =/^(?:[1-9]\d*|0)?(?:\.\d+)?$/;
                ctrl.$parsers.unshift(function(viewValue){
                    if(regex.test(viewValue) || viewValue == ''){
                        ctrl.$setValidity('number',true);
                    }
                    else{
                        ctrl.$setValidity('number',false);
                    }
                    return viewValue;
                })
            }
            else if(scope.property.validation == 3 || scope.property.validation == 5){
                regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\.+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                ctrl.$parsers.unshift(function(viewValue){
                    if(regex.test(viewValue) || viewValue == ''){
                        ctrl.$setValidity('email',true);
                    }
                    else{
                        ctrl.$setValidity('email',false);
                    }
                    return viewValue;
                })
            }
            else if(scope.property.validation == 6 || scope.property.validation == 7){
                regex = /^([+]?[0-9 -]+)$/;
                ctrl.$parsers.unshift(function(viewValue){
                    if(regex.test(viewValue) || viewValue == ''){
                        ctrl.$setValidity('phone',true);
                    }
                    else{
                        ctrl.$setValidity('phone',false);
                    }
                    return viewValue;
                })
            }
        }
    }
});
mainModule.directive('ckEditor', function() {
    return {
        require: '?ngModel',
        link: function(scope, elm, attr, ngModel) {
           // var ck = CKEDITOR.replace(elm[0]);

            var ck = CKEDITOR.replace(elm[0], {
                removePlugins : 'a11yhelp,basicstyles,blockquote,colorbutton,colordialog' +
                    'contextmenu,div,elementspath,find,font' +
                    'justify,keystrokes,list,liststyle,maximize,newpage,pagebreak,pastefromword,pastetext,preview,removeformat,' +
                    'resize,save,showblocks,showborders,sourcearea,stylescombo,table,tabletools,specialchar,tab,undo,wsc',
                width:'400px',
                height:'200px',
                enterMode	 : Number(2)
            });

            if (!ngModel) return;

            ck.on('pasteState', function() {
                scope.$apply(function() {
                    ngModel.$setViewValue(ck.getData());
                });
            });

            ngModel.$render = function(value) {
                ck.setData(ngModel.$viewValue);
            };
        }
    };
});

mainModule.directive("linked",function(){
    return function (scope, element, attrs) {
        var id = attrs["linked"];
        element.on("click",function(){
            document.getElementById(id).click();
        });
    };
});


mainModule.directive("passwordVerify", function() {
    return {
        require: 'ngModel',
        link: function (scope, elem, attrs, ctrl) {
            var firstPassword = '#' + attrs.passwordVerify;
            elem.add(firstPassword).on('keyup', function () {
                scope.$apply(function () {
                    var v = elem.val()===$(firstPassword).val();
                    ctrl.$setValidity('passwordVerify', v);
                });
            });
        }
    }
});

mainModule.directive('prpcombo', function ($http,$compile,$q) {
    return {
        restrict: 'A',
        require: 'ngModel',
        scope: { property: '=', properties: '='},
        link: function(scope, elm, attrs){

            scope.$watch ( attrs.ngModel ,function(v){
                var child_ids = scope.property.child_id;
                var father_item = scope.property.temp_value
                var i , j ,size , children = [];
                var ids = [];
                if(child_ids != null){
                    size = scope.properties.length;

                    if(child_ids.indexOf(',') == - 1){
                        ids.push(child_ids);
                    }else{
                        ids = child_ids.split(',');
                    }

                    for(i = 0; i < size ; i++){
                        for(j = 0 ; j < ids.length ; j++){
                            if(ids[j] == scope.properties[i].id){
                                children.push(scope.properties[i]);
                            }

                        }
                    }

                    if(children.length != 0){
                        if(father_item != undefined && father_item != null){
                            var  child;
                            var httpCalls = [];
                            for(var i = 0 ; i < children.length ; i++){
                                child = children[i];
                                httpCalls.push($http.get(
                                    'autoForm/getChildren?' + 'child_id=' + child.id + '&' + 'father_item_id=' + father_item.id
                                ));
                            }

                            $q.all(httpCalls).then(function(responses) {
                                for(var i = 0 ; i < children.length ; i++){
                                    children[i].lookup_items = responses[i].data;

                                    if(children[i].temp_value != null && children[i].temp_value != ""){//this is for edition that did not work correctly
                                        for(var j = 0 ; j < children[i].lookup_items.length ; j++){
                                            if(children[i].temp_value.id == children[i].lookup_items[j].id){
                                                children[i].temp_value = children[i].lookup_items[j];
                                                break;
                                            }
                                        }
                                    }

                                }
                            })

                        }else{
                            for(var i = 0 ; i < children.length ; i++){
                                children[i].lookup_items = [];
                            }

                        }
                    }
                }
            });
        }
    }
});

mainModule.directive("tbTooltip", function(){
        return function(scope, element, iAttrs) {
            iAttrs.$observe('title', function(value) {
                element.removeData('tooltip');
                element.tooltip();
            });
        }
    });

mainModule.directive('onFinishRender', function ($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            if (scope.$last === true) {
                $timeout(function () {
                    scope.$emit('ngRepeatFinished');
                });
            }
        }
    }
});

