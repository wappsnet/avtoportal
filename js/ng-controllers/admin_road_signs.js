/**
 * Created with JetBrains PhpStorm.
 * User: Edgar
 * Date: 12/14/13
 * Time: 12:25 AM
 * To change this template use File | Settings | File Templates.
 */

mainModule.controller('admin_road_signs_controller', ['$scope', '$http',
    function($scope, $http) {

        $scope.init = function(){
            $http({
                method: 'POST',
                url: 'default/listRoadSigns',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).then(function(response){
                    $scope.roadSignItems = response.data;
                });


        }

        $scope.roadSignItems= [];


        $scope.deleteItem = function(index , itemID){
            $http({
                method: 'POST',
                url: 'default/deleteRoadSign',
                data: 'itemID=' + itemID ,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function(data){
                    $( $scope.roadSignItems).splice(index, 1);
                    $scope.init();
                });
        }

        $scope.editItem = function (itemID) {
            var href = $('#url_helper').find("a").attr("href");
            if (href != undefined) {
                href += "?sign_id=" + itemID;
                window.location = href;
            }
        };




    }]);

