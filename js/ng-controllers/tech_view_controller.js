/**
 * Created with JetBrains PhpStorm.
 * User: Edgar
 * Date: 8/17/13
 * Time: 6:18 PM
 * To change this template use File | Settings | File Templates.
 */
mainModule.controller('techObserve_view_controller', ['$scope', '$http','techObserve',
    function($scope, $http, techObserve) {

        $scope.init = function(){
            //  var trafficPolice_id = parseInt($('#trafficPolice_id').html());
            $scope.promise = $http({
                method: 'POST',
                url: 'googleLocations/techObserveMarkers',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).then(function(response){
                    $scope.markers = response.data;
                });
            $scope.tech_item = techObserve.get({id:1});
        }

        angular.extend($scope, {
            center: {
                latitude: 40.17316305031736, // initial map center latitude
                longitude: 44.70839843750002 // initial map center longitude
            },
            markers: [], // an array of markers,
            zoom: 8
        });


    }]);
