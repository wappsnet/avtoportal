mainModule.controller('autoServiceController', ['$scope', '$http' ,'Group','ArmLocation','uploadService','GroupAnswerAutoService',
    'autoServiceAnnouncements','Services','$q','groupByID',
    function($scope, $http, Group, ArmLocation, uploadService, GroupAnswerAutoService, autoServiceAnnouncements,Services , $q, groupByID) {


        $scope.init = function(){
            var isEdit = $('#editMode').val();
            var httpCalls = [];

            if(isEdit == 1){
                $scope.editInit();
            }else{
                httpCalls.push(  groupByID.getGroupById(10).then(function (data){
                        $scope.groupUserinfo = data;
                    })
                );
                httpCalls.push(  groupByID.getGroupById(17).then(function (data){
                        $scope.groupOne = data;
                    })
                );
                httpCalls.push(  groupByID.getGroupById(19).then(function (data){
                        $scope.term = data;
                    })
                );

                $scope.promise = $q.all(httpCalls);
                $scope.servicesLookup = Services.query(
                    function(data){
                        if(data.length > 0){
                            var newService = {id:1,value:null,price:null};
                            newService.value = data[0];
                            $scope.selectedServices.push(newService);
                        }
                    }
                );
                $scope.locations = ArmLocation.query();
            }
            $scope.imagesToSave = uploadService.getUploads().imagesToSave;
            $scope.imagesToDelete = uploadService.getUploads().imagesToDelete;
        };

        $scope.announcementToEdit = null;
        $scope.locations = [];
        $scope.description;
        $scope.location;
        $scope.subLocation;
        $scope.subLocations;
        $scope.submitted = false;
        $scope.optional_location;
        $scope.selectedServices = [];
        $scope.numberOfServices = 1;
        $scope.currentIdOfService = 1;

        $scope.submit = function(form){
            $scope.submitted = true;
            if(form.$valid){
                $http({
                    method: 'POST',
                    url: 'autoService/insert',
                    data: 'data=' + angular.toJson($scope.autoToJson()) + '&uploadSave=' + angular.toJson($scope.imagesToSave) +
                        '&uploadDelete=' + angular.toJson($scope.imagesToDelete) + '&announcementToEdit=' + $scope.announcementToEdit,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function(){
                        angular.forEach(uploadService.getUploads().images, function (image) {
                            if (image.isUploaded && image.dataId == -1) {
                                image.dataId = 5;
                            }
                        });
                        window.location = document.getElementById('redirectIT').href;
                    });
            }else{
                if($("#announcement_form :input.ng-invalid")){
                    $('html, body').animate({
                        scrollTop: $("#announcement_form :input.ng-invalid").first().offset().top - 50}, 1000);
                }

            }
        }

        $scope.autoToJson = function(){
            var jsonResult;
            var groups = new Array();
            var i, j, group_length, property_length;
            groups.push($scope.groupOne, $scope.term);
            group_length =  groups.length;
            jsonResult = '{"groups":[';
            for(i = 0 ;i < group_length ; i++){
                jsonResult += '{';
                jsonResult += '"id":' + groups[i].id + ',';
                jsonResult += '"properties":[';
                property_length = groups[i].properties.length;
                for(j = 0; j < property_length ; j++){
                    jsonResult += '{';
                    jsonResult += '"id":' + groups[i].properties[j].id + ',';
                    jsonResult += '"type_id":' + groups[i].properties[j].c_property_type_id + ',';
                    if(groups[i].properties[j].c_property_type_id == 2){
                        var propertyTmp = groups[i].properties[j].temp_value;
                        jsonResult += '"value":{'
                            + '"id":' + (propertyTmp == null ? 'null' : propertyTmp.id) + ','
                            + '"name_eng":' + (propertyTmp == null ? 'null' : propertyTmp.name_eng != null ? '"' + propertyTmp.name_eng.replace(/\"/g,'') + '"' : 'null' )  + ','
                            + '"name_rus":' + (propertyTmp == null ? 'null' : propertyTmp.name_rus != null ? '"' + propertyTmp.name_rus.replace(/\"/g,'') + '"' : 'null' ) + ','
                            + '"name_geo":' + (propertyTmp == null ? 'null' : propertyTmp.name_geo != null ? '"' + propertyTmp.name_geo.replace(/\"/g,'') + '"' : 'null')  + ','
                            + '"name_arm":' + (propertyTmp == null ? 'null' : propertyTmp.name_arm != null ? '"' + propertyTmp.name_arm.replace(/\"/g,'') + '"' : 'null')
                            + '}';
                    }else{
                        jsonResult += '"value":' + '"' + groups[i].properties[j].temp_value + '"' ;
                    }
                    jsonResult += '}' + (j != property_length -1 ? ',' : '');
                }
                jsonResult += ']}' + (i != group_length -1 ? ',' : '');
            }
            jsonResult += '],';
            jsonResult += '"description":' + ($scope.description == undefined ? "null" : '"' + $scope.description.replace(/\r?\n/g, '\\n')  + '"') + ',';
            jsonResult += '"location":' + ($scope.location == undefined ? "null" : '"' + $scope.location.id + '"' ) +',';
            jsonResult += '"subLocation":' + ($scope.subLocation == undefined ? "null" :'"' + $scope.subLocation.id + '"' ) + ',' ;
            jsonResult += '"top_id":' + ($scope.top_id == undefined ? "0" :'"' + $scope.top_id + '"' ) + ',' ;
            jsonResult += '"optional_location":' + ($scope.optional_location == undefined ? "null" : '"' +$scope.optional_location  + '"') + ',' ;
            jsonResult += '"longitude":' + ($scope.markers.length == 0 ? "null" : '"' + $scope.markers[0].longitude  + '"') + ','  ;
            jsonResult += '"latitude":' + ($scope.markers.length == 0 ? "null" : '"' + $scope.markers[0].latitude  + '"') + ',';
            jsonResult += '"selected_services":' + JSON.stringify($scope.selectedServices);

            jsonResult += '}';

            return  JSON.parse(jsonResult);
        };

        $scope.toShowSubLocations = function(){
            return $scope.subLocations != null && $scope.subLocations.length != 0;
        };

        angular.extend($scope, {
            center: {
                latitude: 40.17316305031736, // initial map center latitude
                longitude: 44.70839843750002 // initial map center longitude
            },
            markers: [], // an array of markers,
            zoom: 8
        });


        $scope.editInit = function(){
            var announcement_id = $('#announcement_id').val();
            $scope.announcementToEdit = announcement_id;

            $scope.groupOne =$scope.editGroupLoad(17,announcement_id);
            $scope.groupTwo =$scope.editGroupLoad(18,announcement_id);
            $scope.groupThree =$scope.editGroupLoad(12,announcement_id);
            $scope.term = $scope.editGroupLoad(19,announcement_id);
            $scope.ordinaryData(announcement_id);
        }

        $scope.ordinaryData = function(ann_id){
            autoServiceAnnouncements.get({id:ann_id},function(response) {
                $scope.description = response.description;
                var i,j , image;
                $scope.locations = Location.query(function(locations){
                    for(var i = 0; i < locations.length ; i++){
                        if(locations[i].id == response.c_location_id){
                            $scope.location = $scope.locations[i];
                            $scope.subLocations = $scope.location.regions;
                        }
                    }
                    if($scope.subLocations != null){
                        for(var i = 0; i < $scope.subLocations.length ; i++){
                            if($scope.subLocations[i].id == response.c_sub_location_id){
                                $scope.subLocation = $scope.subLocations[i];
                            }
                        }
                    }
                });

                $scope.optional_location =  response.optional_location;

                var uploadImages = uploadService.getUploads().images;
                for(i = 0 ; i < response.images.length ; i++){
                    image =  response.images[i];

                    for(j = 0; j <  uploadImages.length ; j++){
                        if(uploadImages[j].orderId == image.order_id){
                            uploadImages[j].src = 'uploads/' + image.name;
                            uploadImages[j].dataId = image.id;
                            uploadImages[j].isUploaded = true;
                        }
                    }
                }
            });
        }

        $scope.editGroupLoad = function(group_id ,ann_id){
            return  GroupAnswerAutoService.get({id: group_id , ann_id : ann_id},function(response) {
                var props = response.properties;
                for(var i = 0 ; i < props.length ; i++){
                    if(props[i].c_property_type_id == 2 && props[i].lookup_items != null){
                        for(var j = 0; j < props[i].lookup_items.length ; j++){
                            if( props[i].lookup_items[j].id == props[i].temp_value){
                                props[i].temp_value = props[i].lookup_items[j]
                            }
                        }
                    }
                }
            });
        }

        $scope.addingService = function(){
            $scope.numberOfServices++;
            $scope.currentIdOfService++;
            var newService = {id:$scope.currentIdOfService,value:null}
            newService.value = $scope.servicesLookup[0];
            $scope.selectedServices.push(newService);
        }

        $scope.countryChange = function(){
            $scope.subLocations = $scope.location.regions;
        };


    }]);
