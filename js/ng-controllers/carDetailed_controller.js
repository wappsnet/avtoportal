mainModule.controller('carDetailedController',
    function ($scope,$http, banks, appaAutoTypes, appaAutoUsages, appaContractPeriods, $controller) {
        $controller('baseDetailedController', {$scope: $scope});

        $scope.downpayments = [];
        $scope.driver = true;
        $scope.drivers = [
            {'birth_date': '', 'card_date': '', 'appa_rule': false, 'appa_condition': false}
        ];

        $scope.main_page = true;
        $scope.auto_loan = false;
        $scope.auto_insurance = false;

        $scope.toMainPage = function () {
            $scope.main_page = true;
            $scope.auto_loan = false;
            $scope.auto_insurance = false;
            $('#main_tab').addClass('active_tab');
            $('#loan_tab').removeClass('active_tab');
            $('#insurance_tab').removeClass('active_tab');


            $('#div0').show();
            $('#div1').hide();
            $('#div2').hide();
        };

        $scope.$on('ngRepeatFinished', function(ngRepeatFinishedEvent) {
            var left_div_size = $('#left_side').children("div .description_panel").size();
            var left_divs = $('#left_side').children("div .description_panel");
            var right_divs = $('#right_side').children("div .description_panel");
            var current_max = 0;
            for(var i = 0 ; i < left_div_size ; i++){
                if($(left_divs.get(i)).height() > $(right_divs.get(i)).height()){
                    current_max = $(left_divs.get(i)).height();
                }else{
                    current_max = $(right_divs.get(i)).height();
                }

                $(right_divs.get(i)).height(current_max);

                $($(right_divs.get(i)).children("div").get(2)).css("padding-top" , (current_max - 15) / 2);

                if($($(right_divs.get(i)).children("div").get(0)).height() <= 20){
                    $($(right_divs.get(i)).children("div").get(0)).css("padding-top" , (current_max - 15) / 2 - 3);
                }

                $(left_divs.get(i)).height(current_max);

                $($(left_divs.get(i)).children("div").get(2)).css("padding-top" , (current_max - 15) / 2);

                if($($(left_divs.get(i)).children("div").get(0)).height() <= 20){
                    $($(left_divs.get(i)).children("div").get(0)).css("padding-top" , (current_max - 15) / 2 - 3);
                }

            }
        });

        $scope.toLoanPage = function () {
            $scope.main_page = false;
            $scope.auto_loan = true;
            $scope.auto_insurance = false;
            $('#main_tab').removeClass('active_tab');
            $('#loan_tab').addClass('active_tab');
            $('#insurance_tab').removeClass('active_tab');


            $('#div0').hide();
            $('#div1').show();
            $('#div2').hide();

        };

        $scope.toInsurancePage = function () {
            $scope.main_page = false;
            $scope.auto_loan = false;
            $scope.auto_insurance = true;
            $('#main_tab').removeClass('active_tab');
            $('#loan_tab').removeClass('active_tab');
            $('#insurance_tab').addClass('active_tab');
            $('#div0').hide();
            $('#div1').hide();
            $('#div2').show();
        };


        $scope.initIt = function () {
            $scope.superInit('carDetailed');
            $scope.accept_url = 'carFilterHelper/accept';
            $scope.listing_url = "listCarAnnouncements";
            $scope.delete_url = 'carFilterHelper/delete';
            $scope.filter_item = "car_filters";
            initSlider();

            $('#main_tab').addClass('active_tab');
            $scope.banks = banks.query(function (data) {
                if (data.length != 0) {
                    $scope.bank = data[0];
                    $scope.rate = $scope.bank.rate;
                    var index = 1;

                    for (var payment = parseInt($scope.bank.downpayment); payment <= 100; payment += 10) {
                        $scope.downpayments.push({'index': index, 'value': payment});
                        index++;
                    }

                    $scope.downpayment = $scope.downpayments[0];

                    $scope.term_months = [
                        {'index': 1, 'value': 6},
                        {'index': 2, 'value': 12},
                        {'index': 3, 'value': 18},
                        {'index': 4, 'value': 24},
                        {'index': 5, 'value': 30},
                        {'index': 6, 'value': 36},
                        {'index': 7, 'value': 42},
                        {'index': 8, 'value': 48},
                        {'index': 9, 'value': 54},
                        {'index': 10, 'value': 60}
                    ];

                    $scope.term_month = $scope.term_months[0];

                    $scope.monthly_payment = 0;

                } else {
                    $scope.bank = null;
                }
            });

            $scope.autoTypes = appaAutoTypes.query(function (data) {
                $scope.autoType = data[0];
            });
            $scope.autoUsages = appaAutoUsages.query(function (data) {
                $scope.autoUsage = data[0];
            });

            $scope.appaContractPeriods = appaContractPeriods.query(function (data) {
                $scope.autoContractPeriod = data[0];
            });
        };

        $scope.calculate = function () {
            var downpay = $scope.downpayment.value / 100;

            var interest = $scope.rate / 100;

            var months = $scope.term_month.value;

            var price = $scope.announcement.price;

            $scope.amount = Math.round(price * (1 - downpay));

            $scope.monthly_payment = Math.round((((price - price * downpay) * ((interest) / 12)) / (1 - (Math.pow((1 + ((interest) / 12)), (-months))))))

        };


        $scope.getAppaData = function(){
            var data = {
                "autoType":$scope.autoType.id,
                "autoUsage":$scope.autoUsage.id,
                "enginePower":$scope.announcement.engine_power,
                "cardDate":$scope.getMaxCardDate(),
                "birthDate":$scope.getMaxBirthDate(),
                "coefficientId":$scope.autoContractPeriod.id,
                "moreThanOneDriver":!$scope.driver
            };
            return data;

        };

        function card_dateComparator(a,b) {
            var a = new Date(a.card_date);
            var b = new Date(b.card_date);
            return dates.compare(a, b);
        };

        function birth_dateComparator(a,b) {
            var a = new Date(a.birth_date);
            var b = new Date(b.birth_date);
            return dates.compare(a, b);
        };

        $scope.getMaxCardDate = function(){
            var comparator = card_dateComparator;
            var list = $scope.drivers;
            list.sort(comparator);
            $scope.drivers = list;
            return $scope.drivers[0].card_date;
        };

        $scope.getMaxBirthDate = function(){
            var comparator = birth_dateComparator;
            var list = $scope.drivers;
            list.sort(comparator);
            $scope.drivers = list;
            return $scope.drivers[0].birth_date;
        };

        $scope.calculateAppa = function () {
            $http({
                method: 'POST',
                url: 'autoForm/calculateAppa',
                data: "data="+angular.toJson($scope.getAppaData()),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                    $scope.appaPayment = data;
                }).then(function (data) {
                });
        };

        $scope.addDriver = function () {
            $scope.drivers.push({'birth_date': '', 'card_date': '', 'appa_rule': true, 'appa_condition': true});
        };

        $scope.removeDriver = function (index, $event) {
            $event.preventDefault();
            $scope.drivers.splice(index, 1);
        };

    });
