mainModule.controller('carRentAnnouncementsController',
    function ($scope, $controller, sortingOptions) {
        $controller('listTemplateBaseController', {$scope: $scope});

        $scope.init = function () {
            $scope.showFilterByID = true;
            var searchUrl = 'rentCarFilterHelper/getAllFilters';
            var rememberedListCache = 'rent_car_filters';
            var detailedUrl = 'carRentDetailed';
            var acceptUrl = 'rentCarFilterHelper/acceptAndRefresh';
            var deleteUrl = 'rentCarFilterHelper/deleteAndRefresh';
            $scope.initBaseController(searchUrl, rememberedListCache, detailedUrl, acceptUrl, deleteUrl);
            $scope.sortingOptions = sortingOptions.query(function (data) {
                $scope.sortingOption = data[0];
            });
        };


    });
