/**
 * Created with JetBrains PhpStorm.
 * User: Edgar
 * Date: 12/17/13
 * Time: 1:06 AM
 * To change this template use File | Settings | File Templates.
 */

mainModule.controller('admin_traffic_rules_controller', ['$scope', '$http',
    function($scope, $http) {

        $scope.init = function(){
            $http({
                method: 'POST',
                url: 'default/listTrafficRules',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).then(function(response){
                    $scope.rulesItems = response.data;
                });


        }

        $scope.rulesItems= [];


        $scope.deleteItem = function(index , itemID){
            $http({
                method: 'POST',
                url: 'default/deleteTrafficRule',
                data: 'itemID=' + itemID ,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function(data){
                    $( $scope.rulesItems).splice(index, 1);
                    $scope.init();
                });
        }

        $scope.editItem = function (itemID) {
            var href = $('#url_helper').find("a").attr("href");
            if (href != undefined) {
                href += "?rule_id=" + itemID;
                window.location = href;
            }
        };




    }]);


