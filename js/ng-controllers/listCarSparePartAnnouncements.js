mainModule.controller('carSparePartAnnouncementsController',
    function ($scope, $controller, sortingOptions) {

        $controller('listTemplateBaseController', {$scope: $scope});

        $scope.init = function () {
            $scope.showFilterByID = true;
            var searchUrl = 'sparePartFilterHelper/getAllFilters';
            var rememberedListCache = 'spare_part_filters';
            var detailedUrl = 'sparePartDetailed';
            var acceptUrl = 'sparePartFilterHelper/acceptAndRefresh';
            var deleteUrl = 'sparePartFilterHelper/deleteAndRefresh';
            $scope.initBaseController(searchUrl, rememberedListCache, detailedUrl, acceptUrl, deleteUrl);
            $scope.sortingOptions = sortingOptions.query(function (data) {
                $scope.sortingOption = data[0];
            });
        };


    });
