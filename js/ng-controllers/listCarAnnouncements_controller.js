mainModule.controller('carAnnouncementsController',
    function ($scope, $controller, sortingOptions) {
        $controller('listTemplateBaseController', {$scope: $scope});

        $scope.init = function () {
            $scope.showFilterByID = true;
            var searchUrl = 'carFilterHelper/getAllFilters';
            var rememberedListCache = 'car_filters';
            var detailedUrl = 'carDetailed';
            var acceptUrl = 'carFilterHelper/acceptAndRefresh';
            var deleteUrl = 'carFilterHelper/deleteAndRefresh';
            $scope.initBaseController(searchUrl, rememberedListCache, detailedUrl, acceptUrl, deleteUrl);
            $scope.sortingOptions = sortingOptions.query(function(data){
                $scope.sortingOption = data[0];
            });
        };


    });