/**
 * Created with JetBrains PhpStorm.
 * User: Edgar
 * Date: 1/12/14
 * Time: 10:34 PM
 * To change this template use File | Settings | File Templates.
 */
mainModule.controller('autoServicesByPlaceController', ['autoServiceAnnouncements', '$scope', '$http' , '$rootScope', 'sortingOptions',
    function (autoServiceAnnouncements, $scope, $http, $rootScope, sortingOptions) {

        $scope.init = function(){
            var isYerevan = $('#isYerevan').val();
            if(isYerevan == 0){
                $http({
                    method: 'POST',
                    url: 'autoService/servicesByPlace',
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function (data) {
                        $scope.services = data;
                    }).then(function (data) {
                    });

                $http({
                    method: 'POST',
                    url: 'autoService/getAllYerevanServiceMarkers',
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function (data) {
                        var services = data;
                        for(var i = 0 ; i < services.length ;i++){
                            $scope.markers.push({latitude:services[i].latitude , longitude : services[i].longitude });
                        }
                    }).then(function (data) {
                    });


            }else{
                $http({
                    method: 'POST',
                    url: 'autoService/servicesBySubPlace',
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function (data) {
                        $scope.services = data;
                    }).then(function (data) {
                    });

                $http({
                    method: 'POST',
                    url: 'autoService/getAllServiceMarkers',
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function (data) {
                        var services = data;
                        for(var i = 0 ; i < services.length ;i++){
                            $scope.markers.push({latitude:services[i].latitude , longitude : services[i].longitude });
                        }
                    }).then(function (data) {
                    });
            }
        };

        $scope.markers = [];

        angular.extend($scope, {
            center: {
                latitude: 40.17316305031736, // initial map center latitude
                longitude: 44.70839843750002 // initial map center longitude
            },
            markers: [], // an array of markers,
            zoom: 8
        });

        $scope.toDetailedInfo = function (id) {
            var href = $('#url_helper').find("a").attr("href");
            if (href != undefined) {
                href += "?loc_id=" + id;
                window.location = href;
            }
        };




    }]);