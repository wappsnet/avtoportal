mainModule.controller('jobAnnouncementsController',
    function ($scope, $controller, sortingOptions) {

        $controller('listTemplateBaseController', {$scope: $scope});

        $scope.init = function () {
            $scope.showFilterByID = true;
            var searchUrl = 'jobFilterHelper/getAllFilters';
            var rememberedListCache = 'jobDetailed';
            var detailedUrl = 'sparePartDetailed';
            var acceptUrl = 'jobFilterHelper/acceptAndRefresh';
            var deleteUrl = 'jobFilterHelper/deleteAndRefresh';
            $scope.initBaseController(searchUrl, rememberedListCache, detailedUrl, acceptUrl, deleteUrl);
            $scope.sortingOptions = sortingOptions.query(function (data) {
                $scope.sortingOption = data[0];
            });
        };


    });
