
mainModule.controller('autoRentController',
    function($scope, groupByID , $q, $controller, groupAnswersByID, groupAnswersByIDLookup) {
        $controller('baseFormController', {$scope: $scope});

        $scope.init = function(){
            $scope.superInit();
            var isEdit = $('#editMode').val();


            if(isEdit == 1){
                $scope.editInit();
            }else{

                $scope.httpCalls.push(  groupByID.getGroupById(1).then(function (data){
                        $scope.groupOne = data;
                    })
                );
                $scope.httpCalls.push(  groupByID.getGroupById(2).then(function (data){
                        $scope.groupTwo = data;
                    })
                );
                $scope.httpCalls.push(  groupByID.getGroupById(12).then(function (data){
                        $scope.groupThree = data;
                    })
                );

                $scope.httpCalls.push(  groupByID.getGroupById(25).then(function (data){
                    $scope.extra = data;
                })
                );

                $scope.httpCalls.push(  groupByID.getGroupById(5).then(function (data){
                        $scope.groupFive = data;
                    })
                );

                $scope.httpCalls.push(  groupByID.getGroupById(6).then(function (data){
                        $scope.groupSix = data;
                    })
                );

                $scope.httpCalls.push(  groupByID.getGroupById(7).then(function (data){
                        $scope.groupSeven = data;
                    })
                );

                $scope.httpCalls.push(  groupByID.getGroupById(19).then(function (data){
                        $scope.term = data;
                    })
                );

                $scope.promise = $q.all($scope.httpCalls);
            }
        };


        $scope.submit = function(form){
            $scope.submitted = true;
            if(form.$valid){
                $scope.groups = [];
                if($scope.isAdmin){
                    $scope.groups.push($scope.groupOne, $scope.term, $scope.groupTwo, $scope.groupThree, $scope.extra ,
                        $scope.groupFive, $scope.groupSix, $scope.groupSeven, $scope.groupUserinfo);
                }else{
                    $scope.groups.push($scope.groupOne, $scope.term, $scope.groupTwo, $scope.groupThree, $scope.extra ,
                        $scope.groupFive, $scope.groupSix, $scope.groupSeven);
                }
                $scope.formSubmit('rentAuto/insert');
            }else{
                if($("#announcement_form :input.ng-invalid")){
                    $('html, body').animate({
                        scrollTop: $("#announcement_form :input.ng-invalid").first().offset().top - 50}, 1000);
                }
            }
        };

        $scope.editInit = function(){
            var announcement_id = $('#announcement_id').val();

            $scope.announcementToEdit = announcement_id;

            $scope.ordinaryData(announcement_id , 'CarRentAnnouncement', 'CarRentPhotos');

            $scope.httpCalls.push(
                groupAnswersByIDLookup.getGroupAnswersById(1,  announcement_id , 'CarRentProperties').then(function (data){
                    $scope.groupOne = data;
                })
            );

            $scope.httpCalls.push(
                groupAnswersByIDLookup.getGroupAnswersById(2,announcement_id, 'CarRentProperties').then(function (data){
                    $scope.groupTwo = data;
                })
            );

            $scope.httpCalls.push(
                groupAnswersByIDLookup.getGroupAnswersById(12,announcement_id, 'CarRentProperties').then(function (data){
                    $scope.groupThree = data;
                })
            );

            $scope.httpCalls.push(
                groupAnswersByIDLookup.getGroupAnswersById(25,announcement_id, 'CarRentProperties').then(function (data){
                    $scope.extra = data;
                })
            );

            $scope.httpCalls.push(
                groupAnswersByID.getGroupAnswersById(5,  announcement_id , 'CarRentProperties').then(function (data){
                    $scope.groupFive = data;
                })
            );

            $scope.httpCalls.push(
                groupAnswersByID.getGroupAnswersById(6,  announcement_id , 'CarRentProperties').then(function (data){
                    $scope.groupSix = data;
                })
            );

            $scope.httpCalls.push(
                groupAnswersByID.getGroupAnswersById(7,  announcement_id , 'CarRentProperties').then(function (data){
                    $scope.groupSeven = data;
                })
            );

            $scope.httpCalls.push(
                groupAnswersByIDLookup.getGroupAnswersById(19,announcement_id, 'CarRentProperties').then(function (data){
                    $scope.term = data;
                })
            );

            $scope.promise = $q.all($scope.httpCalls);
        };

});
