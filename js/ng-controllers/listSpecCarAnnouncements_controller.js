mainModule.controller('specCarAnnouncementController',
    function ($scope, $http, $controller) {

        $controller('listTemplateBaseController', {$scope: $scope});

        $scope.init = function () {
            $scope.showFilterByID = true;
            var searchUrl = 'specCarFilterHelper/getAllFilters';
            var rememberedListCache = 'spec_car_filters';
            var detailedUrl = 'specCarDetailed';
            var acceptUrl = 'specCarFilterHelper/acceptAndRefresh';
            var deleteUrl = 'specCarFilterHelper/deleteAndRefresh';
            $scope.initBaseController(searchUrl, rememberedListCache, detailedUrl, acceptUrl, deleteUrl);
            $http({
                method: 'POST',
                url: 'site/sortingOptions',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                    $scope.sortingOptions = data;
                    $scope.sortingOption = data[0];
                });
        };


    });
