mainModule.controller('specCarDetailedController',
    function ($scope, $rootScope, $controller) {
        $controller('baseDetailedController', {$scope: $scope});

        $scope.initIt = function () {
            $scope.superInit('specCarDetailed');
            $scope.accept_url = 'specCarFilterHelper/accept';
            $scope.listing_url = "listSpecCarAnnouncements";
            $scope.delete_url = 'specCarFilterHelper/delete';
            $scope.filter_item = "spec_car_filters";
        };
    });
