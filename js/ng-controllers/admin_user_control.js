/**
 * Created with JetBrains PhpStorm.
 * User: Edgar
 * Date: 4/20/14
 * Time: 9:49 PM
 * To change this template use File | Settings | File Templates.
 */
/**
 * Created with JetBrains PhpStorm.
 * User: Edgar
 * Date: 9/14/13
 * Time: 1:09 AM
 * To change this template use File | Settings | File Templates.
 */
mainModule.controller('user_control_controller', ['$scope', '$http',
    function($scope, $http) {

        $scope.init = function(){
            $http({
                method: 'POST',
                url: 'default/getUsers',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).then(function(response){
                    $scope.userItems = response.data;
                });

        }

        $scope.userItems = [];

        $scope.deleteUser = function(index , itemID){
            if(confirm("Are you Sure?")){
                $http({
                    method: 'POST',
                    url: 'default/deleteUser',
                    data: 'user_id=' + itemID ,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function(data){
                        $( $scope.userItems).splice(index, 1);
                        $scope.init();
                    });
            }
        }

    }]);
