mainModule.controller('sparePartDetailedController',
    function ($scope, $rootScope, $controller) {
        $controller('baseDetailedController', {$scope: $scope});

        $scope.initIt = function () {
            $scope.superInit('sparePartDetailed');
            $scope.accept_url = 'sparePartFilterHelper/accept';
            $scope.listing_url = "listSparePartAnnouncements";
            $scope.delete_url = 'sparePartFilterHelper/delete';
            $scope.filter_item = "spare_part_filters";

        };

    });
