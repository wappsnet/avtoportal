mainModule.controller('autosImmediateController', ['$scope', 'immediateAutos', '$http', '$timeout',
    function ($scope, immediateAutos, $http, $timeout) {

        $scope.currentPage = 0;
        $scope.pageSize = 16;
        $scope.numberOfPages = 1;
        $scope.promise;

        $scope.changePage = function (page) {
            var container = jQuery('#autosContainer');
            container.fadeOut();
            container.fadeIn();
            $scope.currentPage = $scope.currentPage + page;
        };

        $scope.setNumberOfPages = function () {
            $scope.numberOfPages = Math.ceil($scope.autos.length / $scope.pageSize);
        };

        $scope.pauseScroll = function(){
            $timeout.cancel($scope.promise);
        };

        $scope.resumeScroll = function(){
            if($scope.numberOfPages > 1){
                $scope.promise = $timeout($scope.scrollAutos, 4000);
            }

        };

        $scope.scrollAutos = function(){
            if($scope.currentPage == $scope.numberOfPages-1){
                $scope.currentPage = 0;
            }else{
                $scope.currentPage = $scope.currentPage + 1;
            }
            $scope.promise = $timeout($scope.scrollAutos, 4000);
        };

        $scope.init = function(){
           $scope.promise1 = $http.get('site/getImediateAutos')
                .success(function (data) {
                    $scope.autos = data;
                    $scope.setNumberOfPages();
                    if($scope.numberOfPages > 1){
                        $scope.promise = $timeout($scope.scrollAutos, 4000);
                    }
                });
        };

        $scope.toDetailedInfo = function (id) {
            var href = $('#url_helper').find("a").attr("href");
            if (href != undefined) {
                href += "?id=" + id;
                window.location = href;
            }
        };

    }]);


mainModule.filter('range', function () {
    return function (input, total) {
        total = parseInt(total);
        for (var i = 0; i < total; i++)
            input.push(i);
        return input;
    };
});

mainModule.controller('newsList', ['$scope', '$http', '$timeout',
    function ($scope, $http, $timeout) {

        $scope.currentIndex = 0;
        $scope.promise;

        $scope.init = function () {
           $scope.promise =  $http({
                method: 'POST',
                url: 'site/topNews',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                    $scope.newsList = data;

                    for(var i = 0 ;i < $scope.newsList.length ; i++){
                        $scope.newsList[i].created = $scope.newsList[i].created;
                        var dateParts = $scope.newsList[i].created.split('-');
                        var subDateParts = dateParts[2].split(" ");
                        var timeParts = subDateParts[1].split(":");
                        $scope.newsList[i].created =new Date(dateParts[0], dateParts[1] , subDateParts[0] , timeParts[0] , timeParts[1] , timeParts[2]);
                    }

                    $scope.newsSize = data.length;
                    $scope.promise = $timeout($scope.scrollNews, 4000);
                }).then(function (data) {
                });
        };

        $scope.pauseScroll = function(){
            $timeout.cancel($scope.promise);
        };

        $scope.resumeScroll = function(){
            $scope.promise = $timeout($scope.scrollNews, 4000);
        };

        $scope.scrollNews = function(){
            if($scope.currentIndex == $scope.newsList.length-1){
                $scope.currentIndex = 0;
            }else{
                $scope.currentIndex = $scope.currentIndex + 1;
            }
            $scope.promise = $timeout($scope.scrollNews, 4000);
        };

        $scope.toDetailedInfoNewS = function (id, $event) {
            var href = $('#index_news').find("a").attr("href");
            if (href != undefined) {
                href += "?id=" + id;
                window.location = href;
            }
        };


        $scope.next = function ($event) {
            $event.preventDefault();
            $scope.currentIndex = $scope.currentIndex + 1;
        };

        $scope.prev = function ($event) {
            $event.preventDefault();
            $scope.currentIndex = $scope.currentIndex - 1;
        };

        $scope.showButton = function(){
            return $scope.currentIndex == $scope.newsSize-1;
        };

        $scope.secureText = function (html){
            var tmp = document.createElement("DIV");
            tmp.innerHTML = html;
            return tmp.textContent || tmp.innerText || "";
        };


    }]);

mainModule.controller('mainFiltersController', ['$scope', '$http','$rootScope','filters',
    function ($scope, $http, $rootScope, filters) {
        $scope.isPerson = false;
        $scope.isDiller = false;
        $scope.isSwop = false;
        $scope.isLoan = false;
        $scope.bodyStyle = "0";
        $scope.searchByType = 'ID';


        $scope.showSimpleSearch = true;
        $scope.showAdvancedSearch = false;
        $scope.showSearchByBody = false;

        $scope.showAdvancedSearchTab = function () {
            $scope.showAdvancedSearch = true;
            $scope.showSearchByBody = false;
            $scope.showSimpleSearch = false;
            $('#filter_auto_in select').css('top', '');
            $('#filter_auto select').css('top', '');
        };

        $scope.showSearchByBodyTab = function () {
            $scope.showAdvancedSearch = false;
            $scope.showSearchByBody = true;
            $scope.showSimpleSearch = false;
        };

        $scope.showSimpleSearchTab = function () {
            $scope.showAdvancedSearch = false;
            $scope.showSearchByBody = false;
            $scope.showSimpleSearch = true;
        };

        $scope.onlyNumbers = /^[0-9]+$/;
        $scope.showFilterByID = true;
        $scope.showFilterByIDError = false;


        $scope.bodyCriterias = [];

        $scope.init = function () {
            window.sessionStorage.removeItem("car_filters");
            $('#filter_auto_default').css('display', '');
            $('#filter_auto_in').css('display', '');
            $('#filter_auto').css('display', '');


            $('.select_row select').customSelect();
            $scope.bodyCriterias = [
                {source: "images/car_icon1.png", label: "Седан", id: "auto_classes", class: "select_row mr40",  height: "40px", propertyId: "234"},
                {source: "images/car_icon2.png", label: "Хэтчбек", id: "auto_classes", class: "select_row mr40", height: "40px", propertyId: "232"},
                {source: "images/car_icon3.png", label: "Купе", id: "auto_classes", class: "select_row mr40", height: "40px", propertyId: "233"},
                {source: "images/car_icon4.png", label: "Кабриолет", id: "auto_classes", class: "select_row mr40", height: "40px", propertyId: "235"},
                {source: "images/car_icon5.png", label: "Универсальный", id: "auto_classes", class: "select_row mr40", height: "40px", propertyId: "236"},
                {source: "images/car_icon6.png", label: "Внедорожник", id: "auto_classes", class: "select_row mr40",  height: "40px", propertyId: "237"},
                {source: "images/car_icon8.png", label: "Микроавтобус", id: "auto_classes", class: "select_row mr40", height: "40px", propertyId: "238"},
                {source: "images/car_icon9.png", label: "Автобус", id: "auto_classes", class: "select_row mr40", height: "40px", propertyId: "241"},
                {source: "images/car_icon10.png", label: "Грузовик", id: "auto_classes", class: "select_row mr40", height: "40px", propertyId: "239"},
                {source: "images/car_icon11.png", label: "Спец. оборудование", id: "auto_classes", class: "select_row mr40", height: "40px", propertyId: "243"},
                {source: "images/car_icon13.png", label: "Мототхника", id: "auto_classes", class: "select_row mr40", height: "40px", propertyId: "240"},
                {source: "images/car_icon14.png", label: "Запчасти", id: "auto_classes", class: "select_row mr40", height: "40px", propertyId: "-55"}

            ];



            filters.getFilterData()
                .then(function (data) {
                    $scope.marks = data.markFilters;
                    $scope.startYears = data.fromYearFilters;
                    $scope.endYears = data.toYearFilters;
                    $scope.startPrices = data.fromPriceFilters;
                    $scope.endPrices = data.toPriceFilters;
                    $scope.crashedOptions = data.crashed;
                    $scope.colors = data.color;
                    $scope.transmissions = data.gearbox;
                    $scope.fuels = data.engine;
                    $scope.volumes = data.engineVolume;
                    $scope.runOptions = data.mileage;
                    $scope.customsClearanceOptions = data.taxOptions;
                    $scope.locations = data.locations;

                }).then(function () {

                    /* Mark and Model*/
                    $scope.currentMark = $scope.marks[0];
                    $scope.currentModel = $scope.currentMark.models[0];
                    var divMark = $('.mark');
                    divMark.children("span").children("span").text($scope.currentMark.label);
                    var divModel = $('.model');
                    divModel.children("span").children("span").text($scope.currentModel.label);

                    /* Start End years*/
                    $scope.startYear = $scope.startYears[0];
                    var startYear = $('.years span:first');
                    startYear.children("span").text($scope.startYear.label);
                    $scope.endYear = $scope.endYears[0];
                    var endYear = $('.years span:last');
                    endYear.text($scope.endYear.label);

                    /* Start End prices*/
                    $scope.startPrice = $scope.startPrices[0];
                    var startPrice = $('.prices span:first');
                    startPrice.children("span").text($scope.startPrice.label);
                    $scope.endPrice = $scope.endPrices[0];
                    var endPrice = $('.prices span:last');
                    endPrice.text($scope.endPrice.label);

                    /* Vtarvac*/
                    $scope.crashed = $scope.crashedOptions[0];
                    var crashedDiv = $('.crashed');
                    crashedDiv.children("span").children("span").text($scope.crashed.label);

                    /* Caolors*/
                    $scope.color = $scope.colors[0];
                    var colorDiv = $('.color');
                    colorDiv.children("span").children("span").text($scope.color.label);

                    /* Transmission*/
                    $scope.transmission = $scope.transmissions[0];
                    var transmissionDiv = $('.transmission');
                    transmissionDiv.children("span").children("span").text($scope.transmission.label);

                    /* Fuel*/
                    $scope.fuel = $scope.fuels[0];
                    var fuelDiv = $('.fuel');
                    fuelDiv.children("span").children("span").text($scope.fuel.label);

                    /* Run*/
                    $scope.run = $scope.runOptions[0];
                    var runDiv = $('.run');
                    runDiv.children("span").children("span").text($scope.run.label);

                    /* Customs Clearance*/
                    $scope.customsClearance = $scope.customsClearanceOptions[0];
                    var customsClearanceDiv = $('.customsClearance');
                    customsClearanceDiv.children("span").children("span").text($scope.customsClearance.label);


                    /* Location and Sub Location*/
                    $scope.currentLocation = $scope.locations[0];
                    $scope.currentSubLocation = $scope.currentLocation.subLocations[0];
                    var divLocation = $('.location');
                    divLocation.children("span").children("span").text($scope.currentLocation.label);
                    var divSubLocation = $('.subLocation');
                    divSubLocation.children("span").children("span").text($scope.currentSubLocation.label);


                    /* Engine Volume*/
                    $scope.volume = $scope.volumes[0];
                    var volumeDiv = $('.volume');
                    volumeDiv.children("span").children("span").text($scope.volume.label);

                    /* Watch*/
                    $scope.$watch('currentMark', function () {
                        $scope.currentModel = $scope.currentMark.models[0];
                        var divModel = $('.model');
                        divModel.children("span").children("span").text($scope.currentModel.label);
                        setTimeout(function() {
                            $scope.setSpaces();
                        }, 400);

                    });

                    $scope.setSpaces = function(){
                        $('#currModel').find('option').each(function(index , option){
                            $(option).html($(option).html().replace(/-/g, "&nbsp;"));
                        })
                    };

                    $scope.$watch('currentLocation', function () {
                        $scope.currentSubLocation = $scope.currentLocation.subLocations[0];
                        var divSubLocation = $('.subLocation');
                        divSubLocation.children("span").children("span").text($scope.currentSubLocation.label);


                    })
                }
            );

        };

        $scope.init();


        $scope.constructFilters = function (filterName, childName, filterValue, filter_condition, filter_type) {
            var filters = JSON.parse(window.sessionStorage.getItem("car_filters"));
            if (!filters) {
                filters = [];
            }
            filters.push({"filter_name": filterName, "child_name": childName, "filter_value": filterValue, "filter_condition": filter_condition, "filter_type": filter_type});
            window.sessionStorage.setItem("car_filters", JSON.stringify(filters));
        };



        $scope.getFilters = function () {
            if ($scope.currentMark.id != "0") {
                $scope.constructFilters($rootScope.messageMap['14'], $rootScope.messageMap['15'], $scope.currentMark.label, "t.mark_id =" + $scope.currentMark.id, 1);
            }
            if ($scope.currentModel.id != "0") {
                if ($scope.currentModel.group != "0") {
                    $scope.constructFilters($rootScope.messageMap['15'], "", $scope.currentModel.label, "t.model_id in ( select id from c_lookup_property where c_property_id = 2 and group_id = " + $scope.currentModel.group + " )", 1);
                } else {
                    $scope.constructFilters($rootScope.messageMap['15'], "", $scope.currentModel.label, "t.model_id =" + $scope.currentModel.id, 1);
                }

            }

            if ($scope.startYear.id != "0" && $scope.endYear.id != "0") {
                $scope.constructFilters($rootScope.messageMap['16'], "", $scope.startYear.label + " - " + $scope.endYear.label, "t.year >" + $scope.startYear.label + " AND " + "t.year <=" + $scope.endYear.label, 1);
            } else if ($scope.startYear.id != "0") {
                $scope.constructFilters($rootScope.messageMap['16'], "", "More than " + $scope.startYear.label, "t.year >" + $scope.startYear.label, 1);
            } else if ($scope.endYear.id != "0") {
                $scope.constructFilters($rootScope.messageMap['16'], "", "Less than " + $scope.endYear.label, "t.year <=" + $scope.endYear.label, 1);
            }

            if ($scope.startPrice.id != "0" && $scope.endPrice.id != "0") {
                $scope.constructFilters($rootScope.messageMap['17'], "", $scope.startPrice.label + " - " + $scope.endPrice.label, "t.price >" + $scope.startPrice.label + " AND " + "t.price <=" + $scope.endPrice.label, 1);
            } else if ($scope.startPrice.id != "0") {
                $scope.constructFilters($rootScope.messageMap['17'], "", "More than " + $scope.startPrice.label, "t.price >" + $scope.startPrice.label, 1);
            } else if ($scope.endPrice.id != "0") {
                $scope.constructFilters($rootScope.messageMap['17'], "", "Less than " + $scope.endPrice.label, "t.price <=" + $scope.endPrice.label, 1);
            }

            if ($scope.color.id !== "0") {
                $scope.constructFilters($rootScope.messageMap['54'], "", $scope.color.label, "t.color_id=" + $scope.color.id, 1);
            }

            if ($scope.transmission.id !== "0") {
                $scope.constructFilters($rootScope.messageMap['49'], "", $scope.transmission.label, "t.gearbox_id=" + $scope.transmission.id, 1);
            }

            if ($scope.fuel.id !== "0") {
                $scope.constructFilters($rootScope.messageMap['51'], "", $scope.fuel.label, "t.engine_id=" + $scope.fuel.id, 1);
            }

            if ($scope.run.id !== "0") {
                $scope.constructFilters($rootScope.messageMap['48'], "", "Less than " + $scope.run.label, "t.mileage<=" + $scope.run.label, 1);
            }

            if ($scope.volume.id !== "0") {
                $scope.constructFilters($rootScope.messageMap['50'], "", $scope.volume.label, "t.engine_volume >" + $scope.volume.start + " and t.engine_volume <=" + $scope.volume.end, 1);
            }

            if ($scope.customsClearance.id !== "0") {
                $scope.constructFilters($rootScope.messageMap['47'], "", $scope.customsClearance.label, "t.tax_id=" + $scope.customsClearance.id, 1);
            }

            if ($scope.currentLocation.id != "0") {
                $scope.constructFilters($rootScope.messageMap['57'], $rootScope.messageMap['58'], $scope.currentLocation.label, "t.c_location_id =" + $scope.currentLocation.id, 1);
            }

            if ($scope.currentSubLocation.id != "0") {
                $scope.constructFilters($rootScope.messageMap['58'], "", $scope.currentSubLocation.label, "t.c_sub_location_id =" + $scope.currentSubLocation.id, 1);
            }

            if ($scope.isDiller) {
                $scope.constructFilters($rootScope.messageMap['55'], "", $rootScope.messageMap['76'], "(user.user_roles_id = 2 OR user.user_roles_id = 3)", 1);
            }

            if ($scope.isPerson) {
                $scope.constructFilters($rootScope.messageMap['55'], "", $rootScope.messageMap['77'], "(user.user_roles_id = 1)", 1);
            }

            if ($scope.isSwop) {
                $scope.constructFilters($rootScope.messageMap['56'], "", $rootScope.messageMap['6'], "t.byExchange = 1", 1);
            }
            if ($scope.isCrashed) {
                $scope.constructFilters($rootScope.messageMap['78'], "", $rootScope.messageMap['6'], "t.is_crashed = 1", 1);
            }

            if ($scope.searchId !== undefined) {
                if($scope.searchByType == 'Phone'){
                    $scope.constructFilters($rootScope.messageMap['81'], "", $scope.searchId, "user_phone.phone_number =" + $scope.searchId + " or unregistered_users.phone_1 = " + $scope.searchId, 2);
                }else if($scope.searchByType == 'ID'){
                    $scope.constructFilters("ID", "", $scope.searchId, "t.id =" + $scope.searchId, 2);
                }


            }

        };


        /*$scope.addByIdFilter = function($event){
         if($scope.searchId !== undefined){
         $scope.addFilter($rootScope.messageMap['133'], "", "ID = " + $scope.searchId, "t.id ="+$scope.searchId, 1, $event);
         $scope.showFilterByID = false;
         }else{
         $event.preventDefault();
         $scope.showFilterByIDError = true;
         }

         };*/

        $scope.makeSearch = function () {
            $scope.getFilters();
            window.location = "listCarAnnouncements";
        };


        $scope.makeSearchByCriteria = function (propertyId) {
            if(propertyId == -55){
                window.location = "listSparePartAnnouncements"
            }else{
                var body = '';
                $.each($scope.bodyCriterias, function (i, item) {
                    if (item.propertyId == propertyId) {
                        body = item.label;
                    }
                });
                $scope.constructFilters($rootScope.messageMap['53'], "", body, "t.body_style_id =" + propertyId, 1);
                window.location = "listCarAnnouncements"
            }

        };


    }]);