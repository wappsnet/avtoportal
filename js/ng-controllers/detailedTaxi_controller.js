mainModule.controller('taxiDetailedController',
    function ($scope, $rootScope, $controller) {
        $controller('baseDetailedController', {$scope: $scope});

        $scope.initIt = function () {
            $scope.superInit('taxiDetailed');
            $scope.accept_url = 'taxiFilterHelper/accept';
            $scope.listing_url = "listTaxiAnnouncements";
            $scope.delete_url = 'taxiFilterHelper/delete';
            $scope.filter_item = "taxi_filters";
        };

    });
