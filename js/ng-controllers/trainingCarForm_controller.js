/**
 * Created with JetBrains PhpStorm.
 * User: Edgar
 * Date: 6/23/13
 * Time: 5:10 PM
 * To change this template use File | Settings | File Templates.
 */
mainModule.controller('trainingCarController',
    function($scope, groupByID , $q, $controller, groupAnswersByID, groupAnswersByIDLookup) {
        $controller('baseFormController', {$scope: $scope});

        $scope.init = function(){
            $scope.superInit();
            var isEdit = $('#editMode').val();

            if(isEdit == 1){
                $scope.editInit();
            }else{
                $scope.httpCalls.push(  groupByID.getGroupById(23).then(function (data){
                    $scope.groupOne = data;
                })
                );
                $scope.httpCalls.push(  groupByID.getGroupById(24).then(function (data){
                    $scope.groupTwo = data;
                })
                );
                $scope.httpCalls.push(  groupByID.getGroupById(19).then(function (data){
                    $scope.term = data;
                })
                );
                $scope.promise = $q.all($scope.httpCalls);
            }
        }

        $scope.submit = function(form){
            $scope.submitted = true;
            if(form.$valid){
                $scope.groups = [];
                if($scope.isAdmin){
                    $scope.groups.push($scope.groupOne, $scope.term, $scope.groupTwo, $scope.groupUserinfo);
                }else{
                    $scope.groups.push($scope.term, $scope.groupOne, $scope.groupTwo);
                }
                $scope.formSubmit('specCarForm/insert');

            }else{
                if($("#announcement_form :input.ng-invalid")){
                    $('html, body').animate({
                        scrollTop: $("#announcement_form :input.ng-invalid").first().offset().top - 50}, 1000);
                }
            }
        }

        $scope.editInit = function(){
            var announcement_id = $('#announcement_id').val();
            $scope.announcementToEdit = announcement_id;

            $scope.ordinaryData(announcement_id , 'SpecCarAnnouncement', 'SpecCarPhotos');

            $scope.httpCalls.push(
                groupAnswersByIDLookup.getGroupAnswersById(23,  announcement_id , 'SpecCarProperties').then(function (data){
                    $scope.groupOne = data;
                })
            );
            $scope.httpCalls.push(
                groupAnswersByIDLookup.getGroupAnswersById(24,  announcement_id , 'SpecCarProperties').then(function (data){
                    $scope.groupTwo = data;
                })
            );
            $scope.httpCalls.push(
                groupAnswersByIDLookup.getGroupAnswersById(19,announcement_id, 'SpecCarProperties').then(function (data){
                    $scope.term = data;
                })
            );
            $scope.promise = $q.all($scope.httpCalls);

        }


    });
