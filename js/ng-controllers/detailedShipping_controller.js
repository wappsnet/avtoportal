mainModule.controller('shippingDetailedController',
    function ($scope, $rootScope, $controller) {
        $controller('baseDetailedController', {$scope: $scope});

        $scope.initIt = function () {
            $scope.superInit('shippingDetailed');
            $scope.accept_url = 'shippingFilterHelper/accept';
            $scope.listing_url = "listShippingAnnouncements";
            $scope.delete_url = 'shippingFilterHelper/delete';
            $scope.filter_item = "shipping_filters";
        };

    });
