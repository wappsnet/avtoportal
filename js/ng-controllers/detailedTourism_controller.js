mainModule.controller('tourismDetailedController',
    function ($scope, $rootScope, $controller) {
        $controller('baseDetailedController', {$scope: $scope});

        $scope.initIt = function () {
            $scope.superInit('tourismDetailed');
            $scope.accept_url = 'tourismFilterHelper/accept';
            $scope.listing_url = "listTourismAnnouncements";
            $scope.delete_url = 'tourismFilterHelper/delete';
            $scope.filter_item = "tourism_filters";
        };

    });
