/**
 * Created with JetBrains PhpStorm.
 * User: Edgar
 * Date: 12/14/13
 * Time: 1:10 AM
 * To change this template use File | Settings | File Templates.
 */

mainModule.controller('road_police_addresses_view_controller', ['$scope', '$http',
    function($scope, $http) {

        $scope.init = function(){
            //  var trafficPolice_id = parseInt($('#trafficPolice_id').html());
            $http({
                method: 'POST',
                url: 'googleLocations/roadPoliceMarkers',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).then(function(response){
                    $scope.markers = response.data;
                });
            //$scope.address_item = RoadPoliceAddresses.get({id:1});
        }

        angular.extend($scope, {
            center: {
                latitude: 40.17316305031736, // initial map center latitude
                longitude: 44.70839843750002 // initial map center longitude
            },
            markers: [], // an array of markers,
            zoom: 8
        });

    }]);

