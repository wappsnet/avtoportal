/**
 * Created with JetBrains PhpStorm.
 * User: Edgar
 * Date: 12/16/13
 * Time: 11:04 PM
 * To change this template use File | Settings | File Templates.
 */

mainModule.controller('news_admin_controller', ['$scope', '$http',
    function($scope, $http ) {

        $scope.init = function(){
            $http({
                method: 'POST',
                url: 'default/listNewsItems',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).then(function(response){
                    $scope.news_items = response.data;

                });
        }

        $scope.news_items = [];

        $scope.deleteItem = function(index , itemID){
            $http({
                method: 'POST',
                url: 'default/deleteNewsItem',
                data: 'itemID=' + itemID ,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function(data){
                    $($scope.news_items).splice(index, 1);
                    $scope.init();
                });
        }

        $scope.editItem = function (itemID) {
            var element = $('#url_helper');
            var href = $('#url_helper').find("a").attr("href");
            if (href != undefined) {
                href += "?news_id=" + itemID;
                window.location = href;
            }
        };



    }]);

