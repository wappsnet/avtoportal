<?php

class AdminModule extends CWebModule
{
	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'admin.models.*',
			'admin.components.*',
		));

        $this->setComponents(array(

            'user' => array(
                'class' => 'CWebUser',
                'allowAutoLogin'=>false,
                'stateKeyPrefix' => 'admin',
                'loginUrl' => Yii::app()->createUrl('admin/index')
            )
        ));
	}

    public function beforeControllerAction($controller, $action)
    {
        if(parent::beforeControllerAction($controller, $action))
        {
            // this method is called before any module controller action is performed
            // you may place customized code here
            $route = $controller->id . '/' . $action->id;
            $publicPages = array(
                'default/index',
            //    'default/error',
            );
            if (Yii::app()->getModule("{$this->id}")->user->isGuest && !in_array($route, $publicPages)){
                /*set the return URL*/
//                $request=Yii::app()->request->getUrl();
//                Yii::app()->user->returnURL=$request;
                /*redirect to module login form*/
                Yii::app()->getModule("{$this->id}")->user->loginRequired();
            }
            else
                return true;
        }
        else
            return false;
    }
}
