<?php

class DefaultController extends Controller
{

    public $layout='//layouts/admin';

    public $lookup_props_with_one_language = array(
        'mark_auto' => 1,
        'model_auto'=> 2,
        'Year_auto'=> 5,
    );

    public $lookup_properties = array(

      "car_announcement" => array(
            'Categories/Body Style'=>4,
            'Year'=> 5,
            'Mileage'=> 8,
            'GearBox'=> 9,
            'Hand Drive'=> 10,
            'Color'=> 11,
            'Interior Color'=> 12,
            'Engine'=> 13,
            'Engine Volume' =>14,
            'Engine Cylinders:'=> 16,
            'Drive Train'=>17,
            'Door Count'=>18,
            'Wheels'=>19,
            'Custom Cleared'=>26,
            'payment type'=>242,
            'url' => 'editCarItems'
        ),

        "car_announcement_mark" => array(
            'mark' => 1,
            'url' => 'editCarMarkItems'
        ),

        "car_announcement_model" => array(
            'model'=> 2,
            'url' => 'editCarModelItems'
        ),


        "wheel_announcement" => array(
            'Spare Part Type' =>145,
            'Type' => 151,
            'Width' => 152,
            'Height' => 153,
            'Inch' => 154,
            'Production' => 155,
            'State' => 224,
            'url' => 'editWheelItems'
        ),
        "spare_part_announcement" => array(
            "Spare part type" => 192,
            "Spare part subtype" => 193,
            "Body style" => 194,
            "Mark" => 195,
            "Model" => 197,
            "State" => 198,
            "Year" => 200,
            'url' => 'editSparePartItems'
        ),

        "spare_part_announcement" => array(
            "Spare part type" => 192,
            "Spare part subtype" => 193,
            "Body style" => 194,
            "Mark" => 195,
            "Model" => 197,
            "State" => 198,
            "Year" => 200,
            'url' => 'editSparePartItems'
        ),
        "auto_service_announcement" => array(
            'Service' => 211,
            'Start Time' => 204,
            'End Time' => 205,
            'url' =>'editAutoServiceItems'
        ),
        "rural_tech_announcement" => array(
            'Technic' => 170,
            'Mark' => 171,
            'Body' => 172,
            'Work Type' => 173,
            'Vendor' => 174,
            'url' =>'editRuralTechItems'
        ),

        "shipping_announcement" => array(
            'Technic' => 225,
            'Mark' => 226,
            'Body' => 227,
            'Work Type' => 228,
            'Vendor' => 229,
            'url' =>'editShippingItems'
        ),

         "tourism_announcement" => array(
            'Transport type' => 219,
            'Place' => 220,
            'Vendor' => 221,
            'Provided' => 222,
            'url' =>'editTourismItems'
        ),
        "transportation_announcement" => array(
            'Transport type' => 213,
            'Country' => 214,
            'Place' => 215,
            'Vendor' => 216,
            'url' =>'editTransportationItems'
        ),



        "evacuator_announcement" => array(
            'Mark' => 179,
            'District' => 180,
            'City/Block' => 181,
            'Price for 1 km' => 182,
            'url' =>'editEvacuatorItems'
        ),

        "taxi_announcement" => array(
            'Mark' => 179,
            'District' => 180,
            'City/Block' => 181,
            'Price for 1 km' => 182,
            'url' =>'editTaxiItems'
        ),
        "training_announcement" =>array(
            'Mark' => 235,
            'Model' => 236,
            'Vendor' => 237,
            'url' =>'editTrainingCarItems'
        ),

        "job_announcement" =>array(
            'Place' => 188,
            'Work' => 189,
            'Work Type' => 190,
            'Work Sort' => 191,
            'Work Time' => 240,
            'url' =>'editJobItems'
        )

    );

    public function accessRules()
    {
        return array(
            array('allow',  // allow authenticated user to perform 'index' and 'view' actions
                'actions'=>array('index'),
                'users'=>array('*'),
            ),

            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('admin','adminCar','editCarItems','addNews','logout' , 'adminAppa','addAppa','editItems' , "lookupAdd","lookupEdit",
                    'editAutoSchool','addAutoSchool','editTechObserve','AdminMarkers','adminItems','carLookupItemsByID', 'deleteLookupItem',
                    'editWheelItems','editSparePartItems', 'editAutoServiceItems','editRuralTechItems','editShippingItems', 'editTourismItems',
                    'editEvacuatorItems','editTaxiItems','editTrainingCarItems','editJobItems','listApppaItems','deleteAppaItem','adminLocation','listSubLocations',
                    'deleteSubLocItem','deleteLocItem','addLocation','editLocation','addSubLocation','editSubLocation','editAppa','adminAutoSchool',
                    'listAutoSchools','deleteAutoSchoolItem' , 'editTaxService' , 'roadPoliceAddresses' , 'editTransportationItems','adminModelGroups',
                    'editModelGroup','addModelGroup','roadPolice','roadPolicePenalty','listRoadSigns', 'roadSigns','addRoadSign','editRoadSign',
                    'deleteRoadSign', 'listNewsItems', 'adminNews' , 'deleteNewsItem', 'editNews' , 'listTrafficRules' , 'addTrafficRule',
                    'trafficRules', 'editTrafficRule', 'deleteTrafficRule', 'addAutoService', 'editAutoService', 'deleteServiceItem','editSiteRules' ,'editCarMarkItems', 'editCarModelItems',
                    'editBank' , 'userControl', 'getUsers'
                ),
                'users'=>array('@'),
            ),
//			array('allow', // allow authenticated user to perform 'create' and 'update' actions
//				'users'=>array('@'),
//			),
//			array('allow', // allow admin user to perform 'admin' and 'delete' actions
//				'actions'=>array(),
//				'expression'=>'Yii::app()->user->isAdmin()',
//			),
            array('deny',  // deny all users
                'users'=>array('*'),
                'message'=>'Access Denied.',
            ),
        );
    }

    public function actionIndex()
    {
        $model = new LoginForm();
        // if it is ajax validation request
        if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if(isset($_POST['LoginForm']))
        {
            $model->attributes=$_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if($model->validate() && $model->login())
                $this->redirect(Yii::app()->baseUrl . '/admin/admin');
        }
        // display the login form
        $this->render('index',array('model'=>$model));
    }

    public function actionAdmin(){
        $this->render('admin');
    }

    public function actionAddNews(){

        $model = new News();

        if (Yii::app()->getRequest()->getIsAjaxRequest())
        {
            echo CActiveForm::validate( array( $model));
            Yii::app()->end();
        }

        if(isset($_POST['News'])){
            $model->attributes = $_POST['News'];

            $tmpToXML = "<div>" . $model->text . "</div>";
            $tmpToXML = html_entity_decode($tmpToXML, ENT_QUOTES, "utf-8");


            $xml = simplexml_load_string($tmpToXML);

            if($xml != null){
                $src = $xml->xpath("//img/@src");

                if(!$src){
                    $model->main_image = '/images/no_photo.jpg';
                }else{
                    $model->main_image = $src[0];
                }

                if($model->save()){
                    $this->redirect(Yii::app()->baseUrl . '/admin/admin');
                }
            }
        }

        $this->render('addNews',array('model'=>$model));
    }

    public function actionEditNews($news_id){
        $model = News::model()->findByPk($news_id);

        if (Yii::app()->getRequest()->getIsAjaxRequest())
        {
            echo CActiveForm::validate( array( $model));
            Yii::app()->end();
        }

        if(isset($_POST['News'])){
            $model->attributes = $_POST['News'];

            $tmpToXML = "<div>" . $model->text . "</div>";
            $tmpToXML = html_entity_decode($tmpToXML, ENT_QUOTES, "utf-8");


            $xml = simplexml_load_string($tmpToXML);

            if($xml != null){
                $src = $xml->xpath("//img/@src");

                if(!$src){
                    $model->main_image = '/images/no_photo.jpg';
                }else{
                    $model->main_image = $src[0];
                }

                if($model->save()){
                    $this->redirect(Yii::app()->baseUrl . '/admin/adminNews');
                }
            }
        }

        $this->render('addNews',array('model'=>$model));
    }

    public function actionAdminAppa(){
        $this->render('admin_appa');
    }

    public function actionAdminNews(){
        $this->render('adminNews');
    }

    public function actionAdminModelGroups(){
        $this->render('_adminGroups');
    }

    public function actionAdminAutoSchool(){
        $this->render('admin_autoschool');
    }

    public function actionRoadPolice(){
        $this->render('road_police');
    }

    public function actionListAutoSchools(){
        $list = Autoschool::model()->findAll();
        echo CJSON::encode($list);
    }

    public function actionListModelGroups(){
        $list = ModelGroups::model()->findAll();
        echo CJSON::encode($list);
    }

    public function actionEditTechObserve(){
        $first = TechObserve::model()->find();

        if($first == null){
            $model = new TechObserve();
        }else{
            $model = $first;
        }

        $criteria = new CDbCriteria;
        $criteria->select = '*';
        $criteria->condition = 'location_type = 3';

        $model->google_markers = CJSON::encode(GoogleMapLocation::model()->findAll($criteria));


        if (Yii::app()->getRequest()->getIsAjaxRequest())
        {
            echo CActiveForm::validate( array( $model));
            Yii::app()->end();
        }

        if(isset($_POST['TechObserve'])){

            $model->attributes = $_POST['TechObserve'];
            $google_locations = json_decode($_POST['TechObserve']['google_markers']);

            if($model->save()){

                $tech_item_id = $model->getPrimaryKey();

                Yii::app()->db->createCommand()->delete('google_map_location','location_type = 3 and belongs_to = :itemID',
                    array(':itemID'=>$tech_item_id ));

                if($google_locations != null){
                    foreach($google_locations as $marker){
                        $google_loc = new GoogleMapLocation();
                        $google_loc->latitude = $marker->latitude;
                        $google_loc->longitude = $marker->longitude;
                        $google_loc->infoWindow = $marker->infoWindow;
                        $google_loc->belongs_to = $tech_item_id;
                        $google_loc->addressName = $marker->addressName;
                        $google_loc->location_type = 3;
                        $google_loc->save();
                    }
                }
                $this->redirect(Yii::app()->baseUrl . '/admin/admin');

            }
        }
        $this->render('editTechObserve',array('model'=>$model));
    }

    public function actionEditSiteRules(){
        $first = SiteRules::model()->find();

        if($first == null){
            $model = new SiteRules();
        }else{
            $model = $first;
        }

        if (Yii::app()->getRequest()->getIsAjaxRequest())
        {
            echo CActiveForm::validate( array( $model));
            Yii::app()->end();
        }

        if(isset($_POST['SiteRules'])){
            $model->attributes = $_POST['SiteRules'];

            if($model->save()){
                $this->redirect(Yii::app()->baseUrl . '/admin/admin');
            }
        }
        $this->render('add_edit_site_rules',array('model'=>$model));
    }

    public function actionEditBank(){
        $first = BanksAutoLoan::model()->find();

        if($first == null){
            $model = new BanksAutoLoan();
        }else{
            $model = $first;
        }

        if (Yii::app()->getRequest()->getIsAjaxRequest())
        {
            echo CActiveForm::validate( array( $model));
            Yii::app()->end();
        }

        if(isset($_POST['BanksAutoLoan'])){
            $model->attributes = $_POST['BanksAutoLoan'];

            if($model->save()){
                $this->redirect(Yii::app()->baseUrl . '/admin/admin');
            }
        }
        $this->render('edit_bank',array('model'=>$model));
    }



    public function actionRoadPoliceAddresses(){
        $first = RoadPoliceAddress::model()->find();

        if($first == null){
            $model = new RoadPoliceAddress();
        }else{
            $model = $first;
        }

        $criteria = new CDbCriteria;
        $criteria->select = '*';
        $criteria->condition = 'location_type = 5';

        $model->google_markers = CJSON::encode(GoogleMapLocation::model()->findAll($criteria));


        if (Yii::app()->getRequest()->getIsAjaxRequest())
        {
            echo CActiveForm::validate( array( $model));
            Yii::app()->end();
        }

        if(isset($_POST['RoadPoliceAddress'])){

            $model->text = $_POST['RoadPoliceAddress']['text'];
            $google_locations = json_decode($_POST['RoadPoliceAddress']['google_markers']);

            if($model->save()){

                $tax_item_id = $model->getPrimaryKey();

                Yii::app()->db->createCommand()->delete('google_map_location','location_type = 5 and belongs_to = :itemID',
                    array(':itemID'=>$tax_item_id ));

                if($google_locations != null){
                    foreach($google_locations as $marker){
                        $google_loc = new GoogleMapLocation();
                        $google_loc->latitude = $marker->latitude;
                        $google_loc->longitude = $marker->longitude;
                        $google_loc->infoWindow = $marker->infoWindow;
                        $google_loc->belongs_to = $tax_item_id;
                        $google_loc->addressName = $marker->addressName;
                        $google_loc->location_type = 5;
                        $google_loc->save();
                    }
                }
                $this->redirect(Yii::app()->baseUrl . '/admin/roadPolice');

            }
        }
        $this->render('road_police_addresses',array('model'=>$model));
    }

    public function actionRoadPolicePenalty(){
        $first = RoadPolicePenalty::model()->find();

        if($first == null){
            $model = new RoadPolicePenalty();
        }else{
            $model = $first;
        }


        if (Yii::app()->getRequest()->getIsAjaxRequest())
        {
            echo CActiveForm::validate( array( $model));
            Yii::app()->end();
        }

        if(isset($_POST['RoadPolicePenalty'])){

            $model->text = $_POST['RoadPolicePenalty']['text'];

            if($model->save()){
                $this->redirect(Yii::app()->baseUrl . '/admin/roadPolice');
            }
        }
        $this->render('road_police_penalty',array('model'=>$model));

    }

    public function actionAddRoadSign(){
        $model = new RoadPoliceRoadSigns();


        if (Yii::app()->getRequest()->getIsAjaxRequest())
        {
            echo CActiveForm::validate( array( $model));
            Yii::app()->end();
        }

        if(isset($_POST['RoadPoliceRoadSigns'])){

            $model->text = $_POST['RoadPoliceRoadSigns']['text'];

            if($model->save()){
                $sign_id = $model->getPrimaryKey();
                $generated_link = Yii::app()->createUrl( 'site/roadSigns' ) . "?road_sign="  . $sign_id;

                $model::model()->updateByPk($sign_id ,array(  'link' => $generated_link));


                $this->redirect(Yii::app()->baseUrl . '/admin/roadSigns');
            }
        }
        $this->render('road_sign_add_edit',array('model'=>$model));

    }

    public function actionAddTrafficRule(){
        $model = new RoadTrafficRules();


        if (Yii::app()->getRequest()->getIsAjaxRequest())
        {
            echo CActiveForm::validate( array( $model));
            Yii::app()->end();
        }

        if(isset($_POST['RoadTrafficRules'])){

            $model->text = $_POST['RoadTrafficRules']['text'];

            if($model->save()){
                $rule_id = $model->getPrimaryKey();
                $generated_link = Yii::app()->createUrl( 'site/trafficRules' ) . "?rule_id="  . $rule_id;

                $model::model()->updateByPk($rule_id ,array(  'link' => $generated_link));


                $this->redirect(Yii::app()->baseUrl . '/admin/trafficRules');
            }
        }
        $this->render('add_edit_trafficRules',array('model'=>$model));

    }

    public function actionEditTrafficRule($rule_id){
        $model = RoadTrafficRules::model()->findByPk($rule_id);


        if (Yii::app()->getRequest()->getIsAjaxRequest())
        {
            echo CActiveForm::validate( array( $model));
            Yii::app()->end();
        }

        if(isset($_POST['RoadTrafficRules'])){

            $model->text = $_POST['RoadTrafficRules']['text'];

            if($model->save()){
                $rule_id = $model->getPrimaryKey();
                $generated_link = Yii::app()->createUrl( 'site/trafficRules' ) . "?$rule_id="  . $rule_id;

                $model::model()->updateByPk($rule_id ,array(  'link' => $generated_link));


                $this->redirect(Yii::app()->baseUrl . '/admin/trafficRules');
            }
        }
        $this->render('add_edit_trafficRules',array('model'=>$model));

    }

    public function actionEditRoadSign($sign_id){
        $model = RoadPoliceRoadSigns::model()->findByPk($sign_id);


        if (Yii::app()->getRequest()->getIsAjaxRequest())
        {
            echo CActiveForm::validate( array( $model));
            Yii::app()->end();
        }

        if(isset($_POST['RoadPoliceRoadSigns'])){

            $model->text = $_POST['RoadPoliceRoadSigns']['text'];

            if($model->save()){
                $this->redirect(Yii::app()->baseUrl . '/admin/roadSigns');
            }
        }
        $this->render('road_sign_add_edit',array('model'=>$model));

    }


    public function actionEditTaxService(){
        $first = TaxService::model()->find();

        if($first == null){
            $model = new TaxService();
        }else{
            $model = $first;
        }

        $criteria = new CDbCriteria;
        $criteria->select = '*';
        $criteria->condition = 'location_type = 4';

        $model->google_markers = CJSON::encode(GoogleMapLocation::model()->findAll($criteria));


        if (Yii::app()->getRequest()->getIsAjaxRequest())
        {
            echo CActiveForm::validate( array( $model));
            Yii::app()->end();
        }

        if(isset($_POST['TaxService'])){

            $model->text = $_POST['TaxService']['text'];
            $google_locations = json_decode($_POST['TaxService']['google_markers']);

            if($model->save()){

                $tax_item_id = $model->getPrimaryKey();

                Yii::app()->db->createCommand()->delete('google_map_location','location_type = 4 and belongs_to = :itemID',
                    array(':itemID'=>$tax_item_id ));

                if($google_locations != null){
                    foreach($google_locations as $marker){
                        $google_loc = new GoogleMapLocation();
                        $google_loc->latitude = $marker->latitude;
                        $google_loc->longitude = $marker->longitude;
                        $google_loc->infoWindow = $marker->infoWindow;
                        $google_loc->belongs_to = $tax_item_id;
                        $google_loc->addressName = $marker->addressName;
                        $google_loc->location_type = 4;
                        $google_loc->save();
                    }
                }
                $this->redirect(Yii::app()->baseUrl . '/admin/admin');

            }
        }
        $this->render('editTaxService',array('model'=>$model));
    }




    private function deleteMarkers($location_type , $belongsTo){
        Yii::app()->db->createCommand()->delete('google_map_location','location_type = :location_type and belongs_to = :item_id',
            array(':item_id'=>$belongsTo , ':location_type'=>$location_type ));
    }

    public function actionAddAppa(){
        $model = new AppaItems();

        if (Yii::app()->getRequest()->getIsAjaxRequest())
        {
            echo CActiveForm::validate( array( $model));
            Yii::app()->end();
        }

        if(isset($_POST['AppaItems'])){
            $model->attributes = $_POST['AppaItems'];
            $google_locations = json_decode($_POST['AppaItems']['google_markers']);

            if($model->save()){
                $appa_item_id = $model->getPrimaryKey();

                if($google_locations != null){
                    foreach($google_locations as $marker){
                        $google_loc = new GoogleMapLocation();
                        $google_loc->latitude = $marker->latitude;
                        $google_loc->longitude = $marker->longitude;
                        $google_loc->infoWindow = $marker->infoWindow;
                        $google_loc->belongs_to = $appa_item_id;
                        $google_loc->addressName = $marker->addressName;
                        $google_loc->location_type = 1;
                        $google_loc->save();
                    }
                }

                $this->redirect(Yii::app()->baseUrl . '/admin/adminAppa');

            }
        }
        $this->render('addAppa',array('model'=>$model));
    }

    public function actionEditAppa($appa_id){
        $model = AppaItems::model()->findByPk($appa_id);

        $criteria = new CDbCriteria;
        $criteria->select = '*';
        $criteria->condition = 'location_type = 1 and belongs_to = :appa_id';
        $criteria->params = array(':appa_id'=>$appa_id);
        $model->google_markers =  CJSON::encode(GoogleMapLocation::model()->findAll($criteria));

        if (Yii::app()->getRequest()->getIsAjaxRequest())
        {
            echo CActiveForm::validate( array( $model));
            Yii::app()->end();
        }

        if(isset($_POST['AppaItems'])){
            $model->attributes = $_POST['AppaItems'];
            $google_locations = json_decode($_POST['AppaItems']['google_markers']);

            if($model->save()){
                $appa_item_id = $model->getPrimaryKey();

                Yii::app()->db->createCommand()->delete('google_map_location','location_type = 1 and belongs_to = :itemID',
                    array(':itemID'=>$appa_item_id ));

                if($google_locations != null){
                    foreach($google_locations as $marker){
                        $google_loc = new GoogleMapLocation();
                        $google_loc->latitude = $marker->latitude;
                        $google_loc->longitude = $marker->longitude;
                        $google_loc->infoWindow = $marker->infoWindow;
                        $google_loc->belongs_to = $appa_item_id;
                        $google_loc->addressName = $marker->addressName;
                        $google_loc->location_type = 1;
                        $google_loc->save();
                    }
                }
                $this->redirect(Yii::app()->baseUrl . '/admin/adminAppa');

            }
        }
        $this->render('addAppa',array('model'=>$model));
    }

    public function actionEditAutoSchool(){
        $model = Autoschool::model()->findByPk(1);

        $criteria = new CDbCriteria;
        $criteria->select = '*';
        $criteria->condition = 'location_type = 2 and belongs_to = :sch_id';
        $criteria->params = array(':sch_id'=>1);
        $model->google_markers = CJSON::encode(GoogleMapLocation::model()->findAll($criteria));

        if (Yii::app()->getRequest()->getIsAjaxRequest())
        {
            echo CActiveForm::validate( array( $model));
            Yii::app()->end();
        }

        if(isset($_POST['Autoschool'])){
            $model->attributes = $_POST['Autoschool'];
            $google_locations = json_decode($_POST['Autoschool']['google_markers']);

            if($model->save()){
                $item_id = $model->getPrimaryKey();

                Yii::app()->db->createCommand()->delete('google_map_location','location_type = 2 and belongs_to = :itemID',
                    array(':itemID'=>$item_id ));

                if($google_locations != null){
                    foreach($google_locations as $marker){
                        $google_loc = new GoogleMapLocation();
                        $google_loc->latitude = $marker->latitude;
                        $google_loc->longitude = $marker->longitude;
                        $google_loc->infoWindow = $marker->infoWindow;
                        $google_loc->belongs_to = $item_id;
                        $google_loc->addressName = $marker->addressName;
                        $google_loc->location_type = 2;
                        $google_loc->save();
                    }
                }
                $this->redirect(Yii::app()->baseUrl . '/admin/admin');

            }
        }
        $this->render('addAutoSchool',array('model'=>$model));
    }

    public function actionAddAutoSchool(){
        $model = new Autoschool();

        if (Yii::app()->getRequest()->getIsAjaxRequest())
        {
            echo CActiveForm::validate( array( $model));
            Yii::app()->end();
        }

        if(isset($_POST['Autoschool'])){
            $model->attributes = $_POST['Autoschool'];
            $google_locations = json_decode($_POST['Autoschool']['google_markers']);

            if($model->save()){
                $autoSchool_item_id = $model->getPrimaryKey();

                if( $google_locations != null){
                    foreach($google_locations as $marker){
                        $google_loc = new GoogleMapLocation();
                        $google_loc->latitude = $marker->latitude;
                        $google_loc->longitude = $marker->longitude;
                        $google_loc->infoWindow = $marker->infoWindow;
                        $google_loc->belongs_to = $autoSchool_item_id;
                        $google_loc->addressName = $marker->addressName;
                        $google_loc->location_type = 2;//autoschool type
                        $google_loc->save();
                    }
                }


                $this->redirect(Yii::app()->baseUrl . '/admin/admin');
            }
        }

        $this->render('addAutoSchool',array('model'=>$model));
    }

    public function  actionAdminCar(){
        $this->render('adminCar');
    }

    public function actionAdminItems(){
        $this->render('admin_items');
    }

    public function actionAddBanner(){
        $model1 = Banners::model()->findByAttributes(array('positionId'=>1))?Banners::model()->findByAttributes(array('positionId'=>1)):new Banners();
        $model2 = Banners::model()->findByAttributes(array('positionId'=>2))?Banners::model()->findByAttributes(array('positionId'=>2)):new Banners();
        $model3 = Banners::model()->findByAttributes(array('positionId'=>3))?Banners::model()->findByAttributes(array('positionId'=>3)):new Banners();
        $model4 = Banners::model()->findByAttributes(array('positionId'=>4))?Banners::model()->findByAttributes(array('positionId'=>4)):new Banners();
        $model5 = Banners::model()->findByAttributes(array('positionId'=>5))?Banners::model()->findByAttributes(array('positionId'=>5)):new Banners();
        $model6 = Banners::model()->findByAttributes(array('positionId'=>6))?Banners::model()->findByAttributes(array('positionId'=>6)):new Banners();


        $position1FileName = $model1->file_name;
        $position2FileName = $model2->file_name;
        $position3FileName = $model3->file_name;
        $position4FileName = $model4->file_name;
        $position5FileName = $model5->file_name;
        $position6FileName = $model6->file_name;

        $position1Url = $model1->url;
        $position2Url = $model2->url;
        $position3Url = $model3->url;
        $position4Url = $model4->url;
        $position5Url = $model5->url;
        $position6Url = $model6->url;


        $position1 = Yii::app()->request->baseUrl . "/uploads/banners/" . $position1FileName;
        $position2 = Yii::app()->request->baseUrl . "/uploads/banners/" . $position2FileName;
        $position3 = Yii::app()->request->baseUrl . "/uploads/banners/" . $position3FileName;
        $position4 = Yii::app()->request->baseUrl . "/uploads/banners/" . $position4FileName;
        $position5 = Yii::app()->request->baseUrl . "/uploads/banners/" . $position5FileName;
        $position6 = Yii::app()->request->baseUrl . "/uploads/banners/" . $position6FileName;

        $isPosition1Img = pathinfo($position1FileName, PATHINFO_EXTENSION) !== "swf" && $position1FileName;
        $isPosition2Img = pathinfo($position2FileName, PATHINFO_EXTENSION) !== "swf" && $position2FileName;
        $isPosition3Img = pathinfo($position3FileName, PATHINFO_EXTENSION) !== "swf" && $position3FileName;
        $isPosition4Img = pathinfo($position4FileName, PATHINFO_EXTENSION) !== "swf" && $position4FileName;
        $isPosition5Img = pathinfo($position5FileName, PATHINFO_EXTENSION) !== "swf" && $position5FileName;
        $isPosition6Img = pathinfo($position6FileName, PATHINFO_EXTENSION) !== "swf" && $position6FileName;

        $this->render('addBanner',
            array('position1'=>$position1, 'position2'=>$position2, 'position3'=>$position3, 'position4'=>$position4, 'position5'=>$position5, 'position6'=>$position6,
                'isPosition1Img'=>$isPosition1Img, 'isPosition2Img'=>$isPosition2Img, 'isPosition3Img'=>$isPosition3Img, 'isPosition4Img'=>$isPosition4Img, 'isPosition5Img'=>$isPosition5Img, 'isPosition6Img'=>$isPosition6Img,
                'position1Url'=>$position1Url, 'position2Url'=>$position2Url, 'position3Url'=>$position3Url, 'position4Url'=>$position4Url, 'position5Url'=>$position5Url, 'position6Url'=>$position6Url));
    }


    public function actionEditCarItems(){
        $this->render('edit_items',array('type' => 'car_announcement'));
    }

    public function actionEditCarMarkItems(){
        $this->render('edit_items',array('type' => 'car_announcement_mark'));
    }

    public function actionEditCarModelItems(){
        $this->render('edit_items',array('type' => 'car_announcement_model'));
    }

    public function actionEditWheelItems(){
        $this->render('edit_items',array('type' => 'wheel_announcement'));
    }

    public function actionEditSparePartItems(){
        $this->render('edit_items',array('type' => 'spare_part_announcement'));
    }

    public function actionEditAutoServiceItems(){
        $this->render('edit_items',array('type' => 'auto_service_announcement'));
    }

    public function actionEditRuralTechItems(){
        $this->render('edit_items',array('type' => 'rural_tech_announcement'));
    }

    public function actionEditShippingItems(){
        $this->render('edit_items',array('type' => 'shipping_announcement'));
    }

    public function actionEditTourismItems(){
        $this->render('edit_items',array('type' => 'tourism_announcement'));
    }

    public function actionEditTransportationItems(){
        $this->render('edit_items',array('type' => 'transportation_announcement'));
    }

    public function actionEditEvacuatorItems(){
        $this->render('edit_items',array('type' => 'evacuator_announcement'));
    }

    public function actionEditTaxiItems(){
        $this->render('edit_items',array('type' => 'taxi_announcement'));
    }

    public function actionEditTrainingCarItems(){
        $this->render('edit_items',array('type' => 'training_announcement'));
    }

    public function actionEditJobItems(){
        $this->render('edit_items',array('type' => 'job_announcement'));
    }

    public function actionAdminMarkers(){
        if(isset($_POST['item_id']) && isset($_POST['location_type'])){
            $item_id = $_POST['item_id'];
            $location_type = $_POST['location_type'];

            $markers = Yii::app()->db->createCommand()->select('id ,infoWindow as infoWindow ,addressName as addressName, latitude , longitude as longitude')->
                from('google_map_location')->where('location_type = :location_type and belongs_to = :item_id',
                    array(':item_id'=>$item_id , ':location_type'=>$location_type ))->query()->readAll();

            echo  CJSON::encode($markers);
        }
    }

    private function getDataProvider($propertyID){
        $criteria = new CDbCriteria;
        $criteria->select = '*';
        $criteria->condition = 'c_property_id = :prp_id';
        $criteria->params = array(':prp_id'=>$propertyID);

        return new CActiveDataProvider(CLookupProperty, array(
            'criteria'=>$criteria,
        ));

    }

    public function actionCarLookupItemsByID(){

        $resultData = array();
        $current = array();

        if(isset($_POST['announcement_type'])){
            $prop_ids = $this->lookup_properties[$_POST['announcement_type']];

            foreach($prop_ids as $key=>$prop_id){
                if($key != 'url'){
                    if($prop_id != 211){//is not auto service
                        $criteria = new CDbCriteria;
                        $criteria->select = '*';
                        $criteria->condition = 'c_property_id = :prp_id';
                        $criteria->params = array(':prp_id'=>$prop_id);

                        if($prop_id == 2){
                            $criteria->order = 'father_id DESC';
                        }

                        $current = CLookupProperty::model()->findAll($criteria);
                        $current["header"] = $key;

                        if($prop_id == 2){
                            foreach($current as $item){
                                if(isset($item) && isset($item->father_id)){
                                    $father_id =  $item->father_id;
                                    if(isset($father_id) && is_numeric($father_id)){
                                        $model = CLookupProperty::model()->findByPk($father_id);
                                        if(isset($model)){
                                            $item->make = $model->name;
                                        }
                                    }
                                }

                            }
                            $current["model"] = 1;
                        }else{
                            $current["model"] = 0;
                        }


                        $current["link"] =  Yii::app()->request->baseUrl . '/admin/lookupAdd?prop_id=' . $prop_id;
                        $current["is_service"] = 0;
                        $resultData[] = $current;
                    }else{
                        $current = Services::model()->findAll();
                        $current["header"] = $key;
                        $current["link"] = Yii::app()->request->baseUrl . '/admin/addAutoService';
                        $current["is_service"] = 1;
                        $resultData[] = $current;
                    }
                }
            }
        }
        echo CJSON::encode($resultData);
    }

    public function actionAddAutoService(){
        $model = new Services();

        if (Yii::app()->getRequest()->getIsAjaxRequest())
        {
            echo CActiveForm::validate(array($model));
            Yii::app()->end();
        }if(isset($_POST['Services'])){
            $model->attributes = $_POST['Services'];

            if($model->save()){
                $this->redirect('editAutoServiceItems');
            }
        }
        $this->render('service_add_edit',array('model'=>$model));
    }

    public function actionEditAutoService($service_id){
        $model = Services::model()->findByPK($service_id);

        if (Yii::app()->getRequest()->getIsAjaxRequest())
        {
            echo CActiveForm::validate(array($model));
            Yii::app()->end();
        }if(isset($_POST['Services'])){
            $model->attributes = $_POST['Services'];

            if($model->save()){
                $this->redirect('editAutoServiceItems');
            }
        }
        $this->render('service_add_edit',array('model'=>$model));
    }


    private function fatherCheck($propID , $islookup){
        $result = null;

        $c_prop_id = null;

        if($islookup){
            $criteria = new CDbCriteria;
            $criteria->select = '*';
            $criteria->condition = 'id = :prp_id';
            $criteria->params = array(':prp_id'=>$propID);
            $current = CLookupProperty::model()->findAll($criteria);
            $c_prop_id = $current[0]->c_property_id;
        }else{
            $c_prop_id = $propID;
        }

        if(isset($c_prop_id)){
            $criteria = new CDbCriteria;
            $criteria->select = '*';
            $criteria->condition = 'id = :prp_type_id';
            $criteria->params = array(':prp_type_id'=>$c_prop_id);

            $currentProperty = CProperty::model()->findAll($criteria);

            if(isset($currentProperty) && count($currentProperty) > 0){
                $id_string = (string)$currentProperty[0]->id;

                $criteria = new CDbCriteria;
                $criteria->select = '*';
                $criteria->condition = "child_id  IS NOT NULL";

                $rows = CProperty::model()->findAll($criteria);

                foreach($rows as $row){
                    if(strpos($row->child_id,',') !== false){
                        $pieces = explode(",", $row->child_id);
                        foreach($pieces as $piece){
                            if($id_string == $piece){
                                $result = $row->id;
                                break;
                            }
                        }
                    }else{
                        if($row->child_id == $id_string){
                            $result = $row->id;
                            break;
                        }
                    }
                }


            }

        }
        if(isset($result) && count($result) > 0){
            return $result;
        }
        return null;

    }

    private function getUpLevelItems($prop_type_id){
        $criteria = new CDbCriteria;
        $criteria->select = '*';
        $criteria->condition = 'c_property_id = :prp_id';
        $criteria->params = array(':prp_id'=>$prop_type_id);

        $children = CLookupProperty::model()->findAll($criteria);

        return $children;
    }

    private function  checkForOneLang($prop_id){
        foreach($this->lookup_props_with_one_language as $one_lang){
            if($one_lang == $prop_id){
                return true;
            }
        }
        return false;
    }

    public function actionLookupAdd($prop_id){
        $model = new CLookupProperty();
        $upLevelPropID = $this->fatherCheck($prop_id,false);

        if(isset($upLevelPropID) && $upLevelPropID != 0){
            $model->upLevelItems = $this->getUpLevelItems($upLevelPropID);
        }


        if (Yii::app()->getRequest()->getIsAjaxRequest())
        {
            echo CActiveForm::validate(array($model));
            Yii::app()->end();
        }if(isset($_POST['CLookupProperty'])){
            $model->attributes = $_POST['CLookupProperty'];

            if(!isset($_POST['CLookupProperty']['group_id'])){
                $model->group_id = null;
            }else{
                $model->group_id = $_POST['CLookupProperty']['group_id'];
            }
            $model->c_property_id = $prop_id;
            if($model->save()){
                $this->redirect($this->digOutUrl($prop_id));
            }
        }
        if($this->checkForOneLang($prop_id)){
            if($prop_id == 2){
                $model_groups = ModelGroups::model()->findAll();
                $this->render('lookup_add_edit',array('model'=>$model , 'one_lang' => 1 , 'model_groups' => $model_groups));
            }else{
                $this->render('lookup_add_edit',array('model'=>$model , 'one_lang' => 1 ));
            }
        }else{
            $this->render('lookup_add_edit',array('model'=>$model , 'one_lang' => 0));
        }

    }

    public function actionLookupEdit($lookup_prop_id){
        $model = CLookupProperty::model()->findByPk($lookup_prop_id);
        $prop_id = $model->c_property_id;
        $upLevelPropID = $this->fatherCheck($lookup_prop_id,true);

        if(isset($upLevelPropID) && $upLevelPropID != 0){
            $model->upLevelItems = $this->getUpLevelItems($upLevelPropID);
        }

        if (Yii::app()->getRequest()->getIsAjaxRequest())
        {
            echo CActiveForm::validate( array( $model));
            Yii::app()->end();
        }if(isset($_POST['CLookupProperty'])){
            $model->attributes = $_POST['CLookupProperty'];

            if(!isset($_POST['CLookupProperty']['group_id'])){
                $model->group_id = null;
            }else{
                $model->group_id = $_POST['CLookupProperty']['group_id'];
            }

            if($model->save()){
                $this->redirect($this->digOutUrl($prop_id));
            }
        }

        if($this->checkForOneLang($model->c_property_id)){
            if($prop_id == 2){
                $model_groups = ModelGroups::model()->findAll();
                $this->render('lookup_add_edit',array('model'=>$model , 'one_lang' => 1 , 'model_groups' => $model_groups));
            }else{
                $this->render('lookup_add_edit',array('model'=>$model , 'one_lang' => 1 ));
            }
        }else{
            $this->render('lookup_add_edit',array('model'=>$model , 'one_lang' => 0));
        }
    }

    private function digOutUrl($prop_id){
        $url = null;
        foreach($this->lookup_properties as $properties){
            foreach($properties as $prop){
                if($prop == $prop_id){
                    $url = $properties['url'];
                }
            }
        }

        if($url != null){
            $url = Yii::app()->baseUrl . '/admin/' . $url;
        }

        return $url;
    }

    public function actionDeleteLookupItem(){
        if(isset($_POST['itemID'])){
            $itemID = $_POST['itemID'];
            Yii::app()->db->createCommand()->delete('c_lookup_property','id = :itemID',
                array(':itemID'=>$itemID ));

            Yii::app()->db->createCommand()->delete('c_lookup_property','father_id = :itemID',
                array(':itemID'=>$itemID ));
        }
    }
    public function actionDeleteServiceItem(){
        if(isset($_POST['itemID'])){
            $itemID = $_POST['itemID'];
            Yii::app()->db->createCommand()->delete('services','id = :itemID',
                array(':itemID'=>$itemID ));
        }
    }

    public function actionListApppaItems(){
        $appaItems = AppaItems::model()->findAll();
        echo CJSON::encode($appaItems);
    }

    public function actionListNewsItems(){
        $newsItems = News::model()->findAll();
        echo CJSON::encode($newsItems);
    }

    public function actionListLocations(){
        $locations = CLocation::model()->findAll();
        echo CJSON::encode($locations);
    }

    public function actionListRoadSigns(){
        $locations = RoadPoliceRoadSigns::model()->findAll();
        echo CJSON::encode($locations);
    }

    public function actionListTrafficRules(){
        $rules = RoadTrafficRules::model()->findAll();
        echo CJSON::encode($rules);
    }

    public function actionRoadSigns(){
        $this->render('road_signs');
    }

    public function actionTrafficRules(){
        $this->render('road_traffic_rules');
    }


    public function actionListSubLocations(){
        $subLocations = CSubLocation::model()->findAll();
        echo CJSON::encode($subLocations);
    }

    public function actionAdminLocation(){
        $this->render('admin_locations');
    }

    public function actionDeleteLocItem(){
        if(isset($_POST['itemID'])){
            $itemID = $_POST['itemID'];
            Yii::app()->db->createCommand()->delete('c_location','id = :itemID',
                array(':itemID'=>$itemID ));
        }
    }

    public function actionDeleteRoadSign(){
        if(isset($_POST['itemID'])){
            $itemID = $_POST['itemID'];
            Yii::app()->db->createCommand()->delete('road_police_road_signs','id = :itemID',
                array(':itemID'=>$itemID ));
        }
    }

    public function actionDeleteTrafficRule(){
        if(isset($_POST['itemID'])){
            $itemID = $_POST['itemID'];
            Yii::app()->db->createCommand()->delete('road_traffic_rules','id = :itemID',
                array(':itemID'=>$itemID ));
        }
    }



    public function actionDeleteSubLocItem(){
        if(isset($_POST['itemID'])){
            $itemID = $_POST['itemID'];
            Yii::app()->db->createCommand()->delete('c_sub_location','id = :itemID',
                array(':itemID'=>$itemID ));
        }
    }

    public function actionDeleteAppaItem(){
        if(isset($_POST['itemID'])){
            $itemID = $_POST['itemID'];
            Yii::app()->db->createCommand()->delete('appa_items','id = :itemID',
                array(':itemID'=>$itemID ));

            Yii::app()->db->createCommand()->delete('google_map_location','location_type = 1 and belongs_to = :itemID',
                array(':itemID'=>$itemID ));

        }
    }

    public function actionDeleteNewsItem(){
        if(isset($_POST['itemID'])){
            $itemID = $_POST['itemID'];
            Yii::app()->db->createCommand()->delete('news','id = :itemID',
                array(':itemID'=>$itemID ));
        }
    }


    public function actionDeleteAutoSchoolItem(){
        if(isset($_POST['itemID'])){
            $itemID = $_POST['itemID'];
            Yii::app()->db->createCommand()->delete('autoschool','id = :itemID',
                array(':itemID'=>$itemID ));

            Yii::app()->db->createCommand()->delete('google_map_location','location_type = 2 and belongs_to = :itemID',
                array(':itemID'=>$itemID ));

        }
    }

    public function actionDeleteModelGroupItem(){
        if(isset($_POST['itemID'])){
            $itemID = $_POST['itemID'];
            Yii::app()->db->createCommand()->delete('model_groups','id = :itemID',
                array(':itemID'=>$itemID ));

        }
    }




    public function actionAddLocation(){
        $model = new CLocation();

        if (Yii::app()->getRequest()->getIsAjaxRequest())
        {
            echo CActiveForm::validate(array($model));
            Yii::app()->end();
        }
        if(isset($_POST['CLocation'])){
            $model->attributes = $_POST['CLocation'];

            $uploadFile = CUploadedFile::getInstance($model,'flag_name');
            $fileName = "";
			if($uploadFile != null){
				$fileName = time().$uploadFile->name;
				$model->flag_name = $fileName;
            }


            if($model->save()){
                $path = Yii::app() -> getBasePath() . "/../uploads/flags/" . $fileName;
                $uploadFile->saveAs($path);

                $this->redirect(Yii::app()->baseUrl . '/admin/' . 'adminLocation');
            }
        }
        $this->render('edit_locations',array('model'=>$model));
    }

    public function actionEditLocation($location_id){
        $model = CLocation::model()->findByPk($location_id);

        if (Yii::app()->getRequest()->getIsAjaxRequest())
        {
            echo CActiveForm::validate(array($model));
            Yii::app()->end();
        }
        if(isset($_POST['CLocation'])){
            $model->attributes = $_POST['CLocation'];
            $uploadFile = CUploadedFile::getInstance($model,'flag_name');
            $fileName = "";
            if($uploadFile != null){
                $fileName = time().$uploadFile->name;
                $model->flag_name = $fileName;
            }


            if($model->save()){
                $path = Yii::app() -> getBasePath() . "/../uploads/flags/" . $fileName;
                $uploadFile->saveAs($path);
                $this->redirect(Yii::app()->baseUrl . '/admin/' . 'adminLocation');
            }
        }
        $this->render('edit_locations',array('model'=>$model));
    }

    public function actionEditModelGroup($group_id){
        $model = ModelGroups::model()->findByPk($group_id);

        if (Yii::app()->getRequest()->getIsAjaxRequest())
        {
            echo CActiveForm::validate(array($model));
            Yii::app()->end();
        }
        if(isset($_POST['ModelGroups'])){
            $model->name = $_POST['ModelGroups']['name'];
            if($model->save()){
                $this->redirect(Yii::app()->baseUrl . '/admin/' . 'adminModelGroups');
            }
        }
        $this->render('add_edit_modelGroups',array('model'=>$model));
    }

    public function actionAddModelGroup(){
        $model = new ModelGroups();

        if (Yii::app()->getRequest()->getIsAjaxRequest())
        {
            echo CActiveForm::validate(array($model));
            Yii::app()->end();
        }
        if(isset($_POST['ModelGroups'])){
            $model->name = $_POST['ModelGroups']['name'];
            if($model->save()){
                $this->redirect(Yii::app()->baseUrl . '/admin/' . 'adminModelGroups');
            }
        }
        $this->render('add_edit_modelGroups',array('model'=>$model));
    }

    public function actionAddSubLocation(){
        $model = new CSubLocation();
        $locations = CLocation::model()->findAll();
        if (Yii::app()->getRequest()->getIsAjaxRequest())
        {
            echo CActiveForm::validate(array($model));
            Yii::app()->end();
        }
        if(isset($_POST['CSubLocation'])){
            $model->attributes = $_POST['CSubLocation'];
            if($model->save()){
                $this->redirect(Yii::app()->baseUrl . '/admin/' . 'adminLocation');
            }
        }
        $this->render('edit_subLocations',array('model'=>$model,'locations' => $locations));
    }

    public function actionEditSubLocation($subLocation_id){
        $model = CSubLocation::model()->findByPk($subLocation_id);
        $locations = CLocation::model()->findAll();

        if (Yii::app()->getRequest()->getIsAjaxRequest())
        {
            echo CActiveForm::validate(array($model));
            Yii::app()->end();
        }
        if(isset($_POST['CSubLocation'])){
            $model->attributes = $_POST['CSubLocation'];
            if($model->save()){
                $this->redirect(Yii::app()->baseUrl . '/admin/' . 'adminLocation');
            }
        }
        $this->render('edit_subLocations',array('model'=>$model,'locations' => $locations));

    }

    public function actionUserControl(){
        $this->render('user_control');
    }

    public function actionGetUsers(){

        $items = Yii::app()->db->createCommand()->select('user.id id, user.email email, user.user_roles_id user_roles_id,
        user.user_name user_name, user.phone1_id phone1_id, up.id up_id , up.phone_number phone_number')->
            from('user')->join('user_phone up','up.id = phone1_id')->where('user.id > 0 and user_roles_id != 4')->query()->readAll();

        echo CJSON::encode($items);
    }

    public function actionDeleteUser(){
        $user_id = $_POST['user_id'];
        $criteria = new CDbCriteria;
        $criteria->select = '*';
        $criteria->condition = 'user_id = :usr_id';
        $criteria->params = array(':usr_id'=>$user_id);

        $this->deleteAnnouncementModel('CarAnnouncement',$criteria);
        $this->deleteAnnouncementModel('CarRentAnnouncement',$criteria);
        $this->deleteAnnouncementModel('EvacuatorAnnouncement',$criteria);
        $this->deleteAnnouncementModel('JobAnnouncement',$criteria);
        $this->deleteAnnouncementModel('RuraltechAnnouncement',$criteria);
        $this->deleteAnnouncementModel('ShippingAnnouncement',$criteria);
        $this->deleteAnnouncementModel('SparePartAnnouncement',$criteria);
        $this->deleteAnnouncementModel('SpecCarAnnouncement',$criteria);
        $this->deleteAnnouncementModel('TaxiAnnouncement',$criteria);
        $this->deleteAnnouncementModel('TouristAnnouncement',$criteria);
        $this->deleteAnnouncementModel('TransportationAnnouncement',$criteria);
        $this->deleteAnnouncementModel('WheelsAnnouncement',$criteria);

        User::model()->deleteByPk($user_id);

    }

    private function deleteAnnouncementModel($model , $criteria){
        $items = $model::model()->findAll($criteria);
        foreach ($items as $item) {
            $model::model()->deleteByPk($item->id);
        }
    }

    public function actionLogout()
    {
        Yii::app()->getModule('admin')->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }



}