<?php

class BannerUploadController extends Controller
{
    private $error = "";
    private $msg = "";
    private $fileElementName = 'qqfile';

    public function accessRules()
    {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('upload', 'cleanUpOnExit'),
                'users' => array('*'),
            ),
        );
    }

    public function actionUpload()
    {
        $sizeLimit = 100 * 1024 * 1024; // maximum file size in bytes
        $positionTypeId = $_POST['id'];
        $url = $_POST['url'];

        if (!empty($_FILES[$this->fileElementName]['error'])) {
            switch ($_FILES[$this->fileElementName]['error']) {
                case '1':
                    $this->error = 'The uploaded file exceeds the upload_max_filesize directive in php.ini';
                    break;
                case '2':
                    $this->error = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
                    break;
                case '3':
                    $this->error = 'The uploaded file was only partially uploaded';
                    break;
                case '4':
                    $this->error = 'No file was uploaded.';
                    break;
                case '6':
                    $this->error = 'Missing a temporary folder';
                    break;
                case '7':
                    $this->error = 'Failed to write file to disk';
                    break;
                case '8':
                    $this->error = 'File upload stopped by extension';
                    break;
                case '999':
                default:
                    $this->error = 'No error code avaiable';
            }
        } elseif (empty($_FILES[$this->fileElementName]['tmp_name']) || $_FILES[$this->fileElementName]['tmp_name'] == 'none') {
            $this->error = 'No file was uploaded..';
        } else {
            $this->msg .= " File Name: " . $_FILES[$this->fileElementName]['name'] . ", ";
            $this->msg .= " File Size: " . @filesize($_FILES[$this->fileElementName]['tmp_name']);
            //for security reason, we force to remove all uploaded file
           // @unlink($_FILES[$this->fileElementName]);
        }
        $fileName = '';
        $ext = '';
        if(!$this->error){
            Yii::import("ext.EAjaxUpload.qqFileUploader");
            Yii::import("application.components.SimpleImage");
            $folder = Yii::app()->getBasePath() . "/../uploads/banners/"; // folder for uploaded files
            $allowedExtensions = array("jpg", "jpeg", "gif", "swf", "mp4"); //array("jpg","jpeg","gif","exe","mov" and etc...

            $uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
            $result = $uploader->handleUpload($folder);
            $fileName = $result['filename']; //GETTING FILE NAME

            $oldBanner = Banners::model()->findByAttributes(array('positionId'=>$positionTypeId));
            if($oldBanner && file_exists(Yii::app()->getBasePath() . "/../uploads/banners/" . $oldBanner->file_name) ){
                unlink(Yii::app()->getBasePath() . "/../uploads/banners/" . $oldBanner->file_name);
            }
            Banners::model()->deleteAllByAttributes(array('positionId'=>$positionTypeId));

            $banner = new Banners();
            $banner->file_name = $fileName;
            $banner->positionId = $positionTypeId;
            $banner->url = $url;
            $banner->save();

            $ext = pathinfo($fileName, PATHINFO_EXTENSION);
            if($ext !== "swf"){
                $image = new SimpleImage();
                $image->load($folder . $fileName);
                $image->resizeWithRestriction(800, 400);
                $image->save($folder . $fileName);
            }
        }

        $result['imagePath'] = Yii::app()->request->baseUrl . "/uploads/banners/" . $fileName;
        $result['title'] = $fileName;
        $result['error'] = $this->error;
        $result['msg'] = $this->msg;
        $result['url'] = $url;
        $result['id'] = -1;
        $result['isFlash'] = $ext === "swf";


        $return = htmlspecialchars(json_encode($result), ENT_NOQUOTES);
        echo $return; // it's array
    }

  /*  public function actionCleanUpOnExit()
    {
        $array = Yii::app()->request->getPost('ids');
        if (is_array($array)) {
            foreach ($array as $src) {
                unlink(Yii::app()->getBasePath() . "/../banners/" . $src);
            }
        }
    }*/

    public function actionGetBanners(){
        $banners = Banners::model()->findAll();
        echo CJSON::encode($banners);
    }

}