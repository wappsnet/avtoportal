<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Edgar
 * Date: 6/29/13
 * Time: 1:35 AM
 * To change this template use File | Settings | File Templates.
 */

class UserAdminIdentity extends CUserIdentity{
    /**
     * Authenticates a user.
     * The example implementation makes sure if the username and password
     * are both 'demo'.
     * In practical applications, this should be changed to authenticate
     * against some persistent user identity storage (e.g. database).
     * @return boolean whether authentication succeeds.
     */
    public function authenticate()
    {
        $users = $this->getUserList();

        if(empty($users)) {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
            return !$this->errorCode;
        }

        if(!isset($users[$this->username])){
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        }
        else if($users[$this->username] !== md5($this->password)){
            $this->errorCode=self::ERROR_PASSWORD_INVALID;
        }
        else {
            $this->errorCode = self::ERROR_NONE;
        }

        return !$this->errorCode;
    }


    private function getUserList()
    {
        $users = Yii::app()->db->createCommand()
            ->select('mu.id,mu.login, mu.password')
            ->from('admin_users mu')
            ->queryAll();

        $retArrayUsers    = array();
        foreach($users as $user)
        {
            $retArrayUsers[$user['login']] = $user['password'];
        }

        return $retArrayUsers;
    }

}