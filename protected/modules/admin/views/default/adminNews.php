<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Edgar
 * Date: 12/16/13
 * Time: 10:57 PM
 * To change this template use File | Settings | File Templates.
 */

$this->menu=array(
    array('label'=>'Add News', 'url'=> Yii::app()->request->baseUrl . '/admin/addNews'),
    array('label'=>'Admin', 'url'=> Yii::app()->request->baseUrl . '/admin/admin'),

);
?>

<div ng-controller="news_admin_controller" ng-init="init();">

    <table border="1">
        <thead>
        <th>ID</th>
        <th> Title </th>
        <th> X </th>
        <th> E </th>
        </thead>
        <tbody>
        <tr ng-repeat="item in news_items">
            <td> {{ item.id }}</td>
            <td> {{ item.title }}</td>
            <td>  <img  src="<?php echo Yii::app()->request->baseUrl; ?>/images/delete.png" width='15' height='15' ng-click="deleteItem($index ,item.id);" /></td>
            <td>  <img  src="<?php echo Yii::app()->request->baseUrl; ?>/images/edit.png" width='15' height='15' ng-click="editItem(item.id);" /></td>
        </tr>
        </tbody>

    </table>

    <div style="display: none" id="url_helper">
        <?php echo CHtml::Link("",Yii::app()->createUrl( 'admin/default/editNews' ));?>
    </div>

</div>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-controllers/admin_news.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-models/services.js"></script>



