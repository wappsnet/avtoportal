<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Edgar
 * Date: 9/14/13
 * Time: 12:30 AM
 * To change this template use File | Settings | File Templates.
 */

$this->menu=array(
    array('label'=>'Admin', 'url'=> Yii::app()->request->baseUrl . '/admin/admin'),
    array('label'=>'Admin Items', 'url'=> Yii::app()->request->baseUrl . '/admin/adminItems'),
);

?>



<div ng-controller="items_controller" ng-init="init();">
<input type="hidden" id="item_type" value="<?php echo $type; ?>" >



<div ng-repeat="items in carItems">
    {{ items.header }}
    <table border="1" ng-show="items.model == 0">
        <thead>
            <th> Name English </th>
            <th> Name Armenian </th>
            <th> Name Russian </th>
            <th> Name Georgian </th>
            <th> X </th>
            <th> E </th>
        </thead>
        <tbody>
        <tr ng-repeat="item in items" ng-show="item.name_eng">
           <td> {{ item.name_eng }}</td>
            <td> {{ item.name_arm }}</td>
            <td> {{ item.name_rus }}</td>
            <td> {{ item.name_geo }}</td>
            <td>  <img  src="<?php echo Yii::app()->request->baseUrl; ?>/images/delete.png" width='15' height='15' ng-click="deleteItem($index , items , item.id, items.is_service);" /></td>
            <td>  <img  src="<?php echo Yii::app()->request->baseUrl; ?>/images/edit.png" width='15' height='15' ng-click="editItem(item.id, items.is_service);" /></td>
        </tr>
        </tbody>

    </table>

    <table border="1" ng-show="items.model == 1">
        <thead>
        <th> Name English </th>
        <th> Name Armenian </th>
        <th> Name Russian </th>
        <th> Name Georgian </th>
        <th> Mark </th>
        <th> X </th>
        <th> E </th>
        </thead>
        <tbody>
        <tr ng-repeat="item in items" ng-show="item.name_eng">
            <td> {{ item.name_eng }}</td>
            <td> {{ item.name_arm }}</td>
            <td> {{ item.name_rus }}</td>
            <td> {{ item.name_geo }}</td>
            <td> {{ item.make }}</td>
            <td>  <img  src="<?php echo Yii::app()->request->baseUrl; ?>/images/delete.png" width='15' height='15' ng-click="deleteItem($index , items , item.id, items.is_service);" /></td>
            <td>  <img  src="<?php echo Yii::app()->request->baseUrl; ?>/images/edit.png" width='15' height='15' ng-click="editItem(item.id, items.is_service);" /></td>
        </tr>
        </tbody>

    </table>

    <br/>
    <a href="{{ items.link }}" >Add</a>
</div>

    <div style="display: none" id="url_helper">
        <?php echo CHtml::Link("",Yii::app()->request->baseUrl . '/admin/lookupEdit');?>
    </div>

    <div style="display: none" id="url_helper_service">
        <?php echo CHtml::Link("",Yii::app()->request->baseUrl . '/admin/editAutoService');?>
    </div>

</div>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-controllers/admin_items_controller.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-models/services.js"></script>