<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Edgar
 * Date: 2/24/14
 * Time: 12:15 AM
 * To change this template use File | Settings | File Templates.
 */

$this->menu=array(
    array('label'=>'Admin', 'url'=> Yii::app()->request->baseUrl . '/admin/admin'),
);
?>

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'lookup',
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
    'enableAjaxValidation'=>true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
        'validateOnChange' => true,
        'validateOnType' => true
    ),
)); ?>

<div>

    <?php echo $form->labelEx($model , 'name_arm'); ?>
    <?php echo $form->textField($model, 'name_arm',array('style'=>'width:200px;clear:both;')); ?>
    <?php echo $form->error($model,'name_arm'); ?>

    <br/>

    <?php echo $form->labelEx($model , 'name_eng'); ?>
    <?php echo $form->textField($model, 'name_eng',array('style'=>'width:200px;clear:both !important;')); ?>
    <?php echo $form->error($model,'name_eng'); ?>
    <br/>

    <?php echo $form->labelEx($model , 'name_geo'); ?>
    <?php echo $form->textField($model, 'name_geo',array('style'=>'width:200px;clear:both;')); ?>
    <?php echo $form->error($model,'name_geo'); ?>
    <br/>

    <?php echo $form->labelEx($model , 'name_rus'); ?>
    <?php echo $form->textField($model, 'name_rus',array('style'=>'width:200px;clear:both;')); ?>
    <?php echo $form->error($model,'name_rus'); ?>
    <br/>

    <?php echo $form->labelEx($model , 'rate'); ?>
    <?php echo $form->textField($model, 'rate',array('style'=>'width:200px;clear:both;')); ?>
    <?php echo $form->error($model,'rate'); ?>
    <br/>

    <?php echo $form->labelEx($model , 'downpayment'); ?>
    <?php echo $form->textField($model, 'downpayment',array('style'=>'width:200px;clear:both;')); ?>
    <?php echo $form->error($model,'downpayment'); ?>
    <br/>

</div>


<br/> <br/>

<?php echo CHtml::submitButton('Submit'); ?>

<?php $this->endWidget(); ?>
