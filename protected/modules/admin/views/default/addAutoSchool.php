<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Edgar
 * Date: 8/16/13
 * Time: 5:57 PM
 * To change this template use File | Settings | File Templates.
 */?>

<?php
$this->menu=array(
    array('label'=>'Admin', 'url'=> Yii::app()->request->baseUrl . '/admin/admin'),

);
?>
<script src="<?php echo Yii::app()->baseUrl.'/ckeditor/ckeditor.js'; ?>"></script>

<div style="display: none" id="item_id"> <?php if(!$model->isNewRecord)  {echo $model->id;} else echo null;  ?>  </div>
<div style="display: none" id="location_type"> 2 </div>

<div ng-controller="google_controller" ng-init="init();">
    <div class="row">

        <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'banner',
            'htmlOptions' => array('enctype' => 'multipart/form-data'),
            'enableAjaxValidation'=>true,
            'clientOptions' => array(
                'validateOnSubmit' => true,
                'validateOnChange' => true,
                'validateOnType' => true
            ),
        )); ?>


        <?php echo $form->labelEx($model,'title'); ?>
        <?php echo $form->textField($model, 'title',array('style'=>'width:800px')); ?>
        <?php echo $form->error($model,'title'); ?>

        <br/> <br/>

        <?php echo $form->textArea($model, 'text', array('id'=>'editor1')); ?>
        <?php echo $form->error($model,'text'); ?>
        <?php echo $form->hiddenField($model,'google_markers',array('id'=>'google_markers')); ?>



        <br/> <br/>

        <?php echo CHtml::submitButton('Submit'); ?>


        <?php $this->endWidget(); ?>
    </div>

    <div style="width: auto; display:inline;">

        <div style="float:left;width: 700px ; ">

            <div id="panel">
                <input id="address" type="textbox" ng-model="currentAddress">
                <textarea ng-model="currentInfoWindow" style="display:block;"  ck-editor>
                </textarea>

                <input type="button" value="Geocode" ng-click="codeAddress();">

                <input type="button" value="Edit_Info" ng-click="editMarkerInfo();" ng-disabled="!editMode">
            </div>
            <google-map center="center" draggable="true" zoom="zoom" markers="markers"
                        class="angular-google-map"  style="height: 400px;width: 600px;"></google-map>
        </div>

        <div style="float:left;width: 300px ;">
            <table class="gridtable">
                <thead>
                <tr>
                    <th style="width: 20px;">
                        X
                    </th>
                    <th style="width: 20px;">
                        E
                    </th>
                    <th>
                        Address
                    </th>
                    <th>
                        Info Window
                    </th>
                </tr>
                </thead>
                <tbody>
                <tr ng-repeat="marker in markers">
                    <td>
                        <img  src="<?php echo Yii::app()->request->baseUrl; ?>/images/delete.png" width='15' height='15' ng-click="deleteMarker($index);" />
                    </td>
                    <td>
                        <img  src="<?php echo Yii::app()->request->baseUrl; ?>/images/edit.png" width='15' height='15' ng-click="prepareEditMarkerInfo($index);" />
                    </td>
                    <td>
                        {{ marker.addressName }}
                    </td>
                    <td>
                        {{ marker.infoWindow }}
                    </td>

                </tr>
                </tbody>
            </table>

        </div>
    </div>

    <script type="text/javascript">
        CKEDITOR.replace( 'editor1', {
            width:'1000px',
            height:'500px',
            enterMode	 : Number(2),
            filebrowserBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=files',
            filebrowserImageBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=images',
            filebrowserFlashBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=flash',
            filebrowserUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=files',
            filebrowserImageUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=images',
            filebrowserFlashUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=flash'
        });
    </script>
</div>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-controllers/google-marker-controller.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-models/services.js"></script>

