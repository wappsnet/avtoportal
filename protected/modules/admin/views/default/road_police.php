<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Edgar
 * Date: 12/1/13
 * Time: 10:59 PM
 * To change this template use File | Settings | File Templates.
 */?>

<?php
$this->menu=array(
    array('label'=>'Addresses', 'url'=> Yii::app()->request->baseUrl . '/admin/roadPoliceAddresses'),
    array('label'=>'Penalty', 'url'=> Yii::app()->request->baseUrl . '/admin/roadPolicePenalty'),
    array('label'=>'RoadSigns', 'url'=> Yii::app()->request->baseUrl . '/admin/roadSigns'),
    array('label'=>'Traffic Rules', 'url'=> Yii::app()->request->baseUrl . '/admin/trafficRules'),


    array('label'=>'Admin', 'url'=> Yii::app()->request->baseUrl . '/admin/admin'),


);
?>