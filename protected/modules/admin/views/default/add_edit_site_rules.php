<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Edgar
 * Date: 2/22/14
 * Time: 5:43 PM
 * To change this template use File | Settings | File Templates.
 */

?>

<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Edgar
 * Date: 8/16/13
 * Time: 11:30 PM
 * To change this template use File | Settings | File Templates.
 */
?>

<?php
$this->menu=array(
    array('label'=>'Admin', 'url'=> Yii::app()->request->baseUrl . '/admin/admin'),
);
?>

<br>

<script src="<?php echo Yii::app()->baseUrl.'/ckeditor/ckeditor.js'; ?>"></script>

<div style="display: none" id="item_id"> <?php if(!$model->isNewRecord)  {echo $model->id;} else echo null;  ?>  </div>

<div >
    <div class="row">

        <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'banner',
            'htmlOptions' => array('enctype' => 'multipart/form-data'),
            'enableAjaxValidation'=>true,
            'clientOptions' => array(
                'validateOnSubmit' => true,
                'validateOnChange' => true,
                'validateOnType' => true
            ),
        )); ?>



        <?php echo $form->textArea($model, 'text', array('id'=>'editor1')); ?>
        <?php echo $form->error($model,'text'); ?>

        <br/> <br/>

        <?php echo CHtml::submitButton('Submit'); ?>


        <?php $this->endWidget(); ?>
    </div>


    <script type="text/javascript">
        CKEDITOR.replace( 'editor1', {
            width:'1000px',
            height:'500px',
            enterMode	 : Number(2),
            filebrowserBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=files',
            filebrowserImageBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=images',
            filebrowserFlashBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=flash',
            filebrowserUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=files',
            filebrowserImageUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=images',
            filebrowserFlashUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=flash'
        });
    </script>
</div>



