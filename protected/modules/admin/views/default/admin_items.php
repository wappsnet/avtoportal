<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Edgar
 * Date: 11/6/13
 * Time: 10:47 PM
 * To change this template use File | Settings | File Templates.
 */

$this->menu=array(
    array('label'=>'Admin', 'url'=> Yii::app()->request->baseUrl . '/admin/admin'),
    array('label'=>'Edit Car Items', 'url'=> Yii::app()->request->baseUrl . '/admin/editCarItems'),
    array('label'=>'Edit Car Mark Items', 'url'=> Yii::app()->request->baseUrl . '/admin/editCarMarkItems'),
    array('label'=>'Edit Car Model Items', 'url'=> Yii::app()->request->baseUrl . '/admin/editCarModelItems'),
    array('label'=>'Edit Wheel Items', 'url'=> Yii::app()->request->baseUrl . '/admin/editWheelItems'),
    array('label'=>'Edit Spare Part Items', 'url'=> Yii::app()->request->baseUrl . '/admin/editSparePartItems'),
    array('label'=>'Edit Auto Service Items', 'url'=> Yii::app()->request->baseUrl . '/admin/editAutoServiceItems'),
    array('label'=>'Edit Rural Items', 'url'=> Yii::app()->request->baseUrl . '/admin/editRuralTechItems'),
    array('label'=>'Edit Shipping Items', 'url'=> Yii::app()->request->baseUrl . '/admin/editShippingItems'),
    array('label'=>'Edit Transportation Items', 'url'=> Yii::app()->request->baseUrl . '/admin/editTransportationItems'),
    array('label'=>'Edit Tourism Items', 'url'=> Yii::app()->request->baseUrl . '/admin/editTourismItems'),
    array('label'=>'Edit Evacuator Items', 'url'=> Yii::app()->request->baseUrl . '/admin/editEvacuatorItems'),
    array('label'=>'Edit Taxi Items', 'url'=> Yii::app()->request->baseUrl . '/admin/editTaxiItems'),
    array('label'=>'Edit Training Car Items', 'url'=> Yii::app()->request->baseUrl . '/admin/editTrainingCarItems'),
    array('label'=>'Edit Job Items', 'url'=> Yii::app()->request->baseUrl . '/admin/editJobItems'),
);

?>

