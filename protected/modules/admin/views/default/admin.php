<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Edgar
 * Date: 6/29/13
 * Time: 9:15 PM
 * To change this template use File | Settings | File Templates.
 */?>

<?php
    $this->menu=array(
        array('label'=>'Admin', 'url'=> Yii::app()->request->baseUrl . '/admin/admin'),
        array('label'=>'Admin news', 'url'=> Yii::app()->request->baseUrl . '/admin/adminNews'),
        array('label'=>'Admin appa', 'url'=> Yii::app()->request->baseUrl . '/admin/adminAppa'),
        array('label'=>'Edit Auto school', 'url'=> Yii::app()->request->baseUrl . '/admin/editAutoSchool'),
        array('label'=>'Edit TechObserve', 'url'=> Yii::app()->request->baseUrl . '/admin/editTechObserve'),
        array('label'=>'Tax Service', 'url'=> Yii::app()->request->baseUrl . '/admin/editTaxService'),
        array('label'=>'Admin Items', 'url'=> Yii::app()->request->baseUrl . '/admin/adminItems'),
        array('label'=>'Admin Locations', 'url'=> Yii::app()->request->baseUrl . '/admin/adminLocation'),
        array('label'=>'Add/Edit Banner', 'url'=> Yii::app()->request->baseUrl . '/admin/addBanner'),
        array('label'=>'Admin Model Groups', 'url'=> Yii::app()->request->baseUrl . '/admin/adminModelGroups'),
        array('label'=>'Road Police', 'url'=> Yii::app()->request->baseUrl . '/admin/roadPolice'),
        array('label'=>'Site Rules', 'url'=> Yii::app()->request->baseUrl . '/admin/editSiteRules'),
        array('label'=>'Edit Bank Data', 'url'=> Yii::app()->request->baseUrl . '/admin/editBank'),
        array('label'=>'User Control', 'url'=> Yii::app()->request->baseUrl . '/admin/userControl'),




        array('label'=>'Logout', 'url'=> Yii::app()->request->baseUrl . '/admin/logout'),
    );
?>

