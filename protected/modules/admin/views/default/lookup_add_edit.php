<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Edgar
 * Date: 9/18/13
 * Time: 1:32 AM
 * To change this template use File | Settings | File Templates.
 */
?>
 <input type="hidden" value="<?php echo $one_lang; ?>" id="one_lang">
<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'lookup',
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
    'enableAjaxValidation'=>true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
        'validateOnChange' => true,
        'validateOnType' => true
    ),
)); ?>

<?php if(isset($model_groups)  && count($model_groups) > 0):?>


    <?php $groupItems = CHtml::listData($model_groups, 'id', 'name'); ?>

    <?php echo $form->labelEx($model,'group_id'); ?>
    <?php echo $form->dropDownList($model, 'group_id', $groupItems, array('empty' => 'no group' , 'style'=>'width:200px')); ?>
    <?php echo $form->error($model,'group_id'); ?>

    <br/> <br/>

<?php endif; ?>



<?php if(isset($model->upLevelItems)  && count($model->upLevelItems) > 0):?>


<?php $fatherItems = CHtml::listData($model->upLevelItems, 'id', 'name_eng'); ?>

<?php echo $form->labelEx($model,'father_id'); ?>
<?php echo $form->dropDownList($model, 'father_id', $fatherItems, array('style'=>'width:200px')); ?>
<?php echo $form->error($model,'father_id'); ?>

    <br/> <br/>

<?php endif; ?>

<div id="multi_lang_div" style="display: none">

    <?php echo $form->labelEx($model,'name_arm'); ?>
    <?php echo $form->textField($model, 'name_arm',array('style'=>'width:300px', 'id' => 'name_arm')); ?>
    <?php echo $form->error($model,'name_arm'); ?>

        <br/> <br/>

    <?php echo $form->labelEx($model,'name_eng'); ?>
    <?php echo $form->textField($model, 'name_eng',array('style'=>'width:300px' , 'id' => 'name_eng')); ?>
    <?php echo $form->error($model,'name_eng'); ?>

        <br/> <br/>

    <?php echo $form->labelEx($model,'name_rus'); ?>
    <?php echo $form->textField($model, 'name_rus',array('style'=>'width:300px', 'id' => 'name_rus')); ?>
    <?php echo $form->error($model,'name_rus'); ?>

        <br/> <br/>


    <?php echo $form->labelEx($model,'name_geo'); ?>
    <?php echo $form->textField($model, 'name_geo',array('style'=>'width:300px' , 'id' => 'name_geo')); ?>
    <?php echo $form->error($model,'name_geo'); ?>

</div>

<div id="one_lang_div" style="display: none">
    name <input type="text" onkeyup="fillAllFields(this);" id="name_unv">
</div>

    <br/> <br/>

<?php echo CHtml::submitButton('Submit'); ?>

<?php $this->endWidget(); ?>

<script type="text/javascript">
    $(document).ready(function(){
        var isOneLang = $('#one_lang').val();
        if(isOneLang == 1){
            $('#one_lang_div').css('display','');
            $('#name_unv').val($('#name_eng').val());
        }else{
            $('#multi_lang_div').css('display','');
        }
    });

    function fillAllFields(elem){
        $('#name_arm').val($(elem).val());
        $('#name_eng').val($(elem).val());
        $('#name_rus').val($(elem).val());
        $('#name_geo').val($(elem).val());
    }
</script>