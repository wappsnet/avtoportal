<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Edgar
 * Date: 7/22/13
 * Time: 12:24 AM
 * To change this template use File | Settings | File Templates.
 */ ?>

 <script src="<?php echo Yii::app()->baseUrl.'/ckeditor/ckeditor.js'; ?>"></script>


<div class="row">

    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'banner',
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
        'enableAjaxValidation'=>true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => true
        ),
    )); ?>


    <?php echo $form->labelEx($model,'title'); ?>
    <?php echo $form->textField($model, 'title',array('style'=>'width:800px')); ?>
    <?php echo $form->error($model,'title'); ?>

    <br/> <br/>

    <?php echo $form->textArea($model, 'text', array('id'=>'editor1')); ?>
    <?php echo $form->error($model,'text'); ?>

    <br/> <br/>

    <?php echo CHtml::submitButton('Submit'); ?>

    <?php $this->endWidget(); ?>
</div>


<script type="text/javascript">
    CKEDITOR.replace( 'editor1', {
        width:'1000px',
        height:'500px',
        enterMode	 : Number(2),
        filebrowserBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=files',
        filebrowserImageBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=images',
        filebrowserFlashBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=flash',
        filebrowserUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=files',
        filebrowserImageUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=images',
        filebrowserFlashUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=flash'
    });
</script>