<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Edgar
 * Date: 11/11/13
 * Time: 1:10 AM
 * To change this template use File | Settings | File Templates.
 */

$this->menu=array(
    array('label'=>'Admin', 'url'=> Yii::app()->request->baseUrl . '/admin/admin'),
);
?>

<div ng-controller="admin_location_controller" ng-init="init();">

    Locations
    <table border="1">
        <thead>
        <th>Name English</th>
        <th>Name Armenian</th>
        <th> Name Russian</th>
        <th> Name Georgian</th>
        <th> X </th>
        <th> E </th>
        </thead>
        <tbody>
        <tr ng-repeat="item in locationItems">
            <td> {{ item.name_eng }}</td>
            <td> {{ item.name_arm }}</td>
            <td> {{ item.name_rus }}</td>
            <td> {{ item.name_geo }}</td>
            <td>  <img  src="<?php echo Yii::app()->request->baseUrl; ?>/images/delete.png" width='15' height='15' ng-click="deleteLoc($index , item.id);" /></td>
            <td>  <img  src="<?php echo Yii::app()->request->baseUrl; ?>/images/edit.png" width='15' height='15' ng-click="editLocItem(item.id);" /></td>
        </tr>
        </tbody>

    </table>

    <br/>
    <a href="<?php echo Yii::app()->baseUrl . '/admin/addLocation'; ?>" >Add</a>
    <br/>
    Sub Locations
    <table border="1">
        <thead>
        <th>Name English</th>
        <th>Name Armenian</th>
        <th> Name Russian</th>
        <th> Name Georgian</th>
        <th> X </th>
        <th> E </th>
        </thead>
        <tbody>
        <tr ng-repeat="item in subLocationItems">
            <td> {{ item.name_eng }}</td>
            <td> {{ item.name_arm }}</td>
            <td> {{ item.name_rus }}</td>
            <td> {{ item.name_geo }}</td>
            <td>  <img  src="<?php echo Yii::app()->request->baseUrl; ?>/images/delete.png" width='15' height='15' ng-click="deleteSubLoc($index , item.id);" /></td>
            <td>  <img  src="<?php echo Yii::app()->request->baseUrl; ?>/images/edit.png" width='15' height='15' ng-click="editSubLocItem(item.id);" /></td>
        </tr>
        </tbody>

    </table>

    <br/>
    <a href="<?php echo Yii::app()->baseUrl . '/admin/addSubLocation'; ?>" >Add</a>
    <br/>

    <div style="display: none" id="url_helper_loc">
        <?php echo CHtml::Link("",Yii::app()->createUrl( 'admin/default/editLocation' ));?>
    </div>

    <div style="display: none" id="url_helper_subloc">
        <?php echo CHtml::Link("",Yii::app()->createUrl( 'admin/default/editSubLocation' ));?>
    </div>
</div>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-controllers/admin_locations.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-models/services.js"></script>

