<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Edgar
 * Date: 11/10/13
 * Time: 1:54 AM
 * To change this template use File | Settings | File Templates.
 */

$this->menu=array(
    array('label'=>'Add Appa', 'url'=> Yii::app()->request->baseUrl . '/admin/addAppa'),
    array('label'=>'Admin', 'url'=> Yii::app()->request->baseUrl . '/admin/admin'),

);
?>

<div ng-controller="appa_admin_controller" ng-init="init();">

    <table border="1">
        <thead>
        <th>Name</th>
        <th> Web Site </th>
        <th> email</th>
        <th> X </th>
        <th> E </th>
        </thead>
        <tbody>
        <tr ng-repeat="item in appa_items">
            <td> {{ item.title }}</td>
            <td> {{ item.web_site}}</td>
            <td> {{ item.email }}</td>
            <td>  <img  src="<?php echo Yii::app()->request->baseUrl; ?>/images/delete.png" width='15' height='15' ng-click="deleteItem($index ,item.id);" /></td>
            <td>  <img  src="<?php echo Yii::app()->request->baseUrl; ?>/images/edit.png" width='15' height='15' ng-click="editItem(item.id);" /></td>
        </tr>
        </tbody>

    </table>

    <div style="display: none" id="url_helper">
        <?php echo CHtml::Link("",Yii::app()->createUrl( 'admin/default/editAppa' ));?>
    </div>

</div>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-controllers/appa_admin_controller.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-models/services.js"></script>

