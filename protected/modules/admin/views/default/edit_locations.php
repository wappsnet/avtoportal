<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Edgar
 * Date: 11/11/13
 * Time: 1:47 AM
 * To change this template use File | Settings | File Templates.
 */

$this->menu=array(
    array('label'=>'Admin Locations', 'url'=> Yii::app()->request->baseUrl . '/admin/adminLocation'),
);
?>

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'lookup',
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
    'enableAjaxValidation'=>true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
        'validateOnChange' => true,
        'validateOnType' => true
    ),
)); ?>

<div>

    <?php echo $form->labelEx($model,'name_arm'); ?>
    <?php echo $form->textField($model, 'name_arm',array('style'=>'width:300px', 'id' => 'name_arm')); ?>
    <?php echo $form->error($model,'name_arm'); ?>

    <br/> <br/>

    <?php echo $form->labelEx($model,'name_eng'); ?>
    <?php echo $form->textField($model, 'name_eng',array('style'=>'width:300px' , 'id' => 'name_eng')); ?>
    <?php echo $form->error($model,'name_eng'); ?>

    <br/> <br/>

    <?php echo $form->labelEx($model,'name_rus'); ?>
    <?php echo $form->textField($model, 'name_rus',array('style'=>'width:300px', 'id' => 'name_rus')); ?>
    <?php echo $form->error($model,'name_rus'); ?>

    <br/> <br/>


    <?php echo $form->labelEx($model,'name_geo'); ?>
    <?php echo $form->textField($model, 'name_geo',array('style'=>'width:300px' , 'id' => 'name_geo')); ?>
    <?php echo $form->error($model,'name_geo'); ?>

    <br/> <br/>

    <div class="row">
        <?php echo $form->labelEx($model,'flag_name'); ?>
        <?php echo $form->fileField($model, 'flag_name'); ?>
    </div>

</div>


<br/> <br/>

<?php echo CHtml::submitButton('Submit'); ?>

<?php $this->endWidget(); ?>

