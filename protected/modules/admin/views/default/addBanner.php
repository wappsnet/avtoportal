<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Yerem
 * Date: 11/12/13
 * Time: 12:59 AM
 * To change this template use File | Settings | File Templates.
 */
?>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ajaxfileupload.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-controllers/banner_upload_controller.js"
        type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/generic.css"/>

<div class="input_row">
<div ng-controller="bannerUploadController">
<input type="hidden" value="auto" id="formType">
<input type="hidden" name="MAX_FILE_SIZE" value="250"/>

<div class="choose_file" style="text-align: center; margin:0 auto;">
    <p><b><h3>Chose image or flash and save in position you want to update</h3></b></p>
    <br>

    <p><i>allowed extentions ("jpg", "jpeg", "gif", "swf")</i></p>
    <img id="loading" src="<?php echo Yii::app()->request->baseUrl; ?>/images/loading.gif"
         style="height: 18px; display: none; ">
    <input id="qqfile" type="file" size="45" name="qqfile" class="input">
</div>

<div>
    <a ng-click="showSecondTab = false" style="text-decoration:underline; cursor: pointer;"><h3><b>Banners for 1-4 positions</b></h3></a>
    <a ng-click="showSecondTab = true" style="text-decoration:underline; cursor: pointer;"><h3><b>Banners for list</b></h3></a>
</div>
<div>


    <div ng-show="!showSecondTab">
        <!-- /*------------------------Position1-------------------------*/ -->
        <div>
            <p><b>Position 1 <i>(preview 480x93)</i></b></p>

            <div class="banner" ng-show="isPosition1Img">
                <a href="<?php echo $position1Url ?>" target="_blank" id="url_position_1">
                    <img ng-src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/banners/{{position1}}"
                         id="img_position_1" width="484px" height="93px"
                         style="display: <?php echo $isPosition1Img ? 'block' : 'none' ?>">
                </a>
            </div>
            <div class="banner" ng-show="isPosition1Img">
                <embed id="flash_position_1" quality="high" bgcolor="#ffffff" width="484px" height="93px"
                       name="mymoviename"
                       type="application/x-shockwave-flash"
                       pluginspage="http://www.macromedia.com/go/getflashplayer" src="<?php echo $position1; ?>"
                       style="display: <?php echo $isPosition1Img ? 'none' : 'block' ?>">
            </div>
            <div>
                <p><i>For image banners also fill url address to redirect onclick (example.
                        http://www.avtomeqena.am)</i></p>
                <input size="80" name="url" ng-model="urlPosition1" type="url" required>
            </div>
            <div>
                <a ng-click="uploadPosition1()" class="choose_file_btn left"
                   style="cursor: pointer"><?php echo Messages::getMessage(207) ?></a>
            </div>
        </div>
        <br><br><br>
        <!--/* ------------------------Position2------------------------- */ -->
        <div>
            <p><b>Position 2<i>(preview 206x270)</i></b></p>

            <div class="banner" ng-show="isPosition2Img">
                <a href="<?php echo $position2Url ?>" target="_blank" id="url_position_2">
                    <img ng-src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/banners/{{position2}}"
                         id="img_position_2" width="200px" height="270px"
                         style="display: <?php echo $isPosition2Img ? 'block' : 'none' ?>">
                </a>
            </div>
            <div class="banner" ng-show="isPosition2Img">
                <embed id="flash_position_2" quality="high" bgcolor="#ffffff" width="200px" height="270px"
                       name="mymoviename"
                       type="application/x-shockwave-flash"
                       pluginspage="http://www.macromedia.com/go/getflashplayer" src="<?php echo $position2; ?>"
                       style="display: <?php echo $isPosition2Img ? 'none' : 'block' ?>">
            </div>
            <div>
                <p><i>For image banners also fill url address to redirect onclick (example.
                        http://www.avtomeqena.am)</i></p>
                <input size="80" name="url" ng-model="urlPosition2" type="url" required>
            </div>
            <div>
                <a ng-click="uploadPosition2()" class="choose_file_btn left"
                   style="cursor: pointer"><?php echo Messages::getMessage(207) ?></a>
            </div>
        </div>

        <br><br><br>
        <!--/* ------------------------Position3------------------------- */ -->
        <div>
            <p><b>Position 3<i>(preview)</i></b></p>

            <div class="banner" ng-show="isPosition3Img">
                <a href="<?php echo $position3Url ?>" target="_blank" id="url_position_3">
                    <img ng-src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/banners/{{position3}}"
                         id="img_position_3" width="200px" height="270px"
                         style="display: <?php echo $isPosition3Img ? 'block' : 'none' ?>">
                </a>
            </div>
            <div class="banner" ng-show="isPosition3Img">
                <embed id="flash_position_3" quality="high" bgcolor="#ffffff" width="200px" height="270px"
                       name="mymoviename"
                       type="application/x-shockwave-flash"
                       pluginspage="http://www.macromedia.com/go/getflashplayer" src="<?php echo $position3; ?>"
                       style="display: <?php echo $isPosition3Img ? 'none' : 'block' ?>">
            </div>
            <div>
                <p><i>For image banners also fill url address to redirect onclick (example.
                        http://www.avtomeqena.am)</i></p>
                <input size="80" name="url" ng-model="urlPosition3" type="url" required>
            </div>
            <div>
                <a ng-click="uploadPosition3()" class="choose_file_btn left"
                   style="cursor: pointer"><?php echo Messages::getMessage(207) ?></a>
            </div>
        </div>
        <br><br><br>
        <!--/* ------------------------Position4------------------------- */ -->
        <div>
            <p><b>Position 4<i>(preview)</i></b></p>

            <div class="banner" ng-show="isPosition4Img">
                <a href="<?php echo $position4Url ?>" target="_blank" id="url_position_4">
                    <img ng-src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/banners/{{position4}}"
                         id="img_position_4" width="200px" height="270px"
                         style="display: <?php echo $isPosition4Img ? 'block' : 'none' ?>">
                </a>
            </div>
            <div class="banner" ng-show="isPosition4Img">
                <embed id="flash_position_4" quality="high" bgcolor="#ffffff" width="200px" height="270px"
                       name="mymoviename"
                       type="application/x-shockwave-flash"
                       pluginspage="http://www.macromedia.com/go/getflashplayer" src="<?php echo $position4; ?>"
                       style="display: <?php echo $isPosition4Img ? 'none' : 'block' ?>">
            </div>
            <div>
                <p><i>For image banners also fill url address to redirect onclick (example.
                        http://www.avtomeqena.am)</i></p>
                <input size="80" name="url" ng-model="urlPosition4" type="url" required>
            </div>
            <div>
                <a ng-click="uploadPosition4()" class="choose_file_btn left"
                   style="cursor: pointer"><?php echo Messages::getMessage(207) ?></a>
            </div>
        </div>

    </div>


    <div ng-show="showSecondTab">

        <br><br><br>
        <!--/* ------------------------Position5------------------------- */ -->
        <div>
            <p><b>Position 5<i>(preview 540x110)</i></b></p>

            <div class="banner" ng-show="isPosition5Img">
                <a href="<?php echo $position5Url ?>" target="_blank" id="url_position_5">
                    <img ng-src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/banners/{{position5}}"
                         id="img_position_5" width="540px" height="110px"
                         style="display: <?php echo $isPosition5Img ? 'block' : 'none' ?>">
                </a>
            </div>
            <div class="banner" ng-show="isPosition5Img">
                <embed id="flash_position_5" quality="high" bgcolor="#ffffff" width="540px" height="110px"
                       name="mymoviename"
                       type="application/x-shockwave-flash"
                       pluginspage="http://www.macromedia.com/go/getflashplayer" src="<?php echo $position5; ?>"
                       style="display: <?php echo $isPosition5Img ? 'none' : 'block' ?>">
            </div>
            <div>
                <p><i>For image banners also fill url address to redirect onclick (example.
                        http://www.avtomeqena.am)</i></p>
                <input size="80" name="url" ng-model="urlPosition5" type="url" required>
            </div>
            <div>
                <a ng-click="uploadPosition5()" class="choose_file_btn left"
                   style="cursor: pointer"><?php echo Messages::getMessage(207) ?></a>
            </div>
        </div>

        <br><br><br>
        <!--/* ------------------------Position6------------------------- */ -->
        <div>
            <p><b>Position 6<i>(preview 540x110)</i></b></p>

            <div class="banner" ng-show="isPosition6Img">
                <a href="<?php echo $position6Url ?>" target="_blank" id="url_position_6">
                    <img ng-src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/banners/{{position6}}"
                         id="img_position_6" width="540px" height="110px"
                         style="display: <?php echo $isPosition6Img ? 'block' : 'none' ?>">
                </a>
            </div>
            <div class="banner" ng-show="isPosition6Img">
                <embed id="flash_position_6" quality="high" bgcolor="#ffffff" width="540px" height="110px"
                       name="mymoviename"
                       type="application/x-shockwave-flash"
                       pluginspage="http://www.macromedia.com/go/getflashplayer" src="<?php echo $position6; ?>"
                       style="display: <?php echo $isPosition6Img ? 'none' : 'block' ?>">
            </div>
            <div>
                <p><i>For image banners also fill url address to redirect onclick (example.
                        http://www.avtomeqena.am)</i></p>
                <input size="80" name="url" ng-model="urlPosition6" type="url" required>
            </div>
            <div>
                <a ng-click="uploadPosition6()" class="choose_file_btn left"
                   style="cursor: pointer"><?php echo Messages::getMessage(207) ?></a>
            </div>
        </div>

    </div>


</div>
</div>
</div>
