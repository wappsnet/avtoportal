
<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Edgar
 * Date: 11/11/13
 * Time: 1:10 AM
 * To change this template use File | Settings | File Templates.
 */

$this->menu=array(
    array('label'=>'Admin', 'url'=> Yii::app()->request->baseUrl . '/admin/admin'),
);
?>


<div ng-controller="user_control_controller" ng-init="init();">


    <table border="1">
        <thead>
        <th>User Name</th>
        <th>Email</th>
        <th>Dealer</th>
        <th>phone number</th>
        <th> X </th>
        </thead>
        <tbody>
        <tr ng-repeat="item in userItems">
            <td> {{ item.user_name }}</td>
            <td> {{ item.email }}</td>
            <td ng-show="item.user_roles_id == 2"> Universal </td>
            <td ng-show="item.user_roles_id == 3"> Official</td>
            <td ng-show="item.user_roles_id == 1"> </td>
            <td>{{ item.phone_number }}</td>
            <td>  <img  src="<?php echo Yii::app()->request->baseUrl; ?>/images/delete.png" width='15' height='15' ng-click="deleteUser($index , item.id);" /></td>
        </tr>
        </tbody>

    </table>

</div>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-controllers/admin_user_control.js"></script>

