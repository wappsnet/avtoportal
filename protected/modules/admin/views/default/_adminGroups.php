<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Edgar
 * Date: 12/8/13
 * Time: 2:01 AM
 * To change this template use File | Settings | File Templates.
 */

$this->menu=array(
    array('label'=>'Add Model Group', 'url'=> Yii::app()->request->baseUrl . '/admin/addModelGroup'),
    array('label'=>'Admin', 'url'=> Yii::app()->request->baseUrl . '/admin/admin'),

);

?>

<div ng-controller="auto_model_groups" ng-init="init()">
    <table border="1">
        <thead>
        <th>Name</th>
        <th> X </th>
        <th> E </th>
        </thead>
        <tbody>
        <tr ng-repeat="item in model_groups">
            <td style="padding-right: 10px;padding-left: 10px"> {{ item.name }}</td>
            <td>  <img  src="<?php echo Yii::app()->request->baseUrl; ?>/images/delete.png" width='15' height='15' ng-click="deleteItem($index ,item.id);" /></td>
            <td>  <img  src="<?php echo Yii::app()->request->baseUrl; ?>/images/edit.png" width='15' height='15' ng-click="editItem(item.id);" /></td>
        </tr>
        </tbody>

    </table>

    <div style="display: none" id="url_helper">
        <?php echo CHtml::Link("",Yii::app()->createUrl( 'admin/default/editModelGroup' ));?>
    </div>
</div>


<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-controllers/admin_model_groups.js"></script>

