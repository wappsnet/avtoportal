<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Edgar
 * Date: 12/14/13
 * Time: 12:22 AM
 * To change this template use File | Settings | File Templates.
 */
?>

<?php
$this->menu=array(
    array('label'=>'Road Police', 'url'=> Yii::app()->request->baseUrl . '/admin/roadPolice'),
    array('label'=>'Add Road Sign', 'url'=> Yii::app()->request->baseUrl . '/admin/addRoadSign'),

    array('label'=>'Admin', 'url'=> Yii::app()->request->baseUrl . '/admin/admin'),

);
?>

<div ng-controller="admin_road_signs_controller" ng-init="init()">
    <table border="1">
        <thead>
        <th>link</th>
        <th> X </th>
        <th> E </th>
        </thead>
        <tbody>
        <tr ng-repeat="item in roadSignItems">
            <td style="padding-right: 10px;padding-left: 10px"> {{ item.link }}</td>
            <td>  <img  src="<?php echo Yii::app()->request->baseUrl; ?>/images/delete.png" width='15' height='15' ng-click="deleteItem($index ,item.id);" /></td>
            <td>  <img  src="<?php echo Yii::app()->request->baseUrl; ?>/images/edit.png" width='15' height='15' ng-click="editItem(item.id);" /></td>
        </tr>
        </tbody>

    </table>

    <div style="display: none" id="url_helper">
        <?php echo CHtml::Link("",Yii::app()->createUrl( 'admin/editRoadSign' ));?>
    </div>
</div>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-controllers/admin_road_signs.js"></script>

