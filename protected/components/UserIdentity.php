<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
    const ERROR_NOT_ACTIVATED_YET=150;

    public $userId;
    public $admin;

    public function getId(){
        return   $this->userId;
    }

	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
        $record = User::model()->findByAttributes(array('email'=>$this->username));
		if($record === null){
			$this->errorCode=self::ERROR_USERNAME_INVALID;
        }
		elseif($record->password !== UserMailService::encryptIt($this->password)){
			$this->errorCode=self::ERROR_PASSWORD_INVALID;
        }elseif($record->user_status == 0){
            $this->errorCode=self::ERROR_NOT_ACTIVATED_YET;
        }
		else{
            $this->userId = $record->id;
			$this->errorCode=self::ERROR_NONE;
        }
		return !$this->errorCode;
	}
}