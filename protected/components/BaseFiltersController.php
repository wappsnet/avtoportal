<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Yerem
 * Date: 10/19/13
 * Time: 12:58 AM
 * To change this template use File | Settings | File Templates.
 */

abstract class BaseFiltersController extends Controller
{

    const CONDITION_AND = 1;
    const CONDITION_OR = 2;



    public function init(){
        parent::init();
    }

    public abstract function getModelByID($id);
    public abstract function actionGetAllFilters();

    public function getDBCriteriaByCondition($addedFilters){
        $criteria = New CDbCriteria();
        $conditionAND = array();
        $conditionOR = array();
        $condition = "";
        if($addedFilters){
            foreach($addedFilters as $filter){
                if($filter->filter_type == BaseFiltersController::CONDITION_AND){
                    $conditionAND[] = $filter->filter_condition;
                }else if($filter->filter_type == BaseFiltersController::CONDITION_OR){
                    $conditionOR[] = $filter->filter_condition;
                }
            }
        }
        $conditionAND = implode(' AND ', $conditionAND);
        $conditionOR = implode(' OR ', $conditionOR);
        if($conditionAND && $conditionOR){
            $condition = $conditionAND ." AND (" . $conditionOR . ")";
        }else if($conditionAND){
            $condition = $conditionAND;
        }else if($conditionOR){
            $condition = $conditionOR;
        }

        if($this->isAdmin()){
           if($condition){
               $condition = "(".$condition.") AND t.isAccepted = 0";
           }else{
               $condition = "t.isAccepted = 0";
           }
       }else{
           if($condition){
               $condition = "(".$condition.") AND t.isAccepted = 1";
           }else{
               $condition = "t.isAccepted = 1";
           }
       }
        $criteria->order = "FLOOR((t.is_top+1)*3/12) DESC, t.created DESC";

       $criteria->condition = $condition;
       $criteria->join = "JOIN user ON user.id = t.user_id";

       return $criteria;
    }


    public function actionAcceptAndRefresh(){
        if($this->actionAccept()){
            $this->actionGetAllFilters();
        }else{
            if($error=Yii::app()->errorHandler->error)
            {
                if(Yii::app()->request->isAjaxRequest)
                    echo $error['message'];
                else
                    $this->render('error', $error);
            }
        }
    }

    public function actionAccept(){
        $id = json_decode($_POST['id']);
        $model = $this->getModelByID($id);
        $model->isAccepted = true;
        return $model->save();
    }

    public function actionDeleteAndRefresh(){

        if($this->actionDelete()){
            $this->actionGetAllFilters();
        }else{
            if($error=Yii::app()->errorHandler->error)
            {
                if(Yii::app()->request->isAjaxRequest)
                    echo $error['message'];
                else
                    $this->render('error', $error);
            }
        }
    }

    public function actionDelete(){
        $id = json_decode($_POST['id']);
        $model = $this->getModelByID($id);
        return $model->delete();
    }

}