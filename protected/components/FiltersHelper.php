<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Yerem
 * Date: 7/11/13
 * Time: 10:53 PM
 * To change this template use File | Settings | File Templates.
 */

class FiltersHelper
{
    public static function getCriteriaByFilters($filters){
        $criteria = New CDbCriteria();
        $condition = array();
        foreach ($filters as $filter){
            $filter_type = $filter->filter_type;
            $property_name = $filter->property_name;
            switch ($filter_type) {
                case 0:
                    if(isset($condition[$property_name])){
                        $condition[$property_name] = "(".$condition[$property_name]. " OR ".FiltersHelper::getEqualCriteria($property_name, $filter->value). ")";
                    }else{
                        $condition[$property_name] = FiltersHelper::getEqualCriteria($property_name, $filter->value);
                    }
                    break;
                case 1:
                    $condition[$property_name] = FiltersHelper::getLessCriteria($property_name, $filter->value);
                    break;
                case 2:
                    $condition[$property_name] = FiltersHelper::getMoreCriteria($property_name, $filter->value);
                    break;
                case 3:
                    if(isset($condition[$property_name])){
                        $condition[$property_name] = "(".$condition[$property_name]. " OR ".FiltersHelper::getBetweenCriteria($property_name, $filter->_value, $filter->end_value) . ")";
                    }else{
                        $condition[$property_name] = FiltersHelper::getBetweenCriteria($property_name, $filter->start_value, $filter->end_value);
                    }
                    break;
                case 4:
                    if($filter->value == 2){
                        $condition['user_roles_id'] = 'user.user_roles_id = 2 || user.user_roles_id = 3 ';
                    }else if($filter->value == 1){
                        $condition['user_roles_id'] = 'user.user_roles_id = 1';
                    }

            }
        }



        $criteria->condition = implode(' AND ', $condition);
        $criteria ->join = "JOIN user ON user.id = t.user_id";
        return $criteria;
    }

    /*public static function generateORConditions($conditionArray){
        foreach($conditionArray as $condition){
            $condition.
        }
    }*/

    public static function getEqualCriteria($propertyName, $value){
        return $propertyName." = ".$value;
    }

    public static function getLessCriteria($propertyName, $value){
        return $propertyName." <= ".$value;
    }

    public static function getMoreCriteria($propertyName, $value){
        return $propertyName." >= ".$value;
    }

    public static function getBetweenCriteria($propertyName, $start_value, $end_value){
        return $propertyName." >= ".$start_value . " AND " . $propertyName . " <= " . $end_value;
    }


}