<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Yerem
 * Date: 9/22/13
 * Time: 6:57 PM
 * To change this template use File | Settings | File Templates.
 */

class DAOFilters
{

    public static function getLocations($condition = null, $functionalTable)
    {
        return Yii::app()->db->createCommand()
            ->select("c_location.name_" . Messages::getLanguageId() . " as name, count(c_location.id) as count, CONCAT('t.c_location_id =', c_location.id) as filter_condition")
            ->from("c_location")
            ->join($functionalTable." as t", "t.c_location_id = c_location.id")
            ->join("user", "user.id = t.user_id")
            ->join("user_phone", "user.phone1_id = user_phone.id ")
            ->leftJoin("unregistered_users", "unregistered_users.id = t.unregistered_user_id")
            ->where($condition)
            ->group("c_location.id")
            ->queryAll();
    }

    public static function getSubLocations($condition = null, $functionalTable)
    {
        return Yii::app()->db->createCommand()
            ->select("c_sub_location.name_" . Messages::getLanguageId() . " as name, count(c_sub_location.id) as count, CONCAT('t.c_sub_location_id =', c_sub_location.id) as filter_condition")
            ->from("c_sub_location")
            ->join($functionalTable." as t", "t.c_sub_location_id = c_sub_location.id")
            ->join("user", "user.id = t.user_id")
            ->join("user_phone", "user.phone1_id = user_phone.id ")
            ->leftJoin("unregistered_users", "unregistered_users.id = t.unregistered_user_id")
            ->where($condition)
            ->group("c_sub_location.id")
            ->queryAll();
    }

    public static function getCarPrices($condition = null, $functionalTable)
    {
        return Yii::app()->db->createCommand()
            ->select("price_filters.name_" . Messages::getLanguageId() . " as name, count(price_filters.id) as count, CONCAT(CONCAT('t.price > ', price_filters.start), ' AND ', CONCAT('t.price <= ', price_filters.end)) as filter_condition")
            ->from("price_filters, " . $functionalTable." as t")
            ->join("user", "user.id = t.user_id")
            ->join("user_phone", "user.phone1_id = user_phone.id ")
            ->leftJoin("unregistered_users", "unregistered_users.id = t.unregistered_user_id")
            ->where("t.price > price_filters.start and t.price <= price_filters.end " . ((is_null($condition) || $condition == "") ? "" : (" AND (" . $condition . ")")))
            ->group("price_filters.name_".Messages::getLanguageId())
            ->queryAll();
    }

    public static function getSalaryPrices($condition = null, $functionalTable)
    {
        return Yii::app()->db->createCommand()
            ->select("salary_price_filters.name_" . Messages::getLanguageId() . " as name, count(salary_price_filters.id) as count, CONCAT(CONCAT('t.price > ', salary_price_filters.start), ' AND ', CONCAT('t.price <= ', salary_price_filters.end)) as filter_condition")
            ->from("salary_price_filters, " . $functionalTable." as t")
            ->join("user", "user.id = t.user_id")
            ->where("t.price > salary_price_filters.start and t.price <= salary_price_filters.end " . ((is_null($condition) || $condition == "") ? "" : (" AND (" . $condition . ")")))
            ->group("salary_price_filters.name_".Messages::getLanguageId())
            ->queryAll();
    }

    public static function getTaxiMinimalPrices($condition = null, $functionalTable)
    {
        return Yii::app()->db->createCommand()
            ->select("taxi_price_filters.name_" . Messages::getLanguageId() . " as name, count(taxi_price_filters.id) as count, CONCAT(CONCAT('t.price >= ', taxi_price_filters.start), ' AND ', CONCAT('t.price <= ', taxi_price_filters.end)) as filter_condition")
            ->from("taxi_price_filters, " . $functionalTable." as t")
            ->join("user", "user.id = t.user_id")
            ->where("t.price >= taxi_price_filters.start and t.price <= taxi_price_filters.end " . ((is_null($condition) || $condition == "") ? "" : (" AND (" . $condition . ")")))
            ->group("taxi_price_filters.name_".Messages::getLanguageId())
            ->queryAll();
    }

    public static function getEvacuatorPrices($condition = null, $functionalTable)
    {
        return Yii::app()->db->createCommand()
            ->select("evacuator_price_filters.name_" . Messages::getLanguageId() . " as name, count(evacuator_price_filters.id) as count, CONCAT(CONCAT('t.price >= ', evacuator_price_filters.start), ' AND ', CONCAT('t.price <= ', evacuator_price_filters.end)) as filter_condition")
            ->from("evacuator_price_filters, " . $functionalTable." as t")
            ->join("user", "user.id = t.user_id")
            ->where("t.price >= evacuator_price_filters.start and t.price <= evacuator_price_filters.end " . ((is_null($condition) || $condition == "") ? "" : (" AND (" . $condition . ")")))
            ->group("evacuator_price_filters.name_".Messages::getLanguageId())
            ->queryAll();
    }

    public static function getMarks($condition = null, $functionalTable)
    {
        return Yii::app()->db->createCommand()
            ->select("count(t.id) count, c_lookup_property.name_" . Messages::getLanguageId() . " name, CONCAT('t.mark_id =', c_lookup_property.id) as filter_condition")
            ->from($functionalTable." as t")
            ->join("c_lookup_property", "t.mark_id = c_lookup_property.id")
            ->join("user", "user.id = t.user_id")
            ->join("user_phone", "user.phone1_id = user_phone.id ")
            ->leftJoin("unregistered_users", "unregistered_users.id = t.unregistered_user_id")
            ->where($condition)
            ->group("t.mark_id")
            ->order('name')
            ->queryAll();
    }

    public static function getCurrentMarkCount($markID) {
        $result =  Yii::app()->db->createCommand()
            ->select("count(car_announcement.mark_id) as count")
            ->from("car_announcement")
            ->where("mark_id = " . $markID . " AND isAccepted = '1'")
            ->queryAll();
        return $result[0]["count"];
    }

    public static function getCarCount($mark) {
        $result =  Yii::app()->db->createCommand()
            ->select("count(car_announcement.mark_id) as count")
            ->from("car_announcement")
            ->where($mark)
            ->queryAll();
        return $result[0]["count"];
    }

    public static function getRentCount($mark) {
        $result =  Yii::app()->db->createCommand()
            ->select("count(car_rent_announcement.mark_id) as count")
            ->from("car_rent_announcement")
            ->where($mark)
            ->queryAll();
        return $result[0]["count"];
    }

    public static function getSparepartCount($mark) {
        $result =  Yii::app()->db->createCommand()
            ->select("count(spare_part_announcement.mark_id) as count")
            ->from("spare_part_announcement")
            ->where($mark)
            ->queryAll();
        return $result[0]["count"];
    }

    public static function getRuralCount($mark) {
        $result =  Yii::app()->db->createCommand()
            ->select("count(ruraltech_announcement.mark_id) as count")
            ->from("ruraltech_announcement")
            ->where($mark)
            ->queryAll();
        return $result[0]["count"];
    }

    public static function getTaxiCount($mark) {
        $result =  Yii::app()->db->createCommand()
            ->select("count(taxi_announcement.mark_id) as count")
            ->from("taxi_announcement")
            ->where($mark)
            ->queryAll();
        return $result[0]["count"];
    }
    public static function getEvacuatorCount($mark) {
        $result =  Yii::app()->db->createCommand()
            ->select("count(evacuator_announcement.mark_id) as count")
            ->from("evacuator_announcement")
            ->where($mark)
            ->queryAll();
        return $result[0]["count"];
    }

    public static function getEvacuatorDistricts($condition = null, $functionalTable)
    {
        return Yii::app()->db->createCommand()
            ->select("count(t.id) count, c_lookup_property.name_" . Messages::getLanguageId() . " name, CONCAT('t.district =', c_lookup_property.id) as filter_condition")
            ->from($functionalTable." as t")
            ->join("c_lookup_property", "t.district = c_lookup_property.id")
            ->join("user", "user.id = t.user_id")
            ->where($condition)
            ->group("t.district")
            ->queryAll();
    }

    public static function getEvacuatorCity($condition = null, $functionalTable)
    {
        return Yii::app()->db->createCommand()
            ->select("count(t.id) count, c_lookup_property.name_" . Messages::getLanguageId() . " name, CONCAT('t.city_block =', c_lookup_property.id) as filter_condition")
            ->from($functionalTable." as t")
            ->join("c_lookup_property", "t.city_block = c_lookup_property.id")
            ->join("user", "user.id = t.user_id")
            ->where($condition)
            ->group("t.city_block")
            ->queryAll();
    }

    public static function getModels($condition = null, $functionalTable)
    {
        return Yii::app()->db->createCommand()
            ->select("count(t.id) count, c_lookup_property.name_" . Messages::getLanguageId() . " name, CONCAT('t.model_id =', c_lookup_property.id) as filter_condition")
            ->from($functionalTable." as t")
            ->join("c_lookup_property", "t.model_id = c_lookup_property.id")
            ->join("user", "user.id = t.user_id")
            ->join("user_phone", "user.phone1_id = user_phone.id ")
            ->leftJoin("unregistered_users", "unregistered_users.id = t.unregistered_user_id")
            ->where($condition)
            ->group("t.model_id")
            ->order('name')
            ->queryAll();
    }

    public static function getProductions($condition = null, $functionalTable)
    {
        return Yii::app()->db->createCommand()
            ->select("count(t.id) count, c_lookup_property.name_" . Messages::getLanguageId() . " name, CONCAT('t.production_id =', c_lookup_property.id) as filter_condition")
            ->from($functionalTable." as t")
            ->join("c_lookup_property", "t.production_id = c_lookup_property.id")
            ->join("user", "user.id = t.user_id")
            ->where($condition)
            ->group("t.production_id")
            ->queryAll();
    }

    public static function getUserRoles($condition = null, $functionalTable)
    {
        return Yii::app()->db->createCommand()
            ->select("count(t.id) count, user_roles.name_" . Messages::getLanguageId() . " name, case
    when (user_roles.client_id = 1) then CONCAT('user.user_roles_id =', 1, ' OR user.user_roles_id =', 4)
    else CONCAT('user.user_roles_id =', user.user_roles_id)
  end as filter_condition")
            ->from($functionalTable." as t")
            ->join("user", "user.id = t.user_id")
            ->join("user_roles", "user.user_roles_id = user_roles.id")
            ->join("user_phone", "user.phone1_id = user_phone.id ")
            ->leftJoin("unregistered_users", "unregistered_users.id = t.unregistered_user_id")
            ->where($condition)
            ->group("user_roles.client_id")
            ->queryAll();
    }

    public static function getSparePartTypes($condition = null, $functionalTable)
    {
        return Yii::app()->db->createCommand()
            ->select("count(t.id) count, c_lookup_property.name_" . Messages::getLanguageId() . " name, CONCAT('t.type_id =', c_lookup_property.id) as filter_condition")
            ->from($functionalTable." as t")
            ->join("c_lookup_property", "t.type_id = c_lookup_property.id")
            ->join("user", "user.id = t.user_id")
            ->where($condition)
            ->group("t.type_id")
            ->queryAll();
    }

    public static function getSparePartSubTypes($condition = null, $functionalTable)
    {
        return Yii::app()->db->createCommand()
            ->select("count(t.id) count, c_lookup_property.name_" . Messages::getLanguageId() . " name, CONCAT('t.subtype_id =', c_lookup_property.id) as filter_condition")
            ->from($functionalTable." as t")
            ->join("c_lookup_property", "t.subtype_id = c_lookup_property.id")
            ->join("user", "user.id = t.user_id")
            ->where($condition)
            ->group("t.subtype_id")
            ->queryAll();
    }

    public static function getBodyTypes($condition = null, $functionalTable)
    {
        return Yii::app()->db->createCommand()
            ->select("count(t.id) count, c_lookup_property.name_" . Messages::getLanguageId() . " name, CONCAT('t.body_style_id =', c_lookup_property.id) as filter_condition")
            ->from($functionalTable." as t")
            ->join("c_lookup_property", "t.body_style_id = c_lookup_property.id")
            ->join("user", "user.id = t.user_id")
            ->join("user_phone", "user.phone1_id = user_phone.id ")
            ->leftJoin("unregistered_users", "unregistered_users.id = t.unregistered_user_id")
            ->where($condition)
            ->group("t.body_style_id")
            ->queryAll();
    }

    public static function getShippingBodyTypes($condition = null, $functionalTable)
    {
        return Yii::app()->db->createCommand()
            ->select("count(t.id) count, c_lookup_property.name_" . Messages::getLanguageId() . " name, CONCAT('t.body_id =', c_lookup_property.id) as filter_condition")
            ->from($functionalTable." as t")
            ->join("c_lookup_property", "t.body_id = c_lookup_property.id")
            ->join("user", "user.id = t.user_id")
            ->where($condition)
            ->group("t.body_id")
            ->queryAll();
    }

    public static function getShippingMarks($condition = null, $functionalTable)
    {
        return Yii::app()->db->createCommand()
            ->select("count(t.id) count, c_lookup_property.name_" . Messages::getLanguageId() . " name, CONCAT('t.mark_id =', c_lookup_property.id) as filter_condition")
            ->from($functionalTable." as t")
            ->join("c_lookup_property", "t.mark_id = c_lookup_property.id")
            ->join("user", "user.id = t.user_id")
            ->where($condition)
            ->group("t.mark_id")
            ->queryAll();
    }

    public static function getShippingTechnics($condition = null, $functionalTable)
    {
        return Yii::app()->db->createCommand()
            ->select("count(t.id) count, c_lookup_property.name_" . Messages::getLanguageId() . " name, CONCAT('t.techique_id =', c_lookup_property.id) as filter_condition")
            ->from($functionalTable." as t")
            ->join("c_lookup_property", "t.techique_id = c_lookup_property.id")
            ->join("user", "user.id = t.user_id")
            ->where($condition)
            ->group("t.techique_id")
            ->queryAll();
    }




    public static function getShippingJobTypes($condition = null, $functionalTable)
    {
        return Yii::app()->db->createCommand()
            ->select("count(t.id) count, c_lookup_property.name_" . Messages::getLanguageId() . " name, CONCAT('t.job_type_id =', c_lookup_property.id) as filter_condition")
            ->from($functionalTable." as t")
            ->join("c_lookup_property", "t.job_type_id = c_lookup_property.id")
            ->join("user", "user.id = t.user_id")
            ->where($condition)
            ->group("t.job_type_id")
            ->queryAll();
    }

    public static function getCarYears($condition = null, $functionalTable)
    {
        return Yii::app()->db->createCommand()
            ->select("count(t.id) count, c_lookup_property.name_" . Messages::getLanguageId() . " name, CONCAT('t.year =', c_lookup_property.name_eng) as filter_condition")
            ->from($functionalTable." as t")
            ->join("c_lookup_property", "t.year = c_lookup_property.name_eng")
            ->join("user", "user.id = t.user_id")
            ->join("user_phone", "user.phone1_id = user_phone.id ")
            ->leftJoin("unregistered_users", "unregistered_users.id = t.unregistered_user_id")
            ->where($condition)
            ->group("t.year")
            ->queryAll();
    }

    public static function getTransportType($condition = null, $functionalTable)
    {
        return Yii::app()->db->createCommand()
            ->select("count(t.id) count, c_lookup_property.name_" . Messages::getLanguageId() . " name, CONCAT('t.transport_type =', c_lookup_property.id) as filter_condition")
            ->from($functionalTable." as t")
            ->join("c_lookup_property", "t.transport_type = c_lookup_property.id")
            ->join("user", "user.id = t.user_id")
            ->where($condition)
            ->group("t.transport_type")
            ->queryAll();
    }

    public static function getTransportCountry($condition = null, $functionalTable)
    {
        return Yii::app()->db->createCommand()
            ->select("count(t.id) count, c_lookup_property.name_" . Messages::getLanguageId() . " name, CONCAT('t.coutry =', c_lookup_property.id) as filter_condition")
            ->from($functionalTable." as t")
            ->join("c_lookup_property", "t.coutry = c_lookup_property.id")
            ->join("user", "user.id = t.user_id")
            ->where($condition)
            ->group("t.coutry")
            ->queryAll();
    }

    public static function getTransportCity($condition = null, $functionalTable)
    {
        return Yii::app()->db->createCommand()
            ->select("count(t.id) count, c_lookup_property.name_" . Messages::getLanguageId() . " name, CONCAT('t.place =', c_lookup_property.id) as filter_condition")
            ->from($functionalTable." as t")
            ->join("c_lookup_property", "t.place = c_lookup_property.id")
            ->join("user", "user.id = t.user_id")
            ->where($condition)
            ->group("t.place")
            ->queryAll();
    }

    public static function getTourismPlaces($condition = null, $functionalTable)
    {
        return Yii::app()->db->createCommand()
            ->select("count(t.id) count, c_lookup_property.name_" . Messages::getLanguageId() . " name, CONCAT('t.place =', c_lookup_property.id) as filter_condition")
            ->from($functionalTable." as t")
            ->join("c_lookup_property", "t.place = c_lookup_property.id")
            ->join("user", "user.id = t.user_id")
            ->where($condition)
            ->group("t.place")
            ->queryAll();
    }

    public static function getTourismTransportType($condition = null, $functionalTable)
    {
        return Yii::app()->db->createCommand()
            ->select("count(t.id) count, c_lookup_property.name_" . Messages::getLanguageId() . " name, CONCAT('t.transport_type =', c_lookup_property.id) as filter_condition")
            ->from($functionalTable." as t")
            ->join("c_lookup_property", "t.transport_type = c_lookup_property.id")
            ->join("user", "user.id = t.user_id")
            ->where($condition)
            ->group("t.transport_type")
            ->queryAll();
    }

    public static function getTourismVendors($condition = null, $functionalTable)
    {
        return Yii::app()->db->createCommand()
            ->select("count(t.id) count, c_lookup_property.name_" . Messages::getLanguageId() . " name, CONCAT('t.vendor =', c_lookup_property.id) as filter_condition")
            ->from($functionalTable." as t")
            ->join("c_lookup_property", "t.vendor = c_lookup_property.id")
            ->join("user", "user.id = t.user_id")
            ->where($condition)
            ->group("t.vendor")
            ->queryAll();
    }

    public static function getTourismProvided($condition = null, $functionalTable)
    {
        return Yii::app()->db->createCommand()
            ->select("count(t.id) count, c_lookup_property.name_" . Messages::getLanguageId() . " name, CONCAT('t.with_driver =', c_lookup_property.id) as filter_condition")
            ->from($functionalTable." as t")
            ->join("c_lookup_property", "t.with_driver = c_lookup_property.id")
            ->join("user", "user.id = t.user_id")
            ->where($condition)
            ->group("t.with_driver")
            ->queryAll();
    }

    public static function getTourismGuide($condition = null, $functionalTable)
    {
        return Yii::app()->db->createCommand()
            ->select("count(t.id) count, (CASE WHEN t.with_guide=0 THEN '".Messages::getMessage(7)."' ELSE '".Messages::getMessage(6)."' END) as name, CONCAT('t.with_guide =', t.with_guide) as filter_condition")
            ->from($functionalTable." as t")
            ->join("user", "user.id = t.user_id")
            ->where($condition)
            ->group("t.with_guide")
            ->queryAll();
    }


    public static function getRuralTechnics($condition = null, $functionalTable)
    {
        return Yii::app()->db->createCommand()
            ->select("count(t.id) count, c_lookup_property.name_" . Messages::getLanguageId() . " name, CONCAT('t.technic_id =', c_lookup_property.id) as filter_condition")
            ->from($functionalTable." as t")
            ->join("c_lookup_property", "t.technic_id = c_lookup_property.id")
            ->join("user", "user.id = t.user_id")
            ->where($condition)
            ->group("t.technic_id")
            ->queryAll();
    }

    public static function getRuralWorkType($condition = null, $functionalTable)
    {
        return Yii::app()->db->createCommand()
            ->select("count(t.id) count, c_lookup_property.name_" . Messages::getLanguageId() . " name, CONCAT('t.work_type_id =', c_lookup_property.id) as filter_condition")
            ->from($functionalTable." as t")
            ->join("c_lookup_property", "t.work_type_id = c_lookup_property.id")
            ->join("user", "user.id = t.user_id")
            ->where($condition)
            ->group("t.work_type_id")
            ->queryAll();
    }

    public static function getCustomClearances($condition = null, $functionalTable)
    {
        return Yii::app()->db->createCommand()
            ->select("count(t.id) count, c_lookup_property.name_" . Messages::getLanguageId() . " name, CONCAT('t.tax_id =', c_lookup_property.id) as filter_condition")
            ->from($functionalTable." as t")
            ->join("c_lookup_property", "t.tax_id = c_lookup_property.id")
            ->join("user", "user.id = t.user_id")
            ->join("user_phone", "user.phone1_id = user_phone.id ")
            ->leftJoin("unregistered_users", "unregistered_users.id = t.unregistered_user_id")
            ->where($condition)
            ->group("t.tax_id")
            ->queryAll();
    }


    public static function getMileages($condition = null, $functionalTable)
    {
        return Yii::app()->db->createCommand()
            ->select("mileage_filters.name_" . Messages::getLanguageId() . " as name, count(mileage_filters.id) as count, CONCAT(CONCAT('t.mileage > ', mileage_filters.start), ' AND ', CONCAT('t.mileage <= ', mileage_filters.end)) as filter_condition")
            ->from("mileage_filters, " . $functionalTable." as t")
            ->join("user", "user.id = t.user_id")
            ->join("user_phone", "user.phone1_id = user_phone.id ")
            ->leftJoin("unregistered_users", "unregistered_users.id = t.unregistered_user_id")
            ->where("t.mileage > mileage_filters.start and t.mileage <= mileage_filters.end " . ((is_null($condition) || $condition == "") ? "" : (" AND (" . $condition . ")")))
            ->group("mileage_filters.name_".Messages::getLanguageId())
            ->queryAll();
    }

    public static function getGearBoxes($condition = null, $functionalTable)
    {
        return Yii::app()->db->createCommand()
            ->select("count(t.id) count, c_lookup_property.name_" . Messages::getLanguageId() . " name, CONCAT('t.gearbox_id =', c_lookup_property.id) as filter_condition")
            ->from($functionalTable." as t")
            ->join("c_lookup_property", "t.gearbox_id = c_lookup_property.id")
            ->join("user", "user.id = t.user_id")
            ->join("user_phone", "user.phone1_id = user_phone.id ")
            ->leftJoin("unregistered_users", "unregistered_users.id = t.unregistered_user_id")
            ->where($condition)
            ->group("t.gearbox_id")
            ->queryAll();
    }


    public static function getEngines($condition = null, $functionalTable)
    {
        return Yii::app()->db->createCommand()
            ->select("count(t.id) count, c_lookup_property.name_" . Messages::getLanguageId() . " name, CONCAT('t.engine_id =', c_lookup_property.id) as filter_condition")
            ->from($functionalTable." as t")
            ->join("c_lookup_property", "t.engine_id = c_lookup_property.id")
            ->join("user", "user.id = t.user_id")
            ->join("user_phone", "user.phone1_id = user_phone.id ")
            ->leftJoin("unregistered_users", "unregistered_users.id = t.unregistered_user_id")
            ->where($condition)
            ->group("t.engine_id")
            ->queryAll();
    }

    public static function getEnginePowers($condition = null, $functionalTable)
    {
        return Yii::app()->db->createCommand()
            ->select("car_power_filters.name_" . Messages::getLanguageId() . " as name, count(car_power_filters.id) as count, CONCAT(CONCAT('t.engine_power > ', car_power_filters.start), ' AND ', CONCAT('t.engine_power <= ', car_power_filters.end)) as filter_condition")
            ->from("car_power_filters, " . $functionalTable." as t")
            ->join("user", "user.id = t.user_id")
            ->join("user_phone", "user.phone1_id = user_phone.id ")
            ->leftJoin("unregistered_users", "unregistered_users.id = t.unregistered_user_id")
            ->where("t.engine_power > car_power_filters.start and t.engine_power <= car_power_filters.end " . ((is_null($condition) || $condition == "") ? "" : (" AND (" . $condition . ")")))
            ->group("car_power_filters.name_".Messages::getLanguageId())
            ->queryAll();
    }

    public static function getByExchange($condition = null, $functionalTable)
    {
        return Yii::app()->db->createCommand()
            ->select("count(t.id) count, (CASE WHEN t.byExchange=0 THEN '".Messages::getMessage(7)."' ELSE '".Messages::getMessage(6)."' END) as name, CONCAT('t.byExchange =', t.byExchange) as filter_condition")
            ->from($functionalTable." as t")
            ->join("user", "user.id = t.user_id")
            ->join("user_phone", "user.phone1_id = user_phone.id ")
            ->leftJoin("unregistered_users", "unregistered_users.id = t.unregistered_user_id")
            ->where("t.byExchange = 1" . ((is_null($condition) || $condition == "") ? "" : (" AND (" . $condition . ")")))
            ->group("t.byExchange")
            ->queryAll();
    }

    public static function getColors($condition = null, $functionalTable)
    {
        return Yii::app()->db->createCommand()
            ->select("count(t.id) count, c_lookup_property.name_" . Messages::getLanguageId() . " name, CONCAT('t.color_id =', c_lookup_property.id) as filter_condition")
            ->from($functionalTable." as t")
            ->join("c_lookup_property", "t.color_id = c_lookup_property.id")
            ->join("user", "user.id = t.user_id")
            ->join("user_phone", "user.phone1_id = user_phone.id ")
            ->leftJoin("unregistered_users", "unregistered_users.id = t.unregistered_user_id")
            ->where($condition)
            ->group("t.color_id")
            ->queryAll();
    }

    public static function getCarRudders($condition, $FUNCTIONAL_TABLE_NAME)
    {
        return Yii::app()->db->createCommand()
            ->select("count(t.id) count, c_lookup_property.name_" . Messages::getLanguageId() . " name, CONCAT('t.rudder_id =', c_lookup_property.id) as filter_condition")
            ->from($FUNCTIONAL_TABLE_NAME." as t")
            ->join("c_lookup_property", "t.rudder_id = c_lookup_property.id")
            ->join("user", "user.id = t.user_id")
            ->join("user_phone", "user.phone1_id = user_phone.id ")
            ->leftJoin("unregistered_users", "unregistered_users.id = t.unregistered_user_id")
            ->where($condition)
            ->group("t.rudder_id")
            ->queryAll();
    }

    public static function getEngineVolumes($condition = null, $functionalTable)
    {
        return Yii::app()->db->createCommand()
            ->select("car_engine_volume_filters.name_" . Messages::getLanguageId() . " as name, count(car_engine_volume_filters.id) as count, CONCAT(CONCAT('t.engine_volume > ', car_engine_volume_filters.start), ' AND ', CONCAT('t.engine_volume <= ', car_engine_volume_filters.end)) as filter_condition")
            ->from("car_engine_volume_filters, " . $functionalTable." as t")
            ->join("user", "user.id = t.user_id")
            ->join("user_phone", "user.phone1_id = user_phone.id ")
            ->leftJoin("unregistered_users", "unregistered_users.id = t.unregistered_user_id")
            ->where("t.engine_volume > car_engine_volume_filters.start and t.engine_volume <= car_engine_volume_filters.end " . ((is_null($condition) || $condition == "") ? "" : (" AND (" . $condition . ")")))
            ->group("car_engine_volume_filters.name_".Messages::getLanguageId())
            ->queryAll();
    }

    public static function getJobTypes($condition = null, $functionalTable)
    {
        return Yii::app()->db->createCommand()
            ->select("count(t.id) count, c_lookup_property.name_" . Messages::getLanguageId() . " name, CONCAT('t.job_type =', c_lookup_property.id) as filter_condition")
            ->from($functionalTable." as t")
            ->join("c_lookup_property", "t.job_type = c_lookup_property.id")
            ->join("user", "user.id = t.user_id")
            ->where($condition)
            ->group("t.job_type")
            ->queryAll();
    }

    public static function getJobSort($condition = null, $functionalTable)
    {
        return Yii::app()->db->createCommand()
            ->select("count(t.id) count, c_lookup_property.name_" . Messages::getLanguageId() . " name, CONCAT('t.job_sort =', c_lookup_property.id) as filter_condition")
            ->from($functionalTable." as t")
            ->join("c_lookup_property", "t.job_sort = c_lookup_property.id")
            ->join("user", "user.id = t.user_id")
            ->where($condition)
            ->group("t.job_sort")
            ->queryAll();
    }

    public static function getTaxiDistricts($condition = null, $functionalTable)
    {
        return Yii::app()->db->createCommand()
            ->select("count(t.id) count, c_lookup_property.name_" . Messages::getLanguageId() . " name, CONCAT('t.district_id =', c_lookup_property.id) as filter_condition")
            ->from($functionalTable." as t")
            ->join("c_lookup_property", "t.district_id = c_lookup_property.id")
            ->join("user", "user.id = t.user_id")
            ->where($condition)
            ->group("t.district_id")
            ->queryAll();
    }

    public static function getTaxiCity($condition = null, $functionalTable)
    {
        return Yii::app()->db->createCommand()
            ->select("count(t.id) count, c_lookup_property.name_" . Messages::getLanguageId() . " name, CONCAT('t.city_block =', c_lookup_property.id) as filter_condition")
            ->from($functionalTable." as t")
            ->join("c_lookup_property", "t.city_block = c_lookup_property.id")
            ->join("user", "user.id = t.user_id")
            ->where($condition)
            ->group("t.city_block")
            ->queryAll();
    }

    public static function getTaxiPerKMPrices($condition = null, $functionalTable)
    {
        return Yii::app()->db->createCommand()
            ->select("count(t.id) count, c_lookup_property.name_" . Messages::getLanguageId() . " name, CONCAT('t.price_per_km =', c_lookup_property.id) as filter_condition")
            ->from($functionalTable." as t")
            ->join("c_lookup_property", "t.price_per_km = c_lookup_property.id")
            ->join("user", "user.id = t.user_id")
            ->where($condition)
            ->group("t.price_per_km")
            ->queryAll();
    }

    public static function getSparePartCondition($condition = null, $functionalTable)
    {
        return Yii::app()->db->createCommand()
            ->select("count(t.id) count, c_lookup_property.name_" . Messages::getLanguageId() . " name, CONCAT('t.state =', c_lookup_property.id) as filter_condition")
            ->from($functionalTable." as t")
            ->join("c_lookup_property", "t.state = c_lookup_property.id")
            ->join("user", "user.id = t.user_id")
            ->where($condition)
            ->group("t.state")
            ->queryAll();
    }

    public static function getEvacuatorPerKMPrices($condition = null, $functionalTable)
    {
        return Yii::app()->db->createCommand()
            ->select("count(t.id) count, c_lookup_property.name_" . Messages::getLanguageId() . " name, CONCAT('t.payment_type =', c_lookup_property.id) as filter_condition")
            ->from($functionalTable." as t")
            ->join("c_lookup_property", "t.payment_type = c_lookup_property.id")
            ->join("user", "user.id = t.user_id")
            ->where($condition)
            ->group("t.payment_type")
            ->queryAll();
    }

    public static function getWheelType($condition = null, $functionalTable)
    {
        return Yii::app()->db->createCommand()
            ->select("count(t.id) count, c_lookup_property.name_" . Messages::getLanguageId() . " name, CONCAT('t.spare_part_id =', c_lookup_property.id) as filter_condition")
            ->from($functionalTable." as t")
            ->join("c_lookup_property", "t.spare_part_id = c_lookup_property.id")
            ->join("user", "user.id = t.user_id")
            ->where($condition)
            ->group("t.spare_part_id")
            ->queryAll();
    }

    public static function getWheelSubType($condition = null, $functionalTable)
    {
        return Yii::app()->db->createCommand()
            ->select("count(t.id) count, c_lookup_property.name_" . Messages::getLanguageId() . " name, CONCAT('t.type_id =', c_lookup_property.id) as filter_condition")
            ->from($functionalTable." as t")
            ->join("c_lookup_property", "t.type_id = c_lookup_property.id")
            ->join("user", "user.id = t.user_id")
            ->where($condition)
            ->group("t.type_id")
            ->queryAll();
    }

    public static function getWheelInches($condition = null, $functionalTable)
    {
        return Yii::app()->db->createCommand()
            ->select("count(t.id) count, c_lookup_property.name_" . Messages::getLanguageId() . " name, CONCAT('t.inches_id =', c_lookup_property.id) as filter_condition")
            ->from($functionalTable." as t")
            ->join("c_lookup_property", "t.inches_id = c_lookup_property.id")
            ->join("user", "user.id = t.user_id")
            ->where($condition)
            ->group("t.inches_id")
            ->queryAll();
    }

    public static function getWheelWidth($condition = null, $functionalTable)
    {
        return Yii::app()->db->createCommand()
            ->select("count(t.id) count, c_lookup_property.name_" . Messages::getLanguageId() . " name, CONCAT('t.width_id =', c_lookup_property.id) as filter_condition")
            ->from($functionalTable." as t")
            ->join("c_lookup_property", "t.width_id = c_lookup_property.id")
            ->join("user", "user.id = t.user_id")
            ->where($condition)
            ->group("t.width_id")
            ->queryAll();
    }

    public static function getWheelHeight($condition = null, $functionalTable)
    {
        return Yii::app()->db->createCommand()
            ->select("count(t.id) count, c_lookup_property.name_" . Messages::getLanguageId() . " name, CONCAT('t.height_id =', c_lookup_property.id) as filter_condition")
            ->from($functionalTable." as t")
            ->join("c_lookup_property", "t.height_id = c_lookup_property.id")
            ->join("user", "user.id = t.user_id")
            ->where($condition)
            ->group("t.height_id")
            ->queryAll();
    }

    public static function getWheelCondition($condition = null, $functionalTable)
    {
        return Yii::app()->db->createCommand()
            ->select("count(t.id) count, c_lookup_property.name_" . Messages::getLanguageId() . " name, CONCAT('t.state =', c_lookup_property.id) as filter_condition")
            ->from($functionalTable." as t")
            ->join("c_lookup_property", "t.state = c_lookup_property.id")
            ->join("user", "user.id = t.user_id")
            ->where($condition)
            ->group("t.state")
            ->queryAll();
    }

    public static function getJobPlaces($condition = null, $functionalTable)
    {
        return Yii::app()->db->createCommand()
            ->select("count(t.id) count, c_lookup_property.name_" . Messages::getLanguageId() . " name, CONCAT('t.place =', c_lookup_property.id) as filter_condition")
            ->from($functionalTable." as t")
            ->join("c_lookup_property", "t.place = c_lookup_property.id")
            ->join("user", "user.id = t.user_id")
            ->where($condition)
            ->group("t.place")
            ->queryAll();
    }

    public static function getJobs($condition = null, $functionalTable)
    {
        return Yii::app()->db->createCommand()
            ->select("count(t.id) count, c_lookup_property.name_" . Messages::getLanguageId() . " name, CONCAT('t.job_name =', c_lookup_property.id) as filter_condition")
            ->from($functionalTable." as t")
            ->join("c_lookup_property", "t.job_name = c_lookup_property.id")
            ->join("user", "user.id = t.user_id")
            ->where($condition)
            ->group("t.job_name")
            ->queryAll();
    }


}