<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Yerem
 * Date: 6/11/13
 * Time: 12:40 AM
 * To change this template use File | Settings | File Templates.
 */

class Messages extends CApplicationComponent
{
    private static $INITIALIZED = '-55';

    function _construct()
    {
    }

    public function init()
    {
        if ( !Yii::app()->cache->get(Messages::$INITIALIZED)) {
            $messages = Yii::app()->db->createCommand()->select('*')->from('c_messages')->queryAll();
            foreach ($messages as $message) {
                Yii::app()->cache->set($message['id'], array('eng' => $message['name_eng'], 'rus' => $message['name_rus'],
                    'geo' => $message['name_geo'], 'arm' => $message['name_arm']));
            }
            Yii::app()->cache->set(Messages::$INITIALIZED, true);
        }
    }

    public static function getMessage($messageId)
    {
        $lang = Messages::getLanguageId();
        $messageArr = Yii::app()->cache->get($messageId);
        if (is_null($messageArr[$lang])) {
            $messageArr = Messages::loadMessageById($messageId);
            Yii::app()->cache->set($messageId, array('eng' => $messageArr['name_eng'], 'rus' => $messageArr['name_rus'],
                'geo' => $messageArr['name_geo'], 'arm' => $messageArr['name_arm']));
        }
        $mess = Yii::app()->cache->get($messageId);
        return $mess[$lang];
    }

    public static function getMessageFullLang($messageId)
    {
        $lang = Messages::getLanguageId();
        $messageArr = Yii::app()->cache->get($messageId);
        if (is_null($messageArr[$lang])) {
            $messageArr = Messages::loadMessageById($messageId);
            Yii::app()->cache->set($messageId, array('eng' => $messageArr['name_eng'], 'rus' => $messageArr['name_rus'],
                'geo' => $messageArr['name_geo'], 'arm' => $messageArr['name_arm']));
        }
        $mess = Yii::app()->cache->get($messageId);
        return $mess;
    }


    public static function getLanguageId()
    {
        if (!isset(Yii::app()->session['languageId'])) {
            Yii::app()->session['languageId'] = 'arm';
        }
        return Yii::app()->session['languageId'];
    }

    private static function loadMessageById($id)
    {
        $message = Yii::app()->db->createCommand()->select('*')->from('c_messages')->
            where('id=' . $id)->queryAll();
        return $message[0];
    }
}