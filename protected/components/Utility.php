<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Edgar
 * Date: 5/11/14
 * Time: 6:26 PM
 * To change this template use File | Settings | File Templates.
 */

class Utility {
    private static  function getProperOrderString($prop_id){

        $prop_id_index = (isset($prop_id['id'])) ? $prop_id['id'] : null;
        if($prop_id == 212){
            $order_query = 'order_id ASC';
        }
        else if($prop_id == 5 || $prop_id_index == 200){
            $order_query = 'name_eng DESC';
        }
        else{
            $order_query = 'name ASC';
        }
        return $order_query;
    }

    public  static function checkIfChild($prop_id , $group_id){
        $children = Yii::app()->db->createCommand()->select('child_id , c_property_group_id')->from('c_property cp')
            ->where('c_property_group_id = :gr_id', array('gr_id' => $group_id ))
            ->query()->readAll();

        foreach ($children as $child) {
            if($child != null && $child['child_id'] != null && $child['child_id'] != ""){
                if(strstr($child['child_id'] , $prop_id)){
                    return true;
                }
            }
        }

        return false;
    }

    public static  function getLookupByID($prop_id , $group_id){

        if(Utility::checkIfChild($prop_id, $group_id)){//if property have father  return null
            return null;
        }
        $order_query = Utility::getProperOrderString($prop_id, $group_id);

        $lookup_items = Yii::app()->db->createCommand()->select('id , c_property_id , name_'.Messages::getLanguageId().' name,
        order_id , father_id ')->from('c_lookup_property')->where('c_property_id = :pr_id',
                array('pr_id' => $prop_id ))->order($order_query)->query()->readAll();


        return $lookup_items;
    }

    public static  function getLookupByIDForAnswers($prop_id , $group_id , $lookup_id){

        if(Utility::checkIfChild($prop_id, $group_id)){
        //if property have father return only selected item (becouse on chnage event children items will be loaded ,  this is for correct answer setting)
            if($lookup_id  != null || $lookup_id != ""){
                $lookup_items = Yii::app()->db->createCommand()->select('id , c_property_id , name_'.Messages::getLanguageId().' name,order_id , father_id ')
                    ->from('c_lookup_property')->where('id = :lk_id',array('lk_id' => $lookup_id ))->query()->readAll();
            }
        }
        else{
            $order_query = Utility::getProperOrderString($prop_id, $group_id);
            $lookup_items = Yii::app()->db->createCommand()->select('id , c_property_id , name_'.Messages::getLanguageId().' name,
                    order_id , father_id ')->from('c_lookup_property')->where('c_property_id = :pr_id',
                    array('pr_id' => $prop_id ))->order($order_query)->query()->readAll();
        }


        return $lookup_items;
    }


    public static  function getLookupByID_father($prop_id , $father_id){

        $order_query = Utility::getProperOrderString($prop_id);

        $lookup_items = Yii::app()->db->createCommand()->select('id , c_property_id , name_'.Messages::getLanguageId().' name,
        order_id , father_id ')->from('c_lookup_property')->where('c_property_id = :pr_id and father_id = :dad_id',
                array('pr_id' => $prop_id  , ':dad_id'=>$father_id))->order($order_query)->query()->readAll();

        return $lookup_items;
    }
}