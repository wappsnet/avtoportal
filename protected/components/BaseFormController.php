<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Edgar
 * Date: 10/13/13
 * Time: 8:17 PM
 * To change this template use File | Settings | File Templates.
 */

class  BaseFormController extends Controller {

    protected $formData;
    protected $imagesToSave;
    protected $imagesToDelete;

    protected $announcementForm;
    protected $imagesForm;
    protected $propertiesForm;

    protected $propertyIds = array();

    protected $extraRightProps = array();
    protected $extraProps = array();

    public function init(){
        parent::init();
    }

    protected  function getPropertyByID( $propertyID){
        foreach($this->formData->groups as $group ){
            foreach($group->properties as $property){
                if($property->id == $propertyID){
                    if($property->type_id != 2){
                        if($property->type_id == 3){
                            return $property->value == 'true' ? 1 : 0;
                        }
                        return  $property->value == null || $property->value == "null" ?  "" : $property->value;
                    }else{
                        return $property->value->id;
                    }
                }
            }
        }
        return  null;
    }

    protected function getMainImage (){
        $main_image = null;
        if($this->imagesToSave != null && count($this->imagesToSave) > 0 ){
            $main_image = $this->imagesToSave[0];
            foreach($this->imagesToSave as $image){
                if($image->orderId < $main_image->orderId){
                    $main_image = $image;
                }
            }
        }
        return $main_image;
    }

    protected function getPropertyNameByID($propertyID){
        foreach($this->formData->groups as $group ){
            foreach($group->properties as $property){
                if($property->id == $propertyID){
                    if($property->type_id != 2){
                        return  $property->value == null || $property->value == "null" ?  "" : $property->value;
                    }else{
                        return $property->value->name;
                    }
                }
            }
        }
        return  null;
    }

    private function getAdditionalOptions($properties){
        $options = array();
        foreach ($properties as $prop){
            if($prop['group_id'] == 5 || $prop['group_id'] == 6 || $prop['group_id'] == 7){
                if($prop['value'] == 'true'){
                    $options[] = $prop;
                }
            }
        }
        return $options;
    }

    private function getCarExchangeState($prop_id , $ann_id , $lang_id){

        $criteria = new CDbCriteria();
        $criteria->condition = 'announcement_id = :ann_id and c_property_id = :prop_id';

        $criteria->params = array(':ann_id'=>$ann_id , ':prop_id'=>$prop_id);

        $result = CarProperty::model()->findAll($criteria);

        if(isset($result) && count($result) > 0){
           if($result[0]->value == 'true'){
               if($lang_id == 1){//arm
                   return  "<span style='font-weight: bold;color: red'>" . Messages::getMessage(46) .'&nbsp;&nbsp' . "</span>";
               }elseif($lang_id == 2){ //eng
                   return  "<span style='font-weight: bold;color: red'>" . Messages::getMessage(46) .'&nbsp;&nbsp' . "</span>";
               }elseif($lang_id == 3){ //rus
                   return  "<span style='font-weight: bold;color: red'>" . Messages::getMessage(46) .'&nbsp;&nbsp' . "</span>";
               }elseif($lang_id == 4){ //geo
                   return  "<span style='font-weight: bold;color: red'>" . Messages::getMessage(46) .'&nbsp;&nbsp' . "</span>";
               }
           }else{
                return "";
           }
        }
        return "";
    }

    private function  extraProps($prop_id , $lang_id){
        foreach($this->extraProps as $extra){
            if($prop_id == $extra['prop_id']){
                if($lang_id == 1){
                    return $extra['name_arm'] .  " ";
                }else if($lang_id == 2){
                    return $extra['name_eng'] .  " ";
                }else if($lang_id == 3){
                    return $extra['name_rus'] .  " ";
                }else if($lang_id == 4){
                    return $extra['name_geo'] .  " ";
                }
            }
        }

        return "";
    }

    private function  extraRightProps($prop_id , $lang_id){
        foreach($this->extraRightProps as $extra){
            if($prop_id == $extra['prop_id']){
                if($lang_id == 1){
                    return " " . $extra['name_arm'];
                }else if($lang_id == 2){
                    return " " . $extra['name_eng'];
                }else if($lang_id == 3){
                    return " " . $extra['name_rus'];
                }else if($lang_id == 4){
                    return " " . $extra['name_geo'];
                }
            }
        }

        return "";
    }

    protected  function getPropertiesSummary($ann_id){

        $summary = array();
        $propModel = $this->propertiesForm;

        $properties = Yii::app()->db->createCommand()->select('value, text_eng, text_arm , text_rus , text_geo, cp.c_property_group_id group_id, cp.id prop_id ,
        cp.name_eng prop_name_eng , cp.name_rus prop_name_rus , cp.name_arm prop_name_arm ,cp.name_geo prop_name_geo , cp.c_property_type_id prop_type ')->
            from($propModel::model()->tableName())->join('c_property cp','cp.id = c_property_id')->
            where('announcement_id = :ann_id',array('ann_id'=>$ann_id))->query()->readAll();

        $options = $this->getAdditionalOptions($properties);

        $summary['arm'] = null;

        if($propModel == 'CarProperty'){
            $summary['arm'] .= $this->getCarExchangeState(25,$ann_id,1);
        }

        foreach ($this->propertyIds as $id){
            foreach ($properties as $prop){
                if($prop['prop_id'] == $id){
                    if($prop['prop_type'] == 2){
                        if($prop['text_arm'] != null && $prop['text_arm'] != "" && $prop['text_arm'] != "null"){
                            $summary['arm'] .=   ($summary['arm'] != null && $summary['arm'] != "" ? ', ' : '') .
                                $this->extraProps($id , 1) . $prop['text_arm'] .$this->extraRightProps($id , 1);
                        }
                    }
                    if($prop['prop_type'] == 1){
                        if($prop['value'] != null && $prop['value'] != "" && $prop['value'] != "null"){
                            $summary['arm'] .=  ($summary['arm'] != null && $summary['arm'] != "" ? ', ' : '') . $prop['value'] . $this->extraRightProps($id , 1);
                        }
                    }
                }
            }
        }
        foreach($options as $opt){
            if($prop['prop_name_arm'] != null && $prop['prop_name_arm'] != "" && $prop['prop_name_arm'] != "null"){
                $summary['arm'] .=  ($summary['arm'] != null ? ', ' : '') . $opt['prop_name_arm'];
            }
        }

        $summary['eng'] = null;

        if($propModel == 'CarProperty'){
            $summary['eng'] .= $this->getCarExchangeState(25,$ann_id,2);
        }

        foreach ($this->propertyIds as $id){
            foreach ($properties as $prop){
                if($prop['prop_id'] == $id){
                    if($prop['prop_type'] == 2){
                        if($prop['text_eng'] != null && $prop['text_eng'] != "" && $prop['text_eng'] != "null"){
                            $summary['eng'] .= ($summary['eng'] != null && $summary['eng'] != "" ? ', ' : '') .
                                $this->extraProps($id , 2) . $prop['text_eng'] . $this->extraRightProps($id , 2);
                        }
                    }
                    if($prop['prop_type'] == 1){
                        if($prop['value'] != null && $prop['value'] != "" && $prop['value'] != "null"){
                            $summary['eng'] .=  ($summary['eng'] != null && $summary['eng'] != ""? ', ' : '') . $prop['value']. $this->extraRightProps($id , 2);
                        }
                    }
                }
            }
        }
        foreach($options as $opt){
            if($prop['prop_name_eng'] != null && $prop['prop_name_eng'] != "" && $prop['prop_name_eng'] != "null"){
                $summary['eng'] .=  ($summary['eng'] != null ? ', ' : '') . $opt['prop_name_eng'];
            }
        }


        $summary['rus'] = null;

        if($propModel == 'CarProperty'){
            $summary['rus'] .= $this->getCarExchangeState(25,$ann_id,3);
        }

        foreach ($this->propertyIds as $id){
            foreach ($properties as $prop){
                if($prop['prop_id'] == $id){
                    if($prop['prop_type'] == 2){
                        if($prop['text_rus'] != null && $prop['text_rus'] != "" && $prop['text_rus'] != "null"){
                            $summary['rus'] .= ($summary['rus'] != null && $summary['rus'] != "" ? ', ' : '') .
                                $this->extraProps($id , 3) .  $prop['text_rus'] . $this->extraRightProps($id , 3);
                        }
                    }
                    if($prop['prop_type'] == 1){
                        if($prop['value'] != null && $prop['value'] != "" && $prop['value'] != "null"){
                            $summary['rus'] .=  ($summary['rus'] != null && $summary['rus'] != "" ? ', ' : '') . $prop['value']. $this->extraRightProps($id , 3);
                        }
                    }
                }
            }
        }
        foreach($options as $opt){
            if($prop['prop_name_rus'] != null && $prop['prop_name_rus'] != "" && $prop['prop_name_rus'] != "null"){
                $summary['rus'] .=  ($summary['rus'] != null ? ', ' : '') . $opt['prop_name_rus'];
            }
        }

        $summary['geo'] = null;

        if($propModel == 'CarProperty'){
            $summary['geo'] .= $this->getCarExchangeState(25,$ann_id,3);
        }

        foreach ($this->propertyIds as $id){
            foreach ($properties as $prop){
                if($prop['prop_id'] == $id){
                    if($prop['prop_type'] == 2){
                        if($prop['text_geo'] != null && $prop['text_geo'] != "" && $prop['text_geo'] != "null"){
                            $summary['geo'] .= ($summary['geo'] != null && $summary['geo'] != "" ? ', ' : '') .
                                $this->extraProps($id , 4) . $prop['text_geo'] . $this->extraRightProps($id , 1);
                        }
                    }
                    if($prop['prop_type'] == 1){
                        if($prop['value'] != null && $prop['value'] != "" && $prop['value'] != "null"){
                            $summary['geo'] .=  ($summary['geo'] != null && $summary['geo'] != "" ? ', ' : '') . $prop['value']. $this->extraRightProps($id , 4);
                        }
                    }
                }
            }
        }
        foreach($options as $opt){
            if($prop['prop_name_geo'] != null && $prop['prop_name_geo'] != "" && $prop['prop_name_geo'] != "null"){
                $summary['geo'] .=  ($summary['geo'] != null ? ', ' : '') . $opt['prop_name_geo'];
            }
        }


        return $summary;
    }

    protected  function isUnregisteredUserHere($email){
        $criteria = new CDbCriteria();
        $criteria->condition = 'email = :mail';
        $criteria->params = array(':mail'=>$email);
        $result = UnregisteredUsers::model()->findAll($criteria);

        if(count($result) > 0){
            return $result[0];
        }

        return null;
    }

    protected  function fillUserData(&$announcement){
//        if(Yii::app()->user->isGuest){
//            $email = $this->getPropertyByID(168);
//            $result = $this->isUnregisteredUserHere($email);
//            $unregistered_user = null;
//            if(isset($result)){
//                $unregistered_user = $result;
//                if(isset($result->id)){
//                    $announcement->unregistered_user_id = $result->id;
//                }
//            }else{
//                $unregistered_user = new UnregisteredUsers();
//                $unregistered_user->name = $this->getPropertyByID(165);
//                $unregistered_user->phone_1 = $this->getPropertyByID(166);
//                $unregistered_user->phone_2 = $this->getPropertyByID(167);
//                $unregistered_user->email = $this->getPropertyByID(168);
//                $unregistered_user->save();
//
//                $announcement->unregistered_user_id = $unregistered_user->getPrimaryKey();
//            }
//
//            if(isset($unregistered_user)){
//                $announcement->user_phone = "<span style='font-weight:bold'>" . ($unregistered_user->name != null ? $unregistered_user->name : "" ). "</span>"
//                    . ' '
//                    . "<span class='phone'>"  . ($unregistered_user->phone_1 != null ?  $unregistered_user->phone_1 : "")
//
//                    . ($unregistered_user->phone_2 != null && $unregistered_user->phone_2 != "null" ?   ',' .$unregistered_user->phone_2 : "") . "</span>";
//            }
//
//            $announcement->user_id = -55;
//        }else

        if($announcement->isNewRecord){
            if($this->isAdmin()){
                $unregistered_user = new UnregisteredUsers();
                $unregistered_user->name = $this->getPropertyByID(165);
                $unregistered_user->phone_1 = $this->getPropertyByID(166);
                $unregistered_user->phone_2 = $this->getPropertyByID(167);
                $unregistered_user->email = $this->getPropertyByID(168);
                $unregistered_user->save();

                $announcement->user_phone = "<span style='font-weight:bold'>" . ($unregistered_user->name != null ? $unregistered_user->name : "") . "</span>"
                    . ' '
                    . "<span class='phone'>"  . ($unregistered_user->phone_1 != null ?  $unregistered_user->phone_1 : "")
                    . ($unregistered_user->phone_2 != null && $unregistered_user->phone_2 != "null"? ', '. $unregistered_user->phone_2 : "") . "</span>";

                $announcement->unregistered_user_id = $unregistered_user->getPrimaryKey();
                $announcement->user_id = Yii::app()->user->id;
            } else {
                $announcement->user_id = Yii::app()->user->id;
                $user = User::model()->findByPk(Yii::app()->user->id);
                $user_phone1 = $user->phone1_id != null ? UserPhone::model()->findByPk((integer)$user->phone1_id) : "";

                if(isset($user->name_is_hidden) && $user->name_is_hidden == 1){
                    $announcement->user_phone = "<span style='font-weight:bold'>" . $user->user_name . "</span>" . ' '
                        . "<span class='phone'>"  . $user_phone1->phone_number. "</span>";
                }else{
                    $announcement->user_phone =  "<span class='phone'>"  . $user_phone1->phone_number. "</span>";
                }

                $announcement->unregistered_user_id = 0;
            }
        }
    }

    protected  function fillOrdinaryData(&$announcement , $isThereLocation){
        $announcement->description = $this->formData->description;
        $announcement->isAccepted = 0;
        $announcement->is_top = $this->formData->top_id;
        $announcement->term_id = $this->getPropertyByID(212);
        $currentDate = date('Y-m-d');

        if($announcement->term_id == 57){//1 week
            $announcement->validDate = date('Y-m-d' , strtotime($currentDate) + (24*3600*7));
        }
        else if($announcement->term_id == 58){//2 week
            $announcement->validDate = date('Y-m-d' , strtotime($currentDate) + (24*3600*14));
        }
        else if($announcement->term_id == 59){//3 week
            $announcement->validDate = date('Y-m-d' , strtotime($currentDate) + (24*3600*21));
        }
        else if($announcement->term_id == 3372){//30 days
            $announcement->validDate = date('Y-m-d' , strtotime($currentDate) + (24*3600*30));
        }
        else if($announcement->term_id == 3373){//60 days
            $announcement->validDate = date('Y-m-d' , strtotime($currentDate) + (24*3600*60));
        }
        else if($announcement->term_id == 3374){//120 days
            $announcement->validDate = date('Y-m-d' , strtotime($currentDate) + (24*3600*120));
        }



        if($isThereLocation){
            $announcement->c_location_id = $this->formData->location;
            $announcement->c_sub_location_id = $this->formData->subLocation;
            $announcement->optional_location = $this->formData->optional_location;
        }

        if($this->isAdmin()){
            $announcement->isAccepted = 1;
        }
    }

    protected  function fillMainImage(&$announcement){
        $tempo = $this->getMainImage($this->imagesToSave);
        if($tempo != null){
            $announcement->main_image = $tempo->title;
        }else{
            $announcement->main_image = 'no_photo.jpg';
        }

    }

    protected  function fillPriceData(&$announcement  , $propId_RUR ,$propId_USD, $propId_EUR){

        $RUR_price = $this->getPropertyByID($propId_RUR);
        $USD_price = $this->getPropertyByID($propId_USD);
        $EUR_price = $this->getPropertyByID($propId_EUR);

        $announcement->rate_id = $this->formData->rate_id;

        if(isset($RUR_price) && is_numeric($RUR_price) && $announcement->rate_id == 1){
            $announcement->price = $RUR_price;
        }else if(isset($USD_price) && is_numeric($USD_price)&& $announcement->rate_id == 2){
            $announcement->price = $USD_price;
        }else if(isset($EUR_price) && is_numeric($EUR_price) && $announcement->rate_id == 3){
            $announcement->price = $EUR_price;
        }
    }

    protected  function fillPriceDataOnlyRUR(&$announcement  , $propId_RUR ){

        $RUR_price = $this->getPropertyByID($propId_RUR);

        if(isset($RUR_price) && is_numeric($RUR_price)){
            $announcement->price = $RUR_price;
            $announcement->rate_id = 1;
        }
    }

    private static function  is_price_filled($amount){
        if(isset($amount) && !is_null($amount) && $amount != "" && $amount != "null"){

        }

    }

    private function deleteImagesOfAnnouncement(){
        $imageModel = $this->imagesForm;
        $images  = array();
        foreach($this->imagesToDelete as $imageToDel){
            $images[] = $imageModel::model()->findByPK($imageToDel->id);
            $imageModel::model()->deleteByPk($imageToDel->id);
        }

        foreach($images as $image){
            if(file_exists(Yii::app() -> getBasePath() . "/../uploads/." . $image->name)){
                unlink(Yii::app() -> getBasePath() . "/../uploads/" . $image->name);
            }
            if(file_exists(Yii::app() -> getBasePath() . "/../uploads/" . $image->name)){
                unlink(Yii::app() -> getBasePath() . "/../uploads/" . $image->name);
            }
        }
    }

    private function deletePropsOfAnnouncement($announcementID){
        $propModel = $this->propertiesForm;
        $criteria = new CDbCriteria();
        $criteria->condition = 'announcement_id = :ann_id';
        $criteria->params = array(':ann_id'=>$announcementID);

        $propModel::model()->deleteAll($criteria);
    }

    protected  function processImages($announcementID, $isEdit){
        $imageModel = $this->imagesForm;
        if($isEdit){
            $this->deleteImagesOfAnnouncement($announcementID);
        }
        foreach($this->imagesToSave as $image){
            $photo = new $imageModel();
            $photo->name = $image->title;
            $photo->order_id = $image->orderId;
            $photo->announcement_id = $announcementID;
            $photo->save();
        }

        foreach($this->imagesToDelete as $image){
            if(file_exists(Yii::app() -> getBasePath() . "/../uploads/thumb." . $image->title)){
                unlink(Yii::app() -> getBasePath() . "/../uploads/thumb." . $image->title);
            }
        }
    }

    protected  function processProperties($announcementID, $isEdit){
        $propModel = $this->propertiesForm;
        if($isEdit){
            $this->deletePropsOfAnnouncement($announcementID);
        }

        foreach($this->formData->groups as $group ){
            foreach($group->properties as $property){
                $answer = new $propModel();
                $answer->announcement_id = $announcementID;
                $answer->c_property_id = $property->id;


                if($property->type_id != 2){
                    $answer->value = $property->value == "null" || $property->value == null ? '' : $property->value;
                }else{
                    if(isset($property->value)){
                        if(isset($property->value->id)){
                            $lookup_property = CLookupProperty::model()->findByPk($property->value->id);
                            $answer->value = $property->value->id;
                            $answer->text_arm = $lookup_property->name_arm;
                            $answer->text_eng = $lookup_property->name_eng;
                            $answer->text_rus = $lookup_property->name_rus;
                            $answer->text_geo = $lookup_property->name_geo;
                        }else{
                            $answer->value = "";
                            $answer->text_arm = "";
                            $answer->text_eng = "";
                            $answer->text_rus = "";
                            $answer->text_geo = "";
                        }
                    }
                }
                $answer ->save();
            }
        }
    }

    protected  function  fillSpecificData(&$announcement){}
    public  function actionInsert(){}

    public static function setOwnerData(&$item){
        if($item->user_id == -55 || Controller::isAdminUser($item->user_id)){
            $user = UnregisteredUsers::model()->findByPk($item->unregistered_user_id);
            $phone1 = $user->phone_1;
            $email = $user->email;
        }else{
            $user = User::model()->findByPk($item->user_id);
            $phone1_id = $user->phone1_id;
            $phone1 = UserPhone::model()->findBySql("Select * From user_phone WHERE id = ".$phone1_id)->phone_number;
            $email = $user->email;
        }
        $label = self::getLabel();
        if(isset($item->c_location_id)){
            $location = CLocation::model()->findByPk($item->c_location_id);
            $item->userInfo = array("phone1" => $phone1, "location"=>$location->$label, "email"=>$email);
        }else{
            $item->userInfo = array("phone1" => $phone1, "location"=>'', "email"=>$email);
        }

    }


    private static function getLabel()
    {
        $lang = Messages::getLanguageId();
        $label = "name_" . $lang;
        return $label;
    }




}