<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
    const ADMIN_ROLE = "4";
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/column1';

    public $layout_top_bar = '';
    public $layout_filter = '';
    public $renderLeftPart = true;
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();

    public $left_side_menu = array();

    public $top_menu = array();

    public function init(){
        Yii::app()->messages;
        $this->initBanners();

        $this->fillLeftSideMenu();
        $this->fillTopMenu();

        $_SESSION['KCFINDER']['uploadURL'] = Yii::app()->baseUrl."/uploads/"; // URL for the uploads folder
        $_SESSION['KCFINDER']['uploadDir'] = Yii::app()->basePath."/../uploads/"; // path to the uploads folder

    }


    private function fillTopMenu(){
        $this->top_menu[] = $this->getMenuItem(Messages::getMessage(243),"#",null);
        $police_items = array();

        $top_sign = RoadPoliceRoadSigns::model()->find();
        $top_rule = RoadTrafficRules::model()->find();


        $police_items[] = $this->getMenuItem(Messages::getMessage(245),Yii::app()->createUrl( 'site/roadPoliceAddresses'), null );
//        $police_items[] = $this->getMenuItem(Messages::getMessage(252),Yii::app()->createUrl( 'site/autoSchool'),null);
        $police_items[] = $this->getMenuItem(Messages::getMessage(246),Yii::app()->createUrl( 'site/roadPolicePenalty'), null );
//        if(isset($top_sign)){
//            $police_items[] = $this->getMenuItem(Messages::getMessage(247),Yii::app()->createUrl( 'site/roadSigns') . "?road_sign=" . $top_sign->id, null );
//        }else{
//            $police_items[] = $this->getMenuItem(Messages::getMessage(247),"#", null );
//        }


        if(isset($top_rule)){
            $police_items[] = $this->getMenuItem(Messages::getMessage(248),Yii::app()->createUrl( 'site/trafficRules') . "?rule_id=" . $top_rule->id, null );
        }else{
            $police_items[] = $this->getMenuItem(Messages::getMessage(248),"#", null );
        }
//        $police_items[] = $this->getMenuItem(Messages::getMessage(250),Yii::app()->createUrl( 'site/tech'),null);
//        $police_items[] = $this->getMenuItem(Messages::getMessage(244),Yii::app()->createUrl( 'site/customs'),null);
        $police_items[] = $this->getMenuItem(Messages::getMessage(325),Yii::app()->createUrl( 'site/addAnnouncement'),null);
        $police_items[] = $this->getMenuItem(Messages::getMessage(168),Yii::app()->createUrl( 'site/siteRules'),null);
        $this->top_menu[] = $this->getMenuItem(Messages::getMessage(249),"#",$police_items);

//        $appa_rows = $this->getAppaItems();
//        $appa_items = array();
//
//        foreach($appa_rows as $row){
//            $appa_items[] = $this->getMenuItem($row->title , Yii::app()->createUrl( 'site/appa' ) . "?id="  . $row->id , null );
//        }

//        $this->top_menu[] = $this->getMenuItem(Messages::getMessage(251),"#",$appa_items);

        $autoschool_rows = $this->getAutoSchoolItems();
        $autoschool_items = array();

        foreach($autoschool_rows as $row){
            $autoschool_items[] = $this->getMenuItem($row->title , Yii::app()->createUrl( 'site/autoSchool' ) . "?id="  . $row->id , null );
        }


        $this->top_menu[] = $this->getMenuItem(Messages::getMessage(253),"site/news",null);

    }



    private function  fillLeftSideMenu(){

        $for_sale_spare_parts = array();
        $service_items = array();
        $for_sale_items = array();

        $for_sale_items[] = $this->getMenuItem($this->getLabelForLeftMenu(115 , $this->personalCarAnnouncementsCount()), Yii::app()->createUrl( 'site/listCarAnnouncementsAll'),null);
        $for_sale_items[] = $this->getMenuItem($this->getLabelForLeftMenu(116 , $this->officialDealerCarAnnouncementsCount()),Yii::app()->createUrl( 'site/officialDealers'),null);
        $for_sale_items[] = $this->getMenuItem($this->getLabelForLeftMenu(117 , $this->universalDealerCarAnnouncementsCount()),Yii::app()->createUrl( 'site/universalDealers'),null);
        $this->left_side_menu[] = $this->getMenuItem($this->getLabelForLeftMenu(118 , $this->carAnnouncementsCount()),"#",$for_sale_items);

        $for_sale_spare_parts[] = $this->getMenuItem($this->getLabelForLeftMenu(119 , $this->wheelAnnouncementsCount()),Yii::app()->createUrl( 'site/listCarWheelAnnouncementsAll'),null);
        $for_sale_spare_parts[] = $this->getMenuItem($this->getLabelForLeftMenu(120 , $this->sparePartAnnouncementsCount()),Yii::app()->createUrl( 'site/listSparePartAnnouncementsAll'),null);
        $this->left_side_menu[] = $this->getMenuItem($this->getLabelForLeftMenu(121 , $this->allSparePartAnnouncementsCount()),"",$for_sale_spare_parts);

        $this->left_side_menu[] = $this->getMenuItem($this->getLabelForLeftMenu(122 , $this->carRentAnnouncementsCount()),Yii::app()->createUrl( 'site/listRentCarAnnouncements'),null);
//        $this->left_side_menu[] = $this->getMenuItem($this->getLabelForLeftMenu(123 , $this->autoServicesAnnouncementsCount()),Yii::app()->createUrl( 'site/listServicesByPlace'),null);
        $this->left_side_menu[] = $this->getMenuItem($this->getLabelForLeftMenu(124 , $this->ruralTechAnnouncementsCount()),Yii::app()->createUrl( 'site/listRuralAnnouncementsAll'),null);

        $service_items[] = $this->getMenuItem($this->getLabelForLeftMenu(125 , $this->taxiAnnouncementsCount()),Yii::app()->createUrl('site/listTaxiAnnouncements'),null);
        $service_items[] = $this->getMenuItem($this->getLabelForLeftMenu(126 , $this->evacuatorAnnouncementsCount()),Yii::app()->createUrl('site/listEvacuatorAnnouncementsAll'),null);
//        $service_items[] = $this->getMenuItem($this->getLabelForLeftMenu(127 , $this->shippingAnnouncementsCount()),Yii::app()->createUrl('site/listShippingAnnouncements'),null);
//        $service_items[] = $this->getMenuItem($this->getLabelForLeftMenu(128 , $this->transportationAnnouncementsCount()),Yii::app()->createUrl('site/listTransportationAnnouncements'),null);//to do  $toNav  is fictive
//        $service_items[] = $this->getMenuItem($this->getLabelForLeftMenu(129 , $this->tourismAnnouncementsCount()),Yii::app()->createUrl('site/listTourismAnnouncements'),null);
//        $service_items[] = $this->getMenuItem($this->getLabelForLeftMenu(130 , $this->trainingCarAnnouncementsCount()),Yii::app()->createUrl('site/listSpecCarAnnouncements'),null);

        $this->left_side_menu[] = $this->getMenuItem($this->getLabelForLeftMenu(131 , $this->allServices()),"#",$service_items);

//        $this->left_side_menu[] = $this->getMenuItem($this->getLabelForLeftMenu(132 , $this->jobCarAnnouncementsCount()),Yii::app()->createUrl( 'site/listJobAnnouncementsAll'),null);//to do  $toNav  is fictive
    }


    private function getLabelForLeftMenu($message_id , $count){
        $label =  Messages::getMessage($message_id);
        if($this->isAdmin()){
            $label .= " " . '(' . $count . ')';
        }
        return  $label;
    }

    private function wheelAnnouncementsCount(){
        $criteria = new CDbCriteria;
        $criteria->condition = 'isAccepted = 0';
        $items = WheelsAnnouncement::model()->findAll($criteria);
        return count($items);
    }

    private function sparePartAnnouncementsCount(){
        $criteria = new CDbCriteria;
        $criteria->condition = 'isAccepted = 0';
        $items = SparePartAnnouncement::model()->findAll($criteria);
        return count($items);
    }

    private function allSparePartAnnouncementsCount(){
        return $this->sparePartAnnouncementsCount() + $this->wheelAnnouncementsCount();
    }

    private function carAnnouncementsCount(){
        $criteria = new CDbCriteria;
        $criteria->condition = 'isAccepted = 0';
        $items = CarAnnouncement::model()->findAll($criteria);
        return count($items);
    }

    private function carRentAnnouncementsCount(){
        $criteria = new CDbCriteria;
        $criteria->condition = 'isAccepted = 0';
        $items = CarRentAnnouncement::model()->findAll($criteria);
        return count($items);
    }

    private function autoServicesAnnouncementsCount(){
        $criteria = new CDbCriteria;
        $criteria->condition = 'isAccepted = 0';
        $items = AutoServiceAnnouncement::model()->findAll($criteria);
        return count($items);
    }

    private function ruralTechAnnouncementsCount(){
        $criteria = new CDbCriteria;
        $criteria->condition = 'isAccepted = 0';
        $items = RuraltechAnnouncement::model()->findAll($criteria);
        return count($items);
    }

    private function taxiAnnouncementsCount(){
        $criteria = new CDbCriteria;
        $criteria->condition = 'isAccepted = 0';
        $items = TaxiAnnouncement::model()->findAll($criteria);
        return count($items);
    }

    private function evacuatorAnnouncementsCount(){
        $criteria = new CDbCriteria;
        $criteria->condition = 'isAccepted = 0';
        $items = EvacuatorAnnouncement::model()->findAll($criteria);
        return count($items);
    }

    private function shippingAnnouncementsCount(){
        $criteria = new CDbCriteria;
        $criteria->condition = 'isAccepted = 0';
        $items = ShippingAnnouncement::model()->findAll($criteria);
        return count($items);
    }

    private function transportationAnnouncementsCount(){
        $criteria = new CDbCriteria;
        $criteria->condition = 'isAccepted = 0';
        $items = TransportationAnnouncement::model()->findAll($criteria);
        return count($items);
    }

    private function tourismAnnouncementsCount(){
        $criteria = new CDbCriteria;
        $criteria->condition = 'isAccepted = 0';
        $items = TouristAnnouncement::model()->findAll($criteria);
        return count($items);
    }

    private function trainingCarAnnouncementsCount(){
        $criteria = new CDbCriteria;
        $criteria->condition = 'isAccepted = 0';
        $items = SpecCarAnnouncement::model()->findAll($criteria);
        return count($items);
    }

    private function jobCarAnnouncementsCount(){
        $criteria = new CDbCriteria;
        $criteria->condition = 'isAccepted = 0';
        $items = JobAnnouncement::model()->findAll($criteria);
        return count($items);
    }

    private  function allServices(){
       return $this->tourismAnnouncementsCount() + $this->trainingCarAnnouncementsCount()
           + $this->tourismAnnouncementsCount() + $this->transportationAnnouncementsCount()
           + $this->shippingAnnouncementsCount() + $this-> evacuatorAnnouncementsCount() + $this->taxiAnnouncementsCount();
    }

    private function personalCarAnnouncementsCount(){
        $count = 0;
        $criteria = new CDbCriteria;
        $criteria->condition = 'isAccepted = 0';
        $items = CarAnnouncement::model()->findAll($criteria);

        foreach($items as $item){
            if(!$this->isDealerUser($item->user_id)){
                $count++;
            }
        }
        return $count;
    }

    private function officialDealerCarAnnouncementsCount(){
        $count = 0;
        $criteria = new CDbCriteria;
        $criteria->condition = 'isAccepted = 0';
        $items = CarAnnouncement::model()->findAll($criteria);

        foreach($items as $item){
            if($this->isOfficialDealerUser($item->user_id)){
                $count++;
            }
        }
        return $count;
    }

    private function universalDealerCarAnnouncementsCount(){
        $count = 0;
        $criteria = new CDbCriteria;
        $criteria->condition = 'isAccepted = 0';
        $items = CarAnnouncement::model()->findAll($criteria);

        foreach($items as $item){
            if($this->isUniversalDealerUser($item->user_id)){
                $count++;
            }
        }
        return $count;

    }



    private function getAppaItems(){
        $criteria = new CDbCriteria;
        $criteria->select = 'title , id';
        $items = AppaItems::model()->findAll($criteria);

        return $items;
    }

    private function getAutoSchoolItems(){
        $criteria = new CDbCriteria;
        $criteria->select = 'title , id';
        $items = Autoschool::model()->findAll($criteria);

        return $items;
    }

    private function  getMenuItem($name , $toNavigate , $sub_items ){
        $item = array();
        $item['name'] = $name;
        $item['toNavigate'] = $toNavigate;
        $item['sub_items'] = $sub_items;

        return $item;

    }

    public function renderFilters(){
        if($this->layout_filter == ''){
            return false;
        }
        return true;
    }

    public function renderTopBar(){
        if($this->layout_top_bar == ''){
            return false;
        }
        return true;
    }

    public function actionChangeLanguage()
    {
        if (isset($_GET['lang']) || isset($_POST['lang'])) {
            Yii::app()->session['languageId'] = $_GET['lang'];
        }
    }

    public function isAdmin(){
        return self::isAdminUser(Yii::app()->user->id);
    }

    public function isDealer(){
        return self::isDealerUser(Yii::app()->user->id);

    }

    public static function isAdminUser($userId){
        if(!$userId){
            return false;
        }
        $model = User::model()->findByPk($userId);

        if($model->user_roles_id === self::ADMIN_ROLE){
            return true;
        }else{
            return false;
        }
    }

    public static function isDealerUser($userId){
        if(!$userId){
            return false;
        }
        $model = User::model()->findByPk($userId);

        if($model->user_roles_id == 3 || $model->user_roles_id == 2){
            return true;
        }else{
            return false;
        }
    }

    public static function isUniversalDealerUser($userId){
        if(!$userId){
            return false;
        }
        $model = User::model()->findByPk($userId);

        if($model->user_roles_id == 2){
            return true;
        }else{
            return false;
        }
    }

    public static function isOfficialDealerUser($userId){
        if(!$userId){
            return false;
        }
        $model = User::model()->findByPk($userId);

        if($model->user_roles_id == 3){
            return true;
        }else{
            return false;
        }
    }

    public function actionIsAdmin(){
        if($this->isAdmin()){
            echo CJSON::encode(array("isAdmin"=>true));
        };
    }

    public $banner = array();

    public function initBanners(){
        $model1 = Banners::model()->findByAttributes(array('positionId'=>1))?Banners::model()->findByAttributes(array('positionId'=>1)):new Banners();
        $model2 = Banners::model()->findByAttributes(array('positionId'=>2))?Banners::model()->findByAttributes(array('positionId'=>2)):new Banners();
        $model3 = Banners::model()->findByAttributes(array('positionId'=>3))?Banners::model()->findByAttributes(array('positionId'=>3)):new Banners();
        $model4 = Banners::model()->findByAttributes(array('positionId'=>4))?Banners::model()->findByAttributes(array('positionId'=>4)):new Banners();
        $model5 = Banners::model()->findByAttributes(array('positionId'=>5))?Banners::model()->findByAttributes(array('positionId'=>5)):new Banners();
        $model6 = Banners::model()->findByAttributes(array('positionId'=>6))?Banners::model()->findByAttributes(array('positionId'=>6)):new Banners();


        $position1FileName = $model1->file_name;
        $position2FileName = $model2->file_name;
        $position3FileName = $model3->file_name;
        $position4FileName = $model4->file_name;
        $position5FileName = $model5->file_name;
        $position6FileName = $model6->file_name;

        $position1Url = $model1->url;
        $position2Url = $model2->url;
        $position3Url = $model3->url;
        $position4Url = $model4->url;
        $position5Url = $model5->url;
        $position6Url = $model6->url;


        $position1 = Yii::app()->request->baseUrl . "/uploads/banners/" . $position1FileName;
        $position2 = Yii::app()->request->baseUrl . "/uploads/banners/" . $position2FileName;
        $position3 = Yii::app()->request->baseUrl . "/uploads/banners/" . $position3FileName;
        $position4 = Yii::app()->request->baseUrl . "/uploads/banners/" . $position4FileName;
        $position5 = Yii::app()->request->baseUrl . "/uploads/banners/" . $position5FileName;
        $position6 = Yii::app()->request->baseUrl . "/uploads/banners/" . $position6FileName;

        $isPosition1Img = pathinfo($position1FileName, PATHINFO_EXTENSION) !== "swf" && $position1FileName;
        $isPosition2Img = pathinfo($position2FileName, PATHINFO_EXTENSION) !== "swf" && $position2FileName;
        $isPosition3Img = pathinfo($position3FileName, PATHINFO_EXTENSION) !== "swf" && $position3FileName;
        $isPosition4Img = pathinfo($position4FileName, PATHINFO_EXTENSION) !== "swf" && $position4FileName;
        $isPosition5Img = pathinfo($position5FileName, PATHINFO_EXTENSION) !== "swf" && $position5FileName;
        $isPosition6Img = pathinfo($position6FileName, PATHINFO_EXTENSION) !== "swf" && $position6FileName;

        $this->banner =
            array('position1'=>$position1, 'position2'=>$position2, 'position3'=>$position3, 'position4'=>$position4, 'position5'=>$position5, 'position6'=>$position6,
                'isPosition1Img'=>$isPosition1Img, 'isPosition2Img'=>$isPosition2Img, 'isPosition3Img'=>$isPosition3Img, 'isPosition4Img'=>$isPosition4Img, 'isPosition5Img'=>$isPosition5Img, 'isPosition6Img'=>$isPosition6Img,
                'position1Url'=>$position1Url, 'position2Url'=>$position2Url, 'position3Url'=>$position3Url, 'position4Url'=>$position4Url, 'position5Url'=>$position5Url, 'position6Url'=>$position6Url);
    }


}