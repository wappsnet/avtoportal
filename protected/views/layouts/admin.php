<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Edgar
 * Date: 6/28/13
 * Time: 2:00 AM
 * To change this template use File | Settings | File Templates.
 */?>
<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" ng-app="main">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/generic.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/admin.css"/>

    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/angular.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/angular_google_maps.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/angular-resource.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-models/main_model.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-controllers/main_controller.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-controllers/main_directives.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/angular-cache-2.3.4.min.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/angular-busy.min.js"></script>
    <script src="http://maps.googleapis.com/maps/api/js?sensor=false&langsuage=en"></script>

</head>


<body class="admin_files">
<?php
Yii::app()->getClientScript()->registerCoreScript('jquery');
?>

<div id="content">
    <div style="float: left">
        <?php
        $this->beginWidget('zii.widgets.CPortlet', array(
            'title' => 'Operations',
        ));
        $this->widget('zii.widgets.CMenu', array(
            'items' => $this->menu,
            'htmlOptions' => array('class' => 'admin_oper'),
        ));
        $this->endWidget();
        ?>
    </div>

    <div style="float: left;margin-left: 50px;margin-top: 30px;">
            <?php echo $content; ?>
    </div>
</div><!-- content -->
</body>
</html>