﻿<!-- *********************** START TOP SEARCH PANEL************************-->
<div ng-controller="mainFiltersController" >

    <div class="generic_page_form " style="overflow: inherit">

        <!-- ==================================== Simple Search======================================== -->
        <div class="form_box" ng-show="showSimpleSearch" id="filter_auto_default" style="display: none;">
<form class="top_bar_1">
            <div class="select_row mark">
                <label><?php echo Messages::getMessage(14) ?></label>

                <select class="big_select  hasCustomSelect" ng-model="currentMark"
                        ng-options="mrk.label for mrk in marks">
                </select>
            </div>

            <div class="select_row model">
                <label><?php echo Messages::getMessage(15) ?></label>
                <select class="big_select  hasCustomSelect" ng-model="currentModel" id="currModel"
                        ng-options="mdel.label for mdel in currentMark.models" >
                </select>
            </div>
            <div class="select_row years">
                <label><?php echo Messages::getMessage(16) ?></label>

                <div style="display: inline">
                    <select class="sml_select hasCustomSelect" ng-model="startYear"
                            ng-options="year.label for year in startYears">
                    </select>
                </div>
                <div style="display: inline">
                    <span>-</span>
                    <select class="sml_select hasCustomSelect" ng-model="endYear"
                            ng-options="year.label for year in endYears">
                    </select>
                </div>
            </div>
            <div class="select_row">
                <label><?php echo Messages::getMessage(17) ?></label>


                <div class="prices">
                    <div style="display: inline">
                        <select class="sml_select hasCustomSelect" ng-model="startPrice"
                                ng-options="price.label for price in startPrices">
                        </select>
                    </div>
                    <div style="display: inline">
                        <span>-</span>
                        <select class="sml_select hasCustomSelect" ng-model="endPrice"
                                ng-options="price.label for price in endPrices">
                        </select>
                    </div>
                </div>
            </div>
            <div class="left">
                <div style="float: left; margin-top:10px;margin-left: 5px;">
                    <input id="crush" type="checkbox" ng-model="isCrashed">
                    <label for="crush" class="left" style="font-weight: bold"><?php echo Messages::getMessage(257) ?></label>
                </div>
                <div style="float: left;  margin-top:10px; margin-left: 35px;">
                    <input id="diller" type="checkbox" ng-model="isDiller">
                    <label for="diller" class="left" style="font-weight: bold"> <?php echo Messages::getMessage(258) ?></label>
                </div>

            </div>

            <div class="">
                <div class="search_line">

                    <div ng-click="showSearchByBodyTab()">
                        <a href="#" class="serach_type"><?php echo Messages::getMessage(259) ?></a>
                    </div>

                    <div ng-click="showAdvancedSearchTab()">
                        <a href="#" class="serach_type"><?php echo Messages::getMessage(260) ?></a>
                    </div>
                </div>
            </div>

            <div class="form_search_box left">
                <div class="search_line">
                    <div ng-click="makeSearch()">
                        <a href="#" class="search"><?php echo Messages::getMessage(261) ?> </a>
                    </div>
                </div>
            </div>
        </div>
        <!-- ==================================== End Simple Search======================================== -->



        <!-- ==================================== Advanced Search======================================== -->
        <div class="form_box" ng-show="showAdvancedSearch" id="filter_auto" style="display: none;">
            <form class="top_bar_2">
                <div class="select_row mark">
                    <label><?php echo Messages::getMessage(14) ?></label>

                    <select class="big_select  hasCustomSelect" ng-model="currentMark"
                            ng-options="mrk.label for mrk in marks">
                    </select>
                </div>

                <div class="select_row model">
                    <label><?php echo Messages::getMessage(15) ?></label>
                    <select class="big_select  hasCustomSelect" ng-model="currentModel"
                            ng-options="mdel.label for mdel in currentMark.models">
                    </select>
                </div>
                <div class="select_row years ml10">
                    <label><?php echo Messages::getMessage(16) ?></label>

                    <div style="display: inline">
                        <select class="sml_select hasCustomSelect" ng-model="startYear"
                                ng-options="year.label for year in startYears">
                        </select>
                    </div>
                    <div style="display: inline">
                        <span>-</span>
                        <select class="sml_select hasCustomSelect" ng-model="endYear"
                                ng-options="year.label for year in endYears">
                        </select>
                    </div>
                </div>
                <div class="select_row">
                    <label><?php echo Messages::getMessage(17) ?></label>

                    <div class="clear"></div>
                    <div class="prices">
                        <div style="display: inline">
                            <select class="sml_select hasCustomSelect" ng-model="startPrice"
                                    ng-options="price.label for price in startPrices">
                            </select>
                        </div>
                        <div style="display: inline">
                            <span>-</span>
                            <select class="sml_select hasCustomSelect" ng-model="endPrice"
                                    ng-options="price.label for price in endPrices">
                            </select>
                        </div>
                    </div>
                </div>

                <div class="select_row color">
                    <label><?php echo Messages::getMessage(54) ?></label>
                    <select class="middle_select hasCustomSelect" ng-model="color"
                            ng-options="color.label for color in colors">
                    </select>
                </div>
                <div class="select_row transmission">
                    <label><?php echo Messages::getMessage(49) ?></label>
                    <select class="middle_select hasCustomSelect" ng-model="transmission"
                            ng-options="transmission.label for transmission in transmissions">
                    </select>
                </div>
                <div class="select_row fuel">
                    <label><?php echo Messages::getMessage(134) ?></label>
                    <select class="middle_select hasCustomSelect" ng-model="fuel"
                            ng-options="fuel.label for fuel in fuels">
                    </select>
                </div>
                <div class="select_row run">
                    <label><?php echo Messages::getMessage(262) ?></label>
                    <select class="middle_select hasCustomSelect" ng-model="run"
                            ng-options="run.label for run in runOptions">
                    </select>
                </div>
                <div class="select_row volume">
                    <label><?php echo Messages::getMessage(50) ?></label>
                    <select class="middle_select hasCustomSelect" ng-model="volume"
                            ng-options="vol.label for vol in volumes">
                    </select>
                </div>
                <div class="select_row customsClearance">
                    <label><?php echo Messages::getMessage(263) ?></label>
                    <select class="middle_select hasCustomSelect" ng-model="customsClearance"
                            ng-options="cust.label for cust in customsClearanceOptions">
                    </select>
                </div>
                <div class="select_row location">
                    <label><?php echo Messages::getMessage(57) ?></label>
                    <select class="middle_select hasCustomSelect" ng-model="currentLocation"
                            ng-options="loc.label for loc in locations">
                    </select>
                </div>
                <div class="select_row subLocation">
                    <label><?php echo Messages::getMessage(58) ?></label>
                    <select class="middle_select hasCustomSelect" ng-model="currentSubLocation"
                            ng-options="subLoc.label for subLoc in currentLocation.subLocations">
                    </select>
                </div>
                <div class="left check_box" style="margin-top: -3px;">

                    <div class="checkbox_row" style="margin-top:16px;">
						 <input id="dealer" type="checkbox" ng-model="isDiller">
                        <label for="dealer" class="left"><?php echo Messages::getMessage(264) ?></label>
                    </div>
                    <div class="checkbox_row clear" >
						  <input id="person" type="checkbox" ng-model="isPerson">
                        <label for="person" class="left"><?php echo Messages::getMessage(265) ?></label>

                    </div>
                </div>
            </form>
        </div>

        <div class="form_search_box left " ng-show="showAdvancedSearch" id="filter_auto_in" style="display: none;">
		<div class="border_box">
            <div style="overflow: hidden";>
                <label><?php echo Messages::getMessage(266) ?></label>
                <div style="padding-top: 5px;">
                    <input id="identity" type="radio" ng-model="searchByType" value="ID" name="searchByType" checked="checked" style="float: left;"><label style="float: left;" for="identity">ID</label>
                    <input id="phone" type="radio" ng-model="searchByType" value="Phone" name="searchByType" style="float: left;"><label style="float: left;" for="phone"><?php echo Messages::getMessage(81) ?></label>
                </div>
                <input type="text" ng-model="searchId" ng-pattern="onlyNumbers" required title="please enter ID" x-webkit-speech lang=""/>
                <!--<div style="color: red" ng-show="showFilterByIDError">Not Valid ID</div>-->
            </div>

            <div class="checkbox_row" style="margin-bottom: -10px !important; margin-top: 29px;">
                <input id="exchange" type="checkbox" ng-model="isSwop">
                <label for="exchange" class="left"><?php echo Messages::getMessage(56) ?></label>
            </div>
            <div class="checkbox_row">
                <input id="crush" type="checkbox" ng-model="isCrashed">
                <label for="crush" class="left"><?php echo Messages::getMessage(78) ?></label>
            </div>

				</div>
            <div class="search_line">
                <div ng-click="makeSearch($event)">
                    <a href="#" class="search"><?php echo Messages::getMessage(267) ?></a>
                </div>
            </div>

            <div class="form_search_box clear">
                <div class="search_line">

                    <div ng-click="showSearchByBodyTab()">
                        <a href="#" class="serach_type"><?php echo Messages::getMessage(268) ?></a>
                    </div>

                </div>
            </div>
        </div>
        <!-- ==================================== End Advanced Search======================================== -->



        <!-- ==================================== Search By Body ======================================== -->
        <div class="car_form_box  ml10" ng-show="showSearchByBody">
            <form>

                <div id="auto_classes" ng-class="criteria.class" ng-repeat="criteria in bodyCriterias" ng-click="makeSearchByCriteria(criteria.propertyId)">
                    <a href="#"><img ng-src="{{criteria.source}}" width="{{criteria.width}}" height="{{criteria.height}}"></a>
                    <label class="car_label" ng-bind="criteria.label"></label>
                </div>

            </form>
        </div>
        <div class="form_search_box clear" ng-show="showSearchByBody" style="float:right; width: 434px; margin-top:-80px;">
            <div class="search_line">

                <div ng-click="showSimpleSearchTab()">
                    <a href="#" class="serach_type2"><?php echo Messages::getMessage(269) ?></a>
                </div>

            </div>
        </div>
        <!-- ==================================== End Search By Body ======================================== -->


    </div>
</div>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-models/services.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-controllers/index_controller.js"></script>
<!-- *********************** START TOP SEARCH PANEL************************-->
