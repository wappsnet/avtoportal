<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Yerem
 * Date: 12/8/13
 * Time: 1:12 AM
 * To change this template use File | Settings | File Templates.
 */
?>

<style>
    .logo_images img {
        margin: 1px;
    }
</style>
<div class="logo_images2">
<div class="logo_images">
    <div id="hit_marks_div">
    <ul class="left1" style="float: left;">
<!--        a-->
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/4.png" alt="" ng-click="toList('Acura', '92')">
            <a class="mark_link" href="#" ng-click="toList('Acura', '92')">Acura (<?php echo DAOFilters::getCurrentMarkCount('92') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/audi.png" alt="" ng-click="toList('Audi', '96')">
            <a class="mark_link" href="#" ng-click="toList('Audi', '96')">Audi (<?php echo DAOFilters::getCurrentMarkCount('96') ?>)</a></li>
<!--b-->
     <li class="hit_marks">
            <img class="mark_images" src="images/logo/6.png" alt="" ng-click="toList('Bentley', '99')">
            <a class="mark_link" href="#" ng-click="toList('Bentley', '99')">Bentley (<?php echo DAOFilters::getCurrentMarkCount('99') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/bmw.png" alt="" ng-click="toList('BMW', '3775')">
            <a class="mark_link" href="#" ng-click="toList('BMW', '3775')">BMW (<?php echo DAOFilters::getCurrentMarkCount('3775') ?>)</a></li>

        <!--c-->
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/chevrolet.png" alt="" ng-click="toList('Chevrolet', '109')">
            <a class="mark_link" href="#" ng-click="toList('Chevrolet', '109')">Chevrolet (<?php echo DAOFilters::getCurrentMarkCount('109') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/9.png" alt="" ng-click="toList('Chrysler', '110')">
            <a class="mark_link" href="#" ng-click="toList('Chrysler', '110')">Chrysler (<?php echo DAOFilters::getCurrentMarkCount('110') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/citroen.png" alt="" ng-click="toList('Citroen', '111')">
            <a class="mark_link" href="#" ng-click="toList('Citroen', '111')">Citroen (<?php echo DAOFilters::getCurrentMarkCount('111') ?>)</a></li>

        <!--d-->
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/daewoo.png" alt="" ng-click="toList('Daewoo', '1')">
            <a class="mark_link" href="#" ng-click="toList('Daewoo', '1')">Daewoo (<?php echo DAOFilters::getCurrentMarkCount('1') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/dodge.png" alt="" ng-click="toList('Dodge', '123')">
            <a class="mark_link" href="#" ng-click="toList('Dodge', '123')">Dodge (<?php echo DAOFilters::getCurrentMarkCount('123') ?>)</a></li>

        <!--        f-->
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/2.png" alt="" ng-click="toList('Ferrari', '127')">
            <a class="mark_link" href="#" ng-click="toList('Ferrari', '127')">Ferrari (<?php echo DAOFilters::getCurrentMarkCount('127') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/2.png" alt="" ng-click="toList('Fiat', '3107')">
            <a class="mark_link" href="#" ng-click="toList('Fiat', '3107')">Fiat (<?php echo DAOFilters::getCurrentMarkCount('3107') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/ford.png" alt="" ng-click="toList('FORD', '129')">
            <a class="mark_link" href="#" ng-click="toList('FORD', '129')">Ford (<?php echo DAOFilters::getCurrentMarkCount('129') ?>)</a></li>
<!--g-->
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/gaz.png" alt="" ng-click="toList('GAZ', '113')">
            <a class="mark_link" href="#" ng-click="toList('GAZ', '113')">GAZ (<?php echo DAOFilters::getCurrentMarkCount('113') ?>)</a></li>
<!--h-->
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/honda.png" alt="" ng-click="toList('HONDA', '137')">
            <a class="mark_link" href="#" ng-click="toList('HONDA', '137')">Honda (<?php echo DAOFilters::getCurrentMarkCount('137') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/hyundai.png" alt="" ng-click="toList('HYUNDAI', '141')">
            <a class="mark_link" href="#" ng-click="toList('HYUNDAI', '141')">Hyundai (<?php echo DAOFilters::getCurrentMarkCount('141') ?>)</a></li>
<!--i-->
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/infiniti.png" alt="" ng-click="toList('INFINITY', '144')">
            <a class="mark_link" href="#" ng-click="toList('INFINITY', '144')">Infinity (<?php echo DAOFilters::getCurrentMarkCount('144') ?>)</a></li>
<!--j-->
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/18.png" alt="" ng-click="toList('JEEP', '150')">
            <a class="mark_link" href="#" ng-click="toList('JEEP', '150')">JEEP (<?php echo DAOFilters::getCurrentMarkCount('150') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/17.png" alt="" ng-click="toList('JAGUAR', '148')">
            <a class="mark_link" href="#" ng-click="toList('JAGUAR', '148')">Jaguar (<?php echo DAOFilters::getCurrentMarkCount('148') ?>)</a></li>

        <!--k-->
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/kia.png" alt="" ng-click="toList('KIA', '154')">
            <a class="mark_link" href="#" ng-click="toList('KIA', '154')">KIA (<?php echo DAOFilters::getCurrentMarkCount('154') ?>)</a></li>
<!--l-->
        <li class="hit_marks">
            <img  class="mark_images" src="images/logo/lada.png" alt="" ng-click="toList('Lada', '212')">
            <a class="mark_link" href="#" ng-click="toList('Lada', '212')">Lada (<?php echo DAOFilters::getCurrentMarkCount('212') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/20.png" alt="" ng-click="toList('LAMBORGHINI', '156')">
            <a class="mark_link" href="#" ng-click="toList('LAMBORGHINI', '156')">Lamborghini (<?php echo DAOFilters::getCurrentMarkCount('156') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/landrover.png" alt="" ng-click="toList('Land Rover', '158')">
            <a class="mark_link" href="#" ng-click="toList('Land Rover', '158')">Land Rover (<?php echo DAOFilters::getCurrentMarkCount('158') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/lexus.png" alt="" ng-click="toList('Lexus', '1476')">
            <a class="mark_link" href="#" ng-click="toList('Lexus', '1476')">Lexus (<?php echo DAOFilters::getCurrentMarkCount('1476') ?>)</a></li>

        <!--m-->
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/mazda.png" alt="" ng-click="toList('Mazda', '1638')">
            <a class="mark_link" href="#" ng-click="toList('Mazda', '1638')">Mazda (<?php echo DAOFilters::getCurrentMarkCount('1638') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/mercedes.png" alt="" ng-click="toList('Mercedes Benz', '2955')">
            <a class="mark_link" href="#" ng-click="toList('Mercedes Benz', '2955')">Mercedes Benz (<?php echo DAOFilters::getCurrentMarkCount('2955') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/mitubishi.png" alt="" ng-click="toList('Mitsubishi', '173')">
            <a class="mark_link" href="#" ng-click="toList('Mitsubishi', '173')">Mitsubishi (<?php echo DAOFilters::getCurrentMarkCount('173') ?>)</a></li>
<!--n-->
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/nissan.png" alt="" ng-click="toList('NISSAN', '177')">
            <a class="mark_link" href="#" ng-click="toList('NISSAN', '177')">Nissan (<?php echo DAOFilters::getCurrentMarkCount('177') ?>)</a></li>
<!--o-->
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/opel.png" alt="" ng-click="toList('Opel', '181')">
            <a class="mark_link" href="#" ng-click="toList('Opel', '181')">Opel (<?php echo DAOFilters::getCurrentMarkCount('181') ?>)</a></li>
<!--p-->
        <li class="hit_marks">
            <!--<img src="images/logo/29.png" alt="" ng-click="toList('NIVA', '212')">-->
            <img class="mark_images" src="images/logo/peugeot.png" alt="" ng-click="toList('Peugeot', '183')">
            <a class="mark_link" href="#" ng-click="toList('Peugeot', '183')">Peugeot (<?php echo DAOFilters::getCurrentMarkCount('183') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/porsche.png" alt="" ng-click="toList('Porsche', '186')">
            <a class="mark_link" href="#" ng-click="toList('Porsche', '186')">Porsche (<?php echo DAOFilters::getCurrentMarkCount('186') ?>)</a></li>
<!--r-->
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/renault.png" alt="" ng-click="toList('Renault', '188')">
            <a class="mark_link" href="#" ng-click="toList('Renault', '188')">Renault (<?php echo DAOFilters::getCurrentMarkCount('188') ?>)</a></li>
<!--s-->
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/shkoda.png" alt="" ng-click="toList('Skoda', '201')">
            <a class="mark_link" href="#" ng-click="toList('Skoda', '201')">Skoda (<?php echo DAOFilters::getCurrentMarkCount('201') ?>)</a></li>
        <li class="hit_marks">
    <img class="mark_images" src="images/logo/subaru.png" alt="" ng-click="toList('Subaru', '204')"/>
        <a class="mark_link" href="#" ng-click="toList('Subaru', '204')">Subaru (<?php echo DAOFilters::getCurrentMarkCount('204') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/suzuki.png" alt="" ng-click="toList('Suzuki', '205')">
            <a class="mark_link" href="#" ng-click="toList('Suzuki', '205')">Suzuki (<?php echo DAOFilters::getCurrentMarkCount('205') ?>)</a></li>
<!--t-->
        <li class="hit_marks">
    <img class="mark_images" src="images/logo/toyota.png" alt="" ng-click="toList('TOYOTA', '209')">
            <a class="mark_link" href="#" ng-click="toList('TOYOTA', '209')">Toyota (<?php echo DAOFilters::getCurrentMarkCount('209') ?>)</a></li>
<!--  v      -->
        <li id="marks_block">
            <a id="all_marks" style="cursor: pointer">Все марки</a>
        </li>

        <li class="hit_marks">
            <img class="mark_images" src="images/logo/volkswagen.png" alt="" ng-click="toList('Volkswagen', '213')">
            <a class="mark_link" href="#" ng-click="toList('Volkswagen', '213')">Volkswagen (<?php echo DAOFilters::getCurrentMarkCount('213') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/volvo.png" alt="" ng-click="toList('Volvo', '112')">
            <a class="mark_link" href="#" ng-click="toList('Volvo', '112')">Volvo (<?php echo DAOFilters::getCurrentMarkCount('112') ?>)</a></li>
<!--u-->
        <li class="hit_marks">
    <img class="mark_images" src="images/logo/uaz.png" alt="" ng-click="toList('UAZ', '210')">
            <a class="mark_link" href="#" ng-click="toList('UAZ', '210')">UAZ (<?php echo DAOFilters::getCurrentMarkCount('210') ?>)</a></li>
        <!--        <li class="hit_marks">-->
<!--    <img class="mark_images" src="images/logo/lancia.png" alt="" ng-click="toList('Lancia', '1420')">-->

    <!--<img src="images/logo/32.png" alt="" ng-click="toList('SAAB', '191')">
    <img src="images/logo/33.png" alt="" ng-click="toList('SKODA', '201')">
    <img src="images/logo/34.png" alt="" ng-click="toList('SUBARU', '204')">
    <img src="images/logo/35.png" alt="" ng-click="toList('SUZUKI', '205')">-->
    </ul>
    </div>
    <div id="all_marks_div">

        <ul class="left1" style="float: left;">
        <!--        a-->
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/ac.jpg" alt="" ng-click="toList('AC', '3843')">
            <a class="mark_link" href="#" ng-click="toList('AC', '3843')">AC (<?php echo DAOFilters::getCurrentMarkCount('3843') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/4.png" alt="" ng-click="toList('Acura', '92')">
            <a class="mark_link" href="#" ng-click="toList('Acura', '92')">Acura (<?php echo DAOFilters::getCurrentMarkCount('92') ?>)</a></li>
       <li class="hit_marks">
            <img class="mark_images" src="images/logo/admiral.jpg" alt="" ng-click="toList('Admiral', '3764')">
            <a class="mark_link" href="#" ng-click="toList('Admiral', '3764')">Admiral (<?php echo DAOFilters::getCurrentMarkCount('3764') ?>)</a></li>
            <li class="hit_marks">
                <img class="mark_images" src="images/logo/alfaromeo.gif" alt="" ng-click="toList('Alfa Romeo', '93')">
                <a class="mark_link" href="#" ng-click="toList('Alfa Romeo', '93')">Alfa Romeo (<?php echo DAOFilters::getCurrentMarkCount('93') ?>)</a></li>
            <li class="hit_marks">
                <img class="mark_images" src="images/logo/alpina.jpg" alt="" ng-click="toList('Alpina', '3766')">
                <a class="mark_link" href="#" ng-click="toList('Alpina', '3766')">Alpina (<?php echo DAOFilters::getCurrentMarkCount('3766') ?>)</a></li>
            <li class="hit_marks">
                <img class="mark_images" src="images/logo/alpine.jpg" alt="" ng-click="toList('Alpine', '3914')">
                <a class="mark_link" href="#" ng-click="toList('Alpine', '3914')">Alpine (<?php echo DAOFilters::getCurrentMarkCount('3914') ?>)</a></li>
            <li class="hit_marks">
                <img class="mark_images" src="images/logo/amc.jpg" alt="" ng-click="toList('AMC', '3915')">
                <a class="mark_link" href="#" ng-click="toList('AMC', '3915')">AMC (<?php echo DAOFilters::getCurrentMarkCount('3915') ?>)</a></li>
            <li class="hit_marks">
                <img class="mark_images" src="images/logo/ariel.jpg" alt="" ng-click="toList('Ariel', '3916')">
                <a class="mark_link" href="#" ng-click="toList('Ariel', '3916')">Ariel (<?php echo DAOFilters::getCurrentMarkCount('3916') ?>)</a></li>
            <li class="hit_marks">
                <img class="mark_images" src="images/logo/aro.jpg" alt="" ng-click="toList('ARO', '3762')">
                <a class="mark_link" href="#" ng-click="toList('ARO', '3762')">ARO (<?php echo DAOFilters::getCurrentMarkCount('3762') ?>)</a></li>
            <li class="hit_marks">
                <img class="mark_images" src="images/logo/asia.jpg" alt="" ng-click="toList('Asia Motors', '423')">
                <a class="mark_link" href="#" ng-click="toList('Asia Motors', '423')">Asia Motors (<?php echo DAOFilters::getCurrentMarkCount('423') ?>)</a></li>
            <li class="hit_marks">
                <img class="mark_images" src="images/logo/aston.jpg" alt="" ng-click="toList('Aston Martin', '95')">
                <a class="mark_link" href="#" ng-click="toList('Aston Martin', '95')">Aston Martin (<?php echo DAOFilters::getCurrentMarkCount('95') ?>)</a></li>
            <li class="hit_marks">
            <img class="mark_images" src="images/logo/audi.png" alt="" ng-click="toList('Audi', '96')">
            <a class="mark_link" href="#" ng-click="toList('Audi', '96')">Audi (<?php echo DAOFilters::getCurrentMarkCount('96') ?>)</a></li>
            <li class="hit_marks">
                <img class="mark_images" src="images/logo/austin.jpg" alt="" ng-click="toList('Austin', '3849')">
                <a class="mark_link" href="#" ng-click="toList('Austin', '3849')">Austin (<?php echo DAOFilters::getCurrentMarkCount('3849') ?>)</a></li>
            <li class="hit_marks">
                <img class="mark_images" src="images/logo/austinH.jpg" alt="" ng-click="toList('Austin Healey', '3917')">
                <a class="mark_link" href="#" ng-click="toList('Austin Healey', '3917')">Austin Healey (<?php echo DAOFilters::getCurrentMarkCount('3917') ?>)</a></li>
            <li class="hit_marks">
                <img class="mark_images" src="images/logo/auotobianci.jpg" alt="" ng-click="toList('Auotobianci', '3859')">
                <a class="mark_link" href="#" ng-click="toList('Auotobianci', '3859')">Auotobianci (<?php echo DAOFilters::getCurrentMarkCount('3859') ?>)</a></li>
            <li class="hit_marks">
                <img class="mark_images" src="images/logo/avia.jpg" alt="" ng-click="toList('AVIA', '97')">
                <a class="mark_link" href="#" ng-click="toList('AVIA', '97')">AVIA (<?php echo DAOFilters::getCurrentMarkCount('97') ?>)</a></li>

            <!--b-->
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/baw.jpg" alt="" ng-click="toList('BAW', '3984')">
            <a class="mark_link" href="#" ng-click="toList('BAW', '3984')">BAW (<?php echo DAOFilters::getCurrentMarkCount('3984') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/bBeijing.jpg" alt="" ng-click="toList('Beijing', '3865')">
            <a class="mark_link" href="#" ng-click="toList('Beijing', '3865')">Beijing (<?php echo DAOFilters::getCurrentMarkCount('3865') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/6.png" alt="" ng-click="toList('Bentley', '99')">
            <a class="mark_link" href="#" ng-click="toList('Bentley', '99')">Bentley (<?php echo DAOFilters::getCurrentMarkCount('99') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/bmw.png" alt="" ng-click="toList('BMW', '3775')">
            <a class="mark_link" href="#" ng-click="toList('BMW', '3775')">BMW (<?php echo DAOFilters::getCurrentMarkCount('3775') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/brilliance.jpg" alt="" ng-click="toList('Brilliance', '3877')">
            <a class="mark_link" href="#" ng-click="toList('Brilliance', '3877')">Brilliance (<?php echo DAOFilters::getCurrentMarkCount('3877') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/bristol.jpg" alt="" ng-click="toList('Bristol', '3885')">
            <a class="mark_link" href="#" ng-click="toList('Bristol', '3885')">Bristol (<?php echo DAOFilters::getCurrentMarkCount('3885') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/bBugatti.jpg" alt="" ng-click="toList('Bugatti', '104')">
            <a class="mark_link" href="#" ng-click="toList('Bugatti', '104')">Bugatti (<?php echo DAOFilters::getCurrentMarkCount('104') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/buick.jpg" alt="" ng-click="toList('Buick', '402')">
            <a class="mark_link" href="#" ng-click="toList('Buick', '402')">Buick (<?php echo DAOFilters::getCurrentMarkCount('402') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/byd.jpg" alt="" ng-click="toList('BYD', '106')">
            <a class="mark_link" href="#" ng-click="toList('BYD', '106')">BYD (<?php echo DAOFilters::getCurrentMarkCount('106') ?>)</a></li>
        <!--c-->
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/Cadilac.jpg" alt="" ng-click="toList('Cadilac', '410')">
            <a class="mark_link" href="#" ng-click="toList('Cadilac', '410')">Cadilac (<?php echo DAOFilters::getCurrentMarkCount('410') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/Callaway.png" alt="" ng-click="toList('Callaway', '3895')">
            <a class="mark_link" href="#" ng-click="toList('Callaway', '3895')">Callaway (<?php echo DAOFilters::getCurrentMarkCount('3895') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/Carbodies.jpg" alt="" ng-click="toList('Carbodies', '3898')">
            <a class="mark_link" href="#" ng-click="toList('Carbodies', '3898')">Carbodies (<?php echo DAOFilters::getCurrentMarkCount('3898') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/Caterham.jpg" alt="" ng-click="toList('Caterham', '3904')">
            <a class="mark_link" href="#" ng-click="toList('Caterham', '3904')">Caterham (<?php echo DAOFilters::getCurrentMarkCount('3904') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/Chana.jpg" alt="" ng-click="toList('Chana', '3909')">
            <a class="mark_link" href="#" ng-click="toList('Chana', '3909')">Chana (<?php echo DAOFilters::getCurrentMarkCount('3904') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/Changan.jpg" alt="" ng-click="toList('Changan', '3985')">
            <a class="mark_link" href="#" ng-click="toList('Changan', '3985')">Changan (<?php echo DAOFilters::getCurrentMarkCount('3985') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/ChangFeng.jpg" alt="" ng-click="toList('ChangFeng', '3986')">
            <a class="mark_link" href="#" ng-click="toList('ChangFeng', '3986')">ChangFeng (<?php echo DAOFilters::getCurrentMarkCount('3986') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/Changhe.jpg" alt="" ng-click="toList('Changhe', '3987')">
            <a class="mark_link" href="#" ng-click="toList('Changhe', '3987')">Changhe (<?php echo DAOFilters::getCurrentMarkCount('3987') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/Chery.jpg" alt="" ng-click="toList('Chery', '108')">
            <a class="mark_link" href="#" ng-click="toList('Chery', '108')">Chery (<?php echo DAOFilters::getCurrentMarkCount('108') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/chevrolet.png" alt="" ng-click="toList('Chevrolet', '109')">
            <a class="mark_link" href="#" ng-click="toList('Chevrolet', '109')">Chevrolet (<?php echo DAOFilters::getCurrentMarkCount('109') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/9.png" alt="" ng-click="toList('Chrysler', '110')">
            <a class="mark_link" href="#" ng-click="toList('Chrysler', '110')">Chrysler (<?php echo DAOFilters::getCurrentMarkCount('110') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/citroen.png" alt="" ng-click="toList('Citroen', '111')">
            <a class="mark_link" href="#" ng-click="toList('Citroen', '111')">Citroen (<?php echo DAOFilters::getCurrentMarkCount('111') ?>)</a></li>

        <!--d-->
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/Dacia.jpg" alt="" ng-click="toList('Dacia', '118')">
            <a class="mark_link" href="#" ng-click="toList('Dacia', '118')">Dacia (<?php echo DAOFilters::getCurrentMarkCount('118') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/Dabi.jpg" alt="" ng-click="toList('Dabi', '3989')">
            <a class="mark_link" href="#" ng-click="toList('Dabi', '3989')">Dabi (<?php echo DAOFilters::getCurrentMarkCount('3989') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/daewoo.png" alt="" ng-click="toList('Daewoo', '1')">
            <a class="mark_link" href="#" ng-click="toList('Daewoo', '1')">Daewoo (<?php echo DAOFilters::getCurrentMarkCount('1') ?>)</a></li>

        <li class="hit_marks">
            <img class="mark_images" src="images/logo/Daihatsu.jpg" alt="" ng-click="toList('Daihatsu', '122')">
            <a class="mark_link" href="#" ng-click="toList('Daihatsu', '122')">Daihatsu (<?php echo DAOFilters::getCurrentMarkCount('122') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/Daimler.jpg" alt="" ng-click="toList('Daimler', '1237')">
            <a class="mark_link" href="#" ng-click="toList('Daimler', '1237')">Daimler (<?php echo DAOFilters::getCurrentMarkCount('1237') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/Dallas.jpg" alt="" ng-click="toList('Dallas', '3990')">
            <a class="mark_link" href="#" ng-click="toList('Dallas', '3990')">Dallas (<?php echo DAOFilters::getCurrentMarkCount('3990') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/Datsun.jpg" alt="" ng-click="toList('Datsun', '2083')">
            <a class="mark_link" href="#" ng-click="toList('Datsun', '2083')">Datsun (<?php echo DAOFilters::getCurrentMarkCount('2083') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/Delorean.jpg" alt="" ng-click="toList('Delorean', '3991')">
            <a class="mark_link" href="#" ng-click="toList('Delorean', '3991')">Delorean (<?php echo DAOFilters::getCurrentMarkCount('3991') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/DeTomaso.jpg" alt="" ng-click="toList('De Tomaso', '3992')">
            <a class="mark_link" href="#" ng-click="toList('De Tomaso', '3992')">De Tomaso (<?php echo DAOFilters::getCurrentMarkCount('3992') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/DFSK.jpg" alt="" ng-click="toList('DFSK', '3993')">
            <a class="mark_link" href="#" ng-click="toList('DFSK', '3993')">DFSK (<?php echo DAOFilters::getCurrentMarkCount('3993') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/Dodge.jpg" alt="" ng-click="toList('Dodge', '123')">
            <a class="mark_link" href="#" ng-click="toList('Dodge', '123')">Dodge (<?php echo DAOFilters::getCurrentMarkCount('123') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/DongFeng.jpg" alt="" ng-click="toList('DongFeng', '3994')">
            <a class="mark_link" href="#" ng-click="toList('DongFeng', '3994')">DongFeng (<?php echo DAOFilters::getCurrentMarkCount('3994') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/Doninvest.jpg" alt="" ng-click="toList('Doninvest', '3995')">
            <a class="mark_link" href="#" ng-click="toList('Doninvest', '3995')">Doninvest (<?php echo DAOFilters::getCurrentMarkCount('3995') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/Derways.jpg" alt="" ng-click="toList('Derways', '3997')">
            <a class="mark_link" href="#" ng-click="toList('Derways', '3997')">Derways (<?php echo DAOFilters::getCurrentMarkCount('3997') ?>)</a></li>

           <!--E-->
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/Eagle.jpg" alt="" ng-click="toList('Eagle', '125')">
            <a class="mark_link" href="#" ng-click="toList('Eagle', '125')">Eagele (<?php echo DAOFilters::getCurrentMarkCount('125') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/Efini.jpg" alt="" ng-click="toList('Efini', '3998')">
            <a class="mark_link" href="#" ng-click="toList('Efini', '3998')">Efini (<?php echo DAOFilters::getCurrentMarkCount('3998') ?>)</a></li>

        <!--        f-->
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/2.png" alt="" ng-click="toList('FAW', '3999')">
            <a class="mark_link" href="#" ng-click="toList('FAW', '3999')">FAW (<?php echo DAOFilters::getCurrentMarkCount('3999') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/2.png" alt="" ng-click="toList('Ferrari', '127')">
            <a class="mark_link" href="#" ng-click="toList('Ferrari', '127')">Ferrari (<?php echo DAOFilters::getCurrentMarkCount('127') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/2.png" alt="" ng-click="toList('Fiat', '128')">
            <a class="mark_link" href="#" ng-click="toList('Fiat', '128')">Fiat (<?php echo DAOFilters::getCurrentMarkCount('128') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/2.png" alt="" ng-click="toList('Fisher', '4000')">
            <a class="mark_link" href="#" ng-click="toList('Fisher', '4000')">Fisher (<?php echo DAOFilters::getCurrentMarkCount('4000') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/ford.png" alt="" ng-click="toList('FORD', '129')">
            <a class="mark_link" href="#" ng-click="toList('FORD', '129')">Ford (<?php echo DAOFilters::getCurrentMarkCount('129') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/2.png" alt="" ng-click="toList('Foton', '130')">
            <a class="mark_link" href="#" ng-click="toList('Foton', '130')">Foton (<?php echo DAOFilters::getCurrentMarkCount('130') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/2.png" alt="" ng-click="toList('FSO', '4001')">
            <a class="mark_link" href="#" ng-click="toList('FSO', '4001')">FSO (<?php echo DAOFilters::getCurrentMarkCount('4001') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/2.png" alt="" ng-click="toList('Fuqi', '4002')">
            <a class="mark_link" href="#" ng-click="toList('Fuqi', '4002')">Fuqi (<?php echo DAOFilters::getCurrentMarkCount('4002') ?>)</a></li>
         <!--g-->
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/gaz.png" alt="" ng-click="toList('GAZ', '113')">
            <a class="mark_link" href="#" ng-click="toList('GAZ', '113')">GAZ (<?php echo DAOFilters::getCurrentMarkCount('113') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/gaz.png" alt="" ng-click="toList('Geely', '132')">
            <a class="mark_link" href="#" ng-click="toList('Geely', '132')">GAZ (<?php echo DAOFilters::getCurrentMarkCount('132') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/gaz.png" alt="" ng-click="toList('Geo', '4003')">
            <a class="mark_link" href="#" ng-click="toList('Geo', '4003')">Geo (<?php echo DAOFilters::getCurrentMarkCount('4003') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/gaz.png" alt="" ng-click="toList('GMC', '133')">
            <a class="mark_link" href="#" ng-click="toList('GMC', '133')">GMC (<?php echo DAOFilters::getCurrentMarkCount('133') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/gaz.png" alt="" ng-click="toList('Great Wall', '134')">
            <a class="mark_link" href="#" ng-click="toList('Great Wall', '134')">Great Wall (<?php echo DAOFilters::getCurrentMarkCount('134') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/gaz.png" alt="" ng-click="toList('Groz', '4004')">
            <a class="mark_link" href="#" ng-click="toList('Groz', '4004')">Groz (<?php echo DAOFilters::getCurrentMarkCount('4004') ?>)</a></li>

        <!--h-->
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/honda.png" alt="" ng-click="toList('Hafei', '4005')">
            <a class="mark_link" href="#" ng-click="toList('Hafei', '4005')">Hafei (<?php echo DAOFilters::getCurrentMarkCount('4005') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/honda.png" alt="" ng-click="toList('Haima', '4006')">
            <a class="mark_link" href="#" ng-click="toList('Haima', '4006')">Haima (<?php echo DAOFilters::getCurrentMarkCount('4006') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/honda.png" alt="" ng-click="toList('Haval', '4007')">
            <a class="mark_link" href="#" ng-click="toList('Haval', '4007')">Haval (<?php echo DAOFilters::getCurrentMarkCount('4007') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/honda.png" alt="" ng-click="toList('Hindustan', '4008')">
            <a class="mark_link" href="#" ng-click="toList('Hindustan', '4008')">Hindustan (<?php echo DAOFilters::getCurrentMarkCount('4008') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/honda.png" alt="" ng-click="toList('HIVO', '4009')">
            <a class="mark_link" href="#" ng-click="toList('HIVO', '4009')">HIVA (<?php echo DAOFilters::getCurrentMarkCount('4009') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/honda.png" alt="" ng-click="toList('Holden', '4010')">
            <a class="mark_link" href="#" ng-click="toList('Holden', '4010')">Holden (<?php echo DAOFilters::getCurrentMarkCount('4010') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/honda.png" alt="" ng-click="toList('HONDA', '137')">
            <a class="mark_link" href="#" ng-click="toList('HONDA', '137')">Honda (<?php echo DAOFilters::getCurrentMarkCount('137') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/honda.png" alt="" ng-click="toList('HuangHai', '4011')">
            <a class="mark_link" href="#" ng-click="toList('HuangHai', '4011')">HuangHai (<?php echo DAOFilters::getCurrentMarkCount('4011') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/honda.png" alt="" ng-click="toList('Hummer', '140')">
            <a class="mark_link" href="#" ng-click="toList('Hummer', '140')">Hummer (<?php echo DAOFilters::getCurrentMarkCount('140') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/hyundai.png" alt="" ng-click="toList('HYUNDAI', '141')">
            <a class="mark_link" href="#" ng-click="toList('HYUNDAI', '141')">Hyundai (<?php echo DAOFilters::getCurrentMarkCount('141') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/hyundai.png" alt="" ng-click="toList('HYUNDAI', '141')">
            <a class="mark_link" href="#" ng-click="toList('HYUNDAI', '141')">Hyundai (<?php echo DAOFilters::getCurrentMarkCount('141') ?>)</a></li>
        <!--i-->
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/infiniti.png" alt="" ng-click="toList('INFINITY', '144')">
            <a class="mark_link" href="#" ng-click="toList('INFINITY', '144')">Infinity (<?php echo DAOFilters::getCurrentMarkCount('144') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/infiniti.png" alt="" ng-click="toList('Innocenti', '4012')">
            <a class="mark_link" href="#" ng-click="toList('Innocenti', '4012')">Innocenti (<?php echo DAOFilters::getCurrentMarkCount('4012') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/infiniti.png" alt="" ng-click="toList('Invicta', '4013')">
            <a class="mark_link" href="#" ng-click="toList('Invicta', '4013')">Invicta (<?php echo DAOFilters::getCurrentMarkCount('4013') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/infiniti.png" alt="" ng-click="toList('Isdera', '4015')">
            <a class="mark_link" href="#" ng-click="toList('Isdera', '4015')">Isdera (<?php echo DAOFilters::getCurrentMarkCount('4015') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/infiniti.png" alt="" ng-click="toList('Isuzu', '145')">
            <a class="mark_link" href="#" ng-click="toList('Isuzu', '145')">Isuzu (<?php echo DAOFilters::getCurrentMarkCount('145') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/infiniti.png" alt="" ng-click="toList('IVECO', '146')">
            <a class="mark_link" href="#" ng-click="toList('IVECO', '146')">IVECO (<?php echo DAOFilters::getCurrentMarkCount('146') ?>)</a></li>
        <!--j-->
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/18.png" alt="" ng-click="toList('JAC', '4016')">
            <a class="mark_link" href="#" ng-click="toList('JAC', '4016')">JAC (<?php echo DAOFilters::getCurrentMarkCount('4016') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/17.png" alt="" ng-click="toList('JAGUAR', '148')">
            <a class="mark_link" href="#" ng-click="toList('JAGUAR', '148')">Jaguar (<?php echo DAOFilters::getCurrentMarkCount('148') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/18.png" alt="" ng-click="toList('JEEP', '150')">
            <a class="mark_link" href="#" ng-click="toList('JEEP', '150')">JEEP (<?php echo DAOFilters::getCurrentMarkCount('150') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/17.png" alt="" ng-click="toList('Jiangran', '4017')">
            <a class="mark_link" href="#" ng-click="toList('Jiangran', '4017')">Jiangran (<?php echo DAOFilters::getCurrentMarkCount('4017') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/17.png" alt="" ng-click="toList('Jinbei', '4018')">
            <a class="mark_link" href="#" ng-click="toList('Jinbei', '4018')">Jinbei (<?php echo DAOFilters::getCurrentMarkCount('4018') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/17.png" alt="" ng-click="toList('JMC', '4019')">
            <a class="mark_link" href="#" ng-click="toList('Jinbei', '4019')">JMC (<?php echo DAOFilters::getCurrentMarkCount('4019') ?>)</a></li>
        <!--k-->
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/kia.png" alt="" ng-click="toList('Koenigsegg', '4020')">
            <a class="mark_link" href="#" ng-click="toList('Koenigsegg', '4020')">Koenigsegg (<?php echo DAOFilters::getCurrentMarkCount('4020') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/kia.png" alt="" ng-click="toList('KIA', '154')">
            <a class="mark_link" href="#" ng-click="toList('KIA', '154')">KIA (<?php echo DAOFilters::getCurrentMarkCount('154') ?>)</a></li>
        <!--l-->
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/kia.png" alt="" ng-click="toList('Lancia', '1420')">
            <a class="mark_link" href="#" ng-click="toList('Lancia', '1420')">KIA (<?php echo DAOFilters::getCurrentMarkCount('1420') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/kia.png" alt="" ng-click="toList('Landwind', '4021')">
            <a class="mark_link" href="#" ng-click="toList('Landwind', '4021')">Landwid (<?php echo DAOFilters::getCurrentMarkCount('4021') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/kia.png" alt="" ng-click="toList('LDV', '4022')">
            <a class="mark_link" href="#" ng-click="toList('LDV', '4022')">LDV (<?php echo DAOFilters::getCurrentMarkCount('4022') ?>)</a></li>
        <li class="hit_marks">
            <img  class="mark_images" src="images/logo/lada.png" alt="" ng-click="toList('Lada', '212')">
            <a class="mark_link" href="#" ng-click="toList('Lada', '212')">Lada (<?php echo DAOFilters::getCurrentMarkCount('212') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/20.png" alt="" ng-click="toList('LAMBORGHINI', '156')">
            <a class="mark_link" href="#" ng-click="toList('LAMBORGHINI', '156')">Lamborghini (<?php echo DAOFilters::getCurrentMarkCount('156') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/landrover.png" alt="" ng-click="toList('Land Rover', '158')">
            <a class="mark_link" href="#" ng-click="toList('Land Rover', '158')">Land Rover (<?php echo DAOFilters::getCurrentMarkCount('158') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/lexus.png" alt="" ng-click="toList('Lexus', '1476')">
            <a class="mark_link" href="#" ng-click="toList('Lexus', '1476')">Lexus (<?php echo DAOFilters::getCurrentMarkCount('1476') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/lexus.png" alt="" ng-click="toList('Lifan', '4023')">
            <a class="mark_link" href="#" ng-click="toList('Lifan', '4023')">Lifan (<?php echo DAOFilters::getCurrentMarkCount('4023') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/lexus.png" alt="" ng-click="toList('Lincoln', '161')">
            <a class="mark_link" href="#" ng-click="toList('Lincoln', '161')">Lincoln (<?php echo DAOFilters::getCurrentMarkCount('161') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/lexus.png" alt="" ng-click="toList('Lotus', '1529')">
            <a class="mark_link" href="#" ng-click="toList('Lotus', '1529')">Lotus (<?php echo DAOFilters::getCurrentMarkCount('1529') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/lexus.png" alt="" ng-click="toList('LTI', '4024')">
            <a class="mark_link" href="#" ng-click="toList('LTI', '4024')">LTI (<?php echo DAOFilters::getCurrentMarkCount('4024') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/lexus.png" alt="" ng-click="toList('Luxgen', '4025')">
            <a class="mark_link" href="#" ng-click="toList('Luxgen', '4025')">Luxgen (<?php echo DAOFilters::getCurrentMarkCount('4025') ?>)</a></li>

        <!--m-->
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/mazda.png" alt="" ng-click="toList('Mahindra', '4026')">
            <a class="mark_link" href="#" ng-click="toList('Mahindra', '4026')">Mahindra (<?php echo DAOFilters::getCurrentMarkCount('4026') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/mazda.png" alt="" ng-click="toList('Marcos', '4028')">
            <a class="mark_link" href="#" ng-click="toList('Marcos', '4028')">Marcos (<?php echo DAOFilters::getCurrentMarkCount('4028') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/mazda.png" alt="" ng-click="toList('Marlin', '4029')">
            <a class="mark_link" href="#" ng-click="toList('Marlin', '4029')">Marlin (<?php echo DAOFilters::getCurrentMarkCount('4029') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/mazda.png" alt="" ng-click="toList('Maruti', '4030')">
            <a class="mark_link" href="#" ng-click="toList('Maruti', '4030')">Maruti (<?php echo DAOFilters::getCurrentMarkCount('4030') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/mazda.png" alt="" ng-click="toList('Maserati', '165')">
            <a class="mark_link" href="#" ng-click="toList('Maserati', '165')">Maserati (<?php echo DAOFilters::getCurrentMarkCount('165') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/mazda.png" alt="" ng-click="toList('Maxus', '4031')">
            <a class="mark_link" href="#" ng-click="toList('Maxus', '4031')">Maxus (<?php echo DAOFilters::getCurrentMarkCount('4031') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/mazda.png" alt="" ng-click="toList('Maybach', '166')">
            <a class="mark_link" href="#" ng-click="toList('Maybach', '166')">Maybach (<?php echo DAOFilters::getCurrentMarkCount('166') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/mazda.png" alt="" ng-click="toList('Mazda', '1638')">
            <a class="mark_link" href="#" ng-click="toList('Mazda', '1638')">Mazda (<?php echo DAOFilters::getCurrentMarkCount('1638') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/mazda.png" alt="" ng-click="toList('Mclaren', '4032')">
            <a class="mark_link" href="#" ng-click="toList('Mclaren', '4032')">Mclaren (<?php echo DAOFilters::getCurrentMarkCount('4032') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/mazda.png" alt="" ng-click="toList('Mega', '4033')">
            <a class="mark_link" href="#" ng-click="toList('Mega', '4033')">Mega (<?php echo DAOFilters::getCurrentMarkCount('4033') ?>)</a></li>
       <li class="hit_marks">
            <img class="mark_images" src="images/logo/mercedes.png" alt="" ng-click="toList('Mercedes Benz', '2955')">
            <a class="mark_link" href="#" ng-click="toList('Mercedes Benz', '2955')">Mercedes Benz (<?php echo DAOFilters::getCurrentMarkCount('2955') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/mitubishi.png" alt="" ng-click="toList('Mercury', '170')">
            <a class="mark_link" href="#" ng-click="toList('Mercury', '170')">Mercury (<?php echo DAOFilters::getCurrentMarkCount('170') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/mitubishi.png" alt="" ng-click="toList('Metrocab', '4034')">
            <a class="mark_link" href="#" ng-click="toList('Metrocab', '4034')">Metrocab (<?php echo DAOFilters::getCurrentMarkCount('4034') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/mitubishi.png" alt="" ng-click="toList('MG', '171')">
            <a class="mark_link" href="#" ng-click="toList('MG', '171')">MG (<?php echo DAOFilters::getCurrentMarkCount('171') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/mitubishi.png" alt="" ng-click="toList('Minelli', '4035')">
            <a class="mark_link" href="#" ng-click="toList('Minelli', '4035')">Minelli (<?php echo DAOFilters::getCurrentMarkCount('4035') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/mitubishi.png" alt="" ng-click="toList('Mini', '172')">
            <a class="mark_link" href="#" ng-click="toList('Mini', '172')">Mini (<?php echo DAOFilters::getCurrentMarkCount('4035') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/mitubishi.png" alt="" ng-click="toList('Mitsubishi', '173')">
            <a class="mark_link" href="#" ng-click="toList('Mitsubishi', '173')">Mitsubishi (<?php echo DAOFilters::getCurrentMarkCount('173') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/mitubishi.png" alt="" ng-click="toList('Mitsuoka', '4036')">
            <a class="mark_link" href="#" ng-click="toList('Mitsuoka', '4036')">Mitsuoka (<?php echo DAOFilters::getCurrentMarkCount('4036') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/mitubishi.png" alt="" ng-click="toList('Monte Carlo', '4037')">
            <a class="mark_link" href="#" ng-click="toList('Monte Carlo', '4037')">Monte Carlo (<?php echo DAOFilters::getCurrentMarkCount('4037') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/mitubishi.png" alt="" ng-click="toList('Morgan', '4038')">
            <a class="mark_link" href="#" ng-click="toList('Morgan', '4038')">Morgan (<?php echo DAOFilters::getCurrentMarkCount('4038') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/mitubishi.png" alt="" ng-click="toList('Marussia', '4039')">
            <a class="mark_link" href="#" ng-click="toList('Marussia', '4039')">Marussi (<?php echo DAOFilters::getCurrentMarkCount('4039') ?>)</a></li>

        <!--n-->
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/nissan.png" alt="" ng-click="toList('NAVECO', '4040')">
            <a class="mark_link" href="#" ng-click="toList('NAVECO', '4040')">NAVECO (<?php echo DAOFilters::getCurrentMarkCount('4040') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/nissan.png" alt="" ng-click="toList('NISSAN', '177')">
            <a class="mark_link" href="#" ng-click="toList('NISSAN', '177')">Nissan (<?php echo DAOFilters::getCurrentMarkCount('177') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/nissan.png" alt="" ng-click="toList('Noble', '4044')">
            <a class="mark_link" href="#" ng-click="toList('Noble', '4044')">Noble (<?php echo DAOFilters::getCurrentMarkCount('4044') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/nissan.png" alt="" ng-click="toList('Nysa', '4047')">
            <a class="mark_link" href="#" ng-click="toList('Nysa', '4047')">Nysa (<?php echo DAOFilters::getCurrentMarkCount('4047') ?>)</a></li>
        <!--o-->
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/nissan.png" alt="" ng-click="toList('Oldsmobile', '180')">
            <a class="mark_link" href="#" ng-click="toList('Oldsmobile', '180')">Oldsmobile (<?php echo DAOFilters::getCurrentMarkCount('180') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/opel.png" alt="" ng-click="toList('Opel', '181')">
            <a class="mark_link" href="#" ng-click="toList('Opel', '181')">Opel (<?php echo DAOFilters::getCurrentMarkCount('181') ?>)</a></li>
        <!--p-->
        <li class="hit_marks">
            <!--<img src="images/logo/29.png" alt="" ng-click="toList('NIVA', '212')">-->
            <img class="mark_images" src="images/logo/peugeot.png" alt="" ng-click="toList('Peugeot', '183')">
            <a class="mark_link" href="#" ng-click="toList('Peugeot', '183')">Peugeot (<?php echo DAOFilters::getCurrentMarkCount('183') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/porsche.png" alt="" ng-click="toList('Porsche', '186')">
            <a class="mark_link" href="#" ng-click="toList('Porsche', '186')">Porsche (<?php echo DAOFilters::getCurrentMarkCount('186') ?>)</a></li>
        <!--r-->
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/renault.png" alt="" ng-click="toList('Renault', '188')">
            <a class="mark_link" href="#" ng-click="toList('Renault', '188')">Renault (<?php echo DAOFilters::getCurrentMarkCount('188') ?>)</a></li>
        <!--s-->
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/shkoda.png" alt="" ng-click="toList('Skoda', '201')">
            <a class="mark_link" href="#" ng-click="toList('Skoda', '201')">Skoda (<?php echo DAOFilters::getCurrentMarkCount('201') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/subaru.png" alt="" ng-click="toList('Subaru', '204')"/>
            <a class="mark_link" href="#" ng-click="toList('Subaru', '204')">Subaru (<?php echo DAOFilters::getCurrentMarkCount('204') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/suzuki.png" alt="" ng-click="toList('Suzuki', '205')">
            <a class="mark_link" href="#" ng-click="toList('Suzuki', '205')">Suzuki (<?php echo DAOFilters::getCurrentMarkCount('205') ?>)</a></li>
        <!--t-->
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/toyota.png" alt="" ng-click="toList('TOYOTA', '209')">
            <a class="mark_link" href="#" ng-click="toList('TOYOTA', '209')">Toyota (<?php echo DAOFilters::getCurrentMarkCount('209') ?>)</a></li>
        <!--  v      -->
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/volkswagen.png" alt="" ng-click="toList('Volkswagen', '213')">
            <a class="mark_link" href="#" ng-click="toList('Volkswagen', '213')">Volkswagen (<?php echo DAOFilters::getCurrentMarkCount('213') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/volvo.png" alt="" ng-click="toList('Volvo', '112')">
            <a class="mark_link" href="#" ng-click="toList('Volvo', '112')">Volvo (<?php echo DAOFilters::getCurrentMarkCount('112') ?>)</a></li>
        <!--u-->
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/uaz.png" alt="" ng-click="toList('UAZ', '210')">
            <a class="mark_link" href="#" ng-click="toList('UAZ', '210')">UAZ (<?php echo DAOFilters::getCurrentMarkCount('210') ?>)</a></li>
        <!--        <li class="hit_marks">-->
        <!--    <img class="mark_images" src="images/logo/lancia.png" alt="" ng-click="toList('Lancia', '1420')">-->

            <li id="marks_block">
            <a id="hit_marks" style="cursor: pointer">Популярные марки</a>
        </li>
        <li class="hit_marks">
                <img class="mark_images" src="images/logo/17.png" alt="" ng-click="toList('JAGUAR', '148')">
                <a class="mark_link" href="#" ng-click="toList('JAGUAR', '148')">Jaguar (<?php echo DAOFilters::getCurrentMarkCount('148') ?>)</a></li>
            <li class="hit_marks">
                <img class="mark_images" src="images/logo/18.png" alt="" ng-click="toList('JEEP', '150')">
                <a class="mark_link" href="#" ng-click="toList('JEEP', '150')">JEEP (<?php echo DAOFilters::getCurrentMarkCount('150') ?>)</a></li>
        <li class="hit_marks">
            <img class="mark_images" src="images/logo/18.png" alt="" ng-click="toList('JEEP', '150')">
            <a class="mark_link" href="#" ng-click="toList('JEEP', '150')">JEEP (<?php echo DAOFilters::getCurrentMarkCount('150') ?>)</a></li>

              </ul>

    </div>
</div>
    </div>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jQueryRotate.js"></script>
<script type="text/javascript">
    jQuery(".mark_images").rotate({
        bind:
        {
            mouseover : function() {
                jQuery(this).rotate({animateTo:45})
            },
            mouseout : function() {
                jQuery(this).rotate({animateTo:0})
            }
        }
    });
</script>
