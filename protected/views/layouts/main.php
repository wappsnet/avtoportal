<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html ng-app="main" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="language" content="en"/>

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/generic.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/header.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/left.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/footer.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/menu.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/fancybox.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/validations.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/superfish.css">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/fc-cropresizer/fc-cropresizer.css"/>
    <!--for header menu styles-->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/selectbox.css"/>
    <!-- for form styles -->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/angular-busy.min.css"/>
    <!-- for form styles -->

    <script src="http://maps.googleapis.com/maps/api/js?sensor=false&language=en"></script>


    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>
<?php
Yii::app()->getClientScript()->registerCoreScript('jquery');
Yii::app()->getClientScript()->registerScriptFile(Yii::app()->request->baseUrl . '/js/angular.min.new.js');
Yii::app()->getClientScript()->registerScriptFile(Yii::app()->request->baseUrl . '/js/angular_google_maps.js');
Yii::app()->getClientScript()->registerScriptFile(Yii::app()->request->baseUrl . '/js/angular-resource.js');
Yii::app()->getClientScript()->registerScriptFile(Yii::app()->request->baseUrl . '/js/ng-models/main_model.js');
Yii::app()->getClientScript()->registerScriptFile(Yii::app()->request->baseUrl . '/js/ng-controllers/main_controller.js');
Yii::app()->getClientScript()->registerScriptFile(Yii::app()->request->baseUrl . '/js/ng-controllers/main_directives.js');
Yii::app()->getClientScript()->registerScriptFile(Yii::app()->request->baseUrl . '/js/angular-cache-2.3.4.min.js');
Yii::app()->getClientScript()->registerScriptFile(Yii::app()->request->baseUrl . '/js/angular-busy.min.js');
Yii::app()->getClientScript()->registerScriptFile(Yii::app()->request->baseUrl . '/js/utils.js');


Yii::app()->getClientScript()->registerScriptFile(Yii::app()->request->baseUrl . '/js/jquery.customSelect.min.js');
Yii::app()->getClientScript()->registerScriptFile(Yii::app()->request->baseUrl . '/js/superfish.js');
Yii::app()->getClientScript()->registerScriptFile(Yii::app()->request->baseUrl . '/js/hoverIntent.js');

/*Yii::app()->clientScript->registerScript('script' ," $(document).ready(function(){
            $('.select_row select').customSelect();
                 jQuery('ul.sf-menu').superfish();
             });
            function postRequest(attributes, doneFunction) {
                return $.ajax(attributes).done(doneFunction);
            }
        ");*/
?>


<div id="fb-root"></div>

<?php

Yii::app()->clientScript->registerScript('facebook', " (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = '//connect.facebook.net/ru_RU/all.js#xfbml=1';
        fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
        ");

Yii::app()->clientScript->registerScript('googleplus', " (function() {
        var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
        po.src = 'https://apis.google.com/js/plusone.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
    })();
        ");

?>
<script type="text/javascript">
</script>


<!--<div ng-hide="true" style="background-color: #fff; opacity: .5; width: 100%; height: 100%;position: fixed; " id="test">
    <span style="background-color: rgb(0,0,0); color: #dae9ff; font-size: 28px " >... Loading</span>
</div>-->
<div class="wrapper">
<div style="width: 100%; height: 100%; position: absolute; background-color: #101010; display: none" id="bolcker">
</div>
<!--  *********************************************** START HEADER*****************************************  -->
<div id="header" ng-controller="loginController" ng-init="init();" style="min-width: 1008px;">

    <script type="text/javascript">
        $(document).ready(function(){
            $('#all_marks').click(function(){
                $('#hit_marks_div').fadeOut(150, function(){
                    $('#hit_marks_div').css({
                        '-moz-transition': 'all 0.5s ease-in',
                        /* WebKit */
                        '-webkit-transition': 'all 0.5s ease-in',
                        /* Opera */
                        '-o-transition': 'all 0.5s ease-in',
                        /* Standard */
                        'transition': 'all 0.5s ease-in',
                        'display':'none'

                    });
            });
                $('#all_marks').fadeOut(100, function(){
                    $('#all_marks').css({
                        '-moz-transition': 'all 0.5s ease-in',
                        /* WebKit */
                        '-webkit-transition': 'all 0.5s ease-in',
                        /* Opera */
                        '-o-transition': 'all 0.5s ease-in',
                        /* Standard */
                        'transition': 'all 0.5s ease-in',
                        'display':'none'
                    });
                });

            $('#all_marks_div').fadeIn(100, function(){



                    $('#all_marks_div').css({
                        '-moz-transition': 'all 200ms ease-in',
                    /* WebKit */
                    '-webkit-transition': 'all 200ms ease-in',
                    /* Opera */
                    '-o-transition': 'all 200ms ease-in',
                    /* Standard */
                    'transition': 'all 200ms ease-in',
                        'display':'block'

                    });


                });
                $('#hit_marks').fadeIn(100, function(){
                    $('#hit_marks').css({
                        '-moz-transition': 'all 200ms ease-in',
                        /* WebKit */
                        '-webkit-transition': 'all 200ms ease-in',
                        /* Opera */
                        '-o-transition': 'all 200ms ease-in',
                        /* Standard */
                        'transition': 'all 200ms ease-in',
                        'display':'block'

                    });
                });
            });
        });

    </script>

    <script type="text/javascript">
        $(document).ready(function(){
            $('#hit_marks').click(function(){
                $('#all_marks_div').fadeOut(150, function(){
                    $('#all_marks_div').css({
                        '-moz-transition': 'all 0.5s ease-in',
                        /* WebKit */
                        '-webkit-transition': 'all 0.5s ease-in',
                        /* Opera */
                        '-o-transition': 'all 0.5s ease-in',
                        /* Standard */
                        'transition': 'all 0.5s ease-in',
                        'display':'none'

                    });
                });
                $('#hit_marks').fadeOut(100, function(){
                    $('#hit_marks').css({
                        '-moz-transition': 'all 0.5s ease-in',
                        /* WebKit */
                        '-webkit-transition': 'all 0.5s ease-in',
                        /* Opera */
                        '-o-transition': 'all 0.5s ease-in',
                        /* Standard */
                        'transition': 'all 0.5s ease-in',
                        'display':'none'
                    });
                });

                $('#hit_marks_div').fadeIn(100, function(){
                    $('#hit_marks_div').css({
                        '-moz-transition': 'all 200ms ease-in',
                        /* WebKit */
                        '-webkit-transition': 'all 200ms ease-in',
                        /* Opera */
                        '-o-transition': 'all 200ms ease-in',
                        /* Standard */
                        'transition': 'all 200ms ease-in',
                        'display':'block'

                    });
                    $('#all_marks').fadeIn(100, function(){
                        $('#all_marks').css({
                            '-moz-transition': 'all 200ms ease-in',
                            /* WebKit */
                            '-webkit-transition': 'all 200ms ease-in',
                            /* Opera */
                            '-o-transition': 'all 200ms ease-in',
                            /* Standard */
                            'transition': 'all 200ms ease-in',
                            'display':'block'

                        });
                        });


                });
            });
        });

    </script>


    <div style="display: none">
        <?php echo CHtml::Link("", Yii::app()->createUrl('site/index'), array('id' => 'redirectIT_home')); ?>
    </div>
    <div class="header_block">


        <div class="car_item_panel">
            <!--            <img src="images/car_item_panel.png " width="1003px;" height="29px;">-->
        </div>


<!--        <div class="left">-->
<!--            --><?php //if ($this->banner['isPosition1Img']): ?>
<!--                <a href="--><?php //echo $this->banner['position1Url'] ?><!--" target="_blank">-->
<!--                    <img src="--><?php //echo $this->banner['position1'] ?><!--" width="484px" height="93px">-->
<!--                </a>-->
<!--            --><?php //endif; ?>
<!--            --><?php //if (!$this->banner['isPosition1Img']): ?>
<!--                <embed quality="high" bgcolor="#ffffff" width="484px" height="93px"-->
<!--                       name = "mymoviename"-->
<!--                       type="application/x-shockwave-flash"-->
<!--                       pluginspage="http://www.macromedia.com/go/getflashplayer"-->
<!--                      ></embed>-->
<!--            --><?php //endif; ?>
<!--        </div>-->
        <?php echo CHtml::Link("<div id='site_logo'><h3 class='logo left'>" . "<img src='images/avto_logo.png' width='230px'>" . "</h3></div>", Yii::app()->createUrl('site/index')); ?>
        <?php $car = DAOFilters::getCarCount('121'); ?>
        <?php $rent = DAOFilters::getRentCount('121'); ?>
        <?php $spare = DAOFilters::getSparepartCount('121'); ?>
        <?php $rural = DAOFilters::getRuralCount('121'); ?>
        <?php $evacuator = DAOFilters::getEvacuatorCount('121'); ?>
        <?php $taxi = DAOFilters::getTaxiCount('121'); ?>
        <div id="counter">
            <span class="count">
            <?php
            $count = $taxi + $evacuator + $rural + $spare + $rent + $car;
            $count1 = strlen($count);
            $split = str_split($count);
            for($i = 4; $i >= 0; $i--){
                if(isset($split[$i])){
                echo '<span class="count_child">'.$split[$i].'</span>';
            }
                else{
                    echo '<span class="count_child">0</span>';
                }
            }
            ?>
                </span>
            <br/>
            <span class="counter_text">объявлений</span>
        </div>
            <span ng-cloak class="ng-cloak">
                <?php if (Yii::app()->user->isGuest): ?>
                    <span>
                    <div class="login left">
                        <span class="log_contain">
                        <span class="main_log">
                        <a class="log_in" href="<?php echo Yii::app()->createUrl('site/needRegister'); ?>">
                            </a>
                            </span>
                            <a class="log_in2" href="<?php echo Yii::app()->createUrl('site/needRegister'); ?>">
                            Войти
                            </a>
                            </span>
                    </div>
                         <span class="site_rules" style="float: left; color: #bbbbbb;">
                            <?php echo CHtml::Link(Messages::getMessage(168), Yii::app()->createUrl('site/siteRules'), array('class' => 'notification_link')); ?>
                        </span>
<span class="site_rules2" style="float: left; color: #bbbbbb;">
                            <?php echo CHtml::Link("Подать обьявление", Yii::app()->createUrl( 'site/addAnnouncement' ));?>
              </span>

 <script type="text/javascript">
     jQuery(".log_in").rotate({
         bind:
         {
             click : function() {
                 jQuery(this).rotate({animateTo:1710})
             }

         }
     });
 </script>

 <script type="text/javascript">
             $('.log_in2').click(function() {
                 $('.log_in').rotate({animateTo:1710})
             });

 </script>
                <?php endif; ?>
            </span>

<!--        <div class="change_language">-->
<!--            <ul>-->
<!--                <li>-->
<!--                    <a onclick="postRequest({'type' : 'GET' ,'url' : '--><?php //echo Yii::app()->createAbsoluteUrl('/site/changeLanguage?lang=arm'); ?><!--'},function(){window.location.reload()})"-->
<!--                       style="cursor: pointer;">-->
<!--                        <img src="--><?php //echo Yii::app()->request->baseUrl . '/images/armenia.png' ?><!--" alt=""-->
<!--                             width="22px" height="16px"/>-->
<!--                    </a>-->
<!--                </li>-->
<!--                <li>-->
<!--                    <a onclick="postRequest({'type' : 'GET' ,'url' : '--><?php //echo Yii::app()->createAbsoluteUrl('/site/changeLanguage?lang=rus'); ?><!--'},function(){window.location.reload()})"-->
<!--                       style="cursor: pointer;">-->
<!--                        <img src="--><?php //echo Yii::app()->request->baseUrl . '/images/rusia.png' ?><!--" alt="" width="22px"-->
<!--                             height="16px"/>-->
<!--                    </a>-->
<!--                </li>-->
<!--                <li>-->
<!--                    <a onclick="postRequest({'type' : 'GET' ,'url' : '--><?php //echo Yii::app()->createAbsoluteUrl('/site/changeLanguage?lang=eng'); ?><!--'},function(){window.location.reload()})"-->
<!--                       style="cursor: pointer;">-->
<!--                        <img src="--><?php //echo Yii::app()->request->baseUrl . '/images/england.png' ?><!--" alt=""-->
<!--                             width="22px" height="16px"/>-->
<!--                    </a>-->
<!--                </li>-->
<!--                <li>-->
<!--                    <a onclick="postRequest({'type' : 'GET' ,'url' : '--><?php //echo Yii::app()->createAbsoluteUrl('/site/changeLanguage?lang=geo'); ?><!--'},function(){window.location.reload()})"-->
<!--                       style="cursor: pointer;">-->
<!--                        <img src="--><?php //echo Yii::app()->request->baseUrl . '/images/georgia.png' ?><!--" alt=""-->
<!--                             width="22px" height="16px"/>-->
<!--                    </a>-->
<!--                </li>-->
<!--            </ul>-->
<!--        </div>-->
<!--    </div>-->

    <?php if (!Yii::app()->user->isGuest): ?>
        <span class="site_rules" style="float: left; color: #434343;">
                            <?php echo CHtml::Link(Messages::getMessage(168), Yii::app()->createUrl('site/siteRules'), array('class' => 'notification_link')); ?>
                        </span>
        <span class="site_rules2" style="float: left; color: #bbbbbb;">
               <?php echo CHtml::Link("Подать обьявление", Yii::app()->createUrl( 'site/addAnnouncement' ));?>
</span>
        <ul id="list">
            <li>

                            <span>
                            <a class="log_in3" style="text-align: right; margin-right: 15px; cursor: pointer;">
                                Мой кабинет
                            </a>
                            </span>
                <ul>
                    <li>
                         <span class="private_info">
                 <a style="color: #434343;" href="<?php echo Yii::app()->createUrl('personalPage') ?>" style="color:#434343;">
                     <?php echo Messages::getMessage(97) ?>
                 </a>
            </span>
                    </li>
                    <li>
                        <span>
                                           <?php echo CHtml::Link("Подать обьявление", Yii::app()->createUrl( 'site/addAnnouncement'), array('style' => 'color:#434343;'));?>

                        </span>
                    </li>
                    <li>
                        <span class="my_announcements">
                <a style="color: #434343;" href="<?php echo Yii::app()->createUrl('myAnnouncements') ?>">
                    <?php echo Messages::getMessage(96) ?><div style="float: right; margin-right:18px;" ng-bind="announcement_size">( )</div>
                </a>

            </span>
                    </li>
                    <li>
                       <span>
                <a style="cursor: pointer;" class="exit_btn"
                   ng-click="logout('<?php echo Yii::app()->createUrl('site/logout') ?>')"><?php echo Messages::getMessage(98) ?></a>
            </span>
                    </li>
                    </ul>
            </li>
            </ul>

    <?php endif; ?>

</div>
<!--    <div id="left_header">-->
<!--       -->
<!--        -->
<!--    </div>-->
    <?php echo $this->renderPartial('//layouts/logos'); ?>

<!--  *********************************************** END HEADER *****************************************  -->


<!--  ******************************************* START MAIN CONTENT **************************************  -->
<div style="width:1345px; margin: auto;">
<!--    <div style="position: absolute; width: 160px;" id="left_banner">-->
<!--        <div class="banner" style="position: absolute">-->
<!--            --><?php //if ($this->banner['isPosition3Img']): ?>
<!--                <a href="--><?php //echo $this->banner['position3Url'] ?><!--" target="_blank">-->
<!--                    <img src="--><?php //echo $this->banner['position3'] ?><!--" width="160px" height="605px">-->
<!--                </a>-->
<!--            --><?php //endif; ?>
<!--            --><?php //if (!$this->banner['isPosition3Img']): ?>
<!--                <embed quality="high" bgcolor="#ffffff" width="160px" height="605px"-->
<!--                       name="mymoviename"-->
<!--                       type="application/x-shockwave-flash"-->
<!--                       pluginspage="http://www.macromedia.com/go/getflashplayer"-->
<!--                       src="--><?php //echo $this->banner['position3'] ?><!--">-->
<!--                </embed>-->
<!--            --><?php //endif; ?>
<!--        </div>-->
<!--    </div>-->
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/annyang/1.1.0/annyang.min.js">
    </script>
    <script>
        if (annyang) {
            // Let's define our first command. First the text we expect, and then the function it should call
            var commands = {
                'hello': function() {
                    $('#footer').animate({bottom: '-400px'});
                }
            };

            // Add our commands to annyang
            annyang.addCommands(commands);

            // Start listening. You can call this here, or attach this call to an event, button, etc.
            annyang.start();
        }
    </script>
    <div id="main_content" class="left">
        <?php if ($this->renderTopBar()): ?>
            <?php echo $this->renderPartial($this->layout_top_bar); ?>
        <?php endif; ?>
        <!-- ************************ START TOP MENU ***************************** -->
        <div class="top_menu3" style="z-index: 100">

            <ul id="examp">
                <?php foreach ($this->top_menu as $menu_item) : ?>
                    <li>
                        <?php echo CHtml::Link($menu_item['name'], Yii::app()->createUrl($menu_item['toNavigate'])); ?>
                        <?php if ($menu_item['sub_items'] != null): ?>
                            <ul style="z-index: 100001">
                                <?php foreach ($menu_item['sub_items'] as $sub_item) : ?>
                                    <li>
                                        <a href="<?php echo $sub_item['toNavigate']; ?>">
                                            <?php echo $sub_item['name']; ?>
                                        </a>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>
                    </li>
                <?php endforeach; ?>
            </ul>

        </div>
        <!-- ************************ END TOP MENU ***************************** -->


        <?php echo $content; ?>
    </div>

<!--    <div style=" margin-left: 1210px; position: absolute; width:160px; height:605px;" id="right_banner">-->
<!--        <div>-->
<!--            --><?php //if ($this->banner['isPosition4Img']): ?>
<!--                <a href="--><?php //echo $this->banner['position4Url'] ?><!--" target="_blank">-->
<!--                    <img src="--><?php //echo $this->banner['position4'] ?><!--" width="160px" height="605px">-->
<!--                </a>-->
<!--            --><?php //endif; ?>
<!--            --><?php //if (!$this->banner['isPosition4Img']): ?>
<!--                <embed quality="high" bgcolor="#ffffff" width="160px" height="605px"-->
<!--                       name="mymoviename"-->
<!--                       type="application/x-shockwave-flash"-->
<!--                       pluginspage="http://www.macromedia.com/go/getflashplayer"-->
<!--                       src="--><?php //echo $this->banner['position4'] ?><!--"-->
<!--                    ></embed>-->
<!--            --><?php //endif; ?>
<!--        </div>-->
<!---->
<!--    </div>-->
    <!--  ******************************************** END MAIN CONTENT ***************************************  -->

    <!--  *********************************************** START FOOTER *****************************************  -->
    <div>

    </div>
</div>
<div id="footer" style="min-width: 1008px;" ng-controller="loginController" ng-init="init();">
    <div class="footer_block" style="padding-left: 35px;">
        <ul class="left">
            <li><a class="ic" href="#" ng-click="toList('Audi', '96')">Audi</a></li>
            <li><a class="ic" href="#" ng-click="toList('FORD', '129')">Ford</a></li>
            <li><a class="ic" href="#" ng-click="toList('INFINITY', '144')">Infinity</a></li>
            <li><a class="ic" href="#" ng-click="toList('Lada', '212')">VAZ(Lada)</a></li>
            <li><a class="ic" href="#" ng-click="toList('Chevrolet', '109')">Chevrolet</a></li>
            <li><a class="ic" href="#" ng-click="toList('HYUNDAI', '141')">Hyundai</a></li>
            <li><a class="ic" href="#" ng-click="toList('KIA', '154')">Kia</a></li>
            <li><a class="ic" href="#" ng-click="toList('Mercedes Benz', '2955')">Mercedes-Benz</a></li>
            <li><a class="ic" href="#" ng-click="toList('Renault', '188')">Renault</a></li>
            <li><a class="ic" href="#" ng-click="toList('TOYOTA', '209')">Toyota</a></li>
            <li><a class="ic" href="#" ng-click="toList('BYD', '106')">BYD</a></li>
            <li><a class="ic" href="#" ng-click="toList('HONDA', '137')">Honda</a></li>
            <li><a class="ic" href="#" ng-click="toList('Land Rover', '158')">Land Rover</a></li>
            <li><a class="ic" href="#" ng-click="toList('NISSAN', '177')">Nissan</a></li>
            <li><a class="ic" href="#" ng-click="toList('Rolls Royce', '189')">Rolls Royce</a></li>
            <li><a class="ic" href="#" ng-click="toList('Volvo', '112')">Volvo</a></li>
            <li><a class="ic" href="#" ng-click="toList('BMW', '2956')">BMW</a></li>
            <li><a class="ic" href="#" ng-click="toList('GAZ', '131')">GAZ</a></li>
            <li><a class="ic" href="#" ng-click="toList('MAN', '164')">MAN</a></li>
            <li><a class="ic" href="#" ng-click="toList('Peugeot', '183')">Peugeot</a></li>
            <li><a class="ic" href="#" ng-click="toList('SKODA', '201')">Skoda</a></li>
            <li><a class="ic" href="#" ng-click="toList('Mazda', '1638')">Mazda</a></li>
            <li><a class="ic" href="#" ng-click="toList('Porsche', '186')">Porsche</a></li>
            <li><a class="ic" href="#" ng-click="toList('SUZUKI', '205')">Suzuki</a></li>
            <li><a class="ic" href="#" ng-click="toList('Mazda', '1638')">Mazda</a></li>
            <li><a class="ic" href="#" ng-click="toList('Porsche', '186')">Porsche</a></li>
            <li><a class="ic" href="#" ng-click="toList('SUZUKI', '205')">Suzuki</a></li>
            <li><a class="ic" href="#" ng-click="toList('Volkswagen', '213')">Volkswagen</a></li>
            </ul>

            <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jQueryRotate.js"></script>
            <script type="text/javascript">
                jQuery(".ic").rotate({
                    bind:
                    {
                        mouseover : function() {
                            jQuery(this).rotate({animateTo:360})
                        },
                        mouseout : function() {
                            jQuery(this).rotate({animateTo:0})
                        }
                    }
                });
            </script>
        <div id="copy_right">
            <a class="copy" href="#">
              &copy; NotAuto.ru
            </a>
            <a class="copy2" href="#">
                Сайт разработан в компании Wappsnet <img class="wappsnet_logo" src="<?php echo Yii::app()->request->baseUrl; ?>/images/Wappsnet.png"/>
            </a>
        </div>
    </div>
</div>
<!--  *********************************************** END FOOTER *****************************************  -->

</div>
<script type="text/javascript">
    $(document).ready(function () {
        //$('.select_row select').customSelect();
        jQuery('ul.sf-menu').superfish();
        $(document).keypress(function (event) {
            var keycode = (event.keyCode ? event.keyCode : event.which);
            var need_reg = $("#need_reg_enter");
            if (keycode == '13') {
                if (need_reg != undefined && $(need_reg).val() == 1) {
                    $('#need_reg').click();
                } else {
                    $('#sign_in').click();
                }
            }
        });

        var elementPositionRight = $('#left_banner').offset();
        var elementPositionLeft = $('#right_banner').offset();

        var prevLeft = 0;

        $(document).scroll(function (evt) {
            var currentLeft = $(this).scrollLeft();
            if (prevLeft != currentLeft) {

                $('#right_banner').css('position', 'static').css('width', '160px');

                $('#left_banner').css('position', 'static').css('width', '160px');

            } else {
                prevLeft = currentLeft;
                if ($(window).scrollTop() > elementPositionRight.top) {
                    $('#left_banner').css('position', 'fixed').css('top', '0').css('width', '160px');
                } else {
                    $('#left_banner').css('position', 'static').css('width', '160px');
                }

                if ($(window).scrollTop() > elementPositionLeft.top) {
                    $('#right_banner').css('position', 'fixed').css('top', '0');
                } else {
                    $('#right_banner').css('position', 'static');
                }
            }
        });
    });
    function postRequest(attributes, doneFunction) {
        //$('#loadingWidget').show();
        sessionStorage.removeItem('car_filters');
        sessionStorage.removeItem('spare_part_filters');
        sessionStorage.removeItem('wheel_filters');
        sessionStorage.removeItem('evacuator_filters');
        sessionStorage.removeItem('job_filters');
        sessionStorage.removeItem('rent_car_filters');
        sessionStorage.removeItem('rural_filters');
        sessionStorage.removeItem('shipping_filters');
        sessionStorage.removeItem('spec_car_filters');
        sessionStorage.removeItem('taxi_filters');
        sessionStorage.removeItem('tourism_filters');
        sessionStorage.removeItem('transport_filters');
        return $.ajax(attributes).done(doneFunction);
    }
</script>
</body>
</html>
