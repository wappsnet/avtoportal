<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Yerem
 * Date: 1/4/14
 * Time: 1:19 AM
 * To change this template use File | Settings | File Templates.
 */
?>

<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/paymentDiv.css"/>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/paymentDivController.js"></script>

<div ng-controller="paymentController" ng-init="init()" ng-class="{true:'active', false:'hidden'}[enabled]">
    <div class="fadeLayout">
    </div>
    <div class="paymentContent">
        <div ng-show="isHomeTop()">
            <img src="images/home_item.png " style="float: left; margin-right: 20px" height="28px" width="26px">
            <p><h2><?php echo Messages::getMessage(32) ?></h2></p>
        </div>
        <div ng-show="isUrgent()">
            <img src="images/top_item.png " style="float: left; margin-right: 20px" height="28px" width="26px">
            <p><h2><?php echo Messages::getMessage(31) ?></h2></p>
        </div>
        <div ng-show="isListTop()">
            <img src="images/search_item.png" style="float: left; margin-right: 20px" height="28px" width="26px">
            <p><h2><?php echo Messages::getMessage(33) ?></h2></p>
        </div>
        <div class="horizontalRule"></div>

        <?php if(!$this->isAdmin()): ?>
            <div style="text-align: center "><h3><?php echo Messages::getMessage(34) ?><h3></div>
            <div>
                <?php echo CHtml::Link("<h3 class='logo left'>" . " <img src='images/po_arca.png' style='margin:15px; cursor: pointer'>". "</h3>",Yii::app()->createUrl( 'arca/connectToArca' ));?>
                <?php echo CHtml::Link("<h3 class='logo left'>" . " <img src='images/po_paypal.png' style='margin:15px; cursor: pointer'>". "</h3>",Yii::app()->createUrl( 'paypal/buy' ));?>
            </div>
        <?php endif ?>

        <div style="right: 0; bottom: 0;position: absolute; margin: 10px">
            <a href="#" class="closeButton" ng-click="close($event)"><?php echo Messages::getMessage(35) ?></a>
        </div>

        <?php if($this->isAdmin()): ?>
            <div style="left: 0; bottom: 0; position: absolute; margin: 10px">
                <a href="#" class="closeButton" ng-click="accept($event)"><?php echo Messages::getMessage(36) ?></a>
            </div>
        <?php endif ?>
    </div>
</div>