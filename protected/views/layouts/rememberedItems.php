<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Yerem
 * Date: 10/26/13
 * Time: 11:42 PM
 * To change this template use File | Settings | File Templates.
 */
?>

<div class="announcement_field_right right decoration" style="width: 233px; margin-left: 5px">
    <input type="text" ng-show = "false" ng-model="tab_id" id="tab_id"/>
    <div class="title5" style="font-size: 10px;  ">
        <a class="a" ng-click="switchToLastViewed()" style="cursor: pointer;float: left; color: #434343;" id="lastView">
            <span ng-bind-template="Просмотров({{ lastViewdItems.length}})"></span>
        </a>
        <a class="a" ng-click="switchToRemembered()" style="cursor: pointer;float: right" id="remembered">
            <span ng-bind-template="Сохраненные({{ getRememberedItemsSize()}})"></span>
        </a>
    </div>
    <div class="left_carousel" id="carusel_1">
        <div class="caruselUp" style="width: 194px" id="car_prev"><span> </span> </div>
        <div id="right_carusel">
            <div ng-repeat="item in lastViewdItems">
                <a ng-href="{{item.action}}?id={{item.car_id}}" >
                    <div>
                        <div class="remembered_thumb_image_container">
                            <img ng-src="uploads/{{item.image_name}}" alt="" style="max-height: 144px; max-width: 214px; margin: 0;" />
                        </div>
                        <p class="carusel_car_type" style="margin-top: 1px; font-weight: bold; color: black; font-size: 15px; text-align: center; padding: 5px; background: #f5f5d7"> {{item.car_name}}</p>

                        <div class="jcarusel_car_date">
                            <span ng-bind="item.year"></span>
                            <span class="purple_text" style="padding: 4px; background: #f5f5d7; margin-bottom:12px; margin-top:1px; text-align: center; color: rgb(93, 78, 127); font-size: small;" ng-bind-template="{{item.price}} {{getCurrencyLabel(announcement.rate_id)}}"/>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="caruselDown" style="width: 194px" id="car_next"><span> </span> </div>
    </div>

    <div class="left_carousel" id="carusel_2">
        <div class="caruselUp" style="width: 194px" id="car_prev_1"><span> </span> </div>
        <div id="right_carusel_1">
            <div ng-repeat="item in rememberedItems">
                <a ng-href="{{item.action}}?id={{item.car_id}}" >
                    <div>
                        <div ng-click="removeRememberedItem($index, $event)" style="cursor: pointer">
                            <img ng-src="images/delete.png"  alt="" />
                        </div>
                        <div class="remembered_thumb_image_container">
                            <img ng-src="uploads/{{item.image_name}}" style="max-height: 144px; max-width: 194px; margin: 0;"/>
                        </div>
                        <p class="carusel_car_type" style="margin-top: 1px; font-weight: bold; color: black; font-size: 15px; text-align: center; padding: 5px; background: #f5f5d7;" ng-bind="item.car_name"></p>
                        <div class="jcarusel_car_date">
                            <span class="purple_text" style="padding: 4px; background: #f5f5d7; margin-bottom:12px; margin-top:1px; text-align: center; color: rgb(93, 78, 127); font-size: small;" ng-bind-template="{{item.price}} {{getCurrencyLabel(announcement.rate_id)}}"></span>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="caruselDown" style="width: 194px" id="car_next_1"><span> </span> </div>
    </div>
</div>

<script type = "text/javascript" >
    $(document).on('ready', function () {
        $("#right_carusel").carouFredSel({
            circular: false,
            infinite: false,
            auto: false,
            items: 4,
            direction: "down",
            scroll: {
                items: 1
            },
            prev: {
                button: "#car_prev",
                key: "left"
            },
            next: {
                button: "#car_next",
                key: "right"
            }
        });
        $("#right_carusel_1").carouFredSel({
            circular: false,
            infinite: false,
            auto: false,
            items: 4,
            direction: "down",
            scroll: {
                items: 1
            },
            prev: {
                button: "#car_prev_1",
                key: "left"
            },
            next: {
                button: "#car_next_1",
                key: "right"
            }
        });
        if(!$('#tab_id').val()){
            $('#carusel_2').hide();
            $('#carusel_1').show();
            $('#lastView').css('color', '#434343');
            $('#lastView').css('text-decoration', 'underline');
            $('#remembered').css('color', '#434343');
            $('#remembered').css('text-decoration', '');

        }else{
            $('#carusel_2').show();
            $('#carusel_1').hide();
            $('#remembered').css('color', '#434343');
            $('#remembered').css('text-decoration', 'underline');
            $('#lastView').css('color', '#434343');
            $('#lastView').css('text-decoration', '');
        }
    });
</script>