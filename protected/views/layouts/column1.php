<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>


    <!-- ********************************************START LEFT PART********************************-->
    <?php if($this->renderLeftPart):?>
    <div class="left_part left">
        <div id="menu">
            <ul class="menu ">
                <?php  foreach($this->left_side_menu as $menu_item) : ?>
                    <li>
                        <?php echo CHtml::Link("<span>" . $menu_item['name'] . "</span>",$menu_item['toNavigate'],array("class"=>"parent"));?>
                        <?php if( $menu_item['sub_items'] != null): ?>
                            <div>
                                <ul class="menu">
                                    <?php  foreach($menu_item['sub_items'] as $sub_item) : ?>
                                        <li>
                                            <a href="<?php echo  $sub_item['toNavigate']; ?>" class="parent">
                                                <span><?php echo  $sub_item['name']; ?></span>
                                            </a>
                                        </li>
                                    <?php  endforeach; ?>
                                </ul>
                            </div>
                        <?php endif; ?>
                    </li>
                <?php  endforeach; ?>
            </ul>
        </div>
        <h3 class="advertisement_title">РЕКЛАМА</h3>
        <div class="banner">
            <?php if($this->banner['isPosition2Img']): ?>
            <a href="<?php echo $this->banner['position2Url'] ?>" target="_blank" id="url_position_3">
                <img src="<?php echo $this->banner['position2'] ?>" width="195px" height="270px">
            </a>
            <?php endif; ?>
            <?php if(!$this->banner['isPosition2Img']): ?>
            <embed quality="high" bgcolor="#ffffff" width="195px" height="270px"
                   name="mymoviename"
                   type="application/x-shockwave-flash"
                   pluginspage="http://www.macromedia.com/go/getflashplayer" src="<?php echo $this->banner['position2']  ?>"
                   > </embed>
            <?php endif; ?>
           <!-- <img src="images/advertising2.png " width="207px" height="194px">-->
        </div>
    </div>
    <?php endif; ?>
    <!-- ********************************************END LEFT PART********************************-->

    <!-- ******************************************** START MAIN PAGE ********************************-->


    <div ng-cloak class="ng-cloak" style="float: left;">
    <?php echo $content; ?>
    </div>
    <!-- ******************************************** END MAIN PAGE ********************************-->
    <!-- ******************************************** START FILTERS ********************************-->
    <?php if($this->renderFilters()): ?>
    <?php echo $this->renderPartial($this->layout_filter); ?>
    <?php endif ?>
    <!-- ******************************************** END FILTERS ********************************-->
<?php $this->endContent(); ?>