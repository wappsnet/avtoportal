<div class="title"><h3>Фильтр поиска</h3></div>
<div class="pl5 pr5">
    <div class="red_block"><h3>Найдено {{announcements.length}} </h3></div>
    <div ng-repeat="item in addedFilters">
        <h3 class="grey_title">{{item.filter_name}}</h3>
        <div class="grey_block">
            <span>
                <a ng-click="removeFilter($index)">
                    <img src="images/delete.png" style="cursor: pointer;">
                    <span ng-bind="item.filter_value" style="display: inline"></span>
                </a>
            </span>
        </div>
    </div>

    <!--    <h2>------------------------------------------------------</h2>-->

    <div ng-repeat="item in filters">
        <h3 class="grey_title">{{item.filter_name}}</h3>

        <div class="grey_block">
            <a ng-click="addFilter(item.filter_name, item.child_name, tmp.name, tmp.filter_condition, item.filter_type, $event)"
               href="#" ng-repeat="tmp in item.filters | limitTo:5">
                <span ng-bind-template=" {{tmp.name}} ({{tmp.count}})"></span>
            </a>
            <span ng-hide="item.filters.length < 6">
                <a href="#" class="right_more_button"ng-click="additional(item.filter_name, $event)">
                    более
                </a>
            </span>
        </div>
    </div>


    <div ng-show="showFilterByID">
        <h3 class="grey_title"><?php echo Messages::getMessage(133) ?></h3>
        <div style="padding-top: 10px;">
            <input id="identity" type="radio" ng-model="searchByType" value="ID" name="searchByType" checked="checked" ><label for="identity">ID</label>
            <input id="phone" type="radio" ng-model="searchByType" value="Phone" name="searchByType"><label for="phone"><?php echo Messages::getMessage(81) ?></label>
        </div>
        <div class="left_search_block">
            <input type="text" ng-model="searchId" ng-pattern="onlyNumbers" required title="please enter ID">
            <a href="" ng-click="addByIdFilter($event)"> </a>
            <span style="color: red" ng-show="showFilterByIDError">Not Valid ID</span>
        </div>
    </div>
</div>

<div id="additional_div" style="width: 400px; height: auto; border: 1px; border-color: darkslategrey ;margin-top:-70px;" ng-show="showAdditionalDiv">
    <div>
        <div class="grey_title">
            <h3 style="float: left">{{additionalFilter.filter_name}}</h3>
            <a ng-click="hideAddition()" style="cursor: pointer; float: right">hide</a>
        </div>

        <div class="grey_block" style="max-height: 218px; overflow-y: scroll;font-size: 12px; font-weight: 700;">
            <div ng-repeat="tmp in additionalFilter.filters" style="float: left; width: 165px">
                <input type="checkbox" ng-model="tmp.checked"/>
                <label ng-bind-template=" {{tmp.name}} ({{tmp.count}})"></label>
            </div>
        </div>
    </div>

    <div ng-click="makeSearch()" class="additional_search_button">
        <?php echo Messages::getMessage(267) ?>
    </div>

</div>