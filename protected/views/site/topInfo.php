<?php

/**
 * Created by JetBrains PhpStorm.
 * User: Yerem
 * Date: 11/16/13
 * Time: 11:34 PM
 * To change this template use File | Settings | File Templates.
 */
?>
<input type="hidden" value="<?php echo $editMode ?>" id="editMode">
<input type="hidden" value="<?php echo $announcementId?>" id="announcement_id">
<div class="title"><h3><?php echo $formCaption; ?></h3></div>

    <div class="user_recommendation">
        <h2 class="for_user"><?php echo Messages::getMessage(301) ?></h2>
        <ul>
            <li>
                <?php echo Messages::getMessage(302) ?> <img
                    src="images/red_icon.png " width="7px" height="5px"><?php echo Messages::getMessage(303) ?>
            </li>
            <li>
                <?php echo Messages::getMessage(304) ?>
            </li>
        </ul>
    </div>