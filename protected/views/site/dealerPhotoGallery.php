<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Yerem
 * Date: 12/2/13
 * Time: 10:38 PM
 * To change this template use File | Settings | File Templates.
 */
?>



<input type="hidden" name="isDealer" value="1" id="is_dealer">


<div style="margin: 5px;">

    <?php if(isset($images) && count($images) > 0): ?>
        <ul id="my_thumbs">
            <?php foreach($images as $image): ?>
                <li>
                    <img src="<?php echo Yii::app()->request->baseUrl . "/uploads/" . $image->img_title; ?>" alt="Photo" >
                </li>
            <?php endforeach; ?>
        </ul>
    <?php endif; ?>
</div>



<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.timers-1.2.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.galleryview-3.0-dev.js"></script>
<link type="text/css" rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/gallery_view_new.css" />


<script type="text/javascript">


   $( document ).ready(function() {

       $('#my_thumbs').galleryView({
           panel_width:590,
           panel_height:450,
           frame_width: 150,
           frame_height: 80,
           filmstrip_position:'right'
       });



    });
</script>