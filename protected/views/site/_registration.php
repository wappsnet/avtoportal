<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-controllers/userForm_controller.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-models/services.js"></script>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ajaxfileupload.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/UploadHelper.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-controllers/upload_controller.js" type="text/javascript"></script>
<script>
    $(document).ready(function () {
            $('#qqfile').change(function () {
                $('#path').val($(this).val());
            });
            $('#qqfile1').change(function () {
                $('#path_logo').val($(this).val());
            });
        }
    );
</script>

<div class="dillers right content decoration" ng-controller="registrationController" ng-init="init();">
<input type="hidden" value="<?php echo $editMode ?>" id="editMode">
    <div style="display: none">
        <?php echo CHtml::Link("",Yii::app()->createUrl( 'site/succeedRegistered' ),array('id'=>'redirectIT')); ?>
    </div>

    <div style="margin-top:-13px;" ng-show="editMode" class="title"> <h3> <?php echo Messages::getMessage(205) ?></h3> </div>
    <div class="left check_field" ng-hide="editMode">(<?php echo Messages::getMessage(206) ?><span> * </span>)</div>
    <form name="userForm" ng-submit="submit2(userForm);" novalidate  method="post" enctype="multipart/form-data">
        <div class="user_part" ng-hide="editMode">
            <div class="radio_row clear">
                <label for="personal">
                <input ng-model="user.isPersonal" value="true" type="radio" name="personal" id="personal" />
                    <span>Частник</span></label>
            </div>
            <div class="radio_row">
                <label for="dealer1">
                <input ng-model="user.isPersonal" value="false" type="radio" name="dealer1" id="dealer1"/><span><?php echo Messages::getMessage(258) ?></span></label>
            </div>
            <span ng-hide="user.isPersonal">
                <div class="radio_row">
                    <label for="universal">
                            <input ng-model="user.isUniversal" value="false" type="radio" name="universal" id="universal">
                        <span>
                        <?php echo Messages::getMessage(117) ?></span></label>
                </div>
                <div class="radio_row">
                    <label for="official">
                            <input ng-model="user.isUniversal" value="true" type="radio" name="official" id="official">
                <span><?php echo Messages::getMessage(116) ?></span></label>
                </div>
            </span>

            <div class="input-help-user-register"
                 ng-show="userForm.personal.$error.required && userForm.personal.$dirty && userForm.personal.$invalid">
                <span>
                <h4><?php echo Messages::getMessage(156) ?></h4>
                    </span>
            </div>
        </div>
        <div class="row">
            <label>Ваш e-mail</label>
            <input style="float: left" name="email" type="email" ng-model="user.email" required  ng-disabled="editMode" x-webkit-speech/>
            <div class="input-help-user-register" ng-show="submitted && userForm.email.$invalid">
                <span ng-show="userForm.email.$error.email">
                    <h4><?php echo Messages::getMessage(156) ?></h4>
                </span>
                <span ng-show="userForm.email.$error.required">
                    <h4><?php echo Messages::getMessage(159) ?></h4>
                </span>
            </div>
        </div>
          <span ng-bind="emailValidationMessage" style="font-weight: bold;color: red; margin: -6px 10px 0 10px; margin-left: 250px; padding: 10px;">

    </span>
        <div class="row">
            <label>Пароль</label>
            <div class="left">
                <input type="password" ng-minlength="6" required name="password" ng-model="user.password" id="psw" x-webkit-speech />
                <span class="input-help-user-register" ng-show="submitted && userForm.password.$invalid">
                    <span ng-show="userForm.password.$error.minlength">
                        <h4><?php echo Messages::getMessage(160) ?></h4>
                    </span>
                    <span ng-show="userForm.password.$error.required">
                        <h4><?php echo Messages::getMessage(161) ?></h4>
                    </span>
                </span>
                <p class="clear"><?php echo Messages::getMessage(162) ?> </p>
            </div>
        </div>
        <div class="row">
            <label><?php echo Messages::getMessage(163) ?></label>
            <input ng-model='user.confirm_password' type="password" required  data-password-verify="psw" name="confirm_password" ng-keypress="isPressed();">
             <span class="input-help-user-register" ng-show="isPassRepeaterPressed && userForm.confirm_password.$invalid">
                <span ng-show="userForm.confirm_password.$error.required">
                   <h4>Field required! </h4>
                </span>
                <span ng-show="userForm.confirm_password.$error.passwordVerify">
                   <h4><?php echo Messages::getMessage(164) ?> </h4>
                </span>
            </span>
        </div>
        <div class="row">
            <label><?php echo Messages::getMessage(141) ?></label>
            <input type="text" ng-model="user.name" name="first_name" required x-webkit-speech />

             <span class="input-help-user-register" ng-show="submitted && userForm.first_name.$invalid" >
                <span ng-show="userForm.first_name.$error.required">
                   <h4><?php echo Messages::getMessage(156) ?> </h4>
                </span>
            </span>
            <div class="checkbox_row">
                <input name="show_name" type="checkbox" id="name" class="clear" ng-model="user.showName">
                <p class="left checkbox_label"> <label for="name"><?php echo Messages::getMessage(307) ?></label></p>
            </div>
        </div>
        <div class="row" ng-hide="user.isPersonal">
            <label><?php echo Messages::getMessage(308) ?></label>
            <input name="organisation" type="text" ng-model="user.organisation" ng-required="!user.isPersonal" x-webkit-speech />
             <span class="input-help-user-register" ng-show="submitted && userForm.organisation.$invalid" >
                <span ng-show="userForm.organisation.$error.required">
                   <h4><?php echo Messages::getMessage(156) ?> </h4>
                </span>
            </span>
        </div>
        <div class="row" ng-hide="user.isPersonal">
            <label><?php echo Messages::getMessage(309) ?></label>
            <input name="address" type="text" ng-model="user.address" x-webkit-speech />
        </div>
        <div class="row" ng-hide="user.isPersonal">
            <label><?php echo Messages::getMessage(310) ?></label>
            <input name="site" type="text" ng-model="user.site" x-webkit-speech />
        </div>
        <div style="width: 775px; overflow: hidden; padding: 10px; clear: left;" ng-hide="user.isPersonal || isAdmin == 1">
            <label style="padding: 13px 10px 0px 0px; float: left; text-align: right; width: 200px;">Логотип</label>

            <div class="input_row" style="width: 401px;">
                <div ng-controller="uploadController" ng-init="init(0)">
                    <input type="hidden" value="auto" id="formType">
                    <input type="hidden" name="MAX_FILE_SIZE" value="250" />
                    <div class="pictures_panel ">
                        <ul>
                            <li ng-repeat="image in images">
                                <a data-id="{{image.dataId}}" ng-show="image.dataId != -55">
                                    <img src="images/delete.png" ng-click="removeImg($index)" style="cursor: pointer; position: absolute" ng-show="image.isUploaded">
                                    <img ng-src="{{image.src}}" id="{{image.id}}" style="width: 105px; height: 95px;" alt="{{image.title}}">
                                </a>
                            </li>
                        </ul>
                        <div class="choose_file" id="logo_upload">
                            <img id="loading1" src="<?php echo Yii::app()->request->baseUrl; ?>/images/loading.gif" style="height: 18px; display: none; " >

                            <p class="form">
                                <input type="text" id="path_logo" style="width: 140px" x-webkit-speech />
                                <label class="add-photo-btn" ><?php echo Messages::getMessage(323) ?>
                                        <span>
                                           <input type="file" id="qqfile1" name="qqfile" x-webkit-speech style="width: 25px;" />
                                        </span>
                                </label>
                                <a ng-click="uploadDealer()"  class="sign_up_button2"  style="cursor: pointer; line-height: 2; margin-left: 5px; float: left;" ><?php echo Messages::getMessage(207) ?></a>

                            </p>

                        </div>
                    </div>
                </div>
            </div>

        </div>

        <span ng-hide="user.isPersonal || isAdmin == 1" style="padding: 10px; background: #F1F1F1; float: left; margin-left: 50px;">

            <google-map center="center" refresh="isDealer();" draggable="true" zoom="zoom" markers="user.dealer_markers" mark-click="true"
                        class="angular-google-map"  style="height: 400px;width: 650px;"></google-map>

        </span>


        <div style="margin-top:20px;" ng-hide="isAdmin == 1">

            <div class="title" ng-hide="user.isPersonal" style="margin-bottom: 13px; margin-left: 50px; float: left; margin-top: 5px; width: 647px;">
                <h3 style="text-align: center;"><?php echo Messages::getMessage(99) ?></h3>
            </div>
            <div style="float: left; margin-left: 5px">
                <div ng-hide="user.isPersonal">
                    <div ng-controller="uploadController" ng-init="init(1)">
                        <input type="hidden" value="auto" id="formType">
                        <input type="hidden" name="MAX_FILE_SIZE" value="250"/>

                        <div class="pictures_panel" style="width: 520px; margin-left: 135px;">
                            <ul>
                                <li ng-repeat="image in images" style="float: left">
                                    <a data-id="{{image.dataId}}" >
                                        <img src="images/delete.png" ng-click="removeImg($index)" style="cursor: pointer; position: absolute" ng-show="image.isUploaded">
                                        <img ng-src="{{image.src}}" id="{{image.id}}" style="width: 158px; margin-right: 5px; height: 135px;" alt="{{image.title}}">
                                    </a>
                                </li>
                            </ul>
                            <div class="choose_file">
                                <img id="loading" src="<?php echo Yii::app()->request->baseUrl; ?>/images/loading.gif" style="height: 18px; display: none; clear: left; float: left ">

                                <p class="form">
                                    <input type="text" id="path" style="width: 260px; margin-left: 0px;" x-webkit-speech />
                                    <label class="add-photo-btn" ><?php echo Messages::getMessage(323) ?>
                                        <span>
                                           <input type="file" id="qqfile" name="qqfile" x-webkit-speech />
                                        </span>
                                    </label>
                                    <a ng-click="upload()"  class="sign_up_button2"  style="cursor: pointer; line-height: 2; margin-left: 5px; float: left;"><?php echo Messages::getMessage(207) ?></a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <label><?php echo Messages::getMessage(311) ?></label>
            <div class="left">
                <select name="is_phone1_mobile" class="diller_sml_select" ng-model="user.phoneType" hidden="true"
                        ng-options="item.name_arm for item in phoneTypes" x-webkit-speech />
                </select>
                <input name="phone1" type="text" class="middle_input" ng-model="user.phone" required="" ng-pattern="/^([+]?[0-9 -]+)$/" x-webkit-speech />
                <span class="input-help-user-register" ng-show="submitted && userForm.phone1.$invalid" >
                    <span ng-show="userForm.phone1.$error.required">
                       <h4><?php echo Messages::getMessage(313) ?></h4>
                    </span>
                </span>
            </div>
        </div>

        <div class="row" ng-hide="editMode">
            <label><?php echo Messages::getMessage(144) ?></label>
            <div class="">
                <input type="text" class="sml_input" ng-model="user.code" x-webkit-speech />

            </div>
			 <div class="refresh_block">
                <span>
                    <img  height="30px"  width="100px"  id="updater" src="<?php echo Yii::app()->createUrl('site/generateCaptcha'); ?>"/>
					</br>
                     <a id="refresher" onclick="generateCaptcha()" style="cursor: pointer"><?php echo Messages::getMessage(146) ?></a>
                </span>
                 <span class="input-help-user-register" ng-show="showCaptchaError" sty>
                    <span>
                       <h4><?php echo Messages::getMessage(145) ?> </h4>
                    </span>
                </span>
            </div>

        </div>
        <div class="user_consent_block">
            <span ng-hide="editMode">
                <div style="margin-top: 20px;">
                    <input type="checkbox" class="left" ng-required="!editMode" ng-model="user.isAgree" name="isAgree" x-webkit-speech />
                    <div class="use_rule left" style="margin-top: 15px;"> (<a href="#"> <?php echo Messages::getMessage(315) ?> </a>)</div>
                </div>
                 <span class="input-help-user-register" ng-show="submitted &&  userForm.isAgree.$invalid">
                    <span ng-show="userForm.isAgree.$error.required">
                        <h4><?php echo Messages::getMessage(165) ?></h4>
                    </span>
                 </span>
            </span>


             <div class="clear pt20">
                <input style="width: 250px;" type="submit" class="sign_up_button" value="<?php echo Messages::getMessage(316) ?>" x-webkit-speech />
            </div>


        </div>
    </form> <!--  form-->
</div>

<script>


    function generateCaptcha() {
        var stamp = new Date().getTime();
        var src = $('#updater').attr("src") + "?st=" + stamp;
        $('#updater').attr("src", src);
    }

</script>