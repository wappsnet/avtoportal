<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Edgar
 * Date: 12/5/13
 * Time: 12:22 AM
 * To change this template use File | Settings | File Templates.
 */
?>

<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />

<input type="hidden" id="dealer_id" value="<?php echo $dealer_id ?>">
<div class="middle_panel left decoration" style="margin-top: 25px;width: 770px !important;" ng-controller="dealerAnnouncements" ng-init="init();">

    <div class="title" style="margin-bottom: 10px;">
        <h3><?php echo Messages::getMessage(72) ?></h3>
        <h3 ng-click="switchToMap()" ng-show="gallery_view" style="cursor: pointer;float: right; margin-right: 15px;color: #ff0000;width: 120px;font-weight: bold">Картой</h3>
        <h3 ng-click="switchToGallery()" ng-show="map_view" style="cursor: pointer;float: right; margin-right: 15px;color: #ff0000;width: 120px;font-weight: bold">Բաներով</h3>

    </div>


    <div ng-show="gallery_view">
        <div style="clear: both"></div>
        <?php echo $this->renderPartial('dealerPhotoGallery', array('images'=>$dealerImages)); ?>
    </div>

    <span ng-show="map_view">
        <div style="clear: both"></div>
        <google-map center="center" refresh="map_view" draggable="true" zoom="zoom" markers="markers"
                    class="angular-google-map"  style="height: 350px;width: 750px;;margin-left: 10px;"></google-map>
    </span>

    <div class="left" ng-controller="dealerAnnouncements" ng-init="init();" style="width: 100%">
            <?php echo $this->renderPartial('_listTemplate',array('title'=>$title, 'detail_url'=>$detail_url, 'isDealerList'=>'true' , 'isCar' =>$isCar, 'banner'=>$this->banner)); ?>
    </div>



</div>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-controllers/dealer_announcements_controller.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-models/services.js"></script>



