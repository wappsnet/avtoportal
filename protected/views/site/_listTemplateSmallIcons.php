<div class="middle_panel_box" ng-repeat="announcement in announcements | startFrom:pagingProperties.getCurrentPage()*pagingProperties.getPageSize() | limitTo:pagingProperties.getPageSize()" ng-show="!showInLargeItems"
     ng-style="showFirstBanner($index) && {'height':'310px'} || showSecondBanner($index) && {'height':'310px'}">
    <div class="left item_block">
        <div class="list_thumb_image_container">
            <img src="images/urgent_am.jpg"  style="position: absolute; margin-top: 2px; margin-left: 2px " ng-show="announcement.is_top==1">
            <a href="<?php echo Yii::app()->createUrl( $detail_url ) . '?id='?>{{announcement.id}}">
                <img ng-src="<?php echo Yii::app()->request->baseUrl . "/uploads/"; ?>{{announcement.main_image}}" style="cursor: pointer; max-width: 122px; max-height: 90px;">
            </a>
            <div class="ann_count">
                <p ng-bind="announcement.imageCount" style="margin-right: 20px"/>
            </div>
        </div>
        <div class="date">
                <span ng-click="addToRememberedList(announcement.id, announcement.announcement_name, announcement.payment_type+' '+announcement.price, announcement.main_image, $index)" style="background:#b395f4 url(<?php echo Yii::app()->request->baseUrl;?>/images/save.png) no-repeat 95px 4px; margin-top: 3px; width: 110px; padding: 4px 8px; cursor: pointer;float: left; color: #ffffff" ng-show="!announcement.remembered">
                    запомнить
                </span>
                <span ng-click="removeFromRememberedList(announcement.id, $index, $event)" style="background:#5d4e7f url(<?php echo Yii::app()->request->baseUrl;?>/images/save1.png) no-repeat 95px 4px; margin-top: 3px; width: 110px; padding: 4px 8px; cursor: pointer;float: left; color: #ffffff" ng-show="announcement.remembered">
                    запомнено
                </span>
            <span style="padding:5px; 8px; width: 116px; margin-top: 4px; background: #fffcf4;  float: left;"> {{ announcement.created | date:'dd.MM.yyyy'}}</span>
        </div>
    </div>
    <?php if(isset($isDealerList)): ?>
    <div class="left description" style="width:600px !important;" >
        <?php else: ?>
        <div class="description" style="float: left;">
            <?php endif; ?>
            <div>
<!--                <img ng-show="announcement.flag_name != undefined && announcement.flag_name != ''"-->
<!--                     ng-src="--><?php //echo Yii::app()->request->baseUrl; ?><!--/uploads/flags/{{announcement.flag_name}}" width="22px" height="16px" class="left">-->
                <a href="<?php echo Yii::app()->createUrl( $detail_url ) . '?id='?>{{announcement.id}}">
                    <h2 class="" style="float: left;"  id="announcement_name">
                        {{announcement.announcement_name}}
                    </h2>
                </a>
                <div style="display: none; position: absolute; background-color: #a8a8a8" class="tooltip_starter">
                    <div>
                        {{getCurrencyValueFirst(announcement.price, announcement.rate_id)}}
                    </div>
                    <div>
                        {{getCurrencyValueSecond(announcement.price, announcement.rate_id)}}
                    </div>
                </div>


                <?php if(isset($isCar) && $isCar == 1): ?>
                    <span>
                        <div class="crushed"  ng-bind="announcement.state">

                        </div>

                         <div class="crushed" style="margin-right: 5px;"  ng-show="announcement.negotiable == 1">

                         </div>
                    </span>
                    <div class="my_price" onmouseover="tooltipStart(this)" onmouseout="tooltipEnd(this)" ng-show="announcement.negotiable != 1">
                        {{announcement.price}} {{ getCurrencyLabel(announcement.rate_id) }}
                    </div>
                <?php endif ?>

                <?php if(isset($isCar) && $isCar != 1): ?>



                    <div class="my_price" onmouseover="tooltipStartX(this)" onmouseout="tooltipEndX(this)">
                        {{announcement.price}} {{ getCurrencyLabel(announcement.rate_id) }}
                    </div>

                    <div class="type"  ng-bind="announcement.payment_type">

                    </div>

                <?php endif ?>

            </div>
            <p class="car_description" style="min-height: 70px;" ng-bind-html= "announcement.properties_summary_arm |truncate:250 |unsafe"></p>

            <div class="" style="margin-left: 130px;margin-top: 8px; float: left" ng-bind-html="announcement.user_phone |unsafe"></div>
        </div>

        <div class="right mr10" ng-show="isAdmin">
            <a class="accept" ng-click="acceptItem(announcement.id, $event)">Accept</a>
            <a class="delete" ng-click="deleteItem(announcement.id, $event)">Delete</a>
        </div>
<!--        <div class="right mr10">-->
<!--            <img src="images/home_item.png " ng-show="announcement.is_top == 2" height="28px" width="26px">-->
<!--            <!--            <img src="images/top_item.png " ng-show="announcement.is_top == 1">-->
<!--            <img src="images/search_item.png" ng-show="announcement.is_top == 3" height="28px" width="26px">-->
<!--        </div>-->

        <div ng-show="showFirstBanner($index)" class="middle_panel_box" style=" top: 60px; float: left;  width: 540px;">
            <?php if($banner['isPosition5Img'] == '1'): ?>
                <a href="<?php echo $banner['position5Url'] ?>" target="_blank">
                    <img src="<?php echo $banner['position5'] ?>" width="540px" style="float: left;" height="110px">
                </a>
            <?php endif; ?>
            <?php if(!$banner['isPosition5Img']): ?>
                <embed quality="high" bgcolor="#ffffff" width="540px" height="110px"
                       name="mymoviename"
                       type="application/x-shockwave-flash"
                       pluginspage="http://www.macromedia.com/go/getflashplayer" src="<?php echo $banner['position5']  ?>" ></embed>
            <?php endif; ?>
        </div>


        <div ng-show="showSecondBanner($index)" class="middle_panel_box" style=" top: 60px; float: left;  width: 540px;>
            <?php if($banner['isPosition6Img'] == 1): ?>
                <a href="<?php echo $banner['position6Url'] ?>" target="_blank">
                    <img src="<?php echo $banner['position6'] ?>" width="540px" height="110px">
                </a>
            <?php endif; ?>
            <?php if(!$banner['isPosition6Img']): ?>
                <embed quality="high" bgcolor="#ffffff" width="540px" height="110px"
                       name="mymoviename"
                       type="application/x-shockwave-flash"
                       pluginspage="http://www.macromedia.com/go/getflashplayer" src="<?php echo $banner['position6']  ?>" ></embed>
            <?php endif; ?>
        </div>



    </div>