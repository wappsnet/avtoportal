<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Edgar
 * Date: 8/22/13
 * Time: 9:58 PM
 * To change this template use File | Settings | File Templates.
 */?>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<div class="left" ng-controller="carWheelAnnouncementsController" ng-init="init()">
    <div cg-busy="{promise:promise,message:'loading',backdrop:true,}" style="height: 700px;">

        <?php echo $this->renderPartial('_listTemplate',array('title'=>$title, 'detail_url'=>$detail_url , 'isCar' =>$isCar )); ?>

    </div>
</div>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-controllers/listCarWheelAnnouncements_controller.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-models/services.js"></script>
