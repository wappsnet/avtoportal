<div style="display: none" id="news_id"> <?php echo $news_id ?>  </div>
<div class="news_page content right decoration" ng-controller="news-detailed-controller" ng-init="initIt();">
    <div class="title"><h3>Новости</h3></div>
        <div class="date_block">
            <h2>{{ news.title }}</h2>
            <span class="left"> {{news.created | date:'yyyy-MM-dd' }} </span>
        </div>
        <div class="news_description" ng-bind-html="news.text | unsafe">
        </div>
</div>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-controllers/newsDetailed.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-models/services.js"></script>