<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-controllers/forgotPassword_controller.js"></script>


<div class=" dillers right content decoration" ng-controller="forgotPasswordController" ng-init="init();">
    <div class="title"><h3> <?php echo Messages::getMessage(171) ?> </h3></div>
    <form name="userForm" ng-submit="submit(userForm);" novalidate method="post" enctype="multipart/form-data" ng-hide="requestSended" class="forgot_password">
        <div class="radio_row_clear" style="margin-bottom: 60px;">
            <p style=" font-weight: bold;"><?php echo Messages::getMessage(178) ?></p>
        </div>
		<div class="forgot_password_box">
			<label class="left mt5"><?php echo Messages::getMessage(172) ?></label>
			<input type="email" ng-model="email" x-webkit-speech class="left"/>

            <input type="submit" class="clear sign_up_button" value="<?php echo Messages::getMessage(180) ?> " style="cursor: pointer"/>
        </div>

    </form>
    <div ng-show="requestSended">
        <div class="radio_row_clear1"><?php echo Messages::getMessage(181) ?></div>
        <div class="radio_row_clear2"> <?php echo Messages::getMessage(182) ?> </div>


            <input type="button" class="to_main"  ng-click="goToIndex();"  value="<?php echo Messages::getMessage(183) ?> " style="cursor: pointer;margin-left: 50px;width: 250px;"/>
            <div class="to_annaunsments">

                <?php echo CHtml::Link("Подать обьявление", Yii::app()->createUrl( 'site/addAnnouncement' ),array('style' => 'color:#ffffff;'));?>

            </div>

    </div>

</div>
