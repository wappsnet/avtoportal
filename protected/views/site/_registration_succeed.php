<?php
/* @var $registered boolean*/
?>

<div class="add_announcement right decoration">
    <div class="title"><h3 class="add_anonc_title"><?php echo Messages::getMessage(167) ?></h3></div>
<?php if($registered): ?>
    <p><h2 class="h2"><?php echo Messages::getMessage(38) ?></h2></p>
<?php else: ?>
    <p><h2 class="h2"><?php echo Messages::getMessage(43) ?></h2></p>
<?php endif; ?>
    </div>