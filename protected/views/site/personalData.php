<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Yerem
 * Date: 11/17/13
 * Time: 1:23 AM
 * To change this template use File | Settings | File Templates.
 */
?>
<?php if($this->isAdmin()): ?>
    <?php echo $this->renderPartial('upgradeAnnouncementRadios'); ?>
<?php endif ?>
<div class="box" ng-show="!registered||isAdmin" >
    <h3 class="car_type"><?php echo Messages::getMessage(205) ?></h3>

    <div class="groups">
        <div ng-repeat="property in groupUserinfo.properties">
            <div ng-switch="property.c_property_type_id" class="{{property.controller_style}}">
                <div ng-switch-when="1" style="position: relative">
                    <ng-form name="form">
                        <label for="id_{{property.id}}"><div style="float: left">{{property.name}} </div>
                            <div style="color: red;float: left" ng-show="property.validation == 1  || property.validation == 4 || property.validation == 6 || property.validation == 5">*</div></label>

                        <input ng-required="property.validation == 1 || property.validation == 4 || property.validation == 6 || property.validation == 5"
                               ng-model="property.temp_value" prpinput
                               property="property" type="text" name="prop"
                               id="id_{{property.id}}"/>

                        <div class="input-help" ng-show="submitted && form.prop.$error.required">
                            <h4><?php echo Messages::getMessage(156) ?></h4>
                        </div>
                        <div class="input-help" ng-show="submitted && form.prop.$error.number">
                            <h4><?php echo Messages::getMessage(157) ?></h4>
                        </div>
                        <div class="input-help" ng-show="submitted && form.prop.$error.phone">
                            <h4><?php echo Messages::getMessage(158) ?></h4>
                        </div>
                        <div class="input-help" ng-show="submitted && form.prop.$error.email">
                            <h4><?php echo Messages::getMessage(159) ?></h4>
                        </div>
                    </ng-form>
                </div>
            </div>
        </div>
    </div>
</div>