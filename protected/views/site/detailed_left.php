<link href="http://stg.odnoklassniki.ru/share/odkl_share.css" rel="stylesheet">
<script src="http://stg.odnoklassniki.ru/share/odkl_share.js" type="text/javascript"></script>
<div class="left_box left">
    <div class="car_box" style="padding-left: 2px;">
        <div class="detailed_main_image_container">
            <a ng-show="announcement.images.length" id="fancy_a" onclick="imgClick();" style="cursor: pointer "
               onmouseover="$('#loop').show();" onmouseout="$('#loop').hide();">
                <img src="images/urgent_am.jpg" style="position: absolute; margin-top: 2px; margin-left: 2px "
                     ng-show="announcement.is_top==1">
                <img src="images/loop.png" class="looping" id="loop" style="display: none">
                <img id="detailed_main_image" alt="zoomable" title=""
                     ng-src="<?php echo Yii::app()->request->baseUrl . "/uploads/"; ?>{{announcement.images[0].name}}"/>
            </a>
            <a ng-show="!announcement.images.length" id="fancy_a" onClick="return false;">
                <img id="detailed_main_image" alt="zoomable" title="" width="376px" height="282px"
                     ng-src="<?php echo Yii::app()->request->baseUrl . '/images/no_photo.jpg'; ?>"/>
            </a>
        </div>
        <div>
            <div class="btn_block">
                <div class="gv_panelNavPrev" style="float:left; display: block !important;"
                     ng-click="prev($event)"></div>
                <div class="gv_panelNavNext" style="display: block !important; float:right; left: 315px;"
                     ng-click="next($event)"></div>
                <!--<a href="#" class="prev_item" ng-click="prev($event)" ng-show="announcement.images.length > 1"><img src="<?php /*echo Yii::app()->request->baseUrl.'/images/left.png'*/ ?>" style="cursor: pointer;height: 61px; width: 47px; top: 50%; "></a>
            <a href="#"  class="next_item" ng-click="next($event)" ng-show="announcement.images.length > 1" style="float: right"><img src="<?php /*echo Yii::app()->request->baseUrl.'/images/right.png'*/ ?>" style="cursor: pointer;height: 61px; width: 47px;top: 50%; "></a>-->
            </div>
            <div class="multizoom2 thumbs" style="width: 400px">
                <ul>
                    <li ng-repeat="image in thumbImages | startFrom:currentPosition | limitTo:6"
                        ng-class-even="'left_important'" ng-class-odd="'right_important'">
                        <div class="detailed_thumb_image_container" ng-mouseenter="onImgHover($event,image);">
                            <img ng-src="<?php echo Yii::app()->request->baseUrl . "/uploads/"; ?>{{image.name}}"
                                 style="max-height: 92px; max-width: 115px;"/>
                        </div>
                    </li>
                </ul>
                <ul>
                    <li ng-repeat="image in thumbImages | startFrom:currentPosition | limitTo:6"
                        ng-class-even="'right_important'" ng-class-odd="'left_important'">
                        <div class="detailed_thumb_image_container" ng-mouseenter="onImgHover($event,image);">
                            <img ng-src="<?php echo Yii::app()->request->baseUrl . "/uploads/"; ?>{{image.name}}"
                                 style="max-height: 92px; max-width: 115px;"/>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="share_buttons" style="width: 365px ; height: 25px;clear:both;margin:10px">
        <script type="text/javascript">(function() {
                if (window.pluso)if (typeof window.pluso.start == "function") return;
                if (window.ifpluso==undefined) { window.ifpluso = 1;
                    var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
                    s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
                    s.src = ('https:' == window.location.protocol ? 'https' : 'http')  + '://share.pluso.ru/pluso-like.js';
                    var h=d[g]('body')[0];
                    h.appendChild(s);
                }})();</script>
        <div class="pluso" data-background="#ffffff" data-options="big,square,line,horizontal,counter,theme=08" data-services="facebook,vkontakte,twitter,google,odnoklassniki"></div>
    </div>

    <div style="display: none" id="image_box">

        <div class="image-browser-close" style="z-index: 1000" onclick="closePopup();">
        </div>

        <ul id="myGallery">
            <li ng-repeat="image in announcement.images">
                <img ng-src="<?php echo Yii::app()->request->baseUrl . "/uploads/"; ?>{{image.name}}"/>
            </li>
        </ul>

    </div>


    <?php echo $this->renderPartial('userInfo'); ?>
</div>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.timers-1.2.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/popup.js"></script>

<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.galleryview-3.0-dev.js"></script>
<link type="text/css" rel="stylesheet"
      href="<?php echo Yii::app()->request->baseUrl; ?>/css/jquery.galleryview-3.0-dev.css"/>

<script type="text/javascript">
    function imgClick() {
        $('#myGallery').galleryView({
            panel_width: 800,
            panel_height: 600,
            frame_width: 150,
            frame_height: 100,
            filmstrip_position: 'right'
        });

        $('#image_box').bPopup({
            fadeSpeed: 'fast', //can be a string ('slow'/'fast') or int
            followSpeed: 'fast' //can be a string ('slow'/'fast') or int
            //scrollBar : 'true'

        });
        window.setTimeout(" $('.gv_thumbnail').children('img').css('left' , 0);", 300);

    }

    function closePopup() {
        $('#image_box').bPopup().close();
    }
</script>
