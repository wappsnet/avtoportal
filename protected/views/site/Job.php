<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Yerem
 * Date: 6/13/13
 * Time: 1:28 AM
 * To change this template use File | Settings | File Templates.
 */
?>



<link href="css/generic.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="css/superfish.css"><!--for header menu styles-->


<script src="js/hoverIntent.js"></script><!--for header menu styles-->
<script src="js/superfish.js"></script><!--for header menu styles-->

<script type="text/javascript" src="js/jquery.selectbox.min.js"></script><!--for form  styles-->
<!--slider-->
<link rel="stylesheet" type="text/css" href="css/slider.css"/>
<script type="text/javascript" src="js/jquery.jcarousel.min.js"></script>
<script type="text/javascript">
    (function ($) {
        $(function () {
            $('select').selectbox();
        })
    })(jQuery)
    jQuery(document).ready(function () {
        jQuery('ul.sf-menu').superfish();
        jQuery('#mycarousel').jcarousel({
            vertical: true,
            scroll: 2
        });
    });

</script>
<!--end slider-->


<div class="left">
    <div class="pagination">
        <ul class="yiiPager">
            <li class="page"><a href="#"> &lt;&lt; </a></li>
            <li class="page"><a href="#">&lt; </a></li>
            <li class="page"><a href="#"> 1</a></li>
            <li class="page"><a href="#"> 2</a></li>
            <li class="page selected"><a href="#"> 3</a></li>
            <li class="page"><a href="#"> 4</a></li>
            <li class="page"><a href="#"> 5</a></li>
            <li class="page"><a href="#"> 6</a></li>
            <li class="page"><a href="#"> 7</a></li>
            <li class="page"><a href="#"> 8</a></li>
            <li class="page"><a href="#"> 9</a></li>
            <li class="page"><a href="#"> 10</a></li>
            <li class="page"><a href="#"> &gt;&gt; </a></li>
            <li class="page"><a href="#"> &gt; </a></li>
        </ul>
    </div>
    <!--pagination-->
    <div class="middle_panel left decoration">
        <div class="title">
            <h3> Работа</h3>

            <div class="icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>//<?php echo Yii::app()->request->baseUrl; ?>/images/title_bg1.png " width="30px" height="26px"></div>
            <div class="icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/title_bg2.png " width="30px" height="26px"></div>
            <form id="top_select" class="selectbox">
                <span class="selectbox" style="display: inline-block; position: relative; z-index: 2;"><div
                        class="select" style="float:left;position:relative;z-index:10000">
                        <div class="text"> 1</div>
                        <b class="trigger"><i class="arrow"></i></b></div><div class="dropdown"
                                                                               style="position: absolute; z-index: 9999; overflow-y: auto; overflow-x: hidden; list-style: none; left: 0px; display: none; height: auto; bottom: auto; top: 33px;">
                        <ul>
                            <li class="selected sel"> 1</li>
                        </ul>
                    </div></span><select id="" name="" style="position: absolute; top: -9999px;">
                    <option value="0"> 1</option>
                </select>
            </form>
        </div>
        <div class="middle_panel_box">
            <div class="left item_block">
                <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/car_item1.png " width="122px" height="90px">

                <div class="date"><span> запомнить</span> <span> 03.05.2013 </span></div>
            </div>
            <div class="left description">
                <div><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/armenia.png " width="22px" height="16px" class="left">

                    <h2 class="left"> 2004 BMW 318 M Paket Individual </h2> <span class="price"> 4.30 USD </span></div>
                <p class="car_description"> Lorem Ipsum is simply dummy text of the printing and typesetting industry .
                    Lorem Ipsum has been the industry's standard dummy tex</p>

                <div class="phone right">091415842</div>

            </div>
        </div>
        <!-- middle_panel_box-->
        <div class="middle_panel_box">
            <div class="left item_block">
                <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/car_item1.png " width="122px" height="90px">

                <div class="date"><span> запомнить</span> <span> 03.05.2013</span></div>
            </div>
            <div class="left description">
                <div><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/armenia.png " width="22px" height="16px" class="left">

                    <h2 class="left">2004 BMW 318 M Paket Individual</h2> <span class="price">4.30 USD</span></div>
                <p class="car_description">Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                    Lorem Ipsum has been the industry's standard dummy tex </p>

                <div class="phone right"> 091415842</div>

            </div>
        </div>
        <!--middle_panel_box-->
        <div class="middle_panel_box">
            <div class="left item_block">
                <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/car_item1.png " width="122px" height="90px">

                <div class="date"><span> запомнить</span> <span> 03.05.2013 </span></div>
            </div>
            <div class="left description">
                <div><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/armenia.png " width="22px" height="16px" class="left">

                    <h2 class="left"> 2004 BMW 318 M Paket Individual </h2> <span class="price"> 4.30 USD </span></div>
                <p class="car_description"> Lorem Ipsum is simply dummy text of the printing and typesetting industry .
                    Lorem Ipsum has been the industry's standard dummy tex</p>

                <div class="phone right">091415842</div>
            </div>
        </div>
        <!-- middle_panel_box-->
        <div class="middle_panel_box">
            <div class="left item_block">
                <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/car_item1.png " width="122px" height="90px">

                <div class="date"><span> запомнить</span> <span> 03.05.2013</span></div>
            </div>
            <div class="left description">
                <div><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/armenia.png " width="22px" height="16px" class="left">

                    <h2 class="left">2004 BMW 318 M Paket Individual</h2> <span class="price">4.30 USD</span></div>
                <p class="car_description">Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                    Lorem Ipsum has been the industry's standard dummy tex </p>

                <div class="phone right"> 091415842</div>
            </div>
        </div>
        <!--middle_panel_box-->
        <div class="middle_panel_box">
            <div class="left item_block">
                <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/car_item1.png " width="122px" height="90px">

                <div class="date"><span> запомнить</span> <span> 03.05.2013 </span></div>
            </div>
            <div class="left description">
                <div><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/armenia.png " width="22px" height="16px" class="left">

                    <h2 class="left"> 2004 BMW 318 M Paket Individual </h2> <span class="price"> 4.30 USD </span></div>
                <p class="car_description"> Lorem Ipsum is simply dummy text of the printing and typesetting industry .
                    Lorem Ipsum has been the industry's standard dummy tex</p>

                <div class="phone right">091415842</div>
            </div>
        </div>
        <!-- middle_panel_box-->
    </div>
    <!-- middle_panel-->
</div>