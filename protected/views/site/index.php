<div class="main_page right">
    <div class="car_banner mb10 decoration" ng-controller="autosImmediateController" ng-init="init()"
         ng-mouseover="pauseScroll()" ng-mouseleave="resumeScroll()"   cg-busy="{promise:promise1,message:'loading',backdrop:true}" >
        <div class="title">
            <h3><?php echo Messages::getMessage(283) ?></h3>
            <ul class="car_pagination">

                <button style="cursor: pointer" ng-click="changePage(-1)" ng-disabled="currentPage == 0"> &lt; </button>
                {{currentPage+1}}/{{numberOfPages}}
                <button style="cursor: pointer" ng-click="changePage(1)" ng-disabled="currentPage >= autos.length/pageSize - 1"> &gt; </button>

            </ul>
        </div>
        <div style="display: none" id="url_helper">
            <?php echo CHtml::Link("",Yii::app()->createUrl( $detail_url )); ?>
        </div>
        <ul class="cars" id="autosContainer">
                <li ng-repeat="auto in autos | startFrom:currentPage*pageSize | limitTo:pageSize" style="margin-top: 0px !important;">
                <div class="top_thumb_image_container">
                    <img ng-click="toDetailedInfo(auto.id,$event);" ng-src="<?php echo Yii::app()->request->baseUrl . "/uploads/"; ?>{{auto.main_image}}" style="max-width: 160px; max-height: 105px; margin: 0">
                </div>
                <div class="car_description">
                    <span style="margin-top:13px; margin-left:10px;" class="left" ng-click="toDetailedInfo(auto.id,$event);" id="announcement_name">
                        <div><span style="font-weight: bold">{{auto.name}}<span> </div>
                        <div> <span style="color: #484848;font-weight: bold">{{auto.year}} г.,</span> <span style="color: #d61111">{{auto.price}} </span></div>
                    </span>
                   <!-- <p>{{ auto.model }}</p>
                    <div>{{ auto.year }}<span>{{ auto.price }}<span></span></span></div>-->
                </div>
            </li>
        </ul>
    </div>

    <div class="selected_car mt10 decoration" ng-controller="newsList" ng-init="init()" id="news_list" ng-mouseover="pauseScroll()" ng-mouseleave="resumeScroll()">
        <div class="title" >
            <h3><?php echo Messages::getMessage(253) ?></h3>
            <ul class="car_pagination">
                <li><a href="javascript:void(0)" ng-click="prev($event)" ng-show="currentIndex > 0" >&lt;</a> </li>
                <li><a>{{currentIndex+1}}/{{newsSize}}</a></li>
                <li><a href="javascript:void(0)" ng-click="next($event)" ng-hide="showButton()"> &gt; </a> </li>
            </ul>
        </div>
        <div id="news">
        <div class="mt5 ml5" ng-repeat="news in newsList | startFrom:currentIndex | limitTo:1">
            <div class="left" style="margin-bottom: 5px;">
                <img ng-show="news.main_image != '/images/no_photo.jpg'" ng-click="toDetailedInfoNewS(news.id,$event);" ng-src="{{news.main_image}}" width="336px" height="232px">
                <img ng-show="news.main_image == '/images/no_photo.jpg'" ng-click="toDetailedInfoNewS(news.id,$event);" ng-src="<?php echo Yii::app()->request->baseUrl . "/images/no_photo.jpg"; ?>" width="354px" height="299px">
            </div>
            <div class="right selected_car_description" >
                <h2 class="left" style="width: 390px" ng-click="toDetailedInfoNewS(news.id,$event);" id="announcement_name">
                    {{ news.title }}
                </h2>
                <div style="margin-left: 2px; float: left; width: 390px; max-height: 300px" ng-bind-html="secureText(news.text)|truncate:330|unsafe  ">
                </div>

                <span class="left" style="margin-top: 15px;margin-left: 20px;"> {{news.created | date:'dd.MM.yyyy' }}  </span>

            </div>
        </div>
        </div>
        <div style="display: none" id="index_news">
            <?php echo CHtml::Link("",Yii::app()->createUrl( 'site/newsDetailed' ));?>
        </div>
    </div>
</div>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-models/services.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-controllers/index_controller.js"></script>