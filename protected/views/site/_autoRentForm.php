<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Edgar
 * Date: 6/23/13
 * Time: 1:31 PM
 * To change this template use File | Settings | File Templates.
 */?>
<div class="content sale_form right car_part_block decoration" ng-controller="autoRentController" ng-init="init()" id="announcement_form"
     cg-busy="{promise:promise,message:'loading',backdrop:true}" >
<div class="box">
<?php echo $this->renderPartial('topInfo', array('editMode'=>$editMode, 'announcementId'=>$announcementId, 'formCaption'=>$formCaption)); ?>
</div>
<form name="myForm" ng-submit="submit(myForm);" novalidate  method="post" enctype="multipart/form-data">
<?php if(!$editMode): ?>
    <?php echo $this->renderPartial('personalData'); ?>
<?php endif; ?>
<div class="box">
    <h3 class="car_type">Параметры</h3>

    <div class="groups">
        <div ng-repeat="property in groupOne.properties">
            <div ng-switch="property.c_property_type_id" class="{{property.controller_style}}">
                <div ng-switch-when="2" style="position: relative">
                    <ng-form name="form">
                        <label for="id_{{property.id}}"><div style="float: left">{{property.name}} </div> <div style="color: red;float: left" ng-show="property.validation == 1">*</div></label>
                        <select data-ng-model="property.temp_value" property="property"
                                properties="groupOne.properties"
                                name="prop"  prpcombo id="id_{{property.id}}"
                                ng-options="item.name for item in property.lookup_items"
                                ng-required="property.validation == 1">
                            <option value=""><?php echo Messages::getMessage(5) ?></option>
                        </select>

                        <div class="input-help-user-register" ng-show="submitted && form.prop.$error.required">
                            <span ng-show="form.prop.$error.required">
                                <h4><?php echo Messages::getMessage(156) ?></h4>
                            </span>
                        </div>
                    </ng-form>
                </div>
            </div>
        </div>
        <div ng-repeat="property in term.properties">
            <div ng-switch="property.c_property_type_id" class="{{property.controller_style}}">
                <div ng-switch-when="2" style="position: relative">
                    <ng-form name="form">
                        <label for="id_{{property.id}}"><div style="float: left">{{property.name}} </div> <div style="color: red;float: left" ng-show="property.validation == 1">*</div></label>
                        <select data-ng-model="property.temp_value" property="property"
                                properties="groupOne.properties"
                                name="prop"  prpcombo id="id_{{property.id}}"
                                ng-options="item.name for item in property.lookup_items"
                                ng-required="property.validation == 1">
                            <option value=""><?php echo Messages::getMessage(5) ?></option>
                        </select>

                        <div class="input-help-user-register" ng-show="submitted && form.prop.$error.required">
                            <span ng-show="form.prop.$error.required">
                                <h4><?php echo Messages::getMessage(156) ?></h4>
                            </span>
                        </div>
                    </ng-form>
                </div>
            </div>
        </div>
        <br/><br/>
    </div>

    <div class="car_value">
        <h1 class="car_value_title"> Правильно оцените ваш автомобиль</h1>

        <p>Махимальная цена</p>

        <p>Средная цена</p>

        <p>Минималая цена</p>
    </div>

</div>

<div class="box">
    <h3 class="car_type"><?php echo Messages::getMessage(204) ?></h3>

    <div class="groups">
        <div ng-repeat="property in groupTwo.properties" class="{{property.controller_style}}">
            <span ng-switch="property.c_property_type_id" >
                <div ng-switch-when="1" style="position: relative">
                    <ng-form name="form">
                        <label for="id_{{property.id}}" class="{{property.label_style}}"><div style="float: left">{{property.name}} </div>
                            <div style="color: red;float: left" ng-show="property.validation == 1 || property.validation == 4">*</div></label>

                        <input ng-required="property.validation == 1 || property.validation == 4"
                               ng-model="property.temp_value" prpinput style="{{ property.style }}"
                               property="property" type="text" name="prop"
                               id="id_{{property.id}}" x-webkit-speech/>

                        <span class="input-help-user-register" ng-show="submitted && (form.prop.$error.required || form.prop.$error.number)">
                            <span ng-show="form.prop.$error.required">
                                <h4><?php echo Messages::getMessage(156) ?></h4>
                            </span>
                             <span ng-show="form.prop.$error.number">
                                <h4><?php echo Messages::getMessage(157) ?></h4>
                            </span>
                        </span>

                    </ng-form>
                </div>
                <div ng-switch-when="2" style="position: relative">
                    <ng-form name="form">
                        <label for="id_{{property.id}}" class="{{property.label_style}}"><div style="float: left">{{property.name}} </div>
                            <div style="color: red;float: left" ng-show="property.validation == 1 || property.validation == 4">*</div></label>
                        <select data-ng-model="property.temp_value" property="property"
                                properties="groupOne.properties" style="{{ property.style }}"
                                name="prop"  prpcombo id="id_{{property.id}}"
                                ng-options="item.name for item in property.lookup_items"
                                ng-required="property.validation == 1">
                            <option value=""><?php echo Messages::getMessage(5) ?></option>
                        </select>

                        <span class="input-help-user-register" ng-show="submitted && form.prop.$error.required">
                            <span ng-show="form.prop.$error.required">
                                <h4><?php echo Messages::getMessage(156) ?></h4>
                            </span>
                        </span>

                    </ng-form>
                </div>
            </span>
        </div>
    </div>
    <div>
        <div class="descrition" id="vivat">
            <div ng-repeat="property in groupThree.properties">
                <div ng-switch="property.c_property_type_id" class="{{property.controller_style}}">


                    <div ng-switch-when="1" style="position: relative">
                        <ng-form name="form">
                            <label for="id_{{property.id}}" class="{{property.label_style}}">{{property.name}}</label>

                            <input ng-required="property.validation == 1 || property.validation == 4" onkeyup="correctPrices(this)"
                                   ng-model="property.temp_value"  prpinput
                                   property="property" type="text" name="prop"
                                   id="id_{{property.id}}" x-webkit-speech/>

                            <span class="input-help-user-register" ng-show="submitted && (form.prop.$error.required || form.prop.$error.number)">
                                    <span ng-show="form.prop.$error.required" style="width: 45px;">
                                        <h4><?php echo Messages::getMessage(156) ?></h4>
                                    </span>
                                     <span ng-show="form.prop.$error.number" style="width: 45px;">
                                        <h4><?php echo Messages::getMessage(157) ?></h4>
                                    </span>
                            </span>
                        </ng-form>
                    </div>

                    <span ng-switch-when="3">
                        <input ng-model="property.temp_value" property="property" type="checkbox"
                               name="id_{{property.id}}" id="id_{{property.id}}"/>
                        <label class="{{property.label_style}}" for="id_{{property.id}}">{{property.name}}</label>
                    </span>

                    <div ng-switch-when="2" style="position: relative">
                        <ng-form name="form">
                            <label for="id_{{property.id}}">{{property.name}}</label>
                            <select data-ng-model="property.temp_value" property="property"
                                    properties="groupOne.properties"
                                    name="prop"  prpcombo id="id_{{property.id}}"
                                    ng-options="item.name for item in property.lookup_items"
                                    ng-required="property.validation == 1">
                                <option value=""><?php echo Messages::getMessage(5) ?></option>
                            </select>

                           <span class="input-help-user-register" ng-show="submitted && form.prop.$error.required">
                                <span ng-show="form.prop.$error.required">
                                    <h4><?php echo Messages::getMessage(156) ?></h4>
                                </span>
                           </span>
                        </ng-form>
                    </div>
                </div>
            </div>

            <div ng-repeat="property in extra.properties">
                <div ng-switch="property.c_property_type_id" class="{{property.controller_style}}">

                    <div ng-switch-when="2" style="position: relative">
                        <ng-form name="form">
                            <label for="id_{{property.id}}">{{property.name}}</label>
                           <span class="input-help-user-register" ng-show="submitted && form.prop.$error.required">
                                <span ng-show="form.prop.$error.required">
                                    <h4><?php echo Messages::getMessage(156) ?></h4>
                                </span>
                           </span>
                        </ng-form>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<script>
    $(document).ready(function(){

        document.getElementById('car_type2').onclick = function() {
            openbox2('dop_inform', this);
            return false;
        };


        function openbox2(id2, car_type2) {
            var div2 = document.getElementById('dop_inform');
            if(div2.style.display == 'block') {
                div2.style.display = 'none';
                car_type2.innerHTML = '<?php echo Messages::getMessage(192) ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; +';
            }
            else {
                div2.style.display = 'block';
                car_type2.innerHTML = '<?php echo Messages::getMessage(192) ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -';
            }
        }
    });
</script>
<script>
    $(document).ready(function(){
        document.getElementById('car_type_h3').onclick = function() {
            openbox('dop_options', this);
            return false;
        };

        function openbox(id, car_type_h3) {
            var div = document.getElementById('dop_options');
            if(div.style.display == 'block') {
                div.style.display = 'none';
                car_type_h3.innerHTML = '<?php echo Messages::getMessage(193) ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+';
            }
            else {
                div.style.display = 'block';
                car_type_h3.innerHTML = '<?php echo Messages::getMessage(193) ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-';
            }
        }
    });
</script>
<div class="box">
    <h3 id="car_type_h3" class="car_type" style="cursor: pointer;"><?php echo Messages::getMessage(193) ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+</h3>
    <div id="dop_options" style="display:none;">
    <div class="check_box">
        <div ng-repeat="property in groupFive.properties">
            <div class="{{property.controller_style}}">
                <input ng-model="property.temp_value" property="property" type="checkbox" name="id_{{property.id}}"
                       id="id_{{property.id}}"/>
                <label class="{{property.label_style}}" for="id_{{property.id}}">{{property.name}}</label>
            </div>
        </div>
    </div>

    <div class="check_box ml20">
        <div ng-repeat="property in groupSix.properties">
            <div class="{{property.controller_style}}">
                <input ng-model="property.temp_value" property="property" type="checkbox" name="id_{{property.id}}"
                       id="id_{{property.id}}"/>
                <label class="{{property.label_style}}" for="id_{{property.id}}">{{property.name}}</label>
            </div>
        </div>
    </div>

    <div class="check_box ml20">
        <div ng-repeat="property in groupSeven.properties">
            <div class="{{property.controller_style}}">
                <input ng-model="property.temp_value" property="property" type="checkbox" name="id_{{property.id}}"
                       id="id_{{property.id}}"/>
                <label class="{{property.label_style}}" for="id_{{property.id}}">{{property.name}}</label>
            </div>
        </div>
    </div>
    </div>
</div>

<div class="box">
    <h3 style="cursor:pointer;" class="car_type" id="car_type2"><?php echo Messages::getMessage(192) ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; +</h3>
    <div id="dop_inform" style="display: none;">
        <input x-webkit-speech id="mike" style="width: 15px; padding: 5px; float: left; margin-left:15px; margin-right:-20px; margin-top:10px;"/>

        <textarea id="txt" class="left info" ng-model="description" style="margin-top: 15px; margin-left:10px;"> </textarea>
        <script>
            $(document).ready(
                function(){
                    var mike = document.getElementById('mike');
                    mike.onfocus = mike.blur;
                    mike.onwebkitspeechchange = function(e) {
                        //console.log(e); // SpeechInputEvent
                        document.getElementById('txt').value = mike.value;
                    };
                }
            );
        </script>
    <div class="user_recommendation warning_box right">
        <h3 class="mb10"><?php echo Messages::getMessage(194) ?></h3>
        <ul class="left">
            <li>
                <?php echo Messages::getMessage(195) ?>
            </li>
            <li>
                <?php echo Messages::getMessage(196) ?>
            </li>
            <li>
                <?php echo Messages::getMessage(197) ?>
            </li>
            <li>
                <?php echo Messages::getMessage(198) ?>
            </li>
        </ul>
        <p><?php echo Messages::getMessage(199) ?></p>
    </div>
        </div>
</div>
<!-- box-->
<div class="box care_place">
    <h3 class="car_type"><?php echo Messages::getMessage(201) ?></h3>

    <div class="panel">

        <ng-form name="form">
            <label>Страна:</label>

            <select id="select_country" ng-model="location" required  name="location"
                    ng-options="item.name for item in locations" ng-change="countryChange();"></select>

           <span class="input-help-user-register" ng-show="submitted && form.location.$error.required">
                <span ng-show="form.location.$error.required">
                    <h4><?php echo Messages::getMessage(156) ?></h4>
                </span>
           </span>

<!--            <div id="icon">-->
<!--                <img ng-src="--><?php //echo Yii::app()->request->baseUrl; ?><!--/uploads/flags/{{location.flag_name}}" class="left" id="country_item">-->
<!--            </div>-->
        </ng-form>

    </div>

    <div class="panel" ng-show="toShowSubLocations();">
        <label class="small_label"><?php echo Messages::getMessage(202) ?></label>
        <select ng-model="subLocation" ng-options="item.name for item in subLocations"></select>
    </div>
    <div class="panel">
        <label style="margin-left: 325px;"><?php echo Messages::getMessage(203) ?></label>
        <input type="text" style="width:180px; padding: 12px 10px; border: 1px dotted #434343;" type="text" ng-model="optional_location" x-webkit-speech/>
    </div>
</div>
<?php echo $this->renderPartial('photoUploader'); ?>
</form>
<!-- ************************* END ANGULAR IMPLEMENTATION ***************************************** -->
</div>

<input type="hidden" id="usd_rate" value="{{rate_to_usd}}">
<input type="hidden" id="eur_rate" value="{{rate_to_euro}}">
<input type="hidden" id="rate_id" value="1">

<script>
    function correctPrices(dom){
        var value = dom.value;
        var usd_rate = $("#usd_rate").val();
        var eur_rate = $("#eur_rate").val();

        if(dom.id == "id_176"){
            $("#id_177").val(Math.ceil(value / usd_rate));
            $("#id_178").val(Math.ceil(value / eur_rate));
            $("#rate_id").val(1);
        }
        else if(dom.id == "id_177"){
            $("#id_176").val(Math.ceil(value * usd_rate));
            $("#id_178").val(Math.ceil(value * usd_rate / eur_rate));
            $("#rate_id").val(2);
        }
        else if(dom.id == "id_178"){
            $("#id_176").val(Math.ceil(value * eur_rate));
            $("#id_177").val(Math.ceil(value * eur_rate / usd_rate));
            $("#rate_id").val(3);
        }

    }
</script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-controllers/autorentForm.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-models/services.js"></script>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ajaxfileupload.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/UploadHelper.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-controllers/upload_controller.js" type="text/javascript"></script>