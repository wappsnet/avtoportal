<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Yerem
 * Date: 8/31/13
 * Time: 10:02 PM
 * To change this template use File | Settings | File Templates.
 */
?>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-controllers/my_announcements.js"></script>
<div class="announcement_field " ng-controller="myAnnouncementsController" ng-init="init()">
    <div class="announcement_field_left left decoration" style="width: 100%">
        <div class="title">
            <h3><?php echo Messages::getMessage(221); ?></h3>
        </div>
        <div class="user_recommendation">
            <h2 class="for_user"><?php echo Messages::getMessage(222); ?></h2>
            <ul>
                <li>
                    <?php echo Messages::getMessage(223); ?>
                </li>
                <li>
                    <?php echo Messages::getMessage(224); ?>
                </li>
                <li>
                    <?php echo Messages::getMessage(225); ?>
                </li>
                <li>
                    <?php echo Messages::getMessage(226); ?>
                </li>
                <li>
                    <?php echo Messages::getMessage(227); ?>
                </li>
                <li>
                    <?php echo Messages::getMessage(228); ?>
                </li>
                <li>
                    <?php echo Messages::getMessage(229); ?>
                </li>

                <li>
                    <?php echo Messages::getMessage(230); ?>
                </li>

            </ul>

        </div>
        <h3 class="car_type"><?php echo Messages::getMessage(231); ?></h3>
        <table style="width: 100%">
            <thead>
            <tr>
                <td class="type" style="width: 200px"><?php echo Messages::getMessage(232); ?></td>
                <td class="type" style="width: 200px"> <?php echo Messages::getMessage(233); ?></td>

                <td class="price"><?php echo Messages::getMessage(234); ?></td>
<!--                <td class="edit">--><?php //echo Messages::getMessage(235); ?><!--</td>-->
                <td class="extend"><?php echo Messages::getMessage(236); ?></td>
                <td class="above"><?php echo Messages::getMessage(237); ?></td>
                <td class="above"><?php echo Messages::getMessage(238); ?></td>
<!--                <td class="speedy">--><?php //echo Messages::getMessage(239); ?><!--</td>-->
<!--                <td class="speedy">--><?php //echo Messages::getMessage(240); ?><!--</td>-->
<!--                <td class="speedy">--><?php //echo Messages::getMessage(241); ?><!--</td>-->
            </tr>
            </thead>
            <tbody>
            <tr ng-repeat="announcement in announcements">
                <td style="width: 200px"> {{announcement.announcement_type}}</td>
                <td style="width: 200px"> {{announcement.announcement_name}}</td>
                <!--<td> {{announcement.announcement_name}}</td>-->
                <td> {{announcement.price}}</td>
<!--                <td> <a href="editForm?id={{announcement.id}}&formType={{announcement.announcement_type_id}}"><img src="images/edit.png " width="14px" height="13px"></a> </td>-->
                <td>
                    <select ng-model="announcement.currentTerm"
                            ng-options="term.label for term in terms" >
                    </select>
<!--                    <img src="images/top_item.png " width="14px" height="12px" style="cursor: pointer" ng-click="checkDate(announcement)">-->
                </td>
                <td ng-bind="announcement.validDate"></td>
                <td> <a style="cursor: pointer" ng-click="deleteAnnouncement(announcement.id, announcement.announcement_type_id, $index, $event)"><img src="images/delete.png " width="13px" height="13px"> </a></td>
<!--                <td> <img src="images/top_item.png " width="14px" height="12px"  ng-click="addToUrgent(announcement.id, announcement.announcement_type_id)" style="cursor: pointer"></td>-->
<!--                <td> <img src="images/search_item.png" width="14px" height="12px" ng-click="addToListTop(announcement.id, announcement.announcement_type_id)" style="cursor: pointer"> </td>-->
<!--                <td> <img src="images/home_item.png" width="14px" height="12px" ng-click="addToHomeTop(announcement.id, announcement.announcement_type_id)" style="cursor: pointer"> </td>-->
            </tr>
            </tbody>
        </table>
        <div class="notification">
            <a href="addAnnouncement" class="add_notification"> <?php echo Messages::getMessage(242); ?></a>
            <?php echo CHtml::Link(Messages::getMessage(168),Yii::app()->createUrl( 'site/siteRules' ),array('class'=>'notification_link'));?>
        </div>


    </div>

    <?php echo $this->renderPartial('//layouts/paymentDiv'); ?>


</div>