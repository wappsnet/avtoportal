<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Edgar
 * Date: 8/4/13
 * Time: 11:57 PM
 * To change this template use File | Settings | File Templates.
 */?>

<div style="display: none" id="appa_id"> <?php echo $appa_id; ?>  </div>

<div class="left" ng-controller="appa_view_controller" ng-init="init();" >


    <div class="middle_panel left decoration" style="margin-top: 25px;width: 770px !important;min-height: 500px;" cg-busy="{promise:promise,message:'loading',backdrop:true}">
        <div class="title">
            <h3 ng-bind="appa_item.title "></h3>
        </div>
        <div class="news_description"  style="padding: 15px;" ng-bind-html="appa_item.text | unsafe">
        </div>

    </div>

    <div style="clear: both"></div>
    <div style="margin-left: 5px">
        <google-map center="center" draggable="true" zoom="zoom" markers="markers"
                    class="angular-google-map"  style="height: 400px;width: 760px;"></google-map>

    </div>
</div>



<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-controllers/appa_view_controller.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-models/services.js"></script>