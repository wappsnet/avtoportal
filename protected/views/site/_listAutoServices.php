<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Edgar
 * Date: 11/17/13
 * Time: 7:32 PM
 * To change this template use File | Settings | File Templates.
 */?>

<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
<input type="hidden" id="loc_id" value="<?php echo $loc_id; ?>" >
<input type="hidden" id="location" value="<?php echo $location; ?>">

<div class="left" ng-controller="autoServicesFromController" ng-init="init();" style="margin-top: 25px">
    <div class="middle_panel left decoration news_list_page">

        <div class="title">
            <h3> Авторемонтный Салон</h3>
        </div>

        <div class="middle_panel_box1" style="min-height: 200px !important;cursor: pointer">

            <div ng-repeat="service in services" style="float: left;margin: 10px;font-size: 12px" >
               <h3 style="font-weight: bold"> <a ng-click="toDetailedInfo(service.id)" > {{service.name}} </a> </h3>

            </div>
        </div>
    </div>
    <div style="clear: both"></div>
    <div style="margin-left: 5px">

        <google-map center="center" draggable="true" zoom="zoom" markers="markers"
                    class="angular-google-map"  style="height: 400px;width: 770px;"></google-map>
    </div>

</div>

<div style="display: none" id="url_helper">
    <?php echo CHtml::Link("",Yii::app()->createUrl( $detail_url ));?>
</div>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-controllers/listAutoSerivesFromPlace_controller.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-models/services.js"></script>