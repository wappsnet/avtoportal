<div class="pagination">
    <ul class="yiiPager">
        <li class="page" ng-hide="pagingProperties.getCurrentPage() == 0">
            <a href="#" ng-click="pagingProperties.setCurrentPage(0)"> &lt;&lt; </a>
        </li>

        <li class="page" ng-hide="pagingProperties.getCurrentPage() == 0">
            <a href="#" ng-click="pagingProperties.setCurrentPage(pagingProperties.getCurrentPage()-1)"> &lt; </a>
        </li>

        <li class="page" ng-show="pagingProperties.getCurrentPage()-5 > 0">
            <a style="cursor: pointer" ng-click="pagingProperties.setCurrentPage(pagingProperties.getCurrentPage()-6)" >
                <span ng-bind="pagingProperties.getCurrentPage()-5"/>
            </a>
        </li>

        <li class="page" ng-show="pagingProperties.getCurrentPage()-4 > 0">
            <a style="cursor: pointer" ng-click="pagingProperties.setCurrentPage(pagingProperties.getCurrentPage()-5)" >
                <span ng-bind="pagingProperties.getCurrentPage()-4"/>
            </a>
        </li>

        <li class="page" ng-show="pagingProperties.getCurrentPage()-3 > 0">
            <a style="cursor: pointer" ng-click="pagingProperties.setCurrentPage(pagingProperties.getCurrentPage()-4)" >
                <span ng-bind="pagingProperties.getCurrentPage()-3"/>
            </a>
        </li>

        <li class="page" ng-show="pagingProperties.getCurrentPage()-2 > 0">
            <a style="cursor: pointer" ng-click="pagingProperties.setCurrentPage(pagingProperties.getCurrentPage()-3)" >
                <span ng-bind="pagingProperties.getCurrentPage()- 2"/>
            </a>
        </li>

        <li class="page" ng-show="pagingProperties.getCurrentPage()-1 > 0">
            <a style="cursor: pointer" ng-click="pagingProperties.setCurrentPage(pagingProperties.getCurrentPage()-2)" >
                <span ng-bind="pagingProperties.getCurrentPage()- 1"/>
            </a>
        </li>

        <li class="page" ng-show="pagingProperties.getCurrentPage() > 0 && pagingProperties.getCurrentPage() < announcements.length">
            <a style="cursor: pointer;" ng-click="pagingProperties.setCurrentPage(pagingProperties.getCurrentPage()-1)">
                <span ng-bind="pagingProperties.getCurrentPage()"/>
            </a>
        </li>

        <li class="page" ng-show="pagingProperties.getCurrentPage() + 1 <= (pagingProperties.getNumberOfPages() | number:0)" >
            <a style="cursor: pointer;text-decoration: underline" ng-click="pagingProperties.setCurrentPage(pagingProperties.getCurrentPage())">
                <span ng-bind="pagingProperties.getCurrentPage() + 1"/>
            </a>
        </li>

        <li class="page" ng-show="pagingProperties.getCurrentPage() + 2 <= (pagingProperties.getNumberOfPages() | number:0)" >
            <a style="cursor: pointer;" ng-click="pagingProperties.setCurrentPage(pagingProperties.getCurrentPage()+1)">
                <span ng-bind="pagingProperties.getCurrentPage() + 2"/>
            </a>
        </li>

        <li class="page" ng-show="pagingProperties.getCurrentPage() + 3 <= (pagingProperties.getNumberOfPages() | number:0)" >
            <a style="cursor: pointer;" ng-click="pagingProperties.setCurrentPage(pagingProperties.getCurrentPage()+2)">
                <span ng-bind="pagingProperties.getCurrentPage() + 3"/>
            </a>
        </li>

        <li class="page" ng-show="pagingProperties.getCurrentPage() + 4 <= (pagingProperties.getNumberOfPages() | number:0)" >
            <a style="cursor: pointer;" ng-click="pagingProperties.setCurrentPage(pagingProperties.getCurrentPage()+3)">
                <span ng-bind="pagingProperties.getCurrentPage() + 4"/>
            </a>
        </li>

        <li class="page" ng-show="pagingProperties.getCurrentPage() + 5 <= (pagingProperties.getNumberOfPages() | number:0)" >
            <a style="cursor: pointer;" ng-click="pagingProperties.setCurrentPage(pagingProperties.getCurrentPage()+4)">
                <span ng-bind="pagingProperties.getCurrentPage() + 5"/>
            </a>
        </li>

        <li class="page" ng-hide="pagingProperties.getCurrentPage() >= announcements.length/pagingProperties.getPageSize() - 1">
            <a href="#" ng-click="pagingProperties.setCurrentPage(pagingProperties.getNumberOfPages()-1)"> &gt;&gt; </a>
        </li>

        <li class="page" ng-hide="pagingProperties.getCurrentPage() >= announcements.length/pagingProperties.getPageSize() - 1">
            <a href="#" ng-click="pagingProperties.setCurrentPage(pagingProperties.getCurrentPage()+1)"> &gt; </a>
        </li>

    </ul>

    <?php if($showOptions): ?>
        <div class="bottom_paging" style="width: auto; margin-top: -5px; margin-right: 20px;">
            <span>одна страница</span>
        <span>
            <select ng-init="pageSizeTMP = pagingProperties.getPageSizeOptions()[0]" ng-model="pageSizeTMP" ng-options="model.id for model in pagingProperties.getPageSizeOptions()" ng-change="pagingProperties.setPageSize(pageSizeTMP.id)">
            </select>
        </span>
            <span>заявка</span>
        </div>
    <?php endif; ?>
</div>