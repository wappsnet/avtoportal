<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Edgar
 * Date: 1/12/14
 * Time: 10:32 PM
 * To change this template use File | Settings | File Templates.
 */?>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
<input type="hidden" id="isYerevan" value="<?php echo $isYerevan; ?>" >
<div class="left" ng-controller="autoServicesByPlaceController" ng-init="init();" style="margin-top: 25px;margin-bottom: 25px;">
    <div class="middle_panel left decoration news_list_page" >

        <div class="title">
            <h3> Авторемонтный Салон</h3>
        </div>

        <div class="middle_panel_box1" style="padding: 10px;" >

            <div ng-repeat="service in services" style="float: left;margin: 10px;font-size: 12px" >
                <h3 style="font-weight: bold;width: 220px !important;cursor: pointer" > <a ng-click="toDetailedInfo(service.id)" > {{service.name}} ({{service.count}}) </a> </h3>

            </div>

            <div style="clear: both"></div>
        </div>
        <div style="clear: both"></div>
    </div>
    <div style="clear: both"></div>
    
    <div style="margin-left: 5px">
        <google-map center="center" draggable="true" zoom="zoom" markers="markers"
                    class="angular-google-map"  style="height: 400px;width: 770px;"></google-map>
    </div>

</div>

<div style="display: none" id="url_helper">
    <?php echo CHtml::Link("",Yii::app()->createUrl( $detail_url ));?>
</div>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-controllers/listAutoServiceByPlace_controller.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-models/services.js"></script>