<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Edgar
 * Date: 7/6/13
 * Time: 7:06 PM
 * To change this template use File | Settings | File Templates.
 */

?>
<div ng-class="ng-clak">

<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/right_carusel.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/datepicker.css"/>
<?php Yii::app()->getClientScript()->registerScriptFile(Yii::app()->request->baseUrl . '/js/jquery.carouFredSel.js'); ?>
<div style="display: none" id="car_id"><?php echo $car_id ?> </div>

<div class="announcement_field" ng-controller="carDetailedController" ng-init="initIt();"
     cg-busy="{promise:promise,message:'loading',backdrop:true}" >

    <div class="announcement_field_left left selected_car_info_block decoration">
        <div class="title" style="width: 755px;">
            <h3 style="float: left"><?php echo Messages::getMessage(284) ?></h3>
            <h3 style="float: right"><?php echo Messages::getMessage(191) ?> - {{announcement.view_count}}</h3>
        </div>

        <ul class="car_description_banner" style="width: 780px;">
            <li class="car_id" ng-click="toMainPage();" id="main_tab">ID {{ announcement.id }}</li>
            <li class="border_item"> <img src="images/li_border_item.png"   width="3"  height="35"  /></li>
            <li class="auto_credit" id="loan_tab"><a href="" ng-click="toLoanPage();" >Автокредит <img src="images/item1.png" class="ml10" />
                </a></li>
            <li class="border_item"> <img src="images/li_border_item.png"   width="3"  height="35"  /></li>
            <li class="" id="insurance_tab"><a href="" ng-click="toInsurancePage();" >РОСГОССТРАХ <img src="images/item2.png" class="ml10" /> </a>
            </li>


            <li class="border_item_car" >
                <span  style="cursor: pointer;">
                    <img src="images/remember_plus.png " ng-show="!remembered" ng-click="addToRememberedList(announcement.id, announcement.announcement_name, announcement.price, announcement.main_image, announcement.rate_id)">
                    <img src="images/remember_minus.png " ng-show="remembered" ng-click="removeFromRememberedList(announcement.id, announcement.announcement_name, announcement.price, announcement.main_image)">
                </span>
            </li>
        </ul>

        <?php if($this->isAdmin()): ?>
            <div class="right" style="margin-right: 15px; margin-bottom: 10px" ng-hide="announcement.isAccepted">
                <a class="accept" ng-click="acceptItem(announcement.id, $event)">Accept</a>
                <a class="delete" style="width: 40px;" ng-click="deleteItem(announcement.id, $event)">Delete</a>
            </div>
        <?php endif ?>

        <div style="width: 785px; margin-right: 0;">

            <?php echo $this->renderPartial('detailed_left'); ?>

            <div style="">
            <?php echo $this->renderPartial('detailed_main'); ?>



            <!-- Bank Calculator-->
            <div  class="left ml15 car_detailed" id="div1" style="display: none">
                <img src="images/name_bank.png">

                <div class="appa_title"><a><h2 class="appa_title_text"> <?php echo Messages::getMessage(147) ?> </h2></a></div>
                <div class="row">
                    <label for="bank">
                        <?php echo Messages::getMessage(148) ?>
                    </label>
                    <input ng-model="banks[0].name_eng" class="right" disabled="true"/>
                    <!--<span ng-bind="banks[0].name_eng" class="row_select_big" style="width:180px !important;" ></span>-->
                   <!-- <select name="bank" ng-model="bank" class="row_select_big" style="width:180px !important;" disabled="true"
                        ng-options="bank.name_eng for bank in banks">
                    </select>-->
                </div>

                <div class="row">
                    <label for="rate">
                        <?php echo Messages::getMessage(149) ?>
                    </label>
                    <input name="rate" ng-model="rate" disabled="true" class="right">
                </div>

                <div class="row">
                    <label for="price">
                        <?php echo Messages::getMessage(150) ?>
                    </label>
                    <input name="price" ng-model="announcement.price"  class="right">
                </div>
                <div class="row">
                    <label for="downpayment">
                        <?php echo Messages::getMessage(151) ?>
                    </label>
                    <select name="downpayment" ng-model="downpayment"
                            ng-options="payment.value for payment in downpayments">
                    </select>
                </div>

                <div class="row">
                    <label for="amount">
                        <?php echo Messages::getMessage(152) ?>
                    </label>
                    <input name="amount" ng-model="amount" disabled="true" class="right">
                </div>

                <div class="row">
                    <label for="term">
                        <?php echo Messages::getMessage(153) ?>
                    </label>
                    <select name="term" ng-model="term_month"
                            ng-options="term.value for term in term_months">
                    </select>
                </div>

                <div class="row">
                    <label for="monthly">
                        <?php echo Messages::getMessage(154) ?>
                    </label>
                    <input name="monthly" ng-model="monthly_payment" disabled="true" class="right">
                </div>

                <div style="margin:20px auto;width:132px";>
                <a ng-click="calculate()" style="cursor: pointer" class="red_btn">
                    <span> <?php echo Messages::getMessage(155) ?></span>
                </a>
            </div>
        </div>
            <!-- Bank Calculator-->

            <!-- Insurance Calculator -->
            <div class="left rosgostrax  ml15 " id="div2" style="display: none">
                <img  src="images/name_appa1.png">

                <div class="appa_title"><h2 class="appa_title_text"><?php echo Messages::getMessage(285) ?></h2></div>
                <div class="row">
                    <label for="auto_type">
                        <?php echo Messages::getMessage(286) ?>
                    </label>
                    <select name="auto_type" ng-model="autoType"
                            ng-options="type.name_eng for type in autoTypes" style="width: 200px">
                    </select>
                </div>

                <div class="row">
                    <label for="auto_usage">
                        <?php echo Messages::getMessage(287) ?>
                    </label>
                    <select name="auto_usage" ng-model="autoUsage"
                            ng-options="usage.name_eng for usage in autoUsages" style="width: 200px">
                    </select>
                </div>

                <div class="row">
                    <label for="car_power">
                        <?php echo Messages::getMessage(288) ?>
                    </label>
                    <input name="car_power" ng-model="announcement.engine_power" disabled="true" class="imp_right">
                </div>

                <div class="row">
                    <label for="car_driver" style="width:292px;">
                        <?php echo Messages::getMessage(289) ?>
                    </label>
                    <input type="checkbox" name="car_driver" ng-model="driver" class="left block" class="imp_right" style="margin-top: 9px; margin-bottom: 9px; float: right;">
                </div>
                <!-- ***************** drivers ************** -->
                <div class="row" ng-show="driver">
                    <div class="row driver_box" ng-repeat="tempDriver in drivers ">
                        <div class="driver_title">
                            <h2>Водитель {{$index+1}}</h2>
                            <a class="delete_item" style="cursor: pointer" ng-click="removeDriver($index, $event)" ng-hide="$index == 0"></a>
                            <a class="minimize_item" style="cursor: pointer" ng-click="tempDriver.hide = !tempDriver.hide"></a>
                        </div>
                        <div ng-show="!tempDriver.hide">
                        <div class="row">
                            <label class="right" style="margin-left: -10px;">
                                <?php echo Messages::getMessage(290) ?>
                            </label>
                            <input calendar ng-model="tempDriver.birth_date" class="left">

                            <div class="left calculator"></div>
                        </div>
                        <div class="row">
                            <label class="mark_item" style="margin-left: -10px;">
                                <?php echo Messages::getMessage(291) ?>

                            </label>

                            <input calendar ng-model="tempDriver.card_date" class="left">

                            <div class="left calculator"></div>
                        </div>
                        <div class="row label_row">

                            <input type="checkbox" ng-model="tempDriver.appa_rule" class="left block" style="margin-top: 9px; margin-bottom: 9px; margin-left: -10px;">
                            <label class="mark_item left" style="margin-left: -5px;">
                                <?php echo Messages::getMessage(292) ?>
                            </label>

                        </div>

                        <div class="row label_row">

                            <input type="checkbox" style="margin-top: 9px; margin-bottom: 9px; margin-left: -5px;" ng-model="tempDriver.appa_condition" class="left block">
                            <label style="margin-left: -5px;">
                                <?php echo Messages::getMessage(293) ?>
                            </label>
                        </div>
                        </div>

                    </div>
                    <a ng-click="addDriver()" style="cursor: pointer">
                        <input type="button" value="Добавить водителя" class="add_driver"/>
                    </a>
                </div>
                <!-- ***************** drivers ************** -->


                <div class="row label_row">

                    <input style="margin-left: 15px; margin-top: 9px;" type="checkbox" name="appa_condition" ng-model="appaCondition" class="left block">
                    <label style="margin-left: -5px; margin-bottom: 9px;" for="appa_condition">
                        <?php echo Messages::getMessage(292) ?>
                    </label>
                </div>


                <div class="row label_row">

                    <input style="margin-left: 15px; margin-top: 9px;" type="checkbox" name="appa_rule" ng-model="appaRule" class="left block">
                    <label style="margin-left: -5px; margin-bottom: 9px;" for="appa_rule">
                        <?php echo Messages::getMessage(293) ?>
                    </label>
                </div>


                <div class="row">

                    <select name="appa_contract" ng-model="autoContractPeriod"
                            ng-options="usage.name_eng for usage in appaContractPeriods" style="width: 200px">
                    </select>
                    <label for="appa_contract">
                        <?php echo Messages::getMessage(294) ?>
                    </label>
                </div>

                <div class="row">

                    <input name="appa_payment" ng-model="appaPayment" disabled="true" style="float:right;">
                    <label for="appa_payment" class=" left">
                        <?php echo Messages::getMessage(295) ?>
                    </label>
                </div>

                <div style="margin:20px auto;width:132px">
                    <a ng-click="calculateAppa()" style="cursor: pointer" class="red_btn">
                        <span> <?php echo Messages::getMessage(296) ?></span>
                    </a>
                </div>

            </div>
            <!-- Insurance Calculator -->
        </div>
        <?php echo $this->renderPartial('detailedCarusel', array('models'=>$models)); ?>

        </div>
    </div>

    <?php echo $this->renderPartial('//layouts/rememberedItems'); ?>


</div>


  <!--  main-content-->


<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-controllers/carDetailed_controller.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-models/services.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/fancybox.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>

</div>