<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Edgar
 * Date: 10/3/13
 * Time: 12:24 AM
 * To change this template use File | Settings | File Templates.
 */
?>

<div ng-class="ng-cloak">
<link href="http://stg.odnoklassniki.ru/share/odkl_share.css" rel="stylesheet">
<script src="http://stg.odnoklassniki.ru/share/odkl_share.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/right_carusel.css"/>
<?php Yii::app()->getClientScript()->registerScriptFile(Yii::app()->request->baseUrl . '/js/jquery.carouFredSel.js'); ?>
<div style="display: none" id="car_id"><?php echo $car_id ?> </div>

<div class="announcement_field" ng-controller="taxiDetailedController" ng-init="initIt();"
     cg-busy="{promise:promise,message:'loading',backdrop:true}">
    <div class="announcement_field_left left selected_car_info_block decoration">
        <div class="title">
            <h3 style="float: left"><?php echo Messages::getMessage(125) ?></h3>
            <h3 style="float: right"><?php echo Messages::getMessage(191) ?> - {{announcement.view_count}}</h3>
        </div>
        <div class="car_description_banner" style="height: 25px;padding: 5px;margin-bottom: 10px;">
            <div style="font-size: 18px;color: red;float: left">ID {{ announcement.id }}</div>
            <div style="cursor: pointer;float: right">
                <img src="images/remember_plus.png " ng-show="!remembered" ng-click="addToRememberedList(announcement.id, announcement.announcement_name, announcement.payment_type+' '+announcement.price, announcement.main_image, announcement.rate_id)">
                <img src="images/remember_minus.png " ng-show="remembered" ng-click="removeFromRememberedList(announcement.id, announcement.announcement_name, announcement.price, announcement.main_image)">
            </div>
        </div>
        <?php if($this->isAdmin()): ?>
            <div class="right" style="margin-right: 15px;margin-bottom: 10px" ng-hide="announcement.isAccepted">
                <a class="accept" ng-click="acceptItem(announcement.id, $event)">Accept</a>
                <a class="delete" style="width: 40px;" ng-click="deleteItem(announcement.id, $event)">Delete</a>
            </div>
        <?php endif ?>

        <div>
            <?php echo $this->renderPartial('detailed_left'); ?>

            <?php echo $this->renderPartial('detailed_main'); ?>
        </div>

    </div>

    <?php echo $this->renderPartial('//layouts/rememberedItems'); ?>


</div>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-controllers/detailedTaxi_controller.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-models/services.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/fancybox.js"></script>

</div>