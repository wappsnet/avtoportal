<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Yerem
 * Date: 10/25/13
 * Time: 12:53 AM
 * To change this template use File | Settings | File Templates.
 */
?>
<div class="Seller_information">
    <div>
        <div ng-show="announcement.isCarForm">
            <div class="left info" style="margin-top: 10px !important; margin-left: 18px;" ng-show="announcement.tax_id"> Растаможен</div>
            <div class="left info" style="margin-top: 10px !important; margin-left: 18px;" ng-show="!announcement.tax_id"> Не растаможен</div>
        </div>
        <div class="right" style="margin-right: 17px; margin-top:10px;"> Дата: <span class="red"> {{announcement.created | date:'yyyy-MM-dd' }}</span>
        </div>
    </div>
    <div class="grey_banner">
        <h2> Информация о продавце </h2>

        <p> Тел. -1 : {{announcement.phone_id}} </p>

        <p> Тел. -2 : {{announcement.userInfo.phone1}} </p>

        <p ng-show="announcement.isCarForm">
            Местонахождения автомобиля - {{announcement.userInfo.location}}
        </p>
        <a style="cursor: pointer" ng-click="findUserAnnouncements($event)">Все предложения продавца</a><br/><br/>
        <a href="#" class="contact_seller" ng-click="showWriteToSeller($event)" ng-show="!renderWriteToSeller">Написать продавцу</a>

        <a href="#" class="contact_seller" ng-click="hideWriteToSeller($event)" ng-show="renderWriteToSeller">Закрыть</a>
        <br/><br/>
        <div ng-show="renderWriteToSeller && !mailSended">
            <form name="myForm" ng-submit="sendMail(myForm);" novalidate  method="post">
            <span>
                <div class="mess_body">
                    <div style="float: left">
                        <label for="user_name" ><div style="float: left"> <?php echo Messages::getMessage(141) ?></div> <div style="color: red;float: left">*</div></label>
                    </div>
                    <br/>
                    <div style="clear: both"></div>
                    <div style="float: left">
                        <input style="padding: 6px; margin-top: 7px; width: 200px; margin-bottom: 13px; border: 1px dotted #434343;" name="user_name" type="text" ng-model="user.name" required x-webkit-speech/>
                        <div class="input-help-user-register" ng-show="userForm.user_name.$invalid">
                            <span ng-show="userForm.user_name.$error.required">
                                <h4><?php echo Messages::getMessage(156) ?></h4>
                            </span>
                        </div>
                    </div>
                    <div style="clear: both"></div>
                    <div style="float: left">
                        <label for="user_phone" > <?php echo Messages::getMessage(142) ?> </label>
                    </div>
                    <br/>
                    <div style="clear: both"></div>
                    <div style="float: left">
                        <input style="padding: 6px; margin-top: 7px; margin-bottom: 13px; width: 200px; border: 1px dotted #434343;" id="user_phone" type="text" ng-model="user.phoneNumber" x-webkit-speech>
                    </div>
                    <div style="clear: both"></div>
                    <div style="float: left">
                        <label for="user_mail" ><div style="float: left"> <?php echo Messages::getMessage(143) ?> </div> <div style="color: red;float: left">*</div></label>
                    </div>
                    <br/>
                    <div style="clear: both"></div>
                    <div style="float: left">
                        <input style="padding: 6px; margin-top: 7px; margin-bottom: 13px; width: 200px; border: 1px dotted #434343;" x-webkit-speech name="user_mail" type="email" ng-model="user.email"  required>
                        <div class="input-help-user-register" ng-show="userForm.user_mail.$invalid">
                            <span ng-show="userForm.user_mail.$error.required">
                                <h4><?php echo Messages::getMessage(156) ?></h4>
                            </span>
                            <span ng-show="userForm.user_mail.$error.email">
                                 <h4>wrong email</h4>
                            </span>
                        </div>
                    </div>
                    <div style="clear: both"></div>
                    <div style="float: left">
                        <div style="float: left">
                            <label for="user_code">
                                <div style="float: left"><?php echo Messages::getMessage(144) ?></div>
                                <div style="color: red;float: left">*</div>
                            </label>
                        </div>
                        <br/>
                        <div style="clear: both"></div>
                        <div style="float: left">
                            <img id="updater" src="<?php echo Yii::app()->createUrl('site/generateCaptcha'); ?>" height="30px" width="80px" style="float: left; margin-top:8px;"/>
                            <input style="padding: 6px; margin-top: 7px; margin-bottom: 13px; width: 110px; margin-left: 10px; border: 1px dotted #434343;" x-webkit-speech type="text" class="sml_input" ng-model="user.code" id="user_code" style="float: left;margin-left: 5px;margin-top: 3px">
                            <span class="input-help-user-register" ng-show="showCaptchaError">
                                <span>
                                   <h4><?php echo Messages::getMessage(145) ?></h4>
                                </span>
                            </span>
                            <div style="clear: both"></div>
                            <div style="float: left">
                                <a id="refresher" onclick="generateCaptcha()" style="cursor: pointer"><?php echo Messages::getMessage(146) ?></a>
                            </div>
                        </div>
                    </div>
                    <div style="clear: both"></div>
                    <div style="float: left;margin-top: 5px">
                        <label for="user_message" ><div style="float: left">Ваше сообщение</div> <div style="color: red;float: left">*</div></label>
                    </div>
                    <br/>
                    <div style="clear: both"></div>
                    <div style="float: left">
                        <input x-webkit-speech id="mike" style="width: 15px; padding: 5px; float: left; margin-left:1px; margin-right:2px; margin-top:10px;"/>
                        <textarea id="user_message" style="padding: 6px; margin-top: 7px; margin-bottom: 13px; width: 170px; border: 1px dotted #434343;" rows="8" cols="40" ng-model="user.message" >
                        </textarea>
                        <script>
                            $(document).ready(
                                function(){
                                    var mike = document.getElementById('mike');
                                    mike.onfocus = mike.blur;
                                    mike.onwebkitspeechchange = function(e) {
                                        //console.log(e); // SpeechInputEvent
                                        document.getElementById('user_message').value = mike.value;
                                    };
                                }
                            );
                        </script>
                    </div>

                </div>
                <div ng-show="mailSended">
                    <p>Message Send</p>
                </div>
            </span>


            <div>
                <input type="submit" class="contact_seller2" value="Отправить" ng-show="renderWriteToSeller">
            </div>

        </div>

        </ng-form>
    </div>
</div>
<script>


    function generateCaptcha() {
        var stamp = new Date().getTime();
        var src = $('#updater').attr("src") + "?st=" + stamp;
        $('#updater').attr("src", src);
    }

</script>