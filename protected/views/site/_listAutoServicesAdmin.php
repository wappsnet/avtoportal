<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Edgar
 * Date: 12/23/13
 * Time: 12:49 AM
 * To change this template use File | Settings | File Templates.
 */

?>

<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
<div class="left" ng-controller="autoServicesController" ng-init="init();">

    <div class="pagination">
        <ul class="yiiPager">
            <li class="page" ng-hide="currentPage == 0"> <a href="#" ng-click="currentPage=0"> &lt;&lt; </a></li>
            <li class="page" ng-hide="currentPage == 0"> <a href="#" ng-click="currentPage=currentPage-1">&lt; </a></li>
            <li class="page"><a ng-show="currentPage-5 > 0" style="cursor: pointer" ng-click="currentPage=currentPage-6" >{{currentPage-5}}</a></li>
            <li class="page"><a ng-show="currentPage-4 > 0" style="cursor: pointer" ng-click="currentPage=currentPage-5" >{{currentPage-4}}</a></li>
            <li class="page"><a ng-show="currentPage-3 > 0" style="cursor: pointer" ng-click="currentPage=currentPage-4" >{{currentPage-3}}</a></li>
            <li class="page"><a ng-show="currentPage-2 > 0" style="cursor: pointer" ng-click="currentPage=currentPage-3" >{{currentPage-2}}</a></li>
            <li class="page"><a ng-show="currentPage-1 > 0" style="cursor: pointer" ng-click="currentPage=currentPage-2">{{currentPage-1}}</a></li>
            <li class="page"><a ng-show="currentPage > 0 && currentPage < services.length" style="cursor: pointer;" ng-click="currentPage=currentPage-1">{{currentPage}}</a></li>
            <li class="page"><a ng-show="currentPage+1 <= (numberOfPages | number:0)" style="cursor: pointer;text-decoration: underline" ng-click="currentPage=currentPage">{{currentPage+1}}</a></li>
            <li class="page"><a ng-show="currentPage+2 <= (numberOfPages | number:0)" style="cursor: pointer" ng-click="currentPage=currentPage+1">{{currentPage+2}}</a></li>
            <li class="page"><a ng-show="currentPage+3 <= (numberOfPages | number:0)" style="cursor: pointer" ng-click="currentPage=currentPage+2">{{currentPage+3}}</a></li>
            <li class="page"><a ng-show="currentPage+4 <= (numberOfPages | number:0)" style="cursor: pointer" ng-click="currentPage=currentPage+3">{{currentPage+4}}</a></li>
            <li class="page"><a ng-show="currentPage+5 <= (numberOfPages | number:0)" style="cursor: pointer" ng-click="currentPage=currentPage+4">{{currentPage+5}}</a></li>
            <li class="page" ng-hide="currentPage >= services.length/pageSize - 1"> <a href="#" ng-click="currentPage=numberOfPages-1"> &gt;&gt; </a></li>
            <li class="page" ng-hide="currentPage >= services.length/pageSize - 1"> <a href="#" ng-click="currentPage=currentPage+1"> &gt; </a></li>
        </ul>
        <?php if($showOptions): ?>
            <div class="bottom_paging" style="width: auto; margin-top: -25px; margin-right: 20px;">
                <span>одна страница</span>
        <span>
            <select ng-init="pageSizeTMP = pagingProperties.getPageSizeOptions()[0]" ng-model="pageSizeTMP" ng-options="model.id for model in pagingProperties.getPageSizeOptions()" ng-change="pagingProperties.setPageSize(pageSizeTMP.id)">
            </select>
        </span>
                <span>заявка</span>
            </div>
        <?php endif; ?>
    </div><!--  pagination-->
    <!---->

    <div class="middle_panel left decoration" style="width: 750px">
        <div class="title">
            <h3><?php echo $title; ?></h3>

            <select name="sorting_option" ng-model="sortingOption"
                    ng-options="type.name_eng for type in sortingOptions" style="width: 140px" ng-change="sort()">
            </select>

        </div>


        <div style="display: none" id="url_helper">
             <?php echo CHtml::Link("",Yii::app()->createUrl( $detail_url ));?>
        </div>
        <div class="middle_panel_box"  ng-repeat="announcement in services | startFrom:currentPage*pageSize | limitTo:pageSize ">
            <div class="left item_block">
                <img ng-src="<?php echo Yii::app()->request->baseUrl . "/uploads/"; ?>{{announcement.main_image}}"
                     width="122px" height="90px" ng-click="toDetailedInfo(announcement.id,$event);" style="cursor: pointer">

                <div class="date"><span
                        ng-click="addToRememberedList(announcement.id, announcement.announcement_name, announcement.price, announcement.main_image, announcement.rate_id)"
                        style="cursor: pointer">remember</span> <span> {{ announcement.created | date:'dd.MM.yyyy'}}</span></div>
            </div>
            <div class="left description">
                <h2 class="left" ng-click="toDetailedInfo(announcement.id,$event);" id="announcement_name">
                    {{announcement.name}}
                </h2>
            </div>
            <div class="right mr10">
                <a class="accept" ng-click="acceptItem(announcement.id, $event)">Accept</a>
                <a class="delete" ng-click="deleteItem(announcement.id, $event)">Delete</a>
            </div>
        </div>
    </div><!-- middle_panel-->
    <div class="bottom_paging">
        <select class="big_select  hasCustomSelect" ng-model="pageSizeTMP" ng-options="model.id for model in pageSizeOptions" ng-change="setPageSize(pageSizeTMP.id)">

        </select>
    </div>

    <script type="text/javascript">
        (function(cash) {
            $(function() {
                window.setTimeout("$('select').selectbox()", 700);
            })
        })(jQuery)

        function showDesc(elem){
            $(elem).find(".desc").show();
        }

        function hideDesc(elem){
            $(elem).find(".desc").hide();
        }

        function tooltipStart(element){
            $(element).prev().css("display", "block");
        }

        function tooltipEnd(element){
            $(element).prev().css("display", "none");
        }


    </script>

    <script type="text/javascript" src="js/jquery.selectbox.min.js"></script>




</div>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-controllers/listAutoServices_controller.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-models/services.js"></script>