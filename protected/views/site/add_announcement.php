<div class="add_announcement right decoration">
    <div class="title"><h3 class="add_anonc_title"><?php echo Messages::getMessage(220); ?> </h3></div>
    <ul class="mt30">

        <?php if(!$this->isDealer()): ?>
            <li class="fm1"><?php echo CHtml::Link(Messages::getMessage(83),Yii::app()->createUrl( 'site/auto' ));?></li>
            <li class="fm2"><?php echo CHtml::Link(Messages::getMessage(86),Yii::app()->createUrl( 'site/autoRent' ));?></li>
            <li class="fm3"><?php echo CHtml::Link(Messages::getMessage(84),Yii::app()->createUrl( 'site/sparePartWheels' ));?></li>
            <li class="fm4"><?php echo CHtml::Link(Messages::getMessage(85),Yii::app()->createUrl( 'site/sparePart' ));?></li>
<!--            <li>--><?php //echo CHtml::Link(Messages::getMessage(87),Yii::app()->createUrl( 'site/autoService' ));?><!--</li>//-->
            <li class="fm5"><?php echo CHtml::Link(Messages::getMessage(88),Yii::app()->createUrl( 'site/ruralTech' ));?></li>
<!--            <li>--><?php //echo CHtml::Link(Messages::getMessage(89),Yii::app()->createUrl( 'site/shipping' ));?><!--</li>-->
<!--            <li class="fm"><img src="--><?php //echo Yii::app()->request->baseUrl; ?><!--/images/application-icon.png"/>--><?php //echo CHtml::Link(Messages::getMessage(90),Yii::app()->createUrl( 'site/transportation' ));?><!--</li>-->
<!--            <li>--><?php //echo CHtml::Link(Messages::getMessage(91),Yii::app()->createUrl( 'site/tourism' ));?><!--</li>//-->
            <li class="fm6"><?php echo CHtml::Link(Messages::getMessage(92),Yii::app()->createUrl( 'site/evacuator' ));?></li>
            <li class="fm7"><?php echo CHtml::Link(Messages::getMessage(93),Yii::app()->createUrl( 'site/taxi' ));?></li>
<!--            <li>--><?php //echo CHtml::Link(Messages::getMessage(94),Yii::app()->createUrl( 'site/trainingCar' ));?><!--</li>//-->
<!--            <li>--><?php //echo CHtml::Link(Messages::getMessage(95),Yii::app()->createUrl( 'site/jobForm' ));?><!--</li>//-->
        <?php endif ?>

        <?php if($this->isDealer()): ?>
            <li><?php echo CHtml::Link(Messages::getMessage(83),Yii::app()->createUrl( 'site/auto' ));?></li>
        <?php endif ?>
    </ul>
</div>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jQueryRotate.js"></script>
<script type="text/javascript">
    jQuery(".fm img").rotate({
        bind:
        {
            mouseover : function() {
                jQuery(this).rotate({animateTo:120})
            },
            mouseout : function() {
                jQuery(this).rotate({animateTo:0})
            }
        }
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
   $(".fm a").click(function(){
      $(".fm").clone()
          .css({'position':'absolute', 'z-index':'10000', 'left':$(".fm").offset()['left'], 'top':$(".fm").offset()['top']})
          .prependTo('.book')
          .animate({opacity:0.9,
          left:$(".book").offset()['left'],
          top:$(".book").offset()['top'],
              width:120,
              height:35
          },
          1000, function(){
              $(this).remove();
          });
   });
});
</script>