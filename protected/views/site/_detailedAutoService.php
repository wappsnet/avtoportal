<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Edgar
 * Date: 11/17/13
 * Time: 9:57 PM
 * To change this template use File | Settings | File Templates.
 */
?>
<div ng-class="ng-cloak">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/right_carusel.css"/>
    <?php Yii::app()->getClientScript()->registerScriptFile(Yii::app()->request->baseUrl . '/js/jquery.carouFredSel.js'); ?>
    <div style="display: none" id="car_id"><?php echo $car_id ?> </div>

    <div class="announcement_field" ng-controller="autoServiceDetailed_Controller" ng-init="initIt();"
         cg-busy="{promise:promise,message:'loading',backdrop:true}">
        <div class="announcement_field_left left selected_car_info_block decoration">
            <div class="title">
                <h3 style="float: left"><?php echo Messages::getMessage(297) ?></h3>
                <h3 style="float: right"><?php echo Messages::getMessage(191) ?> - {{announcement.view_count}}</h3>
            </div>
            <div class="car_description_banner" style="height: 25px;padding: 5px;margin-bottom: 10px;">
                <div style="font-size: 18px;color: red;float: left">ID {{ announcement.id }}</div>

            </div>
            <?php if($this->isAdmin()): ?>
                <div class="right" style="margin-right: 15px;margin-bottom: 10px" ng-hide="announcement.isAccepted">
                    <a class="accept" ng-click="acceptItem(announcement.id, $event)">Accept</a>
                    <a class="delete" style="width: 40px;" ng-click="deleteItem(announcement.id, $event)">Delete</a>
                </div>
            <?php endif ?>

            <div>

                <?php echo $this->renderPartial('detailed_left'); ?>

                <div class="car_middle_box left">
<!--                    <div class="top_row">-->
<!--                        <p class="left">Повысить положение</p>-->
<!--                        <a href="#" class="left"><img src="images/home_item.png " ng-click="showPayment($event, 1)"> </a>-->
<!--                        <a href="#" class="left"><img src="images/top_item.png " ng-click="showPayment($event, 2)" height="28px" width="26px"> </a>-->
<!--                        <a href="#" class="left"><img src="images/search_item.png" ng-click="showPayment($event, 3)" height="28px" width="26px"> </a>-->
<!--                    </div>-->
                    <!-- ******************************************* PAYMENT DIV ************************************************************* -->
                    <div id="additional_div" style="width: 400px; height: auto; border: 5px; border-color: darkslategrey"
                         ng-show="showPaymentDiv">
                        <div>
                            <h3 class="grey_title">Chose payment type</h3>

                            <h3 ng-show="serviceType ==1">Home Page - Add to Top Announcements</h3>

                            <h3 ng-show="serviceType ==2">Home Page - Add to Urgent Announcements</h3>

                            <h3 ng-show="serviceType ==3">Search - Add to Top Announcements</h3>

                            <div class="grey_block">
                            <span style="float: left">
                                <input type="radio" ng-model="paymentType" value="0">  <img src="images/po_arca.png"> <br/>
                            </span>
                            <span>
                                <input type="radio" ng-model="paymentType" value="1">  <img src="images/po_paypal.png"><br/>
                            </span>
                            </div>
                        </div>
                        <a href="#" ng-click="hideAddition($event)">close</a>
                        <a href="#" ng-click="makePayment($event)" ng-show="paymentType==0 || paymentType==1">Make Payment</a>
                    </div>
                    <!-- ************************************** END PAYMENT DIV ********************************************* -->
                    <div class="banner" style="width: 430px;">
                        <br/>
                        <h3 style="font-style: italic;font-weight: bold;height: 30px;"><?php echo Messages::getMessage(299) ?></h3>

                        <div ng-repeat="service in services" style="padding-left: 20px;width: 420px;">
                            <h4 style="font-style: italic;font-weight: bold;float: left"> {{$index + 1}}. &nbsp; {{service.name_eng}}</h4>
                            <h4 style="font-weight: bold;float: left"> &nbsp;&nbsp;&nbsp;  {{service.price}} RUR  </h4>
                        </div>
                        <div style="clear: both"></div>
                        <br/>
                        <div style="width: 400px;">
                            <h3 style="font-style: italic;font-weight: bold;float: left;"><?php echo Messages::getMessage(298) ?> </h3>
                            <h3 style="float: left;margin-left: 15px;">{{announcement.time_to}} &nbsp; - &nbsp;{{announcement.time_from}} </h3>
                        </div>
                        <div style="clear: both"></div>
                        <br/>

                        <div class="additional_information">
                            <h2><?php echo Messages::getMessage(192) ?></h2>
                            <p> {{announcement.description}}</p>
                        </div>
                        <div style="clear: both"></div>

                    </div>

                    <div style="clear: both"></div>
                    <br/>
                    <br/>
                    <google-map center="center" draggable="true" zoom="zoom" markers="markers"
                                class="angular-google-map"  style="height: 300px;width: 400px;"></google-map>


                </div>


            </div>

        </div>


        <?php echo $this->renderPartial('//layouts/rememberedItems'); ?>


    </div>

    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-controllers/detailedAutoService_controller.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-models/services.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/fancybox.js"></script>
</div>