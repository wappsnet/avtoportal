<div>
    <input type="hidden" name="Language" value="<?php echo $this->showAll; ?>" id="showAllAnnouncements">

    <?php echo $this->renderPartial('//site/pagingPart', array('showOptions'=>false)); ?>

    <?php if(isset($isDealerList)): ?>
    <div class="middle_panel left decoration" style="width: 760px;">
    <?php else: ?>
    <div class="middle_panel left decoration" >
    <?php endif; ?>

        <div class="title">
            <h3 style="width: 250px"><?php echo $title; ?></h3>
            <select name="sorting_option" ng-model="sortingOption"
                    ng-options="type.name_eng for type in sortingOptions" style="width: 150px;float: right" ng-change="sort()">
            </select>
            <div class="icon" style="cursor: pointer;float: right" ng-click="switchViewToList()">
                <img src="images/title_bg1.png " width="30px" height="26px" style="margin-top: -3px;" ng-show="showInLargeItems">
                <img src="images/title_bg1.1.png " width="30px" height="26px" style="margin-top: -3px;" ng-show="!showInLargeItems">
            </div>
            <div class="icon" style="cursor: pointer;float: right" ng-click="switchViewToLarge()">
                <img src="images/title_bg2.png " width="30px" height="26px" style="margin-top: -3px;" ng-show="showInLargeItems">
                <img src="images/title_bg2.1.png " width="30px" height="26px" style="margin-top: -3px;" ng-show="!showInLargeItems">
            </div>
        </div>

        <div style="display: none" id="url_helper">
            <?php echo CHtml::Link("",Yii::app()->createUrl( $detail_url ));?>
        </div>


        <?php echo $this->renderPartial('//site/_listTemplateLargeIcons', array('detail_url'=>$detail_url, 'isCar'=>$isCar, 'banner'=>$this->banner)); ?>
        <?php echo $this->renderPartial('//site/_listTemplateSmallIcons', array('detail_url'=>$detail_url, 'isCar'=>$isCar, 'banner'=>$this->banner)); ?>


    </div><!-- middle_panel-->

        <?php echo $this->renderPartial('//site/pagingPart', array('showOptions'=>true)); ?>

</div>
<script type="text/javascript">
    function showDesc(elem){
        $(elem).find(".desc").show();
    }
    function hideDesc(elem){
        $(elem).find(".desc").hide();
    }
    function tooltipStart(element){
        $(element).prev().prev().css("display", "block");
    }
    function tooltipEnd(element){
        $(element).prev().prev().css("display", "none");
    }
    function tooltipStartX(element){
        $(element).prev().css("display", "block");
    }
    function tooltipEndX(element){
        $(element).prev().css("display", "none");
    }
</script>

<script type="text/javascript" src="js/jquery.selectbox.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-models/services.js"></script>