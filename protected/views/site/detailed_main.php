<div class="car_middle_box left" id="div0">
<!--    <div class="top_row">-->
<!--        <p class="left">Повысить положение</p>-->
<!--        <a href="#" class="left"><img src="images/home_item.png " ng-click="addToHomeTop(announcement.id, announcement.announcement_type_id)" height="28px" width="26px"> </a>-->
<!--        <a href="#" class="left"><img src="images/top_item.png "  ng-click="addToUrgent(announcement.id, announcement.announcement_type_id)" height="28px" width="26px"> </a>-->
<!--        <a href="#" class="left"><img src="images/search_item.png" ng-click="addToListTop(announcement.id, announcement.announcement_type_id)" height="28px" width="26px"> </a>-->
<!--    </div>-->
    <?php echo $this->renderPartial('//layouts/paymentDiv'); ?>
    <!-- ******************************************* PAYMENT DIV *************************************************************
    <div id="additional_div" style="width: 400px; height: auto; border: 5px; border-color: darkslategrey"
         ng-show="showPaymentDiv">
        <div>
            <h3 class="grey_title">Chose payment type</h3>

            <h3 ng-show="serviceType ==1">Home Page - Add to Top Announcements</h3>

            <h3 ng-show="serviceType ==2">Home Page - Add to Urgent Announcements</h3>

            <h3 ng-show="serviceType ==3">Search - Add to Top Announcements</h3>

            <div class="grey_block">
                <span style="float: left">
                    <input type="radio" ng-model="paymentType" value="0">  <img src="images/po_arca.png"> <br/>
                </span>
                <span>
                    <input type="radio" ng-model="paymentType" value="1">  <img src="images/po_paypal.png"><br/>
                </span>
            </div>
        </div>
        <a href="#" ng-click="hideAddition($event)">close</a>
        <a href="#" ng-click="makePayment($event)" ng-show="paymentType==0 || paymentType==1">Make Payment</a>
    </div>
     ************************************** END PAYMENT DIV ********************************************* -->


    <div class="car_type" ng-bind="announcement.announcement_name" style="font-size: 15px">
    </div>
    <div class="car_type">

        <div style="display: none; position: absolute; background-color: #a8a8a8" class="tooltip_starter">
            <div>
                {{getCurrencyValueFirst(announcement.price, announcement.rate_id)}}
            </div>
            <div>
                {{getCurrencyValueSecond(announcement.price, announcement.rate_id)}}
            </div>
        </div>

        <span class="right selected_car_price pr5 price_ik" onmouseover="tooltipStart(this)" ng-hide="announcement.negotiable == 1" onmouseout="tooltipEnd(this)">
             {{announcement.price}} {{ getCurrencyLabel(announcement.rate_id) }}</span>

         <span  class="right selected_car_price pr5 price_ik"  ng-bind="announcement.payment_type">
        </span>

        <span  class="right selected_car_price pr5"  ng-show="announcement.negotiable == 1">
            <?php echo Messages::getMessage(179) ?>
        </span>
    </div>
    <div class="banner left banner_border" id="left_side">
        <div ng-repeat="property in announcement.propertiesLeft" ng-class-odd="'description_panel grey'"
             ng-class-even="'description_panel white_block '">
            <div class="left bold " style="min-width: 40px;max-width: 160px;"> {{property.prop_name}}</div>
            <div class="right" style="max-width: 120px; padding-top: 9px;  text-align: right" ng-show="property.prop_type != 3" >{{property.text}}</div>
            <div class="right" ng-show="property.prop_type == 3">
                <img src="images/green_icon.png " width="10px" height="10px"/>
            </div>
        </div>
    </div>
    <div class="banner left" id="right_side">
        <div ng-repeat="property in announcement.propertiesRight" ng-class-odd="'description_panel grey'" on-finish-render
             ng-class-even="'description_panel white_block '">
            <div class="left bold" style="min-width: 40px;max-width: 160px;"> {{property.prop_name}}</div>
            <div class="right" style="max-width: 120px; padding-top: 9px; text-align: right" ng-show="property.prop_type != 3">{{property.text}}</div>
            <div class="right" ng-show="property.prop_type == 3"><img src="images/green_icon.png "
                                                                      width="10px" height="10px"/></div>
        </div>
    </div>

    <div class="additional_information">
        <h2> <?php echo Messages::getMessage(192) ?> </h2>
        <div style="width: 382px; min-height: 80px; padding: 10px;background: none; border:2px solid #ffffff; background: rgba(255, 252, 244, 0.95); border-bottom:2px solid #ffffff;">
            {{announcement.description}}
        </div>
    </div>

    <div style="clear: left;height: 15px;"></div>
</div>

<script>
    function tooltipStart(element){
        $(element).prev().css("display", "block");
    }

    function tooltipEnd(element){
        $(element).prev().css("display", "none");
    }

</script>