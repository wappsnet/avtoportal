<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Edgar
 * Date: 9/25/13
 * Time: 12:18 AM
 * To change this template use File | Settings | File Templates.
 */?>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
<div class="left" ng-controller="evacuatorController" ng-init="init();">
    <div cg-busy="{promise:promise,message:'loading',backdrop:true,}" style="height: 700px;">

        <?php echo $this->renderPartial('_listTemplate',array('title'=>$title, 'detail_url'=>$detail_url , 'isCar' =>$isCar )); ?>

    </div>
</div>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-controllers/listEvacuatorAnnouncements_controller.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-models/services.js"></script>