<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Edgar
 * Date: 7/26/13
 * Time: 12:47 AM
 * To change this template use File | Settings | File Templates.
 */?>
<div class="left" style="width:807px; margin-right:0; margin-bottom:5px;" ng-controller="news-controller" ng-init="init();">
    <input type="hidden" name="Language" value="<?php echo $this->showAll; ?>" id="showAllAnnouncements">
    <div class="pagination" style="width: 785px;">
        <ul class="yiiPager">
            <li class="page" ng-hide="currentPage == 0"> <a href="" ng-click="currentPage=0"> &lt;&lt; </a></li>
            <li class="page" ng-hide="currentPage == 0"> <a href="" ng-click="currentPage=currentPage-1">&lt; </a></li>
            <li class="page"><a ng-show="currentPage-5 > 0" style="cursor: pointer" ng-click="currentPage=currentPage-6" href="">{{currentPage-5}}</a></li>
            <li class="page"><a ng-show="currentPage-4 > 0" style="cursor: pointer" ng-click="currentPage=currentPage-5" href="">{{currentPage-4}}</a></li>
            <li class="page"><a ng-show="currentPage-3 > 0" style="cursor: pointer" ng-click="currentPage=currentPage-4" href="">{{currentPage-3}}</a></li>
            <li class="page"><a ng-show="currentPage-2 > 0" style="cursor: pointer" ng-click="currentPage=currentPage-3" href="">{{currentPage-2}}</a></li>
            <li class="page"><a ng-show="currentPage-1 > 0" style="cursor: pointer" ng-click="currentPage=currentPage-2" href="">{{currentPage-1}}</a></li>
            <li class="page"><a ng-show="currentPage > 0 && currentPage < news.length" style="cursor: pointer;" ng-click="currentPage=currentPage-1" href="">{{currentPage}}</a></li>
            <li class="page"><a ng-show="currentPage+1 <= (numberOfPages | number:0)" style="cursor: pointer;text-decoration: underline" ng-click="currentPage=currentPage" href="">{{currentPage+1}}</a></li>
            <li class="page"><a ng-show="currentPage+2 <= (numberOfPages | number:0)" style="cursor: pointer" ng-click="currentPage=currentPage+1" href="">{{currentPage+2}}</a></li>
            <li class="page"><a ng-show="currentPage+3 <= (numberOfPages | number:0)" style="cursor: pointer" ng-click="currentPage=currentPage+2" href="">{{currentPage+3}}</a></li>
            <li class="page"><a ng-show="currentPage+4 <= (numberOfPages | number:0)" style="cursor: pointer" ng-click="currentPage=currentPage+3" href="">{{currentPage+4}}</a></li>
            <li class="page"><a ng-show="currentPage+5 <= (numberOfPages | number:0)" style="cursor: pointer" ng-click="currentPage=currentPage+4" href="">{{currentPage+5}}</a></li>
            <li class="page" ng-hide="currentPage >= news.length/pageSize - 1"> <a href="#" ng-click="currentPage=numberOfPages-1" href=""> &gt;&gt; </a></li>
            <li class="page" ng-hide="currentPage >= news.length/pageSize - 1"> <a href="#" ng-click="currentPage=currentPage+1" href=""> &gt; </a></li>
        </ul>
    </div><!--  pagination-->
    <div class="middle_panel left decoration news_list_page" style="width: 780px;">

        <div class="middle_panel_box" ng-repeat="a_new in news  | startFrom:currentPage*pageSize | limitTo:pageSize " style="height: auto">
            <div class="left ">
                <div class="left">
                    <img ng-show="a_new.main_image != '/images/no_photo.jpg'" ng-click="toDetailedInfo(a_new.id,$event);" ng-src="{{a_new.main_image}}" width="244px" height="180px">
                    <img ng-show="a_new.main_image == '/images/no_photo.jpg'" ng-click="toDetailedInfo(a_new.id,$event);"  ng-src="<?php echo Yii::app()->request->baseUrl . "/images/no_photo.jpg"; ?>" width="244px" height="180px">
                </div>
            </div>
            <div class="description" style="height: 30px; float:left; margin-left: 15px !important;">
                <h2 style="text-align: center;" ng-click="toDetailedInfo(a_new.id,$event);" id="announcement_name">
                    {{ a_new.title }}
                </h2>
            </div>
            <div style="margin-left: 15px;margin-top: 15px; float: left; width: 500px; max-height: 300px"
                 ng-bind-html="secureText(a_new.text) |truncate:330 |unsafe">
            </div>
            <span class="left" style="margin-top: 30px;margin-left: 20px;"> {{a_new.created | date:'dd.MM.yyyy' }} </span>


        </div>
    </div><!-- middle_panel-->
    <div style="display: none" id="href_news">
        <?php echo CHtml::Link("",Yii::app()->createUrl( 'site/newsDetailed' ));?>
    </div>
</div>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-controllers/news-controller.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-models/services.js"></script>
