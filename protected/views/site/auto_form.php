<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-controllers/userForm_controller.js"></script>
<div ng-controller="registrationController" ng-init="init();">
<div class="content sale_form right car_part_block decoration" ng-controller="autosMainController" ng-init="init()"
     id="announcement_form"
     cg-busy="{promise:promise,message:'loading',backdrop:true}">
<div class="box">
    <?php echo $this->renderPartial('topInfo', array('editMode' => $editMode, 'announcementId' => $announcementId, 'formCaption' => $formCaption)); ?>
</div>
<span id="to_first_step" class="step"><h4>1. Основные параметры.</h4><span id="step_after1" class="step_after2"></span></span><span class="step_after"></span>
<span id="to_second_step" class="step"><h4>2. Дополнительне параметры.</h4><span id="step_after2" class="step_after2"></span></span><span class="step_after"></span>
<span id="to_three_step" class="step"><h4>3. Фотографии.</h4><span class="step_after2" id="step_after3"></span></span><span class="step_after"></span>
<?php if (Yii::app()->user->isGuest) {?>
<span id="to_four_step" class="step"><h4>4. Личные данные.</h4><span class="step_after2" id="step_after4"></span></span>
<?php } ?>
<form name="myForm" ng-submit="submit(myForm);" novalidate method="post" enctype="multipart/form-data">
<?php //if (!$editMode): ?>
<!--    --><?php //echo $this->renderPartial('personalData'); ?>
<?php //endif; ?>
<div id="step_one">
<div class="box">
    <h3 class="car_type"><?php echo Messages::getMessage(204) ?></h3>
    <div class="groups">
        <div ng-repeat="property in groupOne.properties">
            <div ng-switch="property.c_property_type_id" class="{{property.controller_style}}">
                <div ng-switch-when="2" style="position: relative">
                    <ng-form name="form">
                        <label for="id_{{property.id}}">
                            <div style="float: left">{{property.name}}</div>
                            <div style="color: red;float: left" ng-show="property.validation == 1">*</div>
                        </label>
                        <select data-ng-model="property.temp_value" property="property"
                                properties="groupOne.properties"
                                name="prop" prpcombo id="id_{{property.id}}"
                                ng-options="item.name for item in property.lookup_items"
                                ng-required="property.validation == 1">
                            <option value=""> <?php echo Messages::getMessage(5) ?></option>
                        </select>

                        <div class="input-help-user-register" ng-show="submitted && form.prop.$error.required">
                            <span ng-show="form.prop.$error.required">
                                <h4><?php echo Messages::getMessage(156) ?></h4>
                            </span>
                        </div>
                    </ng-form>
                </div>
                <div ng-switch-when="1" style="position: relative">
                    <ng-form name="form">
                        <label for="id_{{property.id}}" class="{{property.label_style}}">{{property.name}}</label>

                        <input ng-required="property.validation == 1 || property.validation == 4"
                               ng-model="property.temp_value" prpinput style="{{ property.style }}"
                               property="property" type="text" name="prop"
                               id="id_{{property.id}}" x-webkit-speech/>

                        <span class="input-help-user-register"
                              ng-show="submitted && (form.prop.$error.required || form.prop.$error.number)">
                            <span ng-show="form.prop.$error.required">
                                <h4><?php echo Messages::getMessage(156) ?></h4>
                            </span>
                             <span ng-show="form.prop.$error.number">
                                <h4><?php echo Messages::getMessage(157) ?></h4>
                            </span>
                        </span>

                    </ng-form>
                </div>
                 <span ng-switch-when="3">
                     <label class="{{property.label_style}}" for="id_{{property.id}}" style="float: left;">{{property.name}}</label>
                        <input ng-model="property.temp_value" property="property" type="checkbox" style="float: left;"
                               name="id_{{property.id}}" id="id_{{property.id}}"/>
                </span>
            </div>
        </div>
        <div ng-repeat="property in term.properties">
            <div ng-switch="property.c_property_type_id" class="{{property.controller_style}}">
                <div ng-switch-when="2" style="position: relative">
                    <ng-form name="form">
                        <label for="id_{{property.id}}">
                            <div style="float: left">{{property.name}}</div>
                            <div style="color: red;float: left" ng-show="property.validation == 1">*</div>
                        </label>
                        <select data-ng-model="property.temp_value" property="property"
                                properties="groupOne.properties"
                                name="prop" prpcombo id="id_{{property.id}}"
                                ng-options="item.name for item in property.lookup_items"
                                ng-required="property.validation == 1">
                            <option value=""> <?php echo Messages::getMessage(5) ?></option>
                        </select>

                        <div class="input-help-user-register" ng-show="submitted && form.prop.$error.required">
                            <span ng-show="form.prop.$error.required">
                                <h4><?php echo Messages::getMessage(156) ?></h4>
                            </span>
                        </div>


                    </ng-form>
                </div>
            </div>
        </div>
        <br/><br/>
    </div>

    <div class="car_value">
        <h1 class="car_value_title"> Правильно заполняйте объявление </h1>

        <p>Пожалуйста, по возможности заполните максимальное количество полей, этим вы поможете людям найти ваше объявление.</p>

        <p>* Обязательные для заполнения поля отмечены звёздочками</p>


    </div>

</div>

<div class="box">
    <h3 class="car_type"><?php echo Messages::getMessage(204) ?></h3>

    <div class="groups">
        <div ng-repeat="property in groupTwo.properties" class="{{property.controller_style}}">
            <span ng-switch="property.c_property_type_id">
                <div ng-switch-when="1" style="position: relative">
                    <ng-form name="form">
                        <label for="id_{{property.id}}" class="{{property.label_style}}">
                            <div style="float: left">{{property.name}}</div>
                            <div style="color: red;float: left"
                                 ng-show="property.validation == 1 || property.validation == 4 || property.validation == 6">
                                *
                            </div>
                        </label>

                        <input
                            ng-required="property.validation == 1 || property.validation == 4 || property.validation == 6"
                            ng-model="property.temp_value" prpinput style="{{ property.style }}"
                            property="property" type="text" name="prop"
                            id="id_{{property.id}}" x-webkit-speech/>

                        <span class="input-help-user-register"
                              ng-show="submitted && (form.prop.$error.required || form.prop.$error.number)">
                            <span ng-show="form.prop.$error.required">
                                <h4><?php echo Messages::getMessage(156) ?></h4>
                            </span>
                             <span ng-show="form.prop.$error.number">
                                <h4><?php echo Messages::getMessage(157) ?></h4>
                            </span>
                        </span>

                    </ng-form>
                </div>
                <div ng-switch-when="2" style="position: relative">
                    <ng-form name="form">
                        <label for="id_{{property.id}}">
                            <div style="float: left">{{property.name}}</div>
                            <div style="color: red;float: left" ng-show="property.validation == 1">*</div>
                        </label>
                        <select data-ng-model="property.temp_value" property="property"
                                properties="groupOne.properties" style="{{ property.style }}"
                                name="prop" prpcombo id="id_{{property.id}}"
                                ng-options="item.name for item in property.lookup_items"
                                ng-required="property.validation == 1">
                            <option value=""> <?php echo Messages::getMessage(5) ?></option>
                        </select>

                        <span class="input-help-user-register" ng-show="submitted && form.prop.$error.required">
                            <span ng-show="form.prop.$error.required">
                                <h4><?php echo Messages::getMessage(156) ?></h4>
                            </span>
                        </span>

                    </ng-form>
                </div>
            </span>
        </div>
    </div>

    <div>
        <div class="descrition" id="vivat">
            <div ng-repeat="property in groupThree.properties">
                <div ng-switch="property.c_property_type_id" class="{{property.controller_style}}">


                    <div ng-switch-when="1" style="position: relative">
                        <ng-form name="form">
                            <label for="id_{{property.id}}" class="{{property.label_style}}">{{property.name}}</label>

                            <input ng-required="property.validation == 1 || property.validation == 4"
                                   onkeyup="correctPrices(this)"
                                   ng-model="property.temp_value" prpinput
                                   property="property" type="text" name="prop"
                                   id="id_{{property.id}}" x-webkit-speech/>

                            <span class="input-help-user-register"
                                  ng-show="submitted && (form.prop.$error.required || form.prop.$error.number)">
                                    <span ng-show="form.prop.$error.required" style="width: 85px; color:#434343;">
                                        <h4><?php echo Messages::getMessage(156) ?></h4>
                                    </span>
                                     <span ng-show="form.prop.$error.number" style="width: 85px; color:#434343;">
                                        <h4><?php echo Messages::getMessage(157) ?></h4>
                                    </span>
                            </span>
                        </ng-form>
                    </div>

                    <span ng-switch-when="3">
                        <input ng-model="property.temp_value" property="property" type="checkbox"
                               ng-change="checkPrices($index , property.temp_value);"
                               name="id_{{property.id}}" id="id_{{property.id}}"/>
                        <label class="{{property.label_style}}" for="id_{{property.id}}">{{property.name}}</label>
                    </span>

                    <div ng-switch-when="2" style="position: relative">
                        <ng-form name="form">
                            <label for="id_{{property.id}}">{{property.name}}</label>
                            <select data-ng-model="property.temp_value" property="property"
                                    properties="groupOne.properties"
                                    name="prop" prpcombo id="id_{{property.id}}"
                                    ng-options="item.name for item in property.lookup_items"
                                    ng-required="property.validation == 1">
                                <option value=""> <?php echo Messages::getMessage(5) ?></option>
                            </select>

                           <span class="input-help-user-register" ng-show="submitted && form.prop.$error.required">
                                <span ng-show="form.prop.$error.required" style="z-index:5000000; width: 85px; color:#434343;">
                                    <h4><?php echo Messages::getMessage(156) ?></h4>
                                </span>
                           </span>
                        </ng-form>
                    </div>
                </div>
            </div>
            <div ng-repeat="property in groupFour.properties">
                <div ng-switch="property.c_property_type_id" class="{{property.controller_style}}">
                    <div ng-switch-when="1" style="position: relative">
                        <ng-form name="form">
                            <label for="id_{{property.id}}" class="{{property.label_style}}">{{property.name}}</label>

                            <input  ng-required="property.validation == 1 || property.validation == 4"
                                   ng-model="property.temp_value" prpinput
                                   property="property" type="text" name="prop"
                                   id="id_{{property.id}}" x-webkit-speech/>

                            <span class="input-help-user-register"
                                  ng-show="submitted && (form.prop.$error.required || form.prop.$error.number)">
                                    <span ng-show="form.prop.$error.required" style="width: 45px;">
                                        <h4><?php echo Messages::getMessage(156) ?></h4>
                                    </span>
                                     <span ng-show="form.prop.$error.number" style="width: 45px;">
                                        <h4><?php echo Messages::getMessage(157) ?></h4>
                                    </span>
                            </span>
                        </ng-form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="steps">
    <span id="step2"  class="form_steps" >Шаг 2: Дополнительне параметры. </span>
</div>
</div>
<script>
    $(document).ready(function () {
        document.getElementById('to_first_step').onclick = function () {
            openbox('step_one', this);
            return false;
        };

        function openbox(id, car_type_h3) {
            var div = document.getElementById('step_one');
            if (div.style.display == 'block') {
                div.style.display = 'block';
            }
            else {
                $('#step_one').fadeIn(700,function(){
                    $('#step_one').css({'display':'block'});
                });
                $('#to_first_step').css({'background':'#4c9b74', 'color':'#ffffff'});

                $('#step_after1').css({'border-color': 'transparent transparent transparent #4c9b74'});
                $("#closed_block").fadeOut(400,function(){
                $("#closed_block").css({'display':'none'});
                });
                $("#step_four").fadeOut(400, function(){
                    $("#step_four").css({'display':'none'});
                });
                $("#step_three").fadeOut(400,function(){
                $("#step_three").css({'display':'none'});
                });
                $("#register").fadeOut(400,function(){
                    $("#register").css({'display':'none'});
                });
            }
        }
    });
</script>
<script>
    $(document).ready(function () {
    $('#step2').click(function(){
        $("html,body").animate({ scrollTop: 620}, 1000);
        return false;
    });
    });
    </script>
<script>
    $(document).ready(function () {
        $('#step3').click(function(){
            $("html,body").animate({ scrollTop: 620}, 1000);
            return false;
        });
    });
</script>
<script>
    $(document).ready(function () {
        $('#step4').click(function(){
            $("html,body").animate({ scrollTop: 620}, 1000);
            return false;
        });
    });
</script>
<script>
    $(document).ready(function () {
        document.getElementById('to_second_step').onclick = function () {
            openbox('closed_block', this);
            return false;
        };

        function openbox(id, car_type_h3) {
            var div = document.getElementById('closed_block');
            if (div.style.display == 'block') {
                div.style.display = 'block';
            }
            else {
                $('#closed_block').fadeIn(700,function(){
                    $('#closed_block').css({'display':'block'});
                });
                $('#to_second_step').css({'background':'#4c9b74', 'color':'#ffffff'});

                $('#step_after2').css({'border-color': 'transparent transparent transparent #4c9b74'});

                $("#step_one").fadeOut(400, function(){
                $("#step_one").css({'display':'none'});
                });
                $("#step_four").fadeOut(400, function(){
                    $("#step_four").css({'display':'none'});
                });
                $("#step_three").fadeOut(400,function(){
                $("#step_three").css({'display':'none'});
                });
                $("#register").fadeOut(400,function(){
                    $("#register").css({'display':'none'});
                });
            }
        }
    });
</script>
<script>
    $(document).ready(function () {
        document.getElementById('to_three_step').onclick = function () {
            openbox('closed_block', this);
            return false;
        };

        function openbox(id, car_type_h3) {
            var div = document.getElementById('step_three');
            if (div.style.display == 'block') {
                div.style.display = 'block';
            }
            else {
                $('#step_three').fadeIn(700,function(){
                    $('#step_three').css({'display':'block'});
                });
                $('#to_three_step').css({'background':'#4c9b74', 'color':'#ffffff'});
                $('#step_after3').css({'border-color': 'transparent transparent transparent #4c9b74'});

                $("#step_one").fadeOut(400, function(){
                $("#step_one").css({'display':'none'});
                });
                $("#step_four").fadeOut(400, function(){
                    $("#step_four").css({'display':'none'});
                });
                $("#closed_block").fadeOut(400,function(){
                $("#closed_block").css({'display':'none'});
                });
                $("#register").fadeOut(400,function(){
                    $("#register").css({'display':'none'});
                });
            }
        }
    });
</script>


<script>
    $(document).ready(function () {
        document.getElementById('step2').onclick = function () {
            openbox('closed_block', this);
            return false;
        };
        function openbox(id, car_type_h3) {
            var div = document.getElementById('closed_block');
            if (div.style.display == 'block') {
                div.style.display = 'block';
            }
            else {
                $('#closed_block').fadeIn(700,function(){
                    $('#closed_block').css({'display':'block'});
                });
                $('#to_second_step').css({'background':'#4c9b74', 'color':'#ffffff'});
                    $('#step_after2').css({'border-color': 'transparent transparent transparent #4c9b74'});

                    $("#step_one").fadeOut(400, function(){
                    $("#step_one").css({'display':'none'});
                });
                    $("#step_four").fadeOut(400, function(){
                        $("#step_four").css({'display':'none'});
                    });
                $("#step_three").fadeOut(400,function(){
                    $("#step_three").css({'display':'none'});
                });
                    $("#register").fadeOut(400,function(){
                        $("#register").css({'display':'none'});
                    });
                }
                }

    });
</script>

<script>
    $(document).ready(function () {
        document.getElementById('step3').onclick = function () {
            openbox('closed_block', this);
            return false;
        };

        function openbox(id, car_type_h3) {
            var div = document.getElementById('step_three');
            if (div.style.display == 'block') {
                div.style.display = 'block';
            }
            else {
                $('#step_three').fadeIn(700,function(){
                    $('#step_three').css({'display':'block'});
                });
                $('#to_three_step').css({'background':'#4c9b74', 'color':'#ffffff'});
                $('#step_after3').css({'border-color': 'transparent transparent transparent #4c9b74'});

                $("#step_one").fadeOut(400, function(){
                    $("#step_one").css({'display':'none'});
                });
                $("#step_four").fadeOut(400, function(){
                    $("#step_four").css({'display':'none'});
                });
                $("#closed_block").fadeOut(400,function(){
                    $("#closed_block").css({'display':'none'});
                });
                $("#register").fadeOut(400,function(){
                    $("#register").css({'display':'none'});
                });
            }
        }
    });
</script>


<div id="closed_block" style="display: none;">
<div class="box">
    <h3 id="car_type_h3" class="car_type" style="cursor: pointer;"> <?php echo Messages::getMessage(193) ?></h3>

    <div id="dop_options">
        <div class="check_box">
            <div ng-repeat="property in groupFive.properties">
                <div class="{{property.controller_style}}">
                    <input ng-checked="property.temp_value=='true'" ng-model="property.temp_value" property="property"
                           type="checkbox" name="id_{{property.id}}"
                           id="id_{{property.id}}"/>
                    <label class="{{property.label_style}}" for="id_{{property.id}}">{{property.name}}</label>
                </div>
            </div>
        </div>
        <div class="check_box ml20">
            <div ng-repeat="property in groupSix.properties">
                <div class="{{property.controller_style}}">
                    <input ng-checked="property.temp_value=='true'" ng-model="property.temp_value" property="property"
                           type="checkbox" name="id_{{property.id}}"
                           id="id_{{property.id}}"/>
                    <label class="{{property.label_style}}" for="id_{{property.id}}">{{property.name}}</label>
                </div>
            </div>
        </div>

        <div class="check_box ml20">
            <div ng-repeat="property in groupSeven.properties">
                <div class="{{property.controller_style}}">
                    <input ng-checked="property.temp_value=='true'" ng-model="property.temp_value" property="property"
                           type="checkbox" name="id_{{property.id}}"
                           id="id_{{property.id}}"/>
                    <label class="{{property.label_style}}" for="id_{{property.id}}">{{property.name}}</label>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="box">
    <h3 class="car_type" id="car_type2" style="cursor: pointer;"><?php echo Messages::getMessage(192) ?></h3>

    <div id="dop_inform">
        <input x-webkit-speech id="mike"
               style="width: 15px; padding: 5px; float: left; margin-left:15px; margin-right:-20px; margin-top:10px;"/>
        <textarea class="left info" id="txt" ng-model="description"
                  style="margin-top: 45px; margin-left:-10px;"> </textarea>

        <div class="user_recommendation warning_box right">
            <h3 class="mb10"><?php echo Messages::getMessage(194) ?></h3>
            <ul class="left">
                <li>
                    <?php echo Messages::getMessage(195) ?>
                </li>
                <li>
                    <?php echo Messages::getMessage(196) ?>
                </li>
                <li>
                    <?php echo Messages::getMessage(197) ?>
                </li>
                <li>
                    <?php echo Messages::getMessage(198) ?>
                </li>
            </ul>
            <p><?php echo Messages::getMessage(199) ?></p>
        </div>
    </div>
</div>
<!-- box-->
<!--<script>-->
<!--    $(document).ready(function () {-->
<!---->
<!--        document.getElementById('car_type3').onclick = function () {-->
<!--            openbox2('dop_inform3', this);-->
<!--            return false;-->
<!--        };-->
<!---->
<!---->
<!--        function openbox2(id2, car_type2) {-->
<!--            var div2 = document.getElementById('dop_inform3');-->
<!--            if (div2.style.display == 'block') {-->
<!--                div2.style.display = 'none';-->
<!--                car_type2.innerHTML = '--><?php //echo Messages::getMessage(321) ?><!-- &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; +';-->
<!--            }-->
<!--            else {-->
<!--                div2.style.display = 'block';-->
<!--                car_type2.innerHTML = '--><?php //echo Messages::getMessage(321) ?><!-- &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -';-->
<!--            }-->
<!--        }-->
<!--    });-->
<!--</script>-->

<!--<div class="box">-->
<!--    <h3 class="car_type" id="car_type3" style="cursor: pointer;">--><?php //echo Messages::getMessage(321) ?><!-- &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-->
<!--        +</h3>-->
<!---->
<!--    <div id="dop_inform3" style="display: none;">-->
<!--        <div class="panel">-->
<!--            <h2 class="h3">Оформление фона</h2>-->
<!--            <div class="check_box1" id ="check_box">-->
<!--            <div ng-repeat="property in groupTwentySix.properties">-->
<!--                <div class="{{property.controller_style}}" id="background">-->
<!--            <input ng-checked="property.temp_value=='true'" ng-model="property.temp_value" property="property"-->
<!--                   type="checkbox" name="id_{{property.id}}"-->
<!--                   id="id_{{property.id}}"/>-->
<!--            <label class="{{property.label_style}}" id="id_{{property.id}}1" for="id_{{property.id}}" style="background: {{property.name}};"><img src="--><?php //echo Yii::app()->request->baseUrl; ?><!--/images/no_photo.jpg"/></label>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!---->
<!--            <script>-->
<!--                $(document).ready(function(){-->
<!--                   $('#id_2501').click(function(){-->
<!--                       $('#id_2501').css({'border': '2px solid #6c9401'});-->
<!--                      $('#id_256').removeAttr('checked');-->
<!--                       $('#id_2561').css({'border': '1px solid #dddddd'});-->
<!--                      $('#id_257').removeAttr('checked');-->
<!--                       $('#id_2571').css({'border': '1px solid #dddddd'});-->
<!--                       $('#id_258').removeAttr('checked');-->
<!--                       $('#id_2581').css({'border': '1px solid #dddddd'});-->
<!--                       $('#id_259').removeAttr('checked');-->
<!--                       $('#id_2591').css({'border': '1px solid #dddddd'});-->
<!--                       $('#id_260').removeAttr('checked');-->
<!--                       $('#id_2601').css({'border': '1px solid #dddddd'});-->
<!--                   });-->
<!--                });-->
<!--            </script>-->
<!---->
<!--            <script>-->
<!--                $(document).ready(function(){-->
<!--                    $('#id_2561').click(function(){-->
<!--                        $('#id_2561').css({'border': '2px solid #6c9401'});-->
<!--                        $('#id_250').removeAttr('checked');-->
<!--                        $('#id_2501').css({'border': '1px solid #dddddd'});-->
<!--                        $('#id_257').removeAttr('checked');-->
<!--                        $('#id_2571').css({'border': '1px solid #dddddd'});-->
<!--                        $('#id_258').removeAttr('checked');-->
<!--                        $('#id_2581').css({'border': '1px solid #dddddd'});-->
<!--                        $('#id_259').removeAttr('checked');-->
<!--                        $('#id_2591').css({'border': '1px solid #dddddd'});-->
<!--                        $('#id_260').removeAttr('checked');-->
<!--                        $('#id_2601').css({'border': '1px solid #dddddd'});-->
<!--                    });-->
<!--                });-->
<!--            </script>-->
<!---->
<!--            <script>-->
<!--                $(document).ready(function(){-->
<!--                    $('#id_2571').click(function(){-->
<!--                        $('#id_2571').css({'border': '2px solid #6c9401'});-->
<!--                        $('#id_256').removeAttr('checked');-->
<!--                        $('#id_2561').css({'border': '1px solid #dddddd'});-->
<!--                        $('#id_250').removeAttr('checked');-->
<!--                        $('#id_2501').css({'border': '1px solid #dddddd'});-->
<!--                        $('#id_258').removeAttr('checked');-->
<!--                        $('#id_2581').css({'border': '1px solid #dddddd'});-->
<!--                        $('#id_259').removeAttr('checked');-->
<!--                        $('#id_2591').css({'border': '1px solid #dddddd'});-->
<!--                        $('#id_260').removeAttr('checked');-->
<!--                        $('#id_2601').css({'border': '1px solid #dddddd'});-->
<!--                    });-->
<!--                });-->
<!--            </script>-->
<!---->
<!--            <script>-->
<!--                $(document).ready(function(){-->
<!--                    $('#id_2581').click(function(){-->
<!--                        $('#id_2581').css({'border': '2px solid #6c9401'});-->
<!--                        $('#id_256').removeAttr('checked');-->
<!--                        $('#id_2561').css({'border': '1px solid #dddddd'});-->
<!--                        $('#id_250').removeAttr('checked');-->
<!--                        $('#id_2501').css({'border': '1px solid #dddddd'});-->
<!--                        $('#id_257').removeAttr('checked');-->
<!--                        $('#id_2571').css({'border': '1px solid #dddddd'});-->
<!--                        $('#id_259').removeAttr('checked');-->
<!--                        $('#id_2591').css({'border': '1px solid #dddddd'});-->
<!--                        $('#id_260').removeAttr('checked');-->
<!--                        $('#id_2601').css({'border': '1px solid #dddddd'});-->
<!--                    });-->
<!--                });-->
<!--            </script>-->
<!---->
<!--            <script>-->
<!--                $(document).ready(function(){-->
<!--                    $('#id_2591').click(function(){-->
<!--                        $('#id_2591').css({'border': '2px solid #6c9401'});-->
<!--                        $('#id_256').removeAttr('checked');-->
<!--                        $('#id_2561').css({'border': '1px solid #dddddd'});-->
<!--                        $('#id_250').removeAttr('checked');-->
<!--                        $('#id_2501').css({'border': '1px solid #dddddd'});-->
<!--                        $('#id_258').removeAttr('checked');-->
<!--                        $('#id_2581').css({'border': '1px solid #dddddd'});-->
<!--                        $('#id_257').removeAttr('checked');-->
<!--                        $('#id_2571').css({'border': '1px solid #dddddd'});-->
<!--                        $('#id_260').removeAttr('checked');-->
<!--                        $('#id_2601').css({'border': '1px solid #dddddd'});-->
<!--                    });-->
<!--                });-->
<!--            </script>-->
<!---->
<!--            <script>-->
<!--                $(document).ready(function(){-->
<!--                    $('#id_2601').click(function(){-->
<!--                        $('#id_2601').css({'border': '2px solid #6c9401'});-->
<!--                        $('#id_256').removeAttr('checked');-->
<!--                        $('#id_2561').css({'border': '1px solid #dddddd'});-->
<!--                        $('#id_250').removeAttr('checked');-->
<!--                        $('#id_2501').css({'border': '1px solid #dddddd'});-->
<!--                        $('#id_258').removeAttr('checked');-->
<!--                        $('#id_2581').css({'border': '1px solid #dddddd'});-->
<!--                        $('#id_257').removeAttr('checked');-->
<!--                        $('#id_2571').css({'border': '1px solid #dddddd'});-->
<!--                        $('#id_259').removeAttr('checked');-->
<!--                        $('#id_2591').css({'border': '1px solid #dddddd'});-->
<!--                    });-->
<!--                });-->
<!--            </script>-->
<!--            <script src="http://static.jsbin.com/js/render/edit.js?3.18.35"></script>-->
<!---->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
<!-- box-->
<script>
    $(document).ready(function () {
        document.getElementById('car_type4').onclick = function () {
            openbox2('dop_inform4', this);
            return false;
        };


        function openbox2(id2, car_type2) {
            var div2 = document.getElementById('dop_inform4');
            if (div2.style.display == 'block') {
                div2.style.display = 'none';
                car_type2.innerHTML = '<?php echo Messages::getMessage(201) ?>&nbsp;&nbsp; +';
            }
            else {
                div2.style.display = 'block';
                car_type2.innerHTML = '<?php echo Messages::getMessage(201) ?>&nbsp;&nbsp; -';
            }
        }
    });
</script>
<div class="box care_place">
    <h3 class="car_type" id="car_type4"><?php echo Messages::getMessage(201) ?>&nbsp;&nbsp; +</h3>
    <div id="dop_inform4" style="display: none;">
    <div class="panel">

        <ng-form name="form">
            <label>Страна:</label>

            <select id="select_country" ng-model="location" required name="location"
                    ng-options="item.name for item in locations" ng-change="countryChange();"></select>

           <span class="input-help-user-register" ng-show="submitted && form.location.$error.required">
                <span ng-show="form.location.$error.required">
                    <h4><?php echo Messages::getMessage(156) ?></h4>
                </span>
           </span>

<!--            <div id="icon"><img-->
<!--                    ng-src="--><?php //echo Yii::app()->request->baseUrl; ?><!--/uploads/flags/{{location.flag_name}}"-->
<!--                    class="left" id="country_item"></div>-->
        </ng-form>

    </div>

    <div class="panel" ng-show="toShowSubLocations();">
        <label class="small_label"><?php echo Messages::getMessage(202) ?></label>
        <select ng-model="subLocation" ng-options="item.name for item in subLocations"></select>
    </div>
    <div class="panel">
        <label style="margin-left: 295px;"><?php echo Messages::getMessage(203) ?></label>
        <input type="text" style="width:180px; padding: 12px 10px; border: 2px solid #dddddd;"
               ng-model="optional_location" x-webkit-speech/>
    </div>
        </div>
</div>
    <div class="steps">
    <span class="form_steps" id="step3">Шаг 3: Фотографии объявления.</span>
</div>
</div>
<div id="step_three" style="display: none;">
<?php echo $this->renderPartial('photoUploader'); ?>
</div>
</form>
</div>
<?php if (Yii::app()->user->isGuest) {?>
<div id="step_four" style="display: none;">
<script>
    $(document).ready(function () {
        document.getElementById('to_four_step').onclick = function () {
            openbox('step_four', this);
            return false;
        };

        function openbox(id, car_type_h3) {
            var div = document.getElementById('step_four');
            if (div.style.display == 'block') {
                div.style.display = 'block';
            }
            else {
                $('#step_four').fadeIn(700,function(){
                    $('#step_four').css({'display':'block'});
                });
                $('#to_four_step').css({'background':'#4c9b74', 'color':'#ffffff'});
                $('#step_after4').css({'border-color': 'transparent transparent transparent #4c9b74'});

                $("#step_one").fadeOut(400, function(){
                    $("#step_one").css({'display':'none'});
                });
                $("#closed_block").fadeOut(400,function(){
                    $("#closed_block").css({'display':'none'});
                });
                $("#step_three").fadeOut(400,function(){
                    $("#step_three").css({'display':'none'});
                });
                $("#register").fadeOut(400,function(){
                    $("#register").css({'display':'none'});
                });
            }
        }
    });
</script>


<script type="text/javascript">
$(document).ready(function(){
   $('.register').click(function(){
       $('#register').fadeIn(300, function(){
           $('#register').css({'display':'block'});
           $('#login5').fadeOut(200,function(){
               $('.register').css({'background':'#8f76c2','color':'#ffffff'});
               $('.login5').css({'background':'#fff','color':'#434343'});
               $('#login5').css({'display':'none'});
           });
       });
   });
});
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.login5').click(function(){
            $('#login5').fadeIn(300, function(){
                $('#login5').css({'display':'block'});
                $('#register').fadeOut(200,function(){
                    $('.login5').css({'background':'#8f76c2','color':'#ffffff'});
                    $('.register').css({'background':'#fff','color':'#434343'});
                    $('#register').css({'display':'none'});
                });
            });
        });
    });
</script>
<!--login-->
<div id="choose_login_or_reg">
    <span class="login5">Войти</span>
    <span class="register">Регистрация</span>
</div>
<div id="sign_up_form"  ng-controller="need_reg_login">
<div id="login5">
<div class="add_announcement right decoration dillers" >
    <div  ng-show="sign_up_form">

        <div style="text-align: center;width: 400px; padding-top: 25px; padding-left: 150px">
            <div class="validation_message6">
                Уважаемый пользователь! Для подачи объявления просим потвердить вашу регистрацию.
                <br/>
                Проерьте пожайлуста вашу почту! На указанный Email-адрес отправлено сообщение с ссылкой для потверждения регистрации.
                <br/>
                После потверждения просим заполнить ниже указанные поля.
            </div>
            <div class="validation_message" ng-show="showValidationMessage" style="color: red; font-weight:bold; margin-top:2px;height: 25px; float: left; margin-left: 50px; width: 100%; text-align: center;">
                <?php echo Messages::getMessage(170) ?>
            </div>

            <div style="float: left;width: 120px;text-align: right; margin-top:17px; margin-right: 10px;"> <?php echo Messages::getMessage(172) ?> </div>
            <input type="text" name="email" id="email" ng-model="email"  style="width: 200px; border:2px solid #aaaaaa; padding:10px 15px; float: left;" x-webkit-speech/>

            <div style="clear: both"></div>

            <div style="float: left;width: 120px;text-align: right; margin-top:37px; margin-right: 10px;"> <?php echo Messages::getMessage(173) ?> </div>
            <input type="password" name="password" id="password" ng-model="password"  style="border:2px solid #aaaaaa; width: 200px; margin-top:25px; padding:10px 15px; float: left;" x-webkit-speech/>

            <div style="clear: both"></div>

            <br/>

            <input type="button" id="clear_up_button" class="clear_up_button" ng-click="login('<?php echo Yii::app()->createUrl('site/login')?>')" id="need_reg"  value="Войти и потвердить подачу объявления" style="cursor: pointer;margin-left: 65px;  margin-top: 28px; width: 320px;"/>
            <div id="loading3" ng-show="showloading" style="float: right; width: 40px; margin-right: -55px; margin-top: 25px;">
                <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/loading3.gif" style="width: 150px;"/>
            </div>
            <input type="hidden" id="need_reg_enter" value="1">
            </div>

    </div>

</div>
    <div id="choose_login_or_reg2" ng-show="showValidationMessage2">
        <a href="#" class="add_notification" linked="submitButton" style="margin-left: 200px;"> <?php echo Messages::getMessage(106) ?></a>

    </div>
</div>
</div>
</div>
<!--registration-->
<div id="register" style="display: none;">
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-controllers/need_reg_login_controller.js"></script>

    <script>
        $(document).ready(function () {
                $('#qqfile').change(function () {
                    $('#path').val($(this).val());
                });
                $('#qqfile1').change(function () {
                    $('#path_logo').val($(this).val());
                });
            }
        );
    </script>

    <div class="dillers right content decoration" ng-controller="registrationController" ng-init="init();">
    <input type="hidden" value="<?php echo $editMode2 ?>" id="editMode2">
    <div style="display: none">
        <?php echo CHtml::Link("",Yii::app()->createUrl( 'site/succeedRegistered' ),array('id'=>'redirectIT')); ?>
    </div>



    <div style="margin-top:-13px;" ng-show="editMode2" class="title"> <h3> <?php echo Messages::getMessage(205) ?></h3> </div>
    <div class="left check_field" ng-hide="editMode2">(<?php echo Messages::getMessage(206) ?><span> * </span>)</div>
    <form id="userForm" name="userForm" ng-submit="submit2(userForm);" novalidate  method="post" enctype="multipart/form-data">
    <div class="user_part" ng-hide="editMode2" style="margin-left: 270px;">
        <div class="radio_row clear">
            <label for="personal">
                <input ng-model="user.isPersonal" value="true" type="radio" name="personal" id="personal" />
                <span>Частник</span></label>
        </div>
        <div class="radio_row">
            <label for="dealer1">
                <input ng-model="user.isPersonal" value="false" type="radio" name="dealer1" id="dealer1"/><span><?php echo Messages::getMessage(258) ?></span></label>
        </div>
            <span ng-hide="user.isPersonal">
                <div class="radio_row">
                    <label for="universal">
                        <input ng-model="user.isUniversal" value="false" type="radio" name="universal" id="universal">
                        <span>
                        <?php echo Messages::getMessage(117) ?></span></label>
                </div>
                <div class="radio_row">
                    <label for="official">
                        <input ng-model="user.isUniversal" value="true" type="radio" name="official" id="official">
                        <span><?php echo Messages::getMessage(116) ?></span></label>
                </div>
            </span>

        <div class="input-help-user-register"
             ng-show="userForm.personal.$error.required && userForm.personal.$dirty && userForm.personal.$invalid">
                <span>
                <h4><?php echo Messages::getMessage(156) ?></h4>
                    </span>
        </div>
    </div>
    <div class="row" style="margin-left: 85px;">
        <label>Ваш e-mail</label>
        <input style="float: left" name="email_reg" type="email" ng-model="user.email_reg" required  x-webkit-speech/>
        <div class="input-help-user-register" ng-show="submitted && userForm.email_reg.$invalid">
                <span ng-show="userForm.email_reg.$error.email_reg">
                    <h4><?php echo Messages::getMessage(156) ?></h4>
                </span>
                <span ng-show="userForm.email_reg.$error.required">
                    <h4><?php echo Messages::getMessage(159) ?></h4>
                </span>
        </div>
    </div>
     <span ng-bind="emailValidationMessage" style="font-weight: bold;color: red; margin: -6px 10px 0 130px; margin-left: 275px; padding: 10px;">

    </span>
    <div class="row" style="margin-left: 85px;">
        <label>Пароль</label>
        <div class="left">
            <input type="password" ng-minlength="6" required name="password_reg" ng-model="user.password_reg" id="psw" x-webkit-speech />
                <span class="input-help-user-register" ng-show="submitted && userForm.password_reg.$invalid">
                    <span ng-show="userForm.password_reg.$error.minlength">
                        <h4><?php echo Messages::getMessage(160) ?></h4>
                    </span>
                    <span ng-show="userForm.password_reg.$error.required">
                        <h4><?php echo Messages::getMessage(161) ?></h4>
                    </span>
                </span>
            <p class="clear"><?php echo Messages::getMessage(162) ?> </p>
        </div>
    </div>
    <div class="row" style="margin-left: 85px;">
        <label><?php echo Messages::getMessage(163) ?></label>
        <input ng-model='user.confirm_password' type="password" required  data-password-verify="psw" name="confirm_password" ng-keypress="isPressed();">
             <span class="input-help-user-register" ng-show="isPassRepeaterPressed && userForm.confirm_password.$invalid">
                <span ng-show="userForm.confirm_password.$error.required">
                   <h4>Field required! </h4>
                </span>
                <span ng-show="userForm.confirm_password.$error.passwordVerify">
                   <h4><?php echo Messages::getMessage(164) ?> </h4>
                </span>
            </span>
    </div>
    <div class="row" style="margin-left: 85px;">
        <label><?php echo Messages::getMessage(141) ?></label>
        <input type="text" ng-model="user.name" name="first_name" required x-webkit-speech />

             <span class="input-help-user-register" ng-show="submitted && userForm.first_name.$invalid" >
                <span ng-show="userForm.first_name.$error.required">
                   <h4><?php echo Messages::getMessage(156) ?> </h4>
                </span>
            </span>
        <div class="checkbox_row">
            <input name="show_name" type="checkbox" id="name" class="clear" ng-model="user.showName">
            <p class="left checkbox_label"> <label for="name"><?php echo Messages::getMessage(307) ?></label></p>
        </div>
    </div>
    <div class="row" ng-hide="user.isPersonal" style="margin-left: 85px;">
        <label><?php echo Messages::getMessage(308) ?></label>
        <input name="organisation" type="text" ng-model="user.organisation" ng-required="!user.isPersonal" x-webkit-speech />
             <span class="input-help-user-register" ng-show="submitted && userForm.organisation.$invalid" >
                <span ng-show="userForm.organisation.$error.required">
                   <h4><?php echo Messages::getMessage(156) ?> </h4>
                </span>
            </span>
    </div>
    <div class="row" ng-hide="user.isPersonal" style="margin-left: 85px;">
        <label><?php echo Messages::getMessage(309) ?></label>
        <input name="address" type="text" ng-model="user.address" x-webkit-speech />
    </div>
    <div class="row" ng-hide="user.isPersonal" style="margin-left: 85px;">
        <label><?php echo Messages::getMessage(310) ?></label>
        <input name="site" type="text" ng-model="user.site" x-webkit-speech />
    </div>
    <div style="width: 775px;  overflow: hidden; padding: 10px; clear: left;" ng-hide="user.isPersonal || isAdmin == 1">
        <label style="padding: 13px 10px 0px 0px; margin-left: 40px; float: left; text-align: right; width: 200px;">Логотип</label>

        <div class="input_row" style="width: 401px;" style="margin-left: 85px;">
            <div ng-controller="uploadController" ng-init="init(0)">
                <input type="hidden" value="auto" id="formType">
                <input type="hidden" name="MAX_FILE_SIZE" value="250" />
                <div class="pictures_panel ">
                    <ul>
                        <li ng-repeat="image in images">
                            <a data-id="{{image.dataId}}" ng-show="image.dataId != -55">
                                <img src="images/delete.png" ng-click="removeImg($index)" style="cursor: pointer; position: absolute" ng-show="image.isUploaded">
                                <img ng-src="{{image.src}}" id="{{image.id}}" style="width: 105px; height: 95px;" alt="{{image.title}}">
                            </a>
                        </li>
                    </ul>
                    <div class="choose_file" id="logo_upload">
                        <img id="loading1" src="<?php echo Yii::app()->request->baseUrl; ?>/images/loading.gif" style="height: 18px; display: none; " >

                        <p class="form">
                            <input type="text" id="path_logo" style="width: 140px" x-webkit-speech />
                            <label class="add-photo-btn" ><?php echo Messages::getMessage(323) ?>
                                <span>
                                           <input type="file" id="qqfile1" name="qqfile" x-webkit-speech style="width: 25px;" />
                                        </span>
                            </label>
                            <a ng-click="uploadDealer()"  class="sign_up_button2"  style="cursor: pointer; line-height: 2; margin-left: 5px; float: left;" ><?php echo Messages::getMessage(207) ?></a>

                        </p>

                    </div>
                </div>
            </div>
        </div>

    </div>

        <span ng-hide="user.isPersonal || isAdmin == 1" style="padding: 10px; background: #F1F1F1; float: left; margin-left: 50px;">

            <google-map center="center" refresh="isDealer();" draggable="true" zoom="zoom" markers="user.dealer_markers" mark-click="true"
                        class="angular-google-map"  style="height: 400px;width: 650px;"></google-map>

        </span>


    <div style="margin-top:20px;" ng-hide="isAdmin == 1">

        <div class="title" ng-hide="user.isPersonal" style="margin-bottom: 13px; margin-left: 50px; float: left; margin-top: 5px; width: 647px;">
            <h3 style="text-align: center;"><?php echo Messages::getMessage(99) ?></h3>
        </div>
        <div style="float: left; margin-left: 5px">
            <div ng-hide="user.isPersonal">
                <div ng-controller="uploadController" ng-init="init(1)">
                    <input type="hidden" value="auto" id="formType">
                    <input type="hidden" name="MAX_FILE_SIZE" value="250"/>

                    <div class="pictures_panel" style="width: 520px; margin-left: 135px;">
                        <ul>
                            <li ng-repeat="image in images" style="float: left">
                                <a data-id="{{image.dataId}}" >
                                    <img src="images/delete.png" ng-click="removeImg($index)" style="cursor: pointer; position: absolute" ng-show="image.isUploaded">
                                    <img ng-src="{{image.src}}" id="{{image.id}}" style="width: 158px; margin-right: 5px; height: 135px;" alt="{{image.title}}">
                                </a>
                            </li>
                        </ul>
                        <div class="choose_file">
                            <img id="loading" src="<?php echo Yii::app()->request->baseUrl; ?>/images/loading.gif" style="height: 18px; display: none; clear: left; float: left ">

                            <p class="form">
                                <input type="text" id="path" style="width: 260px; margin-left: 5px;" x-webkit-speech />
                                <label class="add-photo-btn" ><?php echo Messages::getMessage(323) ?>
                                    <span>
                                           <input type="file" id="qqfile" name="qqfile" x-webkit-speech />
                                        </span>
                                </label>
                                <a ng-click="upload()"  class="sign_up_button2"  style="cursor: pointer; line-height: 2; margin-left: 5px; float: left;"><?php echo Messages::getMessage(207) ?></a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="row" style="margin-left: 85px;">
        <label><?php echo Messages::getMessage(311) ?></label>
        <div class="left">
            <select name="is_phone1_mobile" class="diller_sml_select" ng-model="user.phoneType" hidden="true"
                    ng-options="item.name_arm for item in phoneTypes" x-webkit-speech />
            </select>
            <input name="phone1" type="text" class="middle_input" ng-model="user.phone" required="" ng-pattern="/^([+]?[0-9 -]+)$/" x-webkit-speech />
                <span class="input-help-user-register" ng-show="submitted && userForm.phone1.$invalid" >
                    <span ng-show="userForm.phone1.$error.required">
                       <h4><?php echo Messages::getMessage(313) ?></h4>
                    </span>
                </span>
        </div>
    </div>

    <div class="row" style="margin-left: 85px;" ng-hide="editMode2">
        <label><?php echo Messages::getMessage(144) ?></label>
        <div class="">
            <input type="text" class="sml_input" ng-model="user.code" x-webkit-speech />

        </div>
        <div class="refresh_block">
                <span>
                    <img  height="30px"  width="100px"  id="updater" src="<?php echo Yii::app()->createUrl('site/generateCaptcha'); ?>"/>
                    </br>
                    <a id="refresher" onclick="generateCaptcha()" style="cursor: pointer"><?php echo Messages::getMessage(146) ?></a>
                </span>
                 <span class="input-help-user-register" ng-show="showCaptchaError">
                    <span>
                       <h4><?php echo Messages::getMessage(145) ?> </h4>
                    </span>
                </span>
        </div>

    </div>
    <div class="user_consent_block">
            <span ng-hide="editMode2">
                <div >
                    <input type="checkbox" class="left" ng-required="!editMode2" ng-model="user.isAgree" name="isAgree" x-webkit-speech />
                    <div class="use_rule left" style="margin-top: 15px;"> (<a href="#"> <?php echo Messages::getMessage(315) ?> </a>)</div>
                </div>
                 <span class="input-help-user-register" ng-show="submitted &&  userForm.isAgree.$invalid">
                    <span ng-show="userForm.isAgree.$error.required">
                        <h4><?php echo Messages::getMessage(165) ?></h4>
                    </span>
                 </span>
            </span>


        <div class="clear pt20">
            <input style="width: 450px; margin-left: -120px;" type="submit" id="sign_up_button" class="sign_up_button" value="Регистрация и потверждение объявления"/>
            <div ng-show="reg_load" style="width: 40px; float: right; margin-top:-45px; margin-left: 20px;" id="reg_loading">
                <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/loading3.gif" />
            </div>
        </div>


    </div>
    </form> <!--  form-->
    </div>

    <script>
        function generateCaptcha() {
            var stamp = new Date().getTime();
            var src = $('#updater').attr("src") + "?st=" + stamp;
            $('#updater').attr("src", src);
        }
    </script>
</div>
<!--END step_four-->
<?php } ?>
<!-- ************************* END ANGULAR IMPLEMENTATION ***************************************** -->

<!--    step_five-->


<!--END_step_five-->
<input type="hidden" id="usd_rate" value="{{rate_to_usd}}">
<input type="hidden" id="eur_rate" value="{{rate_to_euro}}">
<input type="hidden" id="rate_id" value="1">

<script>
    function correctPrices(dom) {
        var value = dom.value;
        var usd_rate = $("#usd_rate").val();
        var eur_rate = $("#eur_rate").val();
        if (dom.id == "id_21") {
            angular.element("#id_22").scope().property.temp_value = Math.ceil(value / usd_rate);
            angular.element("#id_23").scope().property.temp_value = Math.ceil(value / eur_rate);
            angular.element("#id_22").scope().$apply();
            angular.element("#id_23").scope().$apply();
            $("#rate_id").val(1);
        }
        else if (dom.id == "id_22") {
            angular.element("#id_21").scope().property.temp_value = Math.ceil(value * usd_rate);
            angular.element("#id_23").scope().property.temp_value = Math.ceil(value * usd_rate / eur_rate);
            angular.element("#id_21").scope().$apply();
            angular.element("#id_23").scope().$apply();
            $("#rate_id").val(2);
        }
        else if (dom.id == "id_23") {
            angular.element("#id_21").scope().property.temp_value = Math.ceil(value * eur_rate);
            angular.element("#id_22").scope().property.temp_value = Math.ceil(value * eur_rate / usd_rate);
            angular.element("#id_21").scope().$apply();
            angular.element("#id_22").scope().$apply();
            $("#rate_id").val(3);
        }
    }
</script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-controllers/autoForm_controller.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-controllers/main_controller.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-controllers/userForm_controller.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-models/services.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ajaxfileupload.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/UploadHelper.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-controllers/upload_controller.js"
        type="text/javascript"></script>