<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Edgar
 * Date: 3/16/14
 * Time: 9:08 PM
 * To change this template use File | Settings | File Templates.
 */?>
<div class="add_announcement right decoration dillers" ng-controller="need_reg_login">
    <div class="title"><h3 class="add_anonc_title"><?php echo Messages::getMessage(317) ?> </h3></div>
    <div>
        <div style="text-align: center;">
            <div class="need_reg">
                <?php echo Messages::getMessage(174) ?>
            </div>
            <div class="get_fast_reg">
                <?php echo Messages::getMessage(176) ?> <?php echo CHtml::Link( Messages::getMessage(175),Yii::app()->createUrl( 'site/register' ),array('style' => 'color:blue; border-bottom:1px dotted blue;margin-left:15px;'));?>
            </div>
        </div>


        <div style="text-align: center;width: 400px; padding-top: 25px; padding-left: 150px">
            <div class="validation_message" ng-show="showValidationMessage" style="color: red; font-weight:bold; margin-top:2px;height: 25px; float: left; margin-left: 50px; width: 100%; text-align: center;">
                <?php echo Messages::getMessage(170) ?>
            </div>
            <div style="float: left;width: 120px;text-align: right; margin-top:17px; margin-right: 10px;"> <?php echo Messages::getMessage(172) ?> </div>
            <input type="text" name="email" id="email" ng-model="email"  style="width: 200px; border:2px solid #aaaaaa; padding:10px 15px; float: left;" x-webkit-speech/>

            <div style="clear: both"></div>

            <div style="float: left;width: 120px;text-align: right; margin-top:37px; margin-right: 10px;"> <?php echo Messages::getMessage(173) ?> </div>
            <input type="password" name="password" id="password" ng-model="password"  style="border:2px solid #aaaaaa; width: 200px; margin-top:25px; padding:10px 15px; float: left;" x-webkit-speech/>

            <div style="clear: both"></div>

            <br/>

            <input type="button" class="clear_up_button" ng-click="login('<?php echo Yii::app()->createUrl('site/login')?>')" id="need_reg"  value="ВХОД" style="cursor: pointer;margin-left: 135px;"/>
            <p class="forgot_my_password" style="cursor: pointer; margin-left: 250px; margin-top: 25px; padding-bottom: 7px; color: #10498a;width:110px; border-bottom: 1px dotted #10498a;" ng-click="goToForgotPassword()"> <?php echo Messages::getMessage(169) ?> </p>
            <input type="hidden" id="need_reg_enter" value="1">

           </div>
    </div>
</div>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-controllers/need_reg_login_controller2.js"></script>