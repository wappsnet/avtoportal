<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Yerem
 * Date: 11/5/13
 * Time: 12:39 AM
 * To change this template use File | Settings | File Templates.
 */
?>
<div class="left" ng-controller="officialDealers_controller" ng-init="initIt();">
    <input type="hidden" id="is_official" value="<?php echo ($isOfficial?1:0) ?>">
    <div class="middle_panel left decoration" style="margin-top: 25px;width: 770px !important;">
        <div class="title">
            <?php if($isOfficial): ?>
            <h3> <?php echo Messages::getMessage(71) ?></h3>
            <?php else: ?>
            <h3> <?php echo Messages::getMessage(70) ?></h3>
            <?php endif; ?>
        </div>

        <!--<div class="html5gallery" data-skin="light" data-width="650" data-height="400" style="display:none;">
            <a href="http://www.youtube.com/embed/YE7VzlLtp-4"><img src="http://img.youtube.com/vi/YE7VzlLtp-4/2.jpg" alt="Youtube Video"></a>
            <a href="http://www.youtube.com/embed/YE7VzlLtp-4"><img src="http://img.youtube.com/vi/YE7VzlLtp-4/2.jpg" alt="Youtube Video"></a>

        </div>-->

        <div ng-repeat="item in dealers" class="left  img_block">

            <img ng-src="<?php echo Yii::app()->request->baseUrl . "/uploads/"; ?>{{item.logo_name}}"
                 width="25px" height="28px" ng-click="goToDealer($index)" class="left" style="cursor: pointer">
                  <div class="name_span" style="cursor: pointer" ng-click="goToDealer($index)" >{{item.organisation_name}}</div>
        </div>
    </div>

    <div style="clear: both"></div>
    <div>
        <google-map center="center" markers="markers" draggable="true" zoom="zoom" class="angular-google-map"  style="height: 400px;width: 770px;"></google-map>
    </div>



</div>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/html5gallery.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-controllers/official_dealers.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-models/services.js"></script>