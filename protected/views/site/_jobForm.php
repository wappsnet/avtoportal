<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Edgar
 * Date: 6/25/13
 * Time: 11:52 PM
 * To change this template use File | Settings | File Templates.
 */?>

<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Edgar
 * Date: 6/23/13
 * Time: 5:00 PM
 * To change this template use File | Settings | File Templates.
 */?>
<div class="content sale_form right car_part_block decoration" ng-controller="jobController" ng-init="init();" id="announcement_form"
     cg-busy="{promise:promise,message:'loading',backdrop:true}">
<div class="box">
    <?php echo $this->renderPartial('topInfo', array('editMode'=>$editMode, 'announcementId'=>$announcementId, 'formCaption'=>$formCaption)); ?>
</div>
<form name="myForm" ng-submit="submit(myForm);" novalidate method="post" enctype="multipart/form-data">
    <?php if(!$editMode): ?>
        <?php echo $this->renderPartial('personalData'); ?>
    <?php endif; ?>
<div class="box">
    <h3 class="car_type"><?php echo Messages::getMessage(318) ?></h3>

    <div class="groups">
        <div ng-repeat="property in groupOne.properties">
            <div ng-switch="property.c_property_type_id" class="{{property.controller_style}}">
                <div ng-switch-when="2" style="position: relative">
                    <ng-form name="form">
                        <label for="id_{{property.id}}"><div style="float: left">{{property.name}} </div> <div style="color: red;float: left" ng-show="property.validation == 1">*</div></label>
                        <select data-ng-model="property.temp_value" property="property"
                                properties="groupOne.properties"
                                name="prop"  prpcombo id="id_{{property.id}}"
                                ng-options="item.name for item in property.lookup_items"
                                ng-required="property.validation == 1">
                            <option value=""><?php echo Messages::getMessage(5) ?></option>
                        </select>

                        <div class="input-help-user-register" ng-show="submitted && form.prop.$error.required">
                            <span ng-show="form.prop.$error.required">
                                <h4><?php echo Messages::getMessage(156) ?></h4>
                            </span>
                        </div>
                    </ng-form>
                </div>
                <div ng-switch-when="1" style="position: relative">
                    <ng-form name="form">
                        <label for="id_{{property.id}}" class="{{property.label_style}}">{{property.name}}</label>

                        <input ng-required="property.validation == 1 || property.validation == 4"
                               ng-model="property.temp_value" prpinput style="{{ property.style }}"
                               property="property" type="text" name="prop"
                               id="id_{{property.id}}"/>

                        <span class="input-help-user-register" ng-show="submitted && (form.prop.$error.required || form.prop.$error.number)">
                            <span ng-show="form.prop.$error.required">
                                <h4><?php echo Messages::getMessage(156) ?></h4>
                            </span>
                             <span ng-show="form.prop.$error.number">
                                <h4><?php echo Messages::getMessage(157) ?></h4>
                            </span>
                        </span>

                    </ng-form>
                </div>
                 <span ng-switch-when="3">
                     <label class="{{property.label_style}}" for="id_{{property.id}}" style="float: left;">{{property.name}}</label>
                        <input ng-model="property.temp_value" property="property" type="checkbox" style="float: left;"
                               name="id_{{property.id}}" id="id_{{property.id}}"/>

                </span>
            </div>
        </div>
        <div ng-repeat="property in term.properties">
            <div ng-switch="property.c_property_type_id" class="{{property.controller_style}}">
                <div ng-switch-when="2" style="position: relative">
                    <ng-form name="form">
                        <label for="id_{{property.id}}"><div style="float: left">{{property.name}} </div> <div style="color: red;float: left" ng-show="property.validation == 1">*</div></label>
                        <select data-ng-model="property.temp_value" property="property"
                                properties="groupOne.properties"
                                name="prop"  prpcombo id="id_{{property.id}}"
                                ng-options="item.name for item in property.lookup_items"
                                ng-required="property.validation == 1">
                            <option value=""><?php echo Messages::getMessage(5) ?></option>
                        </select>

                        <div class="input-help-user-register" ng-show="submitted && form.prop.$error.required">
                            <span ng-show="form.prop.$error.required">
                                <h4><?php echo Messages::getMessage(156) ?></h4>
                            </span>
                        </div>
                    </ng-form>
                </div>
            </div>
        </div>

    </div>
    <div>
        <div class="descrition">
            <div ng-repeat="property in groupTwo.properties">
                <div ng-switch="property.c_property_type_id" class="{{property.controller_style}}">
                    <div ng-switch-when="1" style="position: relative">
                        <ng-form name="form">
                            <label for="id_{{property.id}" class="{{property.label_style}}">{{property.name}}</label>

                            <input ng-required="property.validation == 1 || property.validation == 4" onkeyup="correctPrices(this)"
                                   ng-model="property.temp_value" prpinput
                                   property="property" type="text" name="prop"
                                   id="id_{{property.id}}"/>

                            <span class="input-help-user-register" ng-show="submitted && (form.prop.$error.required || form.prop.$error.number)">
                                <span ng-show="form.prop.$error.required" style="width: 45px;">
                                    <h4><?php echo Messages::getMessage(156) ?></h4>
                                </span>
                                 <span ng-show="form.prop.$error.number" style="width: 45px;">
                                    <h4><?php echo Messages::getMessage(157) ?></h4>
                                </span>
                            </span>
                        </ng-form>
                    </div>

                    <span ng-switch-when="3">
                        <input ng-model="property.temp_value" property="property" type="checkbox"
                               name="id_{{property.id}}" id="id_{{property.id}}"/>
                        <label class="{{property.label_style}}" for="id_{{property.id}">{{property.name}}</label>
                    </span>

                    <div ng-switch-when="2" style="position: relative">
                        <ng-form name="form">
                            <label for="id_{{property.id}">{{property.name}}</label>
                            <select data-ng-model="property.temp_value" property="property"
                                    properties="groupOne.properties"
                                    name="prop"  prpcombo id="id_{{property.id}"
                                    ng-options="item.name for item in property.lookup_items"
                                    ng-required="property.validation == 1">
                                <option value=""><?php echo Messages::getMessage(5) ?></option>
                            </select>

                            <div class="input-help-user-register" ng-show="submitted && form.prop.$error.required">
                                <span ng-show="form.prop.$error.required">
                                    <h4><?php echo Messages::getMessage(156) ?></h4>
                                </span>
                            </div>
                        </ng-form>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="box">
    <h3 class="car_type"><?php echo Messages::getMessage(192) ?></h3>
    <textarea class="left info" ng-model="description"> </textarea>

    <div class="user_recommendation warning_box right">
        <h3 class="mb10"><?php echo Messages::getMessage(194) ?></h3>
        <ul class="left">
            <li>
                <?php echo Messages::getMessage(195) ?>
            </li>
            <li>
                <?php echo Messages::getMessage(196) ?>
            </li>
            <li>
                <?php echo Messages::getMessage(197) ?>
            </li>
            <li>
                <?php echo Messages::getMessage(198) ?>
            </li>
        </ul>
        <p><?php echo Messages::getMessage(199) ?></p>
    </div>
</div>
    <?php echo $this->renderPartial('photoUploader'); ?>

</form>
<!-- ************************* END ANGULAR IMPLEMENTATION ***************************************** -->
</div>
<input type="hidden" id="usd_rate" value="{{rate_to_usd}}">
<input type="hidden" id="eur_rate" value="{{rate_to_euro}}">
<input type="hidden" id="rate_id" value="1">

<script>
    function correctPrices(dom){
        var value = dom.value;
        var usd_rate = $("#usd_rate").val();
        var eur_rate = $("#eur_rate").val();

        if(dom.id == "id_176"){
            $("#id_177").val(Math.ceil(value / usd_rate));
            $("#id_178").val(Math.ceil(value / eur_rate));
            $("#rate_id").val(1);
        }
        else if(dom.id == "id_177"){
            $("#id_176").val(Math.ceil(value * usd_rate));
            $("#id_178").val(Math.ceil(value * usd_rate / eur_rate));
            $("#rate_id").val(2);
        }
        else if(dom.id == "id_178"){
            $("#id_176").val(Math.ceil(value * eur_rate));
            $("#id_177").val(Math.ceil(value * eur_rate / usd_rate));
            $("#rate_id").val(3);
        }

    }
</script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-controllers/jobForm_controller.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-models/services.js"></script>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ajaxfileupload.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/UploadHelper.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-controllers/upload_controller.js" type="text/javascript"></script>