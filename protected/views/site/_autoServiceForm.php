<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Edgar
 * Date: 8/29/13
 * Time: 12:03 AM
 * To change this template use File | Settings | File Templates.
 */
?>

<div class="content sale_form right car_part_block decoration" ng-controller="autoServiceController" ng-init="init()" id="announcement_form"
     cg-busy="{promise:promise,message:'loading',backdrop:true}">
    <div class="box">
<?php echo $this->renderPartial('topInfo', array('editMode'=>$editMode, 'announcementId'=>$announcementId, 'formCaption'=>$formCaption)); ?>
    </div>
<form name="myForm" ng-submit="submit(myForm);" novalidate  method="post" enctype="multipart/form-data">
<div class="box">
    <h3 class="car_type"><?php echo Messages::getMessage(297) ?></h3>

    <div class="groups">
        <div ng-repeat="property in groupOne.properties" class="{{property.controller_style}}">
            <span ng-switch="property.c_property_type_id" >

                <div ng-switch-when="1" style="position: relative">
                    <ng-form name="form">
                        <label for="id_{{property.id}}"><div style="float: left">{{property.name}} </div>
                            <div style="color: red;float: left" ng-show="property.validation == 1  || property.validation == 4 || property.validation == 6 || property.validation == 5">*</div></label>

                        <input ng-required="property.validation == 1 || property.validation == 4 || property.validation == 6 || property.validation == 5"
                               ng-model="property.temp_value" prpinput
                               property="property" type="text" name="prop"
                               id="id_{{property.id}}"/>

                        <div class="input-help" ng-show="submitted && form.prop.$error.required">
                            <h4><?php echo Messages::getMessage(156) ?></h4>
                        </div>
                        <div class="input-help" ng-show="submitted && form.prop.$error.number">
                            <h4><?php echo Messages::getMessage(157) ?></h4>
                        </div>
                        <div class="input-help" ng-show="submitted && form.prop.$error.phone">
                            <h4><?php echo Messages::getMessage(158) ?></h4>
                        </div>
                        <div class="input-help" ng-show="submitted && form.prop.$error.email">
                            <h4><?php echo Messages::getMessage(159) ?></h4>
                        </div>
                    </ng-form>
                </div>

                <div ng-switch-when="2" style="position: relative">
                    <ng-form name="form">
                        <label for="id_{{property.id}}"><div style="float: left">{{property.name}} </div> <div style="color: red;float: left" ng-show="property.validation == 1">*</div></label>
                        <select data-ng-model="property.temp_value" property="property"
                                properties="groupOne.properties" style="{{ property.style }}"
                                name="prop"  prpcombo id="id_{{property.id}}"
                                ng-options="item.name for item in property.lookup_items"
                                ng-required="property.validation == 1">
                            <option value=""><?php echo Messages::getMessage(5) ?></option>
                        </select>

                        <div class="input-help-user-register" ng-show="submitted && form.prop.$error.required">
                            <span ng-show="form.prop.$error.required">
                                <h4><?php echo Messages::getMessage(156) ?></h4>
                            </span>
                        </div>
                    </ng-form>
                </div>
            </span>
        </div>
        <div ng-repeat="property in term.properties">
            <div ng-switch="property.c_property_type_id" class="{{property.controller_style}}">
                <div ng-switch-when="2" style="position: relative">
                    <ng-form name="form">
                        <label for="id_{{property.id}}"><div style="float: left">{{property.name}} </div> <div style="color: red;float: left" ng-show="property.validation == 1">*</div></label>
                        <select data-ng-model="property.temp_value" property="property"
                                properties="groupOne.properties"
                                name="prop"  prpcombo id="id_{{property.id}}"
                                ng-options="item.name for item in property.lookup_items"
                                ng-required="property.validation == 1">
                            <option value=""><?php echo Messages::getMessage(5) ?></option>
                        </select>

                        <div class="input-help-user-register" ng-show="submitted && form.prop.$error.required">
                            <span ng-show="form.prop.$error.required">
                                <h4><?php echo Messages::getMessage(156) ?></h4>
                            </span>
                        </div>
                    </ng-form>
                </div>
            </div>
        </div>
        <br/><br/>
    </div>

    <br/> <br/>

    <div class="box care_place">

        <div class="panel">

            <ng-form name="form">
                <label><?php echo Messages::getMessage(202) ?></label>

               <select id="select_country" ng-model="location" required  name="location" ng-change="countryChange();"
                        ng-options="item.name for item in locations" ></select>

               <span class="input-help-user-register" ng-show="submitted && form.location.$error.required">
                    <span ng-show="form.location.$error.required">
                        <h4><?php echo Messages::getMessage(156) ?></h4>
                    </span>
               </span>

            </ng-form>

        </div>

        <div class="panel" ng-show="toShowSubLocations();">
            <label class="small_label">Сообщество:</label>
            <select ng-model="subLocation" ng-options="item.name for item in subLocations"></select>
        </div>
        <div class="panel" >
            <label><?php echo Messages::getMessage(203) ?></label>
            <input type="text" style="width:230px;" ng-model="optional_location">
        </div>

    </div>

</div>

<div class="box">
    <h3 class="car_type"><?php echo Messages::getMessage(300) ?></h3>

    <div style="padding: 10px">

        <div ng-repeat="service in selectedServices" style="margin-left: 120px">

            <ng-form name="form" style="row">
                <label for="id_{{property.id}}">{{property.name}}</label>
                <select data-ng-model="service.value"
                        id="id_{{property.id}}"
                        ng-options="item.name for item in servicesLookup"
                        required>

                </select>

                <span class="input-help-user-register" ng-show="submitted && form.prop.$error.required">
                    <span ng-show="form.prop.$error.required">
                        <h4><?php echo Messages::getMessage(156) ?></h4>
                    </span>
                </span>

                <input type="text" ng-model="service.price" style="width: 120px;margin-left: 50px">
                <span style="font-weight: bold;margin-left: 10px;">RUR </span>
            </ng-form>
        </div>

        <div style="clear: both;padding-top: 10px;">
            <a ng-click="addingService();" class="cuteButton" style="margin-left:470px;"><?php echo Messages::getMessage(219) ?></a>
        </div>
    </div>
</div>

<div class="box" style="padding-top: 10px;padding-bottom: 15px;">
    <google-map center="center" draggable="true" zoom="zoom" markers="markers" mark-click="true"
                class="angular-google-map left"  style="margin-left:15px;height: 300px;width: 380px;margin-top: 10px"></google-map>

    <div class="user_recommendation warning_box right" style="width: 300px !important;padding-bottom: 40px;">
        <h3 class="mb10"><?php echo Messages::getMessage(305) ?></h3>
        <ul class="left">
            <li>
                <?php echo Messages::getMessage(306) ?>
            </li>

        </ul>
    </div>

</div>

<div class="box">
    <h3 class="car_type"><?php echo Messages::getMessage(192) ?></h3>
    <textarea class="left info" ng-model="description"> </textarea>

    <div class="user_recommendation warning_box right">
        <h3 class="mb10"><?php echo Messages::getMessage(194) ?></h3>
        <ul class="left">
            <li>
                <?php echo Messages::getMessage(195) ?>
            </li>
            <li>
                <?php echo Messages::getMessage(196) ?>
            </li>
            <li>
                <?php echo Messages::getMessage(197) ?>
            </li>
            <li>
                <?php echo Messages::getMessage(198) ?>
            </li>
        </ul>
        <p><?php echo Messages::getMessage(199) ?></p>
    </div>
</div>
    <?php echo $this->renderPartial('photoUploader'); ?>
</form>
<!-- ************************* END ANGULAR IMPLEMENTATION ***************************************** -->
</div>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-controllers/autoServiceForm_controller.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-models/services.js"></script>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ajaxfileupload.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/UploadHelper.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-controllers/upload_controller.js" type="text/javascript"></script>