<ul class="cars" ng-show="showInLargeItems" style="margin: 20px 0px 20px 1px">
    <li ng-repeat="announcement in announcements | startFrom:pagingProperties.getCurrentPage()*pagingProperties.getPageSize() | limitTo:pagingProperties.getPageSize()" style="float: left; margin:2px 3px 17px 3px;" ng-click="toDetailedInfo(announcement.id,$event);"
        onmouseover="showDesc(this)" onmouseout="hideDesc(this)">
        <img src="images/urgent_am.jpg"  style="position: absolute; margin-top: 2px; margin-left: 2px " ng-show="announcement.is_top==1">
        <img class="car_img"  ng-src="<?php echo Yii::app()->request->baseUrl . "/uploads/"; ?>{{announcement.main_image}}" width="186px" height="147px">
        <!--<img ng-src="{{ auto.src }}" width="160px" height="105px">-->

        <div class="remember_row" style="background-color: black; opacity: 0.7; height: 17px;">
                <span ng-click="addToRememberedList(announcement.id, announcement.announcement_name, announcement.payment_type+' '+announcement.price, announcement.main_image, $index, $event)" style="background:url(<?php echo Yii::app()->request->baseUrl;?>/images/save.png) no-repeat 58px 4px; margin-top: -6px; padding: 4px 22px 4px 0; z-index:20; cursor: pointer;float: left; color: #ffffff" ng-show="!announcement.remembered">
                    запомнить
                </span>
                <span ng-click="removeFromRememberedList(announcement.id, $index, $event)" style="background:url(<?php echo Yii::app()->request->baseUrl;?>/images/save1.png) no-repeat 58px 4px; margin-top: -6px; padding: 4px 22px 4px 0; z-index:20; cursor: pointer;float: left; color: #ffffff" ng-show="announcement.remembered">
                 запомнено
                </span>
                <span class="right">
                    <div class="my_price"  style="color: #ffffff;">
                        {{announcement.price}} {{ getCurrencyLabel(announcement.rate_id) }}
                    </div>
                </span>
            <!--<div>{{ auto.year }}<span>{{ auto.price }}<span></span></span></div>-->
        </div>
        <div class="left description desc" style="display: none">
            <div>
<!--                <img ng-src="--><?php //echo Yii::app()->request->baseUrl; ?><!--/uploads/flags/{{announcement.flag_name}}" width="22px" height="16px" class="left">-->
                <h2 class="left" ng-click="toDetailedInfo(announcement.id,$event);" id="announcement_name">
                    {{announcement.announcement_name}}</h2>
                    <span class="price">

                        <div class="my_price" >
                            {{announcement.price}} {{ getCurrencyLabel(announcement.rate_id) }}
                        </div>
                    </span>
            </div>
            <p class="car_description" ng-bind-html= "announcement.properties_summary_arm |truncate:250 |unsafe "> </p>
            <div style="font-weight: bold; float: left">{{ announcement.created | date:'dd.MM.yyyy'}}</div>
            <div class="left" style="margin-left: 60px;" ng-bind-html="announcement.user_phone |unsafe"></div>
        </div>
    </li>
</ul>