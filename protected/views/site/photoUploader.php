<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Yerem
 * Date: 11/17/13
 * Time: 2:10 AM
 * To change this template use File | Settings | File Templates.
 */
?>
<div class="box">
    <h3 class="car_type" id="car_type4"><?php echo Messages::getMessage(322) ?></h3>
    <div class="user_recommendation">
        <h2 class="for_user"><?php echo Messages::getMessage(208) ?></h2>
        <ul>
            <li>
                <?php echo Messages::getMessage(209) ?>
            </li>
            <li>
                <?php echo Messages::getMessage(210) ?>
            </li>
            <li>
                <?php echo Messages::getMessage(211) ?>
            </li>
        </ul>
    </div>
</div>
<!--  box-->
<div class="box">
    <div class="user_recommendation upload_image  left">
        <h3 class="mb10"><?php echo Messages::getMessage(212) ?></h3>
        <ul class="left">
            <li>
                <?php echo Messages::getMessage(213) ?> <span class="grey_font">Select photo</span> <?php echo Messages::getMessage(214) ?>
                <class="grey_font"> <?php echo Messages::getMessage(215) ?>
                </class="grey_font"></li>
            <li>
                <?php echo Messages::getMessage(216) ?>
            </li>
            <li>
                <?php echo Messages::getMessage(217) ?>
            </li>
        </ul>
    </div>
    <!-- Load Feather code -->
    <script type="text/javascript" src="http://feather.aviary.com/js/feather.js"></script>

    <!-- Instantiate Feather -->
    <script type='text/javascript'>
        var featherEditor = new Aviary.Feather({
            apiKey: 'a6078894829917e9',
            apiVersion: 3,
            theme: 'light', // Check out our new 'light' and 'dark' themes!
            tools:'all',
            language:'ru',
            appendTo: '',
            onSave: function(imageID, newURL) {
                var img = document.getElementById(imageID);
                img.src = newURL;

            },
            onError: function(errorObj) {
                alert(errorObj.message);
            }
        });
        function launchEditor(id, src) {
            featherEditor.launch({
                image: id,
                url: src
            });
            return false;
        }
    </script>

    <div id='injection_site'></div>

    <!-- Add an edit button, passing the HTML id of the image and the public URL of the image -->
<div ng-controller="uploadController" ng-init="init(0)">
        <input type="hidden" name="MAX_FILE_SIZE" value="90000" />
        <div class="pictures_panel ">
            <ul>
                <li ng-repeat="image in images">
                    <a  data-id="{{image.dataId}}" >
                        <img src="images/delete.png" ng-click="removeImg($index)" style="cursor: pointer; position: absolute" ng-show="image.isUploaded">
                        <img ng-src="{{image.src}}" id="{{image.id}}" style="width: 125px; height: 105px;" alt="{{image.title}}">
                        <input type="hidden" value="{{image.id}}" class="image_id"/>
                        <p><input class="redactorof_photo" src='http://images.aviary.com/images/edit-photo.png' type='image'  value='Редактировать' onclick="return launchEditor($('.image_id').val(), $(this).attr(src));" /></p>
                    </a>
                     </li>
            </ul>
            <div class="choose_file">
                <img id="loading" src="<?php echo Yii::app()->request->baseUrl; ?>/images/loading.gif" style="height: 18px; display: none; " >
                <p class="form">
                    <input type="text" id="path" style="width: 140px; padding: 10px 10px; margin-top:16px; float: left; border:2px solid #dddddd; margin-left: 5px;" x-webkit-speech/>
                    <label class="add-photo-btn" style="margin-top:15px;" ><?php echo Messages::getMessage(323) ?>
                        <span>
                                           <input type="file" id="qqfile" name="qqfile" style="float:left;"/>
                                        </span>
                    </label>
                </p>

                    <a ng-click="upload()" class="choose_file_btn left" style="cursor: pointer; float: left; line-height: 1.4; margin-top: 16px;"><?php echo Messages::getMessage(108) ?></a>

                <div class="file_block"><?php echo Messages::getMessage(218) ?></div>

            </div>
        </div>
    </div>

    <div class="clear"></div>
    <div class="notification">
        <?php if (Yii::app()->user->isGuest) { ?>
            <script>
                $(document).ready(function () {
                    document.getElementById('step4').onclick = function () {
                        openbox('closed_block', this);
                        return false;
                    };

                    function openbox(id, car_type_h3) {
                        var div = document.getElementById('step_four');
                        if (div.style.display == 'block') {
                            div.style.display = 'block';
                        }
                        else {
                            $('#step_four').fadeIn(700,function(){
                                $('#step_four').css({'display':'block'});
                            });
                            $('#to_four_step').css({'background':'#4c9b74', 'color':'#ffffff'});
                            $('#step_after4').css({'border-color': 'transparent transparent transparent #4c9b74'});

                            $("#step_one").fadeOut(400, function(){
                                $("#step_one").css({'display':'none'});
                            });

                            $("#step_three").fadeOut(400, function(){
                                $("#step_three").css({'display':'none'});
                            });
                            $("#closed_block").fadeOut(400,function(){
                                $("#closed_block").css({'display':'none'});
                            });
                        }
                    }
                });
            </script>

            <span class="form_steps" id="step4">Шаг 4: Ввод личных данных.</span>

            <?php } else {?>
            <a class="add_notification" linked="submitButton" style="margin-left: 15px;"> <?php echo Messages::getMessage(106) ?></a>
            <?php echo CHtml::Link(Messages::getMessage(168),Yii::app()->createUrl( 'site/siteRules' ),array('class'=>'notification_link'));?>


        <?php } ?>
    </div>
</div>
<input type="submit" id="submitButton" style="display: none"/>
<?php if($this->isAdmin()): ?>
<div style="display: none">
    <?php echo CHtml::Link("",Yii::app()->createUrl( 'site/index' ),array('id'=>'redirectIT'));?>
</div>
<?php else: ?>
    <div style="display: none">
            <?php echo CHtml::Link("",Yii::app()->createUrl( 'site/successfullyAdded' ),array('id'=>'redirectIT'));?>
    </div>
<?php endif; ?>
<script>
    $('#qqfile').change(function () {
        $('#path').val($(this).val());
    });
</script>
