<div class="content right car_part_block decoration" ng-controller="spare-wheel-controller" ng-init="init();" id="announcement_form"
     cg-busy="{promise:promise,message:'loading',backdrop:true}" >
    <div class="box">
        <?php echo $this->renderPartial('topInfo', array('editMode'=>$editMode, 'announcementId'=>$announcementId, 'formCaption'=>$formCaption)); ?>
    <form name="myForm" ng-submit="submit(myForm);" novalidate method="post" enctype="multipart/form-data">
        <?php if(!$editMode): ?>
            <?php echo $this->renderPartial('personalData'); ?>
        <?php endif; ?>
            <div class="box">
                <h3 class="car_type">Типы колес</h3>
            </div>
            <div class="groups">
                <div ng-repeat="property in spareProps.properties">
                    <div ng-switch="property.c_property_type_id" class="{{property.controller_style}}">
                        <div ng-switch-when="2" style="position: relative">
                            <ng-form name="form">
                                <label for="id_{{property.id}}"><div style="float: left">{{property.name}} </div> <div style="color: red;float: left" ng-show="property.validation == 1">*</div></label>
                                <select data-ng-model="property.temp_value" property="property"
                                        properties="spareProps.properties"
                                        name="prop" prpcombo id="id_{{property.id}}"
                                        ng-options="item.name for item in property.lookup_items"
                                        ng-required="property.validation == 1">
                                    <option value=""><?php echo Messages::getMessage(5) ?></option>
                                </select>

                                 <span class="input-help-user-register" ng-show="submitted && (form.prop.$error.required || form.prop.$error.number)">
                                    <span ng-show="form.prop.$error.required">
                                        <h4><?php echo Messages::getMessage(156) ?></h4>
                                    </span>
                                 </span>
                            </ng-form>
                        </div>

                        <div ng-switch-when="1" style="position: relative">
                            <ng-form name="form">
                                <label for="id_{{property.id}}"><div style="float: left">{{property.name}} </div>
                                    <div style="color: red;float: left" ng-show="property.validation == 1 || property.validation == 4 || property.validation == 6">*</div></label>

                                <input ng-required="property.validation == 1 || property.validation == 4 || property.validation == 6"
                                       ng-model="property.temp_value" prpinput
                                       property="property" type="text" name="prop"
                                       id="id_{{property.id}}" x-webkit-speech/>

                                <span class="input-help-user-register" ng-show="submitted && (form.prop.$error.required || form.prop.$error.number)">
                                    <span ng-show="form.prop.$error.required">
                                        <h4><?php echo Messages::getMessage(156) ?></h4>
                                    </span>
                                     <span ng-show="form.prop.$error.number">
                                        <h4><?php echo Messages::getMessage(157) ?></h4>
                                    </span>
                            </span>
                            </ng-form>
                        </div>
                    </div>
                </div>
                <div ng-repeat="property in term.properties">
                    <div ng-switch="property.c_property_type_id" class="{{property.controller_style}}">
                        <div ng-switch-when="2" style="position: relative">
                            <ng-form name="form">
                                <label for="id_{{property.id}}"><div style="float: left">{{property.name}} </div> <div style="color: red;float: left" ng-show="property.validation == 1">*</div></label>
                                <select data-ng-model="property.temp_value" property="property"
                                        properties="groupOne.properties"
                                        name="prop"  prpcombo id="id_{{property.id}}"
                                        ng-options="item.name for item in property.lookup_items"
                                        ng-required="property.validation == 1">
                                    <option value=""><?php echo Messages::getMessage(5) ?></option>
                                </select>

                                <div class="input-help-user-register" ng-show="submitted && form.prop.$error.required">
                            <span ng-show="form.prop.$error.required">
                                <h4><?php echo Messages::getMessage(156) ?></h4>
                            </span>
                                </div>
                            </ng-form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="car_value">
                <h1 class="car_value_title"> Правильно оцените ваш автомобиль</h1>

                <p>Махимальная цена</p>

                <p>Средная цена</p>

                <p>Минималая цена</p>
            </div>

            <div class="textarea_box">
                <label>Дополнительная информация:</label>
                <input x-webkit-speech id="mike" style="width: 15px; padding: 5px; float: left; margin-left:15px; margin-right:5px; margin-top:15px;"/>
                <script>
                    $(document).ready(
                        function(){
                            var mike = document.getElementById('mike');
                            mike.onfocus = mike.blur;
                            mike.onwebkitspeechchange = function(e) {
                                //console.log(e); // SpeechInputEvent
                                document.getElementById('txt').value = mike.value;
                            };
                        }
                    );
                </script>
                <textarea id="txt" ng-model="description" style="margin-top: 15px; margin-left:10px;"></textarea>
            </div>

             <br/><br/>
            <div class="box care_place">
                <h3 class="car_type">Местонахождение</h3>

                <div class="panel">
                    <label>Страна:</label>

                    <select id="select_country" ng-model="location" ng-options="item.name for item in locations" required
                            ng-change="countryChange();"></select>

                    <span class="input-help-user-register" ng-show="submitted && form.location.$error.required">
                        <span ng-show="form.location.$error.required">
                            <h4><?php echo Messages::getMessage(156) ?></h4>
                        </span>
                   </span>

<!--                    <div id="icon">-->
<!--                        <img ng-src="--><?php //echo Yii::app()->request->baseUrl; ?><!--/uploads/flags/{{location.flag_name}}" class="left" id="country_item">-->
<!--                    </div>-->
                </div>

                <div class="panel" style="float: right; margin-right: 95px;" ng-show="toShowSubLocations();">
                    <label class="small_label"><?php echo Messages::getMessage(202) ?></label>
                    <select ng-model="subLocation" ng-options="item.name for item in subLocations"></select>
                </div>
                <div class="panel">
                    <label style="margin-left:325px;"><?php echo Messages::getMessage(203) ?></label>
                    <input  style="width:180px; padding: 12px 10px; border: 1px dotted #434343;" type="text" ng-model="optional_location" x-webkit-speech/>
                </div>
            </div>
        <?php echo $this->renderPartial('photoUploader'); ?>
        </form>

    </div>
    <!--  box-->

</div>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-controllers/sparepartWheelForm-controler.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-models/services.js"></script>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ajaxfileupload.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/UploadHelper.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ng-controllers/upload_controller.js"
        type="text/javascript"></script>