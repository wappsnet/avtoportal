<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Yerem
 * Date: 11/22/13
 * Time: 12:41 AM
 * To change this template use File | Settings | File Templates.
 */
?>
    <div id="slider_div" class="bottom_carousel" style="overflow: hidden;width: 770px">

        <div class="jMy_Carusel observe_bot" id="jMy_Carusel" style=" width: 4000px; left: -146px; height: 130px" class="slider1">
            <ul id="slider_ul" class="slider1" >

                <?php foreach($models as $model): ?>
                    <li class="worker-item_li">
                    <span class='ob_carousel_top' >
                        <p><?php if (strlen($model->announcement_name) > 15) {
                                echo mb_substr ($model->announcement_name , 0 , 15 , 'utf-8'). "...";
                            }else{
                                echo $model->announcement_name;
                            }
                            ?>
                        </p>
                    </span>
                        <?php $currUrl = Yii::app()->request->requestUri; ?>
                        <a href="<?php echo (preg_replace("/id=(.*)/","id=".$model->id, $currUrl)); ?>">
                            <img ng-src="<?php echo Yii::app()->request->baseUrl . "/uploads/".$model->main_image; ?>" width='144' height='118' />
                        </a>
                    <span class='ob_carousel_bottom' style="font-weight: bold;">
                           <p><?php echo $model->price ?> {{getCurrencyLabel(<?php echo $model->rate_id?>)}}</p>
                    </span>
                    </li>
                <?php endforeach; ?>

            </ul>
        </div>
        <div id="slider_left" style=" z-index: 1000; position: relative; height: 100%; width: 47px">
            <div class="gv_panelNavPrev" style="top: 55px; left: 10px; display: block;"></div>
            <!--<img src="<?php /*echo Yii::app()->request->baseUrl.'/images/left.png'*/?>" style="cursor: pointer;height: 61px; width: 47px; top: 50%; margin-top: -83px;">-->
        </div>
        <div id="slider_right" style=" z-index: 1000;position: relative; left: 725px; height: 100%; width: 47px">
            <div class="gv_panelNavNext" style="top: 55px; right: 10px; display: block;"></div>
            <!--<img src="<?php /*echo Yii::app()->request->baseUrl.'/images/right.png'*/?>" style="cursor: pointer;height: 61px; width: 47px;top: 50%; margin-top: -83px;">-->
        </div>
    </div>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/sliderHelper.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sliderHelper.css"/>

<script type="text/javascript">
    $(document).ready(function() {
        //initSlider();
    });
</script>
