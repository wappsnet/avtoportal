<?php

define('NL', PHP_EOL);
date_default_timezone_set('UTC');

class RateCommand extends CConsoleCommand {

    public $connection = null;
    public $msg = '';

    public function run($args) {
    	RateCommand::loadRatesToDB();
    }
    
    private static function isDomainAvailible($domain)
    {
    	$headers = @get_headers($domain);
    	if(strpos($headers[0],'200')===false && strpos($headers[0],'302')===false){
    		return false;
    	}else{
    		return true;
    	}
    }
    
    private static function loadRatesToDB(){
    	if (RateCommand::isDomainAvailible('http://www.cba.am/_layouts/rssreader.aspx?rss=280F57B8-763C-4EE4-90E0-8136C13E47DA')){
    		$rates = array();
    		$xml=("http://www.cba.am/_layouts/rssreader.aspx?rss=280F57B8-763C-4EE4-90E0-8136C13E47DA");
    		$xmlDoc = new DOMDocument();
    		
    		$xmlDoc->load($xml);
    		$x=$xmlDoc->getElementsByTagName('item');
			$connection=Yii::app()->db;    		
    		foreach ($x as $item){
    			$item_title=$item->getElementsByTagName('title')->item(0)->childNodes->item(0)->nodeValue;
    			$item_link=$item->getElementsByTagName('link')->item(0)->childNodes->item(0)->nodeValue;
    			if (strpos($item_title,'USD') !== false ||
    			strpos($item_title,'RUB') !== false ||
    			strpos($item_title,'EUR') !== false) {
    				$arr = explode(' - 1 - ', $item_title, 2);
    				$sql = 'update rates_from_cba set value ='.number_format($arr[1], 2, '.', '').' where rate = "'.$arr[0].'"';
    				$command=$connection->createCommand($sql);
    				$command->execute();
    			}
    		}
    	}else {
    		// i don't decided yet
    	}
    }
    
}

?>
