<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'onBeginRequest'=>create_function('$event', 'return ob_start("ob_gzhandler");'),
    'onEndRequest'=>create_function('$event', 'return ob_end_flush();'),
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'NotAuto',


	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
        'application.utils.*',
		'application.models.*',
		'application.components.*',
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
        'admin'=>array(),
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'test',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('localhost', '127.0.0.1','::1'),
		),

	),

	// application components
	'components'=>array(
	
		'Paypal' => array(
            'class'=>'application.components.Paypal',
             'apiUsername' => '',
            'apiPassword' => '',
            'apiSignature' => '',
            'apiLive' => false,

            'returnUrl' => 'paypal/confirm/', //regardless of url management component
            'cancelUrl' => 'paypal/cancel/', //regardless of url management component
        ),

        'cache'=>array(
            'class'=>'system.caching.CFileCache',
        ),

		'user'=>array(
			// enable cookie-based authentication
            'allowAutoLogin'=>false,
 //           'class' => 'UserIdentity',
            'loginUrl'=>array('admin/index'),
		),
		// uncomment the following to enable URLs in path-format

        'session'=>array(
            'class' => 'CDbHttpSession',
            'timeout'=>1*24*60*60,//1 day
            'autoStart' => true,
            'behaviors' => array()
        ),
       'urlManager'=>array(
            'urlFormat'=>'path',
            'showScriptName'=>false,
            'rules'=>array(
               array('upload/upload', 'pattern'=>'/upload'),

                array('upload/cleanUpOnExit', 'pattern'=>'/cleanUpOnExit'),
                array('site/loadUploads', 'pattern'=>'/loadUploads'),

                array('site/dealerCarAnnouncements', 'pattern'=>'/dealerCarAnnouncements'),
                array('site/needRegister', 'pattern'=>'/needRegister'),
                array('site/personalPage', 'pattern'=>'/personalPage'),
                array('site/officialDealers', 'pattern'=>'/officialDealers'),
                array('site/universalDealers', 'pattern'=>'/universalDealers'),
                array('site/activateUser', 'pattern'=>'/activateUser'),
                array('site/forgotPassword', 'pattern'=>'/forgotPassword'),
                array('site/activateForgotPassword', 'pattern'=>'/activateForgotPassword'),
                array('site/editForm', 'pattern'=>'/editForm'),
                array('site/myAnnouncements', 'pattern'=>'/myAnnouncements'),
                array('site/succeedRegistered', 'pattern'=>'/succeedRegistered'),
                array('site/index', 'pattern'=>'/index'),
                array('site/successfullyAdded', 'pattern'=>'/successfullyAdded'),
                array('site/auto', 'pattern'=>'/auto'),
                array('site/sparePartWheels', 'pattern'=>'/sparePartWheels'),
                array('site/sparePart', 'pattern'=>'/sparePart'),
                array('site/autoRent', 'pattern'=>'/autoRent'),
                array('site/autoService', 'pattern'=>'/autoService'),
                array('site/addAnnouncement', 'pattern'=>'/addAnnouncement'),
                array('site/ruralTech', 'pattern'=>'/ruralTech'),
                array('site/taxi', 'pattern'=>'/taxi'),
                array('site/shipping', 'pattern'=>'/shipping'),
                array('site/trainingCar', 'pattern'=>'/trainingCar'),
                array('site/evacuator', 'pattern'=>'/evacuator'),
                array('site/jobForm', 'pattern'=>'/jobForm'),
                array('site/jobList', 'pattern'=>'/jobList'),
                array('site/carRepair', 'pattern'=>'/carRepair'),
                array('site/register', 'pattern'=>'/register'),
                array('site/news', 'pattern'=>'/news'),
                array('site/listCarAnnouncements', 'pattern'=>'/listCarAnnouncements'),
                array('site/listCarAnnouncementsAll', 'pattern'=>'/listCarAnnouncementsAll'),
                array('site/listCarWheelAnnouncementsAll', 'pattern'=>'/listCarWheelAnnouncementsAll'),
                array('site/listSparePartAnnouncementsAll', 'pattern'=>'/listSparePartAnnouncementsAll'),
                array('site/listSparePartAnnouncements', 'pattern'=>'/listSparePartAnnouncements'),
                array('site/listRentCarAnnouncements', 'pattern'=>'/listRentCarAnnouncements'),
                array('site/contact', 'pattern'=>'/contact'),
                array('site/login', 'pattern'=>'/login'),
                array('site/carDetailed', 'pattern'=>'/carDetailed'),
                array('site/newsDetailed', 'pattern'=>'/newsDetailed'),
                array('site/appa', 'pattern'=>'/appa'),
                array('site/customs', 'pattern'=>'/customs'),
                array('site/tech', 'pattern'=>'/tech'),
                array('site/roadPoliceAddresses', 'pattern'=>'/roadPoliceAddresses'),
                array('site/autoSchool', 'pattern'=>'/autoSchool'),
                array('site/listRuralAnnouncementsAll', 'pattern'=>'/listRuralAnnouncementsAll'),
                array('site/listJobAnnouncementsAll', 'pattern'=>'/listJobAnnouncementsAll'),
                array('site/listTaxiAnnouncements', 'pattern'=>'/listTaxiAnnouncements'),
                array('site/listEvacuatorAnnouncementsAll', 'pattern'=>'/listEvacuatorAnnouncementsAll'),
                array('site/listEvacuatorAnnouncements', 'pattern'=>'/listEvacuatorAnnouncements'),
                array('site/listShippingAnnouncements', 'pattern'=>'/listShippingAnnouncements'),
                array('site/tourism', 'pattern'=>'/tourist'),
                array('site/transportation', 'pattern'=>'/transportation'),
                array('site/listTransportationAnnouncements', 'pattern'=>'/listTransportationAnnouncements'),
                array('site/listTourismAnnouncements', 'pattern'=>'/listTourismAnnouncements'),
                array('site/wheelDetailed', 'pattern'=>'/wheelDetailed'),
                array('site/sparePartDetailed', 'pattern'=>'/sparePartDetailed'),
                array('site/carRentDetailed', 'pattern'=>'/carRentDetailed'),
                array('site/taxiDetailed', 'pattern'=>'/taxiDetailed'),
                array('site/specCarDetailed', 'pattern'=>'/specCarDetailed'),
                array('site/evacuatorDetailed', 'pattern'=>'/evacuatorDetailed'),
                array('site/shippingDetailed', 'pattern'=>'/shippingDetailed'),
                array('site/transportationDetailed', 'pattern'=>'/transportationDetailed'),
                array('site/tourismDetailed', 'pattern'=>'/tourismDetailed'),
                array('site/ruralTechDetailed', 'pattern'=>'/ruralTechDetailed'),
                array('site/jobDetailed', 'pattern'=>'/jobDetailed'),
                array('site/listSpecCarAnnouncements', 'pattern'=>'/listSpecCarAnnouncements'),
                array('site/listAutoServices', 'pattern'=>'/listAutoServices'),
                array('site/autoServiceDetailed', 'pattern'=>'/autoServiceDetailed'),
                array('site/roadPolicePenalty', 'pattern'=>'/roadPolicePenalty'),
                array('site/roadSigns', 'pattern'=>'/roadSigns'),
                array('site/trafficRules', 'pattern'=>'/trafficRules'),
                array('site/listServicesByPlace', 'pattern'=>'/listServicesByPlace'),
                array('site/listAutoServicesFromPlace', 'pattern'=>'/listAutoServicesFromPlace'),
                array('site/listAutoServicesFromSubPlace', 'pattern'=>'/listAutoServicesFromSubPlace'),
                array('site/siteRules', 'pattern'=>'/siteRules'),


                array('test/list', 'pattern'=>'test/<model:\w+>', 'verb'=>'GET'),
                array('test/view', 'pattern'=>'test/<model:\w+>/<id:\d+>', 'verb'=>'GET'),
                array('test/update', 'pattern'=>'test/<model:\w+>/<id:\d+>', 'verb'=>'POST'),
                array('test/delete', 'pattern'=>'test/<model:\w+>/<id:\d+>', 'verb'=>'DELETE'),
                array('test/create', 'pattern'=>'test/<model:\w+>', 'verb'=>'POST'),

                array('rest/list', 'pattern'=>'rest/<model:\w+>', 'verb'=>'GET'),
                array('rest/view', 'pattern'=>'rest/<model:\w+>/<id:\d+>', 'verb'=>'GET'),
                array('rest/update', 'pattern'=>'rest/<model:\w+>/<id:\d+>', 'verb'=>'POST'),
                array('rest/delete', 'pattern'=>'rest/<model:\w+>/<id:\d+>', 'verb'=>'DELETE'),
                array('rest/create', 'pattern'=>'rest/<model:\w+>', 'verb'=>'POST'),

                array('admin/default/index', 'pattern'=>'admin/index'),
                array('admin/default/admin', 'pattern'=>'admin/admin'),
                array('admin/default/adminCar', 'pattern'=>'admin/adminCar'),
                array('admin/default/editCarItems', 'pattern'=>'admin/editCarItems'),
                array('admin/default/addNews', 'pattern'=>'admin/addNews'),
                array('admin/default/adminAppa', 'pattern'=>'admin/adminAppa'),
                array('admin/default/addAppa', 'pattern'=>'admin/addAppa'),
                array('admin/default/editAutoSchool', 'pattern'=>'admin/editAutoSchool'),
                array('admin/default/addAutoSchool', 'pattern'=>'admin/addAutoSchool'),
                array('admin/default/editTechObserve', 'pattern'=>'admin/editTechObserve'),
                array('admin/default/adminItems', 'pattern'=>'admin/adminItems'),
                array('admin/default/addBanner', 'pattern'=>'admin/addBanner'),
                array('admin/default/lookupAdd', 'pattern'=>'admin/lookupAdd'),
                array('admin/default/deleteLookupItem', 'pattern'=>'admin/deleteLookupItem'),
                array('admin/default/lookupEdit', 'pattern'=>'admin/lookupEdit'),
                array('admin/default/editWheelItems', 'pattern'=>'admin/editWheelItems'),
                array('admin/default/editSparePartItems', 'pattern'=>'admin/editSparePartItems'),
                array('admin/default/editAutoServiceItems', 'pattern'=>'admin/editAutoServiceItems'),
                array('admin/default/editRuralTechItems', 'pattern'=>'admin/editRuralTechItems'),
                array('admin/default/editShippingItems', 'pattern'=>'admin/editShippingItems'),
                array('admin/default/editTourismItems', 'pattern'=>'admin/editTourismItems'),
                array('admin/default/editEvacuatorItems', 'pattern'=>'admin/editEvacuatorItems'),
                array('admin/default/editTaxiItems', 'pattern'=>'admin/editTaxiItems'),
                array('admin/default/editTrainingCarItems', 'pattern'=>'admin/editTrainingCarItems'),
                array('admin/default/editJobItems', 'pattern'=>'admin/editJobItems'),
                array('admin/default/adminLocation', 'pattern'=>'admin/adminLocation'),
                array('admin/default/addLocation', 'pattern'=>'admin/addLocation'),
                array('admin/default/editLocation', 'pattern'=>'admin/editLocation'),
                array('admin/default/addSubLocation', 'pattern'=>'admin/addSubLocation'),
                array('admin/default/editSubLocation', 'pattern'=>'admin/editSubLocation'),
                array('admin/default/editAppa', 'pattern'=>'admin/editAppa'),
                array('admin/default/adminAutoSchool', 'pattern'=>'admin/adminAutoSchool'),
                array('admin/default/editTaxService', 'pattern'=>'admin/editTaxService'),
                array('admin/default/roadPoliceAddresses', 'pattern'=>'admin/roadPoliceAddresses'),
                array('admin/default/editTransportationItems', 'pattern'=>'admin/editTransportationItems'),
                array('admin/default/adminModelGroups', 'pattern'=>'admin/adminModelGroups'),
                array('admin/default/editModelGroup', 'pattern'=>'admin/editModelGroup'),
                array('admin/default/addModelGroup', 'pattern'=>'admin/addModelGroup'),
                array('admin/default/roadPolice', 'pattern'=>'admin/roadPolice'),
                array('admin/default/roadPolicePenalty', 'pattern'=>'admin/roadPolicePenalty'),
                array('admin/default/roadSigns', 'pattern'=>'admin/roadSigns'),
                array('admin/default/addRoadSign', 'pattern'=>'admin/addRoadSign'),
                array('admin/default/editRoadSign', 'pattern'=>'admin/editRoadSign'),
                array('admin/default/adminNews', 'pattern'=>'admin/adminNews'),
                array('admin/default/editNews', 'pattern'=>'admin/editNews'),
                array('admin/default/addTrafficRule', 'pattern'=>'admin/addTrafficRule'),
                array('admin/default/trafficRules', 'pattern'=>'admin/trafficRules'),
                array('admin/default/editTrafficRule', 'pattern'=>'admin/editTrafficRule'),
                array('admin/default/addAutoService', 'pattern'=>'admin/addAutoService'),
                array('admin/default/editAutoService', 'pattern'=>'admin/editAutoService'),
                array('admin/default/deleteServiceItem', 'pattern'=>'admin/deleteServiceItem'),
                array('admin/default/editSiteRules', 'pattern'=>'admin/editSiteRules'),
				array('admin/default/editBank', 'pattern'=>'admin/editBank'),
				array('admin/default/userControl', 'pattern'=>'admin/userControl'),
				array('admin/default/editCarMarkItems', 'pattern'=>'admin/editCarMarkItems'),
                array('admin/default/editCarModelItems', 'pattern'=>'admin/editCarModelItems'),



                array('admin/default/logout', 'pattern'=>'admin/logout'),

                array('admin/bannerUpload/upload', 'pattern'=>'admin/upload'),


                '<controller:\w+>/<id:\d+>'=>'<controller>/<id>',
                '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>/<id>',
                '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',

            ),
        ),
		// uncomment the following to use a MySQL database

		'db'=>array(
			'connectionString' => 'mysql:host=localhost;port=3306;dbname=notautodatabase',
            'schemaCachingDuration'=>0, //60*60*24*365,
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => '',
			'charset' => 'utf8',
            'enableProfiling' => true
		),

		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),

         'mail'=>array(
            'class'=>'application.extensions.yii-mail.YiiMail',
            'transportType'=>'smtp',
            'transportOptions'=>array(
                'host'=>'sv29.byethost29.org',
                'encryption'=>'tls', // use ssl
                'username'=>'vacharq@vacharq.am',
                'password'=>'TkuX%2BW0FAl',
                'port'=>465, // ssl port for gmail
            ),
            'viewPath' => 'application.views.announcement',
            'logging' => true,
            'dryRun' => false
        ),

        'messages'=>array('class'=>'application.components.Messages'),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'avtomeqena.am@gmail.com',
	),
);
