
<?php

return array(
        'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
        'name'=>'Cron',
        'preload'=>array('log'),
        'import'=>array(
                'application.models.*',
  				'application.components.*',
        ),
        // application components
        'components'=>array(
                'db'=>array(
		    		'connectionString' => 'mysql:host=localhost;dbname=mydb_new',
		    		'emulatePrepare' => true,
		    		'username' => 'root',
		    		'password' => '',
		    		'charset' => 'utf8',
		    		'enableProfiling' => true
				),		
//                'mailer' => array(
//                    'class' => 'application.extensions.mailer.EMailer',
//                    'pathViews' => 'application.views.email',
//                    'pathLayouts' => 'application.views.email.layouts'
//                ),
                
                
                
        )  
);
