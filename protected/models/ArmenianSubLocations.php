<?php

/**
 * This is the model class for table "armenian_sub_locations".
 *
 * The followings are the available columns in table 'armenian_sub_locations':
 * @property integer $id
 * @property integer $location_id
 * @property string $name_eng
 * @property string $name_arm
 * @property string $name_geo
 * @property string $name_rus
 */
class ArmenianSubLocations extends CActiveRecord
{
    public $name;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ArmenianSubLocations the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'armenian_sub_locations';
	}

    public function getAttributes($names = true)
    {
        $attributes = parent::getAttributes($names);
        $attributes['name'] = $this->name;
        return $attributes;
    }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('location_id', 'numerical', 'integerOnly'=>true),
			array('name_eng, name_arm, name_geo, name_rus', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, location_id, name_eng, name_arm, name_geo, name_rus', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'location_id' => 'Location',
			'name_eng' => 'Name Eng',
			'name_arm' => 'Name Arm',
			'name_geo' => 'Name Geo',
			'name_rus' => 'Name Rus',
		);
	}

    public function findAll($condition = '', $params = array())
    {
        $list = parent::findAll($condition, $params);

        foreach ($list as $item) {
            if(Messages::getLanguageId() == 'arm'){
                $item->name =  $item->name_arm;
            }elseif(Messages::getLanguageId() == 'eng'){
                $item->name =  $item->name_eng;
            }elseif(Messages::getLanguageId() == 'rus'){
                $item->name =  $item->name_rus;
            }elseif(Messages::getLanguageId() == 'geo'){
                $item->name =  $item->name_geo;
            }
        }
        return $list;
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('location_id',$this->location_id);
		$criteria->compare('name_eng',$this->name_eng,true);
		$criteria->compare('name_arm',$this->name_arm,true);
		$criteria->compare('name_geo',$this->name_geo,true);
		$criteria->compare('name_rus',$this->name_rus,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}