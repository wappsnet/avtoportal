<?php

/**
 * This is the model class for table "spec_car_announcement".
 *
 * The followings are the available columns in table 'spec_car_announcement':
 * @property integer $id
 * @property string $created
 * @property string $description
 * @property integer $term_id
 * @property string $properties_summary_eng
 * @property string $properties_summary_arm
 * @property string $properties_summary_rus
 * @property string $properties_summary_geo
 * @property string $main_image
 * @property integer $user_id
 * @property integer $unregistered_user_id
 * @property integer $is_top
 * @property integer $price
 * @property integer $rate_id
 * @property integer $mark_id
 * @property integer $model_id
 * @property integer $vendor
 * @property integer $isAccepted
 * @property string $announcement_name
 * @property string $user_phone
 * @property string $validDate
 * @property string $view_count
 */
class SpecCarAnnouncement extends CActiveRecord
{
    public $images = array();
    public $propertiesLeft = array();
    public $propertiesRight = array();
    public $userPhones;
    public $userInfo;
    public $imageCount;
    public $payment_type;


    /**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return SpecCarAnnouncement the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'spec_car_announcement';
	}

    public function getAttributes($names = true)
    {
        $attributes = parent::getAttributes($names);
        $attributes['images'] = $this->images;
        $attributes['propertiesLeft'] = $this->propertiesLeft;
        $attributes['propertiesRight'] = $this->propertiesRight;
        $attributes['userPhones'] = $this->userPhones;
        $attributes['userInfo'] = $this->userInfo;
        $attributes['imageCount'] = $this->imageCount;
        $attributes['payment_type'] = $this->payment_type;

        return $attributes;
    }

    public function findAll($condition = '', $params = array())
    {
        $list = parent::findAll($condition, $params);
        foreach($list as $item){
            $item->imageCount = count(SpecCarPhotos::model()->findAllByAttributes(array("announcement_id"=>$item->id)));
            $item->payment_type = Messages::getMessage(320);
        }
        return $list;
    }

    public function findByPk($pk, $condition = '', $params = array())
    {
        $item = parent::findByPk($pk, $condition, $params);
        $this->payment_type = Messages::getMessage(320);

        return $item;
    }


    /**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('term_id, user_id, unregistered_user_id, is_top, price, rate_id, mark_id, model_id, vendor, isAccepted', 'numerical', 'integerOnly'=>true),
			array('description', 'length', 'max'=>500),
			array('properties_summary_eng, properties_summary_arm, properties_summary_rus, properties_summary_geo', 'length', 'max'=>300),
			array('main_image', 'length', 'max'=>50),
			array('created', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, created, description, term_id, properties_summary_eng, properties_summary_arm, properties_summary_rus, properties_summary_geo, main_image, user_id, unregistered_user_id, is_top, price, rate_id, mark_id, model_id, vendor, isAccepted', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'created' => 'Created',
			'description' => 'Description',
			'term_id' => 'Term',
			'properties_summary_eng' => 'Properties Summary Eng',
			'properties_summary_arm' => 'Properties Summary Arm',
			'properties_summary_rus' => 'Properties Summary Rus',
			'properties_summary_geo' => 'Properties Summary Geo',
			'main_image' => 'Main Image',
			'user_id' => 'User',
			'unregistered_user_id' => 'Unregistered User',
			'is_top' => 'Is Top',
			'price' => 'Price',
			'rate_id' => 'Rate',
			'mark_id' => 'Mark',
			'model_id' => 'Model',
			'vendor' => 'Vendor',
			'isAccepted' => 'Is Accepted',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('term_id',$this->term_id);
		$criteria->compare('properties_summary_eng',$this->properties_summary_eng,true);
		$criteria->compare('properties_summary_arm',$this->properties_summary_arm,true);
		$criteria->compare('properties_summary_rus',$this->properties_summary_rus,true);
		$criteria->compare('properties_summary_geo',$this->properties_summary_geo,true);
		$criteria->compare('main_image',$this->main_image,true);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('unregistered_user_id',$this->unregistered_user_id);
		$criteria->compare('is_top',$this->is_top);
		$criteria->compare('price',$this->price);
		$criteria->compare('rate_id',$this->rate_id);
		$criteria->compare('mark_id',$this->mark_id);
		$criteria->compare('model_id',$this->model_id);
		$criteria->compare('vendor',$this->vendor);
		$criteria->compare('isAccepted',$this->isAccepted);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}