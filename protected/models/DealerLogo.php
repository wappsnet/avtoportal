<?php

/**
 * This is the model class for table "dealer_logo".
 *
 * The followings are the available columns in table 'dealer_logo':
 * @property integer $id
 * @property string $logo_name
 * @property string $created
 * @property integer $dealer_id
 */
class DealerLogo extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return DealerLogo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'dealer_logo';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('dealer_id', 'numerical', 'integerOnly'=>true),
			array('logo_name', 'length', 'max'=>50),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, logo_name, created, dealer_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'logo_name' => 'Logo Name',
			'created' => 'Created',
			'dealer_id' => 'Dealer',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('logo_name',$this->logo_name,true);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('dealer_id',$this->dealer_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}