<?php

/**
 * This is the model class for table "shipping_announcement".
 *
 * The followings are the available columns in table 'shipping_announcement':
 * @property integer $id
 * @property string $description
 * @property string $main_image
 * @property integer $techique_id
 * @property integer $job_type_id
 * @property string $properties_summary_eng
 * @property string $properties_summary_arm
 * @property string $properties_summary_geo
 * @property string $properties_summary_rus
 * @property integer $term_id
 * @property integer $unregistered_user_id
 * @property integer $is_top
 * @property integer $with_driver
 * @property integer $vendor
 * @property integer $rate_id
 * @property string $created
 * @property integer $user_id
 * @property integer $price
 * @property integer $mark_id
 * @property integer $body_id
 * @property integer $isAccepted
 * @property string $announcement_name
 * @property string $user_phone
 * @property string $validDate
 * @property string $view_count
 */
class ShippingAnnouncement extends CActiveRecord
{
    public $images = array();
    public $propertiesLeft = array();
    public $propertiesRight = array();
    public $userPhones;
    public $imageCount;
    public $userInfo;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ShippingAnnouncement the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'shipping_announcement';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('techique_id, job_type_id, term_id, unregistered_user_id, is_top, with_driver, vendor, rate_id, user_id, price, mark_id, body_id, isAccepted', 'numerical', 'integerOnly'=>true),
			//array('description', 'length', 'max'=>500),
			array('main_image', 'length', 'max'=>100),
			array('properties_summary_eng, properties_summary_arm, properties_summary_geo, properties_summary_rus', 'length', 'max'=>1000),
			array('created', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, description, main_image, techique_id, job_type_id, properties_summary_eng, properties_summary_arm, properties_summary_geo, properties_summary_rus, term_id, unregistered_user_id, is_top, with_driver, vendor, rate_id, created, user_id, price, mark_id, body_id, isAccepted', 'safe', 'on'=>'search'),
		);
	}

    public function getAttributes($names = true)
    {
        $attributes = parent::getAttributes($names);
        $attributes['images'] = $this->images;
        $attributes['propertiesLeft'] = $this->propertiesLeft;
        $attributes['propertiesRight'] = $this->propertiesRight;
        $attributes['userPhones'] = $this->userPhones;
        $attributes['imageCount'] = $this->imageCount;
        $attributes['userInfo'] = $this->userInfo;

        return $attributes;
    }

    public function findAll($condition = '', $params = array())
    {
        $list = parent::findAll($condition, $params);
        foreach($list as $item){
            $item->imageCount = count(ShippingPhotos::model()->findAllByAttributes(array("announcement_id"=>$item->id)));
        }
        return $list;
    }


    /**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'description' => 'Description',
			'main_image' => 'Main Image',
			'techique_id' => 'Techique',
			'job_type_id' => 'Job Type',
			'properties_summary_eng' => 'Properties Summary Eng',
			'properties_summary_arm' => 'Properties Summary Arm',
			'properties_summary_geo' => 'Properties Summary Geo',
			'properties_summary_rus' => 'Properties Summary Rus',
			'term_id' => 'Term',
			'unregistered_user_id' => 'Unregistered User',
			'is_top' => 'Is Top',
			'with_driver' => 'With Driver',
			'vendor' => 'Vendor',
			'rate_id' => 'Rate',
			'created' => 'Created',
			'user_id' => 'User',
			'price' => 'Price',
			'mark_id' => 'Mark',
			'body_id' => 'Body',
			'isAccepted' => 'Is Accepted',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('main_image',$this->main_image,true);
		$criteria->compare('techique_id',$this->techique_id);
		$criteria->compare('job_type_id',$this->job_type_id);
		$criteria->compare('properties_summary_eng',$this->properties_summary_eng,true);
		$criteria->compare('properties_summary_arm',$this->properties_summary_arm,true);
		$criteria->compare('properties_summary_geo',$this->properties_summary_geo,true);
		$criteria->compare('properties_summary_rus',$this->properties_summary_rus,true);
		$criteria->compare('term_id',$this->term_id);
		$criteria->compare('unregistered_user_id',$this->unregistered_user_id);
		$criteria->compare('is_top',$this->is_top);
		$criteria->compare('with_driver',$this->with_driver);
		$criteria->compare('vendor',$this->vendor);
		$criteria->compare('rate_id',$this->rate_id);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('price',$this->price);
		$criteria->compare('mark_id',$this->mark_id);
		$criteria->compare('body_id',$this->body_id);
		$criteria->compare('isAccepted',$this->isAccepted);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}