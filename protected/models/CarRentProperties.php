<?php

/**
 * This is the model class for table "car_rent_properties".
 *
 * The followings are the available columns in table 'car_rent_properties':
 * @property integer $id
 * @property integer $announcement_id
 * @property integer $c_property_id
 * @property string $value
 * @property string $text_eng
 * @property string $text_rus
 * @property string $text_geo
 * @property string $text_arm
 * @property integer $property_type
 */
class CarRentProperties extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CarRentProperties the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'car_rent_properties';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('announcement_id, c_property_id, property_type', 'numerical', 'integerOnly'=>true),
			array('value, text_eng, text_rus, text_geo, text_arm', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, announcement_id, c_property_id, value, text_eng, text_rus, text_geo, text_arm, property_type', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'announcement_id' => 'Announcement',
			'c_property_id' => 'C Property',
			'value' => 'Value',
			'text_eng' => 'Text Eng',
			'text_rus' => 'Text Rus',
			'text_geo' => 'Text Geo',
			'text_arm' => 'Text Arm',
			'property_type' => 'Property Type',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('announcement_id',$this->announcement_id);
		$criteria->compare('c_property_id',$this->c_property_id);
		$criteria->compare('value',$this->value,true);
		$criteria->compare('text_eng',$this->text_eng,true);
		$criteria->compare('text_rus',$this->text_rus,true);
		$criteria->compare('text_geo',$this->text_geo,true);
		$criteria->compare('text_arm',$this->text_arm,true);
		$criteria->compare('property_type',$this->property_type);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}