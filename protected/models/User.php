<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $id
 * @property integer $user_roles_id
 * @property string $user_name
 * @property string $email
 * @property string $password
 * @property string $description
 * @property integer $phone1_id
 * @property integer $phone2_id
 * @property integer $name_is_hidden
 * @property string $organisation_name
 * @property string $web_site
 * @property string $organisation_address
 * @property integer $user_status
 * @property string $longitude
 * @property string $latitude
 *
 */
class User extends CActiveRecord
{
    public $phone;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
//			array('user_roles_id, phone1_id, phone2_id', 'required'),
//			array('user_roles_id, phone1_id, phone2_id, name_is_hidden, user_status', 'numerical', 'integerOnly'=>true),
//			array('user_name, email, password, description', 'length', 'max'=>45),
//			array('organisation_name, web_site, organisation_address', 'length', 'max'=>50),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, user_roles_id, user_name, email, password, description, phone1_id, phone2_id, name_is_hidden, organisation_name, web_site, organisation_address, user_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(

		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_roles_id' => 'User Roles',
			'user_name' => 'User Name',
			'email' => 'Email',
			'password' => 'Password',
			'description' => 'Description',
			'phone1_id' => 'Phone1',
			'phone2_id' => 'Phone2',
			'name_is_hidden' => 'Name Is Hidden',
			'organisation_name' => 'Organisation Name',
			'web_site' => 'Web Site',
			'organisation_address' => 'Organisation Address',
			'user_status' => 'User Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_roles_id',$this->user_roles_id);
		$criteria->compare('user_name',$this->user_name,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('phone1_id',$this->phone1_id);
		$criteria->compare('name_is_hidden',$this->name_is_hidden);
		$criteria->compare('organisation_name',$this->organisation_name,true);
		$criteria->compare('web_site',$this->web_site,true);
		$criteria->compare('organisation_address',$this->organisation_address,true);
		$criteria->compare('user_status',$this->user_status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}