<?php

/**
 * This is the model class for table "appa_items".
 *
 * The followings are the available columns in table 'appa_items':
 * @property integer $id
 * @property string $text
 * @property string $title
 * @property string $created
 * @property string $phone_1
 * @property string $phone_2
 * @property string $fax
 * @property string $web_site
 * @property string $email
 */
class AppaItems extends CActiveRecord
{
    public $google_markers;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AppaItems the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'appa_items';
	}

    public function getAttributes($names = true)
    {
        $attributes = parent::getAttributes($names);
        $attributes['google_markers'] = $this->google_markers;
        return $attributes;
    }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('created', 'required'),
			array('title', 'length', 'max'=>50),
		//	array('phone_1, phone_2, fax, email', 'length', 'max'=>20),
		//	array('web_site', 'length', 'max'=>30),
		//	array('text', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, text, title, created, phone_1, phone_2, fax, web_site, email', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'text' => 'Text',
			'title' => 'Title',
			'created' => 'Created',
			'phone_1' => 'Phone 1',
			'phone_2' => 'Phone 2',
			'fax' => 'Fax',
			'web_site' => 'Web Site',
			'email' => 'Email',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('text',$this->text,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('phone_1',$this->phone_1,true);
		$criteria->compare('phone_2',$this->phone_2,true);
		$criteria->compare('fax',$this->fax,true);
		$criteria->compare('web_site',$this->web_site,true);
		$criteria->compare('email',$this->email,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}