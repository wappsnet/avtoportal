<?php

/**
 * This is the model class for table "ruraltech_announcement".
 *
 * The followings are the available columns in table 'ruraltech_announcement':
 * @property integer $id
 * @property string $created
 * @property string $description
 * @property integer $term_id
 * @property integer $technic_id
 * @property integer $mark_id
 * @property integer $body_id
 * @property integer $with_driver
 * @property integer $c_location_id
 * @property integer $c_sub_location_id
 * @property string $optional_location
 * @property string $properties_summary_eng
 * @property string $properties_summary_arm
 * @property string $properties_summary_rus
 * @property string $properties_summary_geo
 * @property string $main_image
 * @property integer $user_id
 * @property integer $unregistered_user_id
 * @property integer $is_top
 * @property integer $price
 * @property integer $rate_id
 * @property integer $work_type_id
 * @property integer $isAccepted
 * @property string $announcement_name
 * @property string $user_phone
 * @property string $validDate
 * @property string $view_count
 * @property string $phone_id
 */
class RuraltechAnnouncement extends CActiveRecord
{
    public $images = array();
    public $propertiesLeft = array();
    public $propertiesRight = array();
    public $userPhones;
    public $imageCount;
    public $flag_name;
    public $vendor;
    public $userInfo;
    public $payment_type;



	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return RuraltechAnnouncement the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ruraltech_announcement';
	}

    public function getAttributes($names = true)
    {
        $attributes = parent::getAttributes($names);
        $attributes['images'] = $this->images;
        $attributes['propertiesLeft'] = $this->propertiesLeft;
        $attributes['propertiesRight'] = $this->propertiesRight;
        $attributes['userPhones'] = $this->userPhones;
        $attributes['imageCount'] = $this->imageCount;
        $attributes['flag_name'] = $this->flag_name;
        $attributes['userInfo'] = $this->userInfo;
        $attributes['payment_type'] = $this->payment_type;

        return $attributes;
    }

    public function findAll($condition = '', $params = array())
    {
        $list = parent::findAll($condition, $params);
        $criteria = new CDbCriteria;
        $criteria->select = '*';
        $criteria->condition = 'c_property_id = 173';
        $payment_types = CLookupProperty::model()->findAll($criteria);

        foreach($list as $item){
            $item->imageCount = count(RuraltechPhoto::model()->findAllByAttributes(array("announcement_id"=>$item->id)));
            if(isset($item->c_location_id) && is_numeric($item->c_location_id) ){
                $pk = (int) $item->c_location_id;
                if(CLocation::model()->exists('id = :loc_id', array(":loc_id"=>$pk ))){
                    $loc = CLocation::model()->findByPk($pk);
                    if(isset($loc)){
                        $item->flag_name = $loc->flag_name;
                    }else{
                        $item->flag_name = "";
                    }
                }
            }else{
                $item->flag_name = "";
            }
//            if($payment_types != null){
//                foreach($payment_types as $type){
//                    if($type->id == $item->payment_type_id){
//                        $item->payment_type = $type->name_arm;
//                    }
//                }
//            }
        }
        return $list;
    }

    public function findByPk($pk, $condition = '', $params = array())
    {
        $item = parent::findByPk($pk, $condition, $params);

        $criteria = new CDbCriteria;
        $criteria->select = '*';
        $criteria->condition = 'c_property_id = 173';
        $payment_types = CLookupProperty::model()->findAll($criteria);

//        if($payment_types != null){
//            foreach($payment_types as $type){
//                if($type->id == $item->payment_type_id){
//                    $item->payment_type = $type->name_arm;
//                }
//            }
//        }

        return $item;
    }




    /**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
		//	array('created', 'required'),
		//	array('term_id, technic_id, mark_id, body_id, with_driver, c_location_id, c_sub_location_id, user_id, unregistered_user_id, is_top, price, rate_id, work_type_id, isAccepted', 'numerical', 'integerOnly'=>true),
			array('description', 'length', 'max'=>1000),
			array('optional_location', 'length', 'max'=>50),
			array('properties_summary_eng, properties_summary_arm, properties_summary_rus, properties_summary_geo', 'length', 'max'=>200),
			array('main_image', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, created, description, term_id, technic_id, mark_id, body_id, with_driver, c_location_id, c_sub_location_id, optional_location, properties_summary_eng, properties_summary_arm, properties_summary_rus, properties_summary_geo, main_image, user_id, unregistered_user_id, is_top, price, rate_id, work_type_id, isAccepted, phone_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'created' => 'Created',
			'description' => 'Description',
			'term_id' => 'Term',
			'technic_id' => 'Technic',
			'mark_id' => 'Mark',
			'body_id' => 'Body',
			'with_driver' => 'With Driver',
			'c_location_id' => 'C Location',
			'c_sub_location_id' => 'C Sub Location',
			'optional_location' => 'Optional Location',
			'properties_summary_eng' => 'Properties Summary Eng',
			'properties_summary_arm' => 'Properties Summary Arm',
			'properties_summary_rus' => 'Properties Summary Rus',
			'properties_summary_geo' => 'Properties Summary Geo',
			'main_image' => 'Main Image',
			'user_id' => 'User',
			'unregistered_user_id' => 'Unregistered User',
			'is_top' => 'Is Top',
			'price' => 'Price',
			'rate_id' => 'Rate',
			'work_type_id' => 'Work Type',
            'isAccepted' => 'Is Accepted',
            'phone_id' => 'Phone',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('term_id',$this->term_id);
		$criteria->compare('technic_id',$this->technic_id);
		$criteria->compare('mark_id',$this->mark_id);
		$criteria->compare('body_id',$this->body_id);
		$criteria->compare('with_driver',$this->with_driver);
		$criteria->compare('c_location_id',$this->c_location_id);
		$criteria->compare('c_sub_location_id',$this->c_sub_location_id);
		$criteria->compare('optional_location',$this->optional_location,true);
		$criteria->compare('properties_summary_eng',$this->properties_summary_eng,true);
		$criteria->compare('properties_summary_arm',$this->properties_summary_arm,true);
		$criteria->compare('properties_summary_rus',$this->properties_summary_rus,true);
		$criteria->compare('properties_summary_geo',$this->properties_summary_geo,true);
		$criteria->compare('main_image',$this->main_image,true);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('unregistered_user_id',$this->unregistered_user_id);
		$criteria->compare('is_top',$this->is_top);
		$criteria->compare('price',$this->price);
		$criteria->compare('rate_id',$this->rate_id);
		$criteria->compare('work_type_id',$this->work_type_id);
		$criteria->compare('isAccepted',$this->isAccepted);
        $criteria->compare('phone_id',$this->phone_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}