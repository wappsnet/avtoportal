<?php

/**
 * This is the model class for table "google_map_location".
 *
 * The followings are the available columns in table 'google_map_location':
 * @property integer $id
 * @property integer $location_type
 * @property string $latitude
 * @property string $longitude
 * @property string $addressName
 * @property integer $belongs_to
 * @property string $infoWindow
 */
class GoogleMapLocation extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return GoogleMapLocation the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'google_map_location';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('location_type, belongs_to', 'numerical', 'integerOnly'=>true),
			//array('latitude, longitude', 'length', 'max'=>15),
			//array('addressName', 'length', 'max'=>50),
			//array('infoWindow', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, location_type, latitude, longitude, addressName, belongs_to, infoWindow', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'location_type' => 'Location Type',
			'latitude' => 'Latitude',
			'longitude' => 'Longitude',
			'addressName' => 'addressName',
			'belongs_to' => 'Belongs To',
			'infoWindow' => 'infoWindow',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('location_type',$this->location_type);
		$criteria->compare('latitude',$this->latitude,true);
		$criteria->compare('longitude',$this->longitude,true);
		$criteria->compare('addressName',$this->addressName,true);
		$criteria->compare('belongs_to',$this->belongs_to);
		$criteria->compare('infoWindow',$this->infoWindow,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}