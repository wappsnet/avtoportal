<?php

/**
 * This is the model class for table "transportation_announcement".
 *
 * The followings are the available columns in table 'transportation_announcement':
 * @property integer $id
 * @property string $created
 * @property string $description
 * @property integer $price
 * @property integer $rate_id
 * @property integer $term_id
 * @property string $properties_summary_eng
 * @property string $properties_summary_rus
 * @property string $properties_summary_geo
 * @property string $properties_summary_arm
 * @property string $main_image
 * @property integer $unregistered_user_id
 * @property integer $user_id
 * @property integer $is_top
 * @property integer $transport_type
 * @property integer $place
 * @property integer $vendor
 * @property integer $with_driver
 * @property integer $coutry
 * @property integer $isAccepted
 * @property string $announcement_name
 * @property string $user_phone
 * @property string $validDate
 * @property string $view_count
 * @property string $phone_id
 */
class TransportationAnnouncement extends CActiveRecord
{
    public $images = array();
    public $propertiesLeft = array();
    public $propertiesRight = array();
    public $imageCount;
    public $userInfo;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return TransportationAnnouncement the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'transportation_announcement';
	}

    public function getAttributes($names = true)
    {
        $attributes = parent::getAttributes($names);
        $attributes['images'] = $this->images;
        $attributes['propertiesLeft'] = $this->propertiesLeft;
        $attributes['propertiesRight'] = $this->propertiesRight;
        $attributes['imageCount'] = $this->imageCount;
        $attributes['userInfo'] = $this->userInfo;

        return $attributes;
    }

    public function findAll($condition = '', $params = array())
    {
        $list = parent::findAll($condition, $params);
        foreach($list as $item){
            $item->imageCount = count(TransportationPhotos::model()->findAllByAttributes(array("announcement_id"=>$item->id)));
        }
        return $list;
    }



	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
		//	array('created', 'required'),
		//	array('price, rate_id, term_id, unregistered_user_id, user_id, is_top, transport_type, place, vendor, with_driver, coutry, isAccepted', 'numerical', 'integerOnly'=>true),
			array('description, properties_summary_eng, properties_summary_rus, properties_summary_geo, properties_summary_arm', 'length', 'max'=>500),
			array('main_image', 'length', 'max'=>50),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, created, description, price, rate_id, term_id, properties_summary_eng, properties_summary_rus, properties_summary_geo, properties_summary_arm, main_image, unregistered_user_id, user_id, is_top, transport_type, place, vendor, with_driver, coutry, isAccepted, phone_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'created' => 'Created',
			'description' => 'Description',
			'price' => 'Price',
			'rate_id' => 'Rate',
			'term_id' => 'Term',
			'properties_summary_eng' => 'Properties Summary Eng',
			'properties_summary_rus' => 'Properties Summary Rus',
			'properties_summary_geo' => 'Properties Summary Geo',
			'properties_summary_arm' => 'Properties Summary Arm',
			'main_image' => 'Main Image',
			'unregistered_user_id' => 'Unregistered User',
			'user_id' => 'User',
			'is_top' => 'Is Top',
			'transport_type' => 'Transport Type',
			'place' => 'Place',
			'vendor' => 'Vendor',
			'with_driver' => 'With Driver',
			'coutry' => 'Coutry',
			'isAccepted' => 'Is Accepted',
            'phone_id' => 'Phone',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('price',$this->price);
		$criteria->compare('rate_id',$this->rate_id);
		$criteria->compare('term_id',$this->term_id);
		$criteria->compare('properties_summary_eng',$this->properties_summary_eng,true);
		$criteria->compare('properties_summary_rus',$this->properties_summary_rus,true);
		$criteria->compare('properties_summary_geo',$this->properties_summary_geo,true);
		$criteria->compare('properties_summary_arm',$this->properties_summary_arm,true);
		$criteria->compare('main_image',$this->main_image,true);
		$criteria->compare('unregistered_user_id',$this->unregistered_user_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('is_top',$this->is_top);
		$criteria->compare('transport_type',$this->transport_type);
		$criteria->compare('place',$this->place);
		$criteria->compare('vendor',$this->vendor);
		$criteria->compare('with_driver',$this->with_driver);
		$criteria->compare('coutry',$this->coutry);
		$criteria->compare('isAccepted',$this->isAccepted);
		$criteria->compare('phone_id',$this->phone_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}