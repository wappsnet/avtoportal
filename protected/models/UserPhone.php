<?php

/**
 * This is the model class for table "user_phone".
 *
 * The followings are the available columns in table 'user_phone':
 * @property integer $id
 * @property integer $c_phone_types_id
 * @property string $phone_number
 * @property string $description
 *
 * The followings are the available model relations:
 * @property User[] $users
 * @property User[] $users1
 * @property CPhoneTypes $cPhoneTypes
 */
class UserPhone extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return UserPhone the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user_phone';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('c_phone_types_id', 'required'),
			array('c_phone_types_id', 'numerical', 'integerOnly'=>true),
			array('phone_number', 'length', 'max'=>45),
			array('description', 'length', 'max'=>250),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, c_phone_types_id, phone_number, description', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'users' => array(self::HAS_MANY, 'User', 'phone1_id'),
			'users1' => array(self::HAS_MANY, 'User', 'phone2_id'),
			'cPhoneTypes' => array(self::BELONGS_TO, 'CPhoneTypes', 'c_phone_types_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'c_phone_types_id' => 'C Phone Types',
			'phone_number' => 'Phone Number',
			'description' => 'Description',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('c_phone_types_id',$this->c_phone_types_id);
		$criteria->compare('phone_number',$this->phone_number,true);
		$criteria->compare('description',$this->description,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}