<?php

/**
 * This is the model class for table "taxi_announcement".
 *
 * The followings are the available columns in table 'taxi_announcement':
 * @property integer $id
 * @property integer $user_id
 * @property string $description
 * @property string $created
 * @property integer $mark_id
 * @property integer $district_id
 * @property integer $city_block
 * @property integer $price_per_km
 * @property string $properties_summary_eng
 * @property string $properties_summary_rus
 * @property string $properties_summary_geo
 * @property string $properties_summary_arm
 * @property integer $unregistered_user_id
 * @property integer $is_top
 * @property integer $price
 * @property integer $rate_id
 * @property integer $term_id
 * @property string $main_image
 * @property integer $isAccepted
 * @property string $announcement_name
 * @property string $user_phone
 * @property string $validDate
 * @property string $view_count
 * @property string $phone_id
 */
class TaxiAnnouncement extends CActiveRecord
{
    public $images = array();
    public $propertiesLeft = array();
    public $propertiesRight = array();
    public $imageCount;
    public $userInfo;
    public $payment_type;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return TaxiAnnouncement the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'taxi_announcement';
	}



    public function getAttributes($names = true)
    {
        $attributes = parent::getAttributes($names);
        $attributes['images'] = $this->images;
        $attributes['propertiesLeft'] = $this->propertiesLeft;
        $attributes['propertiesRight'] = $this->propertiesRight;
        $attributes['imageCount'] = $this->imageCount;
        $attributes['userInfo'] = $this->userInfo;
        $attributes['payment_type'] = $this->payment_type;

        return $attributes;
    }

    public function findAll($condition = '', $params = array())
    {
        $list = parent::findAll($condition, $params);
        foreach($list as $item){
            $item->imageCount = count(TaxiPhotos::model()->findAllByAttributes(array("announcement_id"=>$item->id)));
            $item->payment_type = Messages::getMessage(100);
        }
        return $list;
    }

    public function findByPk($pk, $condition = '', $params = array())
    {
        $item = parent::findByPk($pk, $condition, $params);
        $this->payment_type = Messages::getMessage(100);

        return $item;
    }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, mark_id, district_id, city_block, price_per_km, unregistered_user_id, is_top, price, rate_id, term_id, isAccepted', 'numerical', 'integerOnly'=>true),
			array('description', 'length', 'max'=>1000),
			array('properties_summary_eng, properties_summary_rus, properties_summary_geo, properties_summary_arm', 'length', 'max'=>300),
			array('main_image', 'length', 'max'=>50),
			array('created', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, user_id, description, created, mark_id, district_id, city_block, price_per_km, properties_summary_eng, properties_summary_rus, properties_summary_geo, properties_summary_arm, unregistered_user_id, is_top, price, rate_id, term_id, main_image, isAccepted, phone_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'description' => 'Description',
			'created' => 'Created',
			'mark_id' => 'Mark',
			'district_id' => 'District',
			'city_block' => 'City Block',
			'price_per_km' => 'Price Per Km',
			'properties_summary_eng' => 'Properties Summary Eng',
			'properties_summary_rus' => 'Properties Summary Rus',
			'properties_summary_geo' => 'Properties Summary Geo',
			'properties_summary_arm' => 'Properties Summary Arm',
			'unregistered_user_id' => 'Unregistered User',
			'is_top' => 'Is Top',
			'price' => 'Price',
			'rate_id' => 'Rate',
			'term_id' => 'Term',
			'main_image' => 'Main Image',
            'isAccepted' => 'Is Accepted',
            'phone_id' => 'Phone',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('mark_id',$this->mark_id);
		$criteria->compare('district_id',$this->district_id);
		$criteria->compare('city_block',$this->city_block);
		$criteria->compare('price_per_km',$this->price_per_km);
		$criteria->compare('properties_summary_eng',$this->properties_summary_eng,true);
		$criteria->compare('properties_summary_rus',$this->properties_summary_rus,true);
		$criteria->compare('properties_summary_geo',$this->properties_summary_geo,true);
		$criteria->compare('properties_summary_arm',$this->properties_summary_arm,true);
		$criteria->compare('unregistered_user_id',$this->unregistered_user_id);
		$criteria->compare('is_top',$this->is_top);
		$criteria->compare('price',$this->price);
		$criteria->compare('rate_id',$this->rate_id);
		$criteria->compare('term_id',$this->term_id);
		$criteria->compare('main_image',$this->main_image,true);
		$criteria->compare('isAccepted',$this->isAccepted);
        $criteria->compare('phone_id',$this->phone_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}