<?php

/**
 * This is the model class for table "c_sub_location".
 *
 * The followings are the available columns in table 'c_sub_location':
 * @property integer $id
 * @property integer $c_location_id
 * @property string $name_eng
 * @property string $name_rus
 * @property string $name_geo
 * @property string $name_arm
 *
 * The followings are the available model relations:
 * @property CLocation $cLocation
 * @property CarAnnouncement[] $carAnnouncements
 * @property EvacuatorAnnouncement[] $evacuatorAnnouncements
 * @property ShippingAnnouncement[] $shippingAnnouncements
 * @property ShippingAnnouncement[] $shippingAnnouncements1
 * @property SparePartAnnouncement[] $sparePartAnnouncements
 * @property TaxiAnnouncement[] $taxiAnnouncements
 */
class CSubLocation extends CActiveRecord
{
    public $name;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CSubLocation the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'c_sub_location';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('c_location_id', 'required'),
			array('c_location_id', 'numerical', 'integerOnly'=>true),
			array('name_eng, name_rus, name_geo, name_arm', 'length', 'max'=>45),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, c_location_id, name_eng, name_rus, name_geo, name_arm', 'safe', 'on'=>'search'),
		);
	}

    public function getAttributes($names = true)
    {
        $attributes = parent::getAttributes($names);
        $attributes['name'] = $this->name;
        return $attributes;
    }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'cLocation' => array(self::BELONGS_TO, 'CLocation', 'c_location_id'),
			'carAnnouncements' => array(self::HAS_MANY, 'CarAnnouncement', 'c_sub_location_id'),
			'evacuatorAnnouncements' => array(self::HAS_MANY, 'EvacuatorAnnouncement', 'c_sub_location_id'),
			'shippingAnnouncements' => array(self::HAS_MANY, 'ShippingAnnouncement', 'c_sub_location_id'),
			'shippingAnnouncements1' => array(self::HAS_MANY, 'ShippingAnnouncement', 'c_sub_location_c_location_id'),
			'sparePartAnnouncements' => array(self::HAS_MANY, 'SparePartAnnouncement', 'c_sub_location_id'),
			'taxiAnnouncements' => array(self::HAS_MANY, 'TaxiAnnouncement', 'c_sub_location_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'c_location_id' => 'C Location',
			'name_eng' => 'Name Eng',
			'name_rus' => 'Name Rus',
			'name_geo' => 'Name Geo',
			'name_arm' => 'Name Arm',
		);
	}

    public function findAll($condition = '', $params = array())
    {
        $list = parent::findAll($condition, $params);

        foreach ($list as $item) {
            if(Messages::getLanguageId() == 'arm'){
                $item->name =  $item->name_arm;
            }elseif(Messages::getLanguageId() == 'eng'){
                $item->name =  $item->name_eng;
            }elseif(Messages::getLanguageId() == 'rus'){
                $item->name =  $item->name_rus;
            }elseif(Messages::getLanguageId() == 'geo'){
                $item->name =  $item->name_geo;
            }
        }
        return $list;
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('c_location_id',$this->c_location_id);
		$criteria->compare('name_eng',$this->name_eng,true);
		$criteria->compare('name_rus',$this->name_rus,true);
		$criteria->compare('name_geo',$this->name_geo,true);
		$criteria->compare('name_arm',$this->name_arm,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}