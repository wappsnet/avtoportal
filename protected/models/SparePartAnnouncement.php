<?php

/**
 * This is the model class for table "spare_part_announcement".
 *
 * The followings are the available columns in table 'spare_part_announcement':
 * @property integer $id
 * @property string $created
 * @property string $type_id
 * @property string $subtype_id
 * @property string $body_style_id
 * @property string $mark_id
 * @property string $model_id
 * @property string $price
 * @property string $description
 * @property integer $c_location_id
 * @property integer $c_sub_location_id
 * @property string $optional_location
 * @property string $properties_summary_eng
 * @property string $properties_summary_arm
 * @property string $properties_summary_rus
 * @property string $properties_summary_geo
 * @property string $main_image
 * @property integer $term_id
 * @property integer $unregistered_user_id
 * @property integer $is_top
 * @property integer $user_id
 * @property integer $rate_id
 * @property integer $isAccepted
 * @property string $announcement_name
 * @property string $user_phone
 * @property string $validDate
 * @property string $view_count
 * @property string $phone_id
 */
class SparePartAnnouncement extends CActiveRecord
{
    public $images = array();
    public $propertiesLeft = array();
    public $propertiesRight = array();
    public $userPhones;
    public $userInfo;
    public $imageCount;
    public $flag_name;
    public $state;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return SparePartAnnouncement the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'spare_part_announcement';
	}

    public function getAttributes($names = true)
    {
        $attributes = parent::getAttributes($names);
        $attributes['images'] = $this->images;
        $attributes['propertiesLeft'] = $this->propertiesLeft;
        $attributes['propertiesRight'] = $this->propertiesRight;
        $attributes['userPhones'] = $this->userPhones;
        $attributes['userInfo'] = $this->userInfo;
        $attributes['imageCount'] = $this->imageCount;
        $attributes['flag_name'] = $this->flag_name;
        $attributes['state'] = $this->state;

        return $attributes;
    }

    private function getState($prop_id , $ann_id){
        $criteria = new CDbCriteria();
        $criteria->condition = 'announcement_id = :ann_id and c_property_id = :prop_id';

        $criteria->params = array(':ann_id'=>$ann_id , ':prop_id'=>$prop_id);

        $result = SparePartProperty::model()->findAll($criteria);

        if(isset($result) && count($result) > 0 && $result[0]->text_arm != "null"){
            return $result[0]->text_arm;
        }

        return "";

    }

    public function findAll($condition = '', $params = array())
    {
        $list = parent::findAll($condition, $params);
        foreach($list as $item){
            $item->imageCount = count(SparePartPhotos::model()->findAllByAttributes(array("announcement_id"=>$item->id)));
            $item->state = $this->getState(198,$item->id);

            if(isset($item->c_location_id) && is_numeric($item->c_location_id) ){
                $pk = (int) $item->c_location_id;
                if(CLocation::model()->exists('id = :loc_id', array(":loc_id"=>$pk ))){
                    $loc = CLocation::model()->findByPk($pk);
                    if(isset($loc)){
                        $item->flag_name = $loc->flag_name;
                    }else{
                        $item->flag_name = "";
                    }
                }
            }else{
                $item->flag_name = "";
            }

        }
        return $list;
    }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('c_location_id, c_sub_location_id, term_id, unregistered_user_id, is_top, user_id, rate_id, isAccepted', 'numerical', 'integerOnly'=>true),
			array('type_id, subtype_id, body_style_id, mark_id, model_id, price', 'length', 'max'=>20),
			array('description, properties_summary_eng, properties_summary_arm, properties_summary_rus, properties_summary_geo', 'length', 'max'=>1000),
			array('optional_location, main_image', 'length', 'max'=>50),
			array('created', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, created, type_id, subtype_id, body_style_id, mark_id, model_id, price, description, c_location_id, c_sub_location_id, optional_location, properties_summary_eng, properties_summary_arm, properties_summary_rus, properties_summary_geo, main_image, term_id, unregistered_user_id, is_top, user_id, rate_id, isAccepted, phone_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'created' => 'Created',
			'type_id' => 'Type',
			'subtype_id' => 'Subtype',
			'body_style_id' => 'Body Style',
			'mark_id' => 'Mark',
			'model_id' => 'Model',
			'price' => 'Price',
			'description' => 'Description',
			'c_location_id' => 'C Location',
			'c_sub_location_id' => 'C Sub Location',
			'optional_location' => 'Optional Location',
			'properties_summary_eng' => 'Properties Summary Eng',
			'properties_summary_arm' => 'Properties Summary Arm',
			'properties_summary_rus' => 'Properties Summary Rus',
			'properties_summary_geo' => 'Properties Summary Geo',
			'main_image' => 'Main Image',
			'term_id' => 'Term',
			'unregistered_user_id' => 'Unregistered User',
			'is_top' => 'Is Top',
			'user_id' => 'User',
			'rate_id' => 'Rate',
			'isAccepted' => 'Is Accepted',
            'phone_id' => 'Phone',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('type_id',$this->type_id,true);
		$criteria->compare('subtype_id',$this->subtype_id,true);
		$criteria->compare('body_style_id',$this->body_style_id,true);
		$criteria->compare('mark_id',$this->mark_id,true);
		$criteria->compare('model_id',$this->model_id,true);
		$criteria->compare('price',$this->price,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('c_location_id',$this->c_location_id);
		$criteria->compare('c_sub_location_id',$this->c_sub_location_id);
		$criteria->compare('optional_location',$this->optional_location,true);
		$criteria->compare('properties_summary_eng',$this->properties_summary_eng,true);
		$criteria->compare('properties_summary_arm',$this->properties_summary_arm,true);
		$criteria->compare('properties_summary_rus',$this->properties_summary_rus,true);
		$criteria->compare('properties_summary_geo',$this->properties_summary_geo,true);
		$criteria->compare('main_image',$this->main_image,true);
		$criteria->compare('term_id',$this->term_id);
		$criteria->compare('unregistered_user_id',$this->unregistered_user_id);
		$criteria->compare('is_top',$this->is_top);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('rate_id',$this->rate_id);
		$criteria->compare('isAccepted',$this->isAccepted);
        $criteria->compare('phone_id',$this->phone_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}