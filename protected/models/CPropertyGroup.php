<?php

/**
 * This is the model class for table "c_property_group".
 *
 * The followings are the available columns in table 'c_property_group':
 * @property integer $id
 * @property string $name_eng
 * @property string $name_rus
 * @property string $name_geo
 * @property string $name_arm
 * @property array $properties
 *
 * The followings are the available model relations:
 * @property CProperty[] $cProperties
 */
class CPropertyGroup extends CActiveRecord
{

    public $properties = array();
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CPropertyGroup the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'c_property_group';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name_eng, name_rus, name_geo, name_arm', 'length', 'max'=>45),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name_eng, name_rus, name_geo, name_arm', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'cProperties' => array(self::HAS_MANY, 'CProperty', 'c_property_group_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name_eng' => 'Name Eng',
			'name_rus' => 'Name Rus',
			'name_geo' => 'Name Geo',
			'name_arm' => 'Name Arm',
		);
	}

    public function findAll($condition = '', $params = array())
    {
        $list = parent::findAll($condition, $params);
        $criteria = new CDbCriteria();
        $criteria->condition = 'group_id = :prp_id';
        foreach ($list as $item) {
            $criteria->params = array(':prp_id'=>$item->id);
            $item->properties = CarPropertiesView::model()->findAll($criteria);
        }
        return $list;
    }

    public function findByPk($pk, $condition = '', $params = array())
    {
        $item = parent::findByPk($pk, $condition, $params);
        $criteria = new CDbCriteria();
        $criteria->condition = 'group_id = :prp_id';
        $criteria->params = array(':prp_id'=>$item->id);
        $item->properties = CarPropertiesView::model()->findAll($criteria);
        return $item;
    }

    public function getAttributes($names = true)
    {
        $attributes = parent::getAttributes($names);
        $attributes['properties'] = $this->properties;
        return $attributes;
    }


    /**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name_eng',$this->name_eng,true);
		$criteria->compare('name_rus',$this->name_rus,true);
		$criteria->compare('name_geo',$this->name_geo,true);
		$criteria->compare('name_arm',$this->name_arm,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}