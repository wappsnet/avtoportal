<?php

/**
 * This is the model class for table "c_property".
 *
 * The followings are the available columns in table 'c_property':
 * @property integer $id
 * @property integer $c_property_type_id
 * @property integer $c_property_group_id
 * @property string $name_eng
 * @property string $name_rus
 * @property string $name_geo
 * @property string $name_arm
 * @property string $description_eng
 * @property string $description_rus
 * @property string $description_geo
 * @property string $description_arm
 * @property integer $validation
 * @property integer $show_info
 * @property string $label_style
 * @property string $controller_style
 * @property integer $property_resolver
 * @property string $child_id
 * @property integer $order_in_group
 * @property string $style
 */
class CProperty extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CProperty the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'c_property';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('c_property_type_id, c_property_group_id', 'required'),
			array('c_property_type_id, c_property_group_id, validation, show_info, property_resolver, child_id, order_in_group', 'numerical', 'integerOnly'=>true),
			array('name_eng, name_rus, name_geo, name_arm, label_style, controller_style', 'length', 'max'=>45),
			array('description_eng, description_rus, description_geo, description_arm', 'length', 'max'=>250),
			array('style', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, c_property_type_id, c_property_group_id, name_eng, name_rus, name_geo, name_arm, description_eng, description_rus, description_geo, description_arm, validation, show_info, label_style, controller_style, property_resolver, child_id, order_in_group, style', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'c_property_type_id' => 'C Property Type',
			'c_property_group_id' => 'C Property Group',
			'name_eng' => 'Name Eng',
			'name_rus' => 'Name Rus',
			'name_geo' => 'Name Geo',
			'name_arm' => 'Name Arm',
			'description_eng' => 'Description Eng',
			'description_rus' => 'Description Rus',
			'description_geo' => 'Description Geo',
			'description_arm' => 'Description Arm',
			'validation' => 'Validation',
			'show_info' => 'Show Info',
			'label_style' => 'Label Style',
			'controller_style' => 'Controller Style',
			'property_resolver' => 'Property Resolver',
			'child_id' => 'Child',
			'order_in_group' => 'Order In Group',
			'style' => 'Style',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('c_property_type_id',$this->c_property_type_id);
		$criteria->compare('c_property_group_id',$this->c_property_group_id);
		$criteria->compare('name_eng',$this->name_eng,true);
		$criteria->compare('name_rus',$this->name_rus,true);
		$criteria->compare('name_geo',$this->name_geo,true);
		$criteria->compare('name_arm',$this->name_arm,true);
		$criteria->compare('description_eng',$this->description_eng,true);
		$criteria->compare('description_rus',$this->description_rus,true);
		$criteria->compare('description_geo',$this->description_geo,true);
		$criteria->compare('description_arm',$this->description_arm,true);
		$criteria->compare('validation',$this->validation);
		$criteria->compare('show_info',$this->show_info);
		$criteria->compare('label_style',$this->label_style,true);
		$criteria->compare('controller_style',$this->controller_style,true);
		$criteria->compare('property_resolver',$this->property_resolver);
		$criteria->compare('child_id',$this->child_id);
		$criteria->compare('order_in_group',$this->order_in_group);
		$criteria->compare('style',$this->style,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}