<?php

/**
 * This is the model class for table "banks_auto_loan".
 *
 * The followings are the available columns in table 'banks_auto_loan':
 * @property integer $id
 * @property integer $rate
 * @property integer $downpayment
 * @property string $name_arm
 * @property string $name_eng
 * @property string $name_rus
 * @property string $name_geo
 */
class BanksAutoLoan extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return BanksAutoLoan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'banks_auto_loan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('rate, downpayment', 'numerical', 'integerOnly'=>true),
			array('name_arm, name_eng, name_rus, name_geo', 'length', 'max'=>50),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, rate, downpayment, name_arm, name_eng, name_rus, name_geo', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'rate' => 'Rate',
			'downpayment' => 'Downpayment',
			'name_arm' => 'Name Arm',
			'name_eng' => 'Name Eng',
			'name_rus' => 'Name Rus',
			'name_geo' => 'Name Geo',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('rate',$this->rate);
		$criteria->compare('downpayment',$this->downpayment);
		$criteria->compare('name_arm',$this->name_arm,true);
		$criteria->compare('name_eng',$this->name_eng,true);
		$criteria->compare('name_rus',$this->name_rus,true);
		$criteria->compare('name_geo',$this->name_geo,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}