<?php

/**
 * This is the model class for table "car_properties_view".
 *
 * The followings are the available columns in table 'car_properties_view':
 * @property integer $id
 * @property integer $c_property_type_id
 * @property string $name_eng
 * @property string $name_rus
 * @property string $name_geo
 * @property string $name_arm
 * @property string $description_eng
 * @property string $description_rus
 * @property string $description_geo
 * @property string $description_arm
 * @property integer $validation
 * @property integer $show_info
 * @property integer $group_id
 * @property integer $child_id
 * @property string $label_style
 * @property string $controller_style
 * @property string $group_name_arm
 * @property string $group_name_eng
 * @property string $group_name_geo
 * @property string $group_name_rus
 */
class CarPropertiesView extends CActiveRecord
{
    public $temp_value;
    public $lookup_items = array();
    public $name;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CarPropertiesView the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'car_properties_view';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('c_property_type_id', 'required'),
			array('id, c_property_type_id, validation, show_info, group_id, child_id', 'numerical', 'integerOnly'=>true),
			array('name_eng, name_rus, name_geo, name_arm, label_style, controller_style, group_name_arm, group_name_eng, group_name_geo, group_name_rus', 'length', 'max'=>45),
			array('description_eng, description_rus, description_geo, description_arm', 'length', 'max'=>250),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, c_property_type_id, name_eng, name_rus, name_geo, name_arm, description_eng, description_rus, description_geo, description_arm, validation, show_info, group_id, child_id, label_style, controller_style, group_name_arm, group_name_eng, group_name_geo, group_name_rus', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

    public function findAll($condition = '', $params = array())
    {
        $children = array();
        $list = parent::findAll($condition, $params);
        $criteria = new CDbCriteria();
        $criteria->condition = 'c_property_id = :prp_id';
        $criteria->order = 'name_eng ASC';

        foreach ($list as $item) {
            $criteria->params = array(':prp_id'=>$item->id);
            if($item->id == 212){
                $criteria->order = 'order_id ASC';
            }
            if($item->id == 5 || $item->id == 200){
                $criteria->order = 'name_eng DESC';
            }

            $item->lookup_items = CLookupProperty::model()->findAll($criteria);
            $item->temp_value = null;
            if(Messages::getLanguageId() == 'arm'){
                $item->name =  $item->name_arm;
            }elseif(Messages::getLanguageId() == 'eng'){
                $item->name =  $item->name_eng;
            }elseif(Messages::getLanguageId() == 'rus'){
                $item->name =  $item->name_rus;
            }elseif(Messages::getLanguageId() == 'geo'){
                $item->name =  $item->name_geo;
            }
            if($item->child_id != null){
                $children[] = $item->child_id;
            }
        }

        foreach($children as $childID){ //we must  give empty  lookup to combo's that have a father
            foreach ($list as $item) {
                if(strstr($childID , $item->id)){
                    $item->lookup_items = null;

                }
            }
        }

        return $list;
    }

    /*  public function findByPk($pk,$condition = '', $params = array()){
          $item = parent::findByPk($pk,$condition, $params);

          $criteria = new CDbCriteria();
          $criteria->condition = 'c_property_id = :prp_id';
          $criteria->params = array(':prp_id'=>$item->id);
          $item->lookup_items = CLookupProperty::model()->findAll($criteria);
          $item->temp_value = null;

          return $item;
      }*/

    public function getAttributes($names = true)
    {
        $attributes = parent::getAttributes($names);
        $attributes['lookup_items'] = $this->lookup_items;
        $attributes['temp_value'] = $this->temp_value;
        $attributes['name'] = $this->name;
        return $attributes;
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'c_property_type_id' => 'C Property Type',
			'name_eng' => 'Name Eng',
			'name_rus' => 'Name Rus',
			'name_geo' => 'Name Geo',
			'name_arm' => 'Name Arm',
			'description_eng' => 'Description Eng',
			'description_rus' => 'Description Rus',
			'description_geo' => 'Description Geo',
			'description_arm' => 'Description Arm',
			'validation' => 'Validation',
			'show_info' => 'Show Info',
			'group_id' => 'Group',
			'child_id' => 'Child',
			'label_style' => 'Label Style',
			'controller_style' => 'Controller Style',
			'group_name_arm' => 'Group Name Arm',
			'group_name_eng' => 'Group Name Eng',
			'group_name_geo' => 'Group Name Geo',
			'group_name_rus' => 'Group Name Rus',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('c_property_type_id',$this->c_property_type_id);
		$criteria->compare('name_eng',$this->name_eng,true);
		$criteria->compare('name_rus',$this->name_rus,true);
		$criteria->compare('name_geo',$this->name_geo,true);
		$criteria->compare('name_arm',$this->name_arm,true);
		$criteria->compare('description_eng',$this->description_eng,true);
		$criteria->compare('description_rus',$this->description_rus,true);
		$criteria->compare('description_geo',$this->description_geo,true);
		$criteria->compare('description_arm',$this->description_arm,true);
		$criteria->compare('validation',$this->validation);
		$criteria->compare('show_info',$this->show_info);
		$criteria->compare('group_id',$this->group_id);
		$criteria->compare('child_id',$this->child_id);
		$criteria->compare('label_style',$this->label_style,true);
		$criteria->compare('controller_style',$this->controller_style,true);
		$criteria->compare('group_name_arm',$this->group_name_arm,true);
		$criteria->compare('group_name_eng',$this->group_name_eng,true);
		$criteria->compare('group_name_geo',$this->group_name_geo,true);
		$criteria->compare('group_name_rus',$this->group_name_rus,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}