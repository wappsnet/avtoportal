<?php

/**
 * This is the model class for table "spec_car_rent_announcement".
 *
 * The followings are the available columns in table 'spec_car_rent_announcement':
 * @property integer $id
 * @property integer $spec_car_rent_id
 * @property integer $announcement_id
 *
 * The followings are the available model relations:
 * @property Announcement $announcement
 * @property SpecCarRent $specCarRent
 */
class SpecCarRentAnnouncement extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return SpecCarRentAnnouncement the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'spec_car_rent_announcement';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('spec_car_rent_id, announcement_id', 'required'),
			array('spec_car_rent_id, announcement_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, spec_car_rent_id, announcement_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'announcement' => array(self::BELONGS_TO, 'Announcement', 'announcement_id'),
			'specCarRent' => array(self::BELONGS_TO, 'SpecCarRent', 'spec_car_rent_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'spec_car_rent_id' => 'Spec Car Rent',
			'announcement_id' => 'Announcement',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('spec_car_rent_id',$this->spec_car_rent_id);
		$criteria->compare('announcement_id',$this->announcement_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}