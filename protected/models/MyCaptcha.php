<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Edgar
 * Date: 8/28/13
 * Time: 5:54 PM
 * To change this template use File | Settings | File Templates.
 */

class MyCaptcha extends CFormModel{
    public $verifyCode;

    function rules(){
        return array(
            array(
                'verifyCode',
                'captcha',
                // авторизованным пользователям код можно не вводить
             //   'allowEmpty'=>!Yii::app()->user->isGuest || !CCaptcha::checkRequirements(),
            ),
        );
    }

    function attributeLabels(){
        return array(
            'verifyCode' => 'Код проверки',
        );
    }

}