<?php

/**
 * This is the model class for table "user_phone_type".
 *
 * The followings are the available columns in table 'user_phone_type':
 * @property integer $id
 * @property string $name_arm
 * @property string $name_eng
 * @property string $name_geo
 * @property string $name_rus
 */
class UserPhoneType extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return UserPhoneType the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user_phone_type';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name_arm, name_eng, name_geo, name_rus', 'length', 'max'=>15),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name_arm, name_eng, name_geo, name_rus', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name_arm' => 'Name Arm',
			'name_eng' => 'Name Eng',
			'name_geo' => 'Name Geo',
			'name_rus' => 'Name Rus',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name_arm',$this->name_arm,true);
		$criteria->compare('name_eng',$this->name_eng,true);
		$criteria->compare('name_geo',$this->name_geo,true);
		$criteria->compare('name_rus',$this->name_rus,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}