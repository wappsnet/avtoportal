<?php

/**
 * This is the model class for table "car_rent_announcement".
 *
 * The followings are the available columns in table 'car_rent_announcement':
 * @property integer $id
 * @property string $description
 * @property integer $user_id
 * @property string $created
 * @property integer $price
 * @property integer $mark_id
 * @property integer $model_id
 * @property string $user_phone
 * @property string $phone_id
 * @property integer $rate_id
 * @property integer $year
 * @property integer $color_id
 * @property integer $gearbox_id
 * @property integer $engine_id
 * @property integer $mileage
 * @property integer $engine_volume
 * @property string $engine_power
 * @property string $main_image
 * @property integer $c_location_id
 * @property integer $c_sub_location_id
 * @property string $announcement_name
 * @property integer $body_style_id
 * @property string $optional_location
 * @property string $properties_summary_eng
 * @property string $properties_summary_arm
 * @property string $properties_summary_rus
 * @property string $properties_summary_geo
 * @property integer $term_id
 * @property integer $unregistered_user_id
 * @property integer $is_top
 * @property integer $isAccepted
 * @property string $validDate
 * @property string $view_count
 * @property integer $payment_type_id
 *
 */
class CarRentAnnouncement extends CActiveRecord
{
    public $images = array();
    public $propertiesLeft = array();
    public $propertiesRight = array();
    public $userPhones;
    public $imageCount;
    public $flag_name;
    public $userInfo;
    public $payment_type;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CarRentAnnouncement the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'car_rent_announcement';
	}


    public function getAttributes($names = true)
    {
        $attributes = parent::getAttributes($names);
        $attributes['images'] = $this->images;
        $attributes['propertiesLeft'] = $this->propertiesLeft;
        $attributes['propertiesRight'] = $this->propertiesRight;
        $attributes['userPhones'] = $this->userPhones;
        $attributes['imageCount'] = $this->imageCount;
        $attributes['flag_name'] = $this->flag_name;
        $attributes['userInfo'] = $this->userInfo;
        $attributes['payment_type'] = $this->payment_type;

        return $attributes;
    }



    public function findAll($condition = '', $params = array())
    {
        $list = parent::findAll($condition, $params);
        $criteria = new CDbCriteria;
        $criteria->select = '*';
        $criteria->condition = 'c_property_id = 242';
        $payment_types = CLookupProperty::model()->findAll($criteria);


        foreach($list as $item){
            $item->imageCount = count(CarRentPhotos::model()->findAllByAttributes(array("announcement_id"=>$item->id)));
            if(isset($item->c_location_id) && is_numeric($item->c_location_id) ){
                $pk = (int) $item->c_location_id;
                if(CLocation::model()->exists('id = :loc_id', array(":loc_id"=>$pk ))){
                    $loc = CLocation::model()->findByPk($pk);
                    if(isset($loc)){
                        $item->flag_name = $loc->flag_name;
                    }else{
                        $item->flag_name = "";
                    }
                }
            }else{
                $item->flag_name = "";
            }
            if($payment_types != null){
                foreach($payment_types as $type){
                    if($type->id == $item->payment_type_id){
                        $item->payment_type = $type->name_arm;
                    }
                }
            }

        }
        return $list;
    }

    public function findByPk($pk, $condition = '', $params = array())
    {
        $item = parent::findByPk($pk, $condition, $params);

        $criteria = new CDbCriteria;
        $criteria->select = '*';
        $criteria->condition = 'c_property_id = 242';
        $payment_types = CLookupProperty::model()->findAll($criteria);

        if($payment_types != null){
            foreach($payment_types as $type){
                if($type->id == $item->payment_type_id){
                    $item->payment_type = $type->name_arm;
                }
            }
        }

        return $item;
    }



    /**
     * @return array relational rules.
     */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('user_id, price, mark_id, model_id, user_phone, rate_id, year, color_id, gearbox_id, engine_id, mileage, engine_volume, engine_power, c_location_id, c_sub_location_id, body_style_id, term_id, unregistered_user_id, is_top, isAccepted', 'numerical', 'integerOnly'=>true),
			array('description, properties_summary_eng, properties_summary_arm, properties_summary_rus, properties_summary_geo', 'length', 'max'=>1000),
			array('main_image', 'length', 'max'=>100),
			array('announcement_name, optional_location', 'length', 'max'=>50),
			array('created', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, description, user_id, created, price, mark_id, model_id, user_phone, rate_id, year, color_id, gearbox_id, engine_id, mileage, engine_volume, engine_power, main_image, c_location_id, c_sub_location_id, announcement_name, body_style_id, optional_location, properties_summary_eng, properties_summary_arm, properties_summary_rus, properties_summary_geo, term_id, unregistered_user_id, is_top, isAccepted, phone_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'description' => 'Description',
			'user_id' => 'User',
			'created' => 'Created',
			'price' => 'Price',
			'mark_id' => 'Mark',
			'model_id' => 'Model',
			'user_phone' => 'User Phone',
			'rate_id' => 'Rate',
			'year' => 'Year',
			'color_id' => 'Color',
			'gearbox_id' => 'Gearbox',
			'engine_id' => 'Engine',
			'mileage' => 'Mileage',
			'engine_volume' => 'Engine Volume',
			'engine_power' => 'Tax',
			'main_image' => 'Main Image',
			'c_location_id' => 'C Location',
			'c_sub_location_id' => 'C Sub Location',
			'announcement_name' => 'Car Name',
			'body_style_id' => 'Body Style',
			'optional_location' => 'Optional Location',
			'properties_summary_eng' => 'Properties Summary Eng',
			'properties_summary_arm' => 'Properties Summary Arm',
			'properties_summary_rus' => 'Properties Summary Rus',
			'properties_summary_geo' => 'Properties Summary Geo',
			'term_id' => 'Term',
			'unregistered_user_id' => 'Unregistered User',
			'is_top' => 'Is Top',
			'isAccepted' => 'Is Accepted',
            'phone_id' => 'Phone ID',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('price',$this->price);
		$criteria->compare('mark_id',$this->mark_id);
		$criteria->compare('model_id',$this->model_id);
		$criteria->compare('user_phone',$this->user_phone);
		$criteria->compare('rate_id',$this->rate_id);
		$criteria->compare('year',$this->year);
		$criteria->compare('color_id',$this->color_id);
		$criteria->compare('gearbox_id',$this->gearbox_id);
		$criteria->compare('engine_id',$this->engine_id);
		$criteria->compare('mileage',$this->mileage);
		$criteria->compare('engine_volume',$this->engine_volume);
        $criteria->compare('engine_power',$this->engine_power,true);
		$criteria->compare('main_image',$this->main_image,true);
		$criteria->compare('c_location_id',$this->c_location_id);
		$criteria->compare('c_sub_location_id',$this->c_sub_location_id);
		$criteria->compare('announcement_name',$this->announcement_name,true);
		$criteria->compare('body_style_id',$this->body_style_id);
		$criteria->compare('optional_location',$this->optional_location,true);
		$criteria->compare('properties_summary_eng',$this->properties_summary_eng,true);
		$criteria->compare('properties_summary_arm',$this->properties_summary_arm,true);
		$criteria->compare('properties_summary_rus',$this->properties_summary_rus,true);
		$criteria->compare('properties_summary_geo',$this->properties_summary_geo,true);
		$criteria->compare('term_id',$this->term_id);
		$criteria->compare('unregistered_user_id',$this->unregistered_user_id);
		$criteria->compare('is_top',$this->is_top);
		$criteria->compare('isAccepted',$this->isAccepted);
        $criteria->compare('phone_id',$this->phone_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}