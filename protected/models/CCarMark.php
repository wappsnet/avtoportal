<?php

/**
 * This is the model class for table "c_car_mark".
 *
 * The followings are the available columns in table 'c_car_mark':
 * @property integer $id
 * @property string $name_eng
 * @property string $name_rus
 * @property string $name_geo
 * @property string $name_arm
 * @property string $description_eng
 * @property string $description_rus
 * @property string $description_geo
 * @property string $description_arm
 * @property integer $order_id
 *
 * The followings are the available model relations:
 * @property CCarModel[] $cCarModels
 */
class CCarMark extends CActiveRecord
{

    public $models = array();
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CCarMark the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'c_car_mark';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('order_id', 'numerical', 'integerOnly'=>true),
			array('name_eng, name_rus, name_geo, name_arm', 'length', 'max'=>45),
			array('description_eng, description_rus, description_geo, description_arm', 'length', 'max'=>250),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name_eng, name_rus, name_geo, name_arm, description_eng, description_rus, description_geo, description_arm, order_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'cCarModels' => array(self::HAS_MANY, 'CCarModel', 'c_car_mark_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name_eng' => 'Name Eng',
			'name_rus' => 'Name Rus',
			'name_geo' => 'Name Geo',
			'name_arm' => 'Name Arm',
			'description_eng' => 'Description Eng',
			'description_rus' => 'Description Rus',
			'description_geo' => 'Description Geo',
			'description_arm' => 'Description Arm',
			'order_id' => 'Order',
		);
	}

    public function findAll($condition = '', $params = array())
    {
        $list = parent::findAll($condition, $params);
        $criteria = new CDbCriteria();
        $criteria->condition = 'c_car_mark_id = :prp_id';
        foreach ($list as $item) {
            $criteria->params = array(':prp_id'=>$item->id);
            $item->models = CCarModel::model()->findAll($criteria);
        }
        return $list;
    }

    public function findByPk($pk, $condition = '', $params = array())
    {
        $item = parent::findByPk($pk, $condition, $params);
        $criteria = new CDbCriteria();
        $criteria->condition = 'c_car_mark_id = :prp_id';
        $criteria->params = array(':prp_id'=>$item->id);
        $item->models = CCarModel::model()->findAll($criteria);
    }

    public function getAttributes($names = true)
    {
        $attributes = parent::getAttributes($names);
        $attributes['models'] = $this->models;
        return $attributes;
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name_eng',$this->name_eng,true);
		$criteria->compare('name_rus',$this->name_rus,true);
		$criteria->compare('name_geo',$this->name_geo,true);
		$criteria->compare('name_arm',$this->name_arm,true);
		$criteria->compare('description_eng',$this->description_eng,true);
		$criteria->compare('description_rus',$this->description_rus,true);
		$criteria->compare('description_geo',$this->description_geo,true);
		$criteria->compare('description_arm',$this->description_arm,true);
		$criteria->compare('order_id',$this->order_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

}