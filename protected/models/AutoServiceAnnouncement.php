<?php

/**
 * This is the model class for table "auto_service_announcement".
 *
 * The followings are the available columns in table 'auto_service_announcement':
 * @property integer $id
 * @property string $description
 * @property string $created
 * @property string $price
 * @property integer $rate_id
 * @property string $name
 * @property string $time_to
 * @property string $time_from
 * @property string $phone_1
 * @property string $phone_2
 * @property string $email
 * @property integer $term_id
 * @property integer $c_location_id
 * @property integer $c_sub_location_id
 * @property string $optional_location
 * @property string $main_image
 * @property string $longitude
 * @property string $latitude
 * @property string $web_site
 * @property integer $service_id
 * @property integer $isAccepted
 * @property integer $is_top
 */
class AutoServiceAnnouncement extends CActiveRecord
{
    public $images = array();
    public $services = array();

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AutoServiceAnnouncement the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'auto_service_announcement';
	}

    public function getAttributes($names = true)
    {
        $attributes = parent::getAttributes($names);
        $attributes['images'] = $this->images;
        $attributes['services'] = $this->services;

        return $attributes;
    }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('created', 'required'),
			//array('user_id, rate_id, term_id, c_location_id, c_sub_location_id, service_id, isAccepted, unregistered_user_id', 'numerical', 'integerOnly'=>true),
			array('description', 'length', 'max'=>1000),
			array('price, time_to, time_from', 'length', 'max'=>10),
			array('name, phone_1, phone_2', 'length', 'max'=>20),
			array('email, optional_location, main_image, web_site', 'length', 'max'=>50),
			array('longitude, latitude', 'length', 'max'=>30),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, description, created, price, rate_id, name, time_to, time_from, phone_1, phone_2, email, term_id, c_location_id, c_sub_location_id, optional_location, main_image, longitude, latitude, web_site, service_id, isAccepted', 'safe', 'on'=>'search'),
		);
	}

    public function findByPk($pk, $condition = '', $params = array())
    {
        $item = parent::findByPk($pk, $condition, $params);


        $criteria = new CDbCriteria();
        $criteria->condition = 'announcement_id = :ann_id';
        $criteria->params = array(':ann_id'=>$item->id);
        $item->images = AutoServicePhotos::model()->findAll($criteria);



        $criteria = new CDbCriteria();
        $criteria->condition = 'announcement_id = :ann_id';
        $criteria->params = array(':ann_id'=>$item->id);
        $item->services = SelectedServices::model()->findAll($criteria);


//        if($item->user_id == -55 || Controller::isAdminUser($item->user_id)){
//            $user = UnregisteredUsers::model()->findByPk($item->unregistered_user_id);
//            $phone1 = $user->phone_1;
//            $phone2 = $user->phone_2;
//        }else{
//            $user = User::model()->findByPk($item->user_id);
//            $phone1_id = $user->phone1_id;
//            $phone2_id = $user->phone2_id;
//            $phone1 = UserPhone::model()->findBySql("Select * From user_phone WHERE id = ".$phone1_id)->phone_number;
//            $phone2 = UserPhone::model()->findBySql("Select * From user_phone WHERE id = ".$phone2_id)->phone_number;
//        }
//
//        $label = $this->getLabel();
//        $location = CLocation::model()->findByPk($item->c_location_id);
//        $item->userInfo = array("phone1" => $phone1, "phone2"=>$phone2, "location"=>$location->$label);

        return $item;
    }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'description' => 'Description',
			'user_id' => 'User',
			'created' => 'Created',
			'price' => 'Price',
			'rate_id' => 'Rate',
			'name' => 'Name',
			'time_to' => 'Time To',
			'time_from' => 'Time From',
			'phone_1' => 'Phone 1',
			'phone_2' => 'Phone 2',
			'email' => 'Email',
			'term_id' => 'Term',
			'c_location_id' => 'C Location',
			'c_sub_location_id' => 'C Sub Location',
			'optional_location' => 'Optional Location',
			'main_image' => 'Main Image',
			'longitude' => 'Longitude',
			'latitude' => 'Latitude',
			'web_site' => 'Web Site',
			'service_id' => 'Service',
			'isAccepted' => 'Is Accepted',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('price',$this->price,true);
		$criteria->compare('rate_id',$this->rate_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('time_to',$this->time_to,true);
		$criteria->compare('time_from',$this->time_from,true);
		$criteria->compare('phone_1',$this->phone_1,true);
		$criteria->compare('phone_2',$this->phone_2,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('term_id',$this->term_id);
		$criteria->compare('c_location_id',$this->c_location_id);
		$criteria->compare('c_sub_location_id',$this->c_sub_location_id);
		$criteria->compare('optional_location',$this->optional_location,true);
		$criteria->compare('main_image',$this->main_image,true);
		$criteria->compare('longitude',$this->longitude,true);
		$criteria->compare('latitude',$this->latitude,true);
		$criteria->compare('web_site',$this->web_site,true);
		$criteria->compare('service_id',$this->service_id);
		$criteria->compare('isAccepted',$this->isAccepted);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}