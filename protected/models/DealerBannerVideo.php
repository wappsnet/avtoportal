<?php

/**
 * This is the model class for table "dealer_banner_video".
 *
 * The followings are the available columns in table 'dealer_banner_video':
 * @property integer $id
 * @property integer $dealer_id
 * @property integer $type_id
 * @property string $img_title
 * @property string $youtube_link
 */
class DealerBannerVideo extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return DealerBannerVideo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'dealer_banner_video';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('dealer_id, type_id', 'numerical', 'integerOnly'=>true),
			array('img_title, youtube_link', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, dealer_id, type_id, img_title, youtube_link', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'dealer_id' => 'Dealer',
			'type_id' => 'Type',
			'img_title' => 'Img Title',
			'youtube_link' => 'Youtube Link',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('dealer_id',$this->dealer_id);
		$criteria->compare('type_id',$this->type_id);
		$criteria->compare('img_title',$this->img_title,true);
		$criteria->compare('youtube_link',$this->youtube_link,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}