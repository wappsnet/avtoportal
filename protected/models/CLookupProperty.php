<?php

/**
 * This is the model class for table "c_lookup_property".
 *
 * The followings are the available columns in table 'c_lookup_property':
 * @property integer $id
 * @property integer $c_property_id
 * @property string $name_eng
 * @property string $name_rus
 * @property string $name_geo
 * @property string $name_arm
 * @property integer $order_id
 * @property integer $father_id
 * @property integer $group_id
 * The followings are the available model relations:
 * @property CProperty $cProperty
 */
class CLookupProperty extends CActiveRecord
{
    public $name;
    public $upLevelItems = array();
    public $make;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CLookupProperty the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function getAttributes($names = true)
    {
        $attributes = parent::getAttributes($names);
        $attributes['upLevelItems'] = $this->upLevelItems;
        $attributes['name'] = $this->name;
        $attributes['make'] = $this->make;

        return $attributes;
    }

    public function findAll($condition = '', $params = array())
    {
        $list = parent::findAll($condition, $params);
        foreach ($list as $item) {
            if(Messages::getLanguageId() == 'arm'){
                $item->name =  $item->name_arm;
            }elseif(Messages::getLanguageId() == 'eng'){
                $item->name =  $item->name_eng;
            }elseif(Messages::getLanguageId() == 'rus'){
                $item->name =  $item->name_rus;
            }elseif(Messages::getLanguageId() == 'geo'){
                $item->name =  $item->name_geo;
            }

        }
        return $list;
    }

    public function findByPk($pk, $condition = '', $params = array())
    {
        $item = parent::findByPk($pk, $condition, $params);

        if(isset($item)){
            if(Messages::getLanguageId() == 'arm'){
                $item->name =  $item->name_arm;
            }elseif(Messages::getLanguageId() == 'eng'){
                $item->name =  $item->name_eng;
            }elseif(Messages::getLanguageId() == 'rus'){
                $item->name =  $item->name_rus;
            }elseif(Messages::getLanguageId() == 'geo'){
                $item->name =  $item->name_geo;
            }
        }
        return $item;
    }

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'c_lookup_property';
	}



	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('c_property_id', 'required'),
			array('c_property_id, order_id, father_id', 'numerical', 'integerOnly'=>true),
			array('name_eng, name_rus, name_geo, name_arm', 'length', 'max'=>45),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, c_property_id, name_eng, name_rus, name_geo, name_arm, order_id, father_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'cProperty' => array(self::BELONGS_TO, 'CProperty', 'c_property_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
            'upLevelItems' => 'Fathers',
			'id' => 'ID',
			'c_property_id' => 'C Property',
			'name_eng' => 'Name Eng',
			'name_rus' => 'Name Rus',
			'name_geo' => 'Name Geo',
			'name_arm' => 'Name Arm',
			'order_id' => 'Order',
			'father_id' => 'Father',
            'group_id' => 'Group',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('c_property_id',$this->c_property_id);
		$criteria->compare('name_eng',$this->name_eng,true);
		$criteria->compare('name_rus',$this->name_rus,true);
		$criteria->compare('name_geo',$this->name_geo,true);
		$criteria->compare('name_arm',$this->name_arm,true);
		$criteria->compare('order_id',$this->order_id);
		$criteria->compare('father_id',$this->father_id);
        $criteria->compare('group_id',$this->group_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}