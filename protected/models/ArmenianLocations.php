<?php

/**
 * This is the model class for table "armenian_locations".
 *
 * The followings are the available columns in table 'armenian_locations':
 * @property integer $id
 * @property string $name_eng
 * @property string $name_rus
 * @property string $name_arm
 * @property string $name_geo
 */
class ArmenianLocations extends CActiveRecord
{
    public $regions = array();
    public $name;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ArmenianLocations the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'armenian_locations';
	}

    public function findAll($condition = '', $params = array())
    {
        $list = parent::findAll($condition, $params);
        $criteria = new CDbCriteria();
        $criteria->condition = 'location_id = 1';
        $list[0]->regions = ArmenianSubLocations::model()->findAll($criteria);

        foreach ($list as $item) {
            if(Messages::getLanguageId() == 'arm'){
                $item->name =  $item->name_arm;
            }elseif(Messages::getLanguageId() == 'eng'){
                $item->name =  $item->name_eng;
            }elseif(Messages::getLanguageId() == 'rus'){
                $item->name =  $item->name_rus;
            }elseif(Messages::getLanguageId() == 'geo'){
                $item->name =  $item->name_geo;
            }
        }

        return $list;
    }

    public function getAttributes($names = true)
    {
        $attributes = parent::getAttributes($names);
        $attributes['regions'] = $this->regions;
        $attributes['name'] = $this->name;
        return $attributes;
    }

    public function findByPk($pk, $condition = '', $params = array())
    {
        $item = parent::findByPk($pk, $condition, $params);
        $criteria = new CDbCriteria();
        $criteria->condition = 'location_id = 1';
        $item->regions = ArmenianSubLocations::model()->findAll($criteria);

        if(Messages::getLanguageId() == 'arm'){
            $item->name =  $item->name_arm;
        }elseif(Messages::getLanguageId() == 'eng'){
            $item->name =  $item->name_eng;
        }elseif(Messages::getLanguageId() == 'rus'){
            $item->name =  $item->name_rus;
        }elseif(Messages::getLanguageId() == 'geo'){
            $item->name =  $item->name_geo;
        }

        return $item;

    }


    /**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name_eng, name_rus, name_arm, name_geo', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name_eng, name_rus, name_arm, name_geo', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name_eng' => 'Name Eng',
			'name_rus' => 'Name Rus',
			'name_arm' => 'Name Arm',
			'name_geo' => 'Name Geo',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name_eng',$this->name_eng,true);
		$criteria->compare('name_rus',$this->name_rus,true);
		$criteria->compare('name_arm',$this->name_arm,true);
		$criteria->compare('name_geo',$this->name_geo,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}