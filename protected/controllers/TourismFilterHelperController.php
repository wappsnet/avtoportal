<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Yerem
 * Date: 9/22/13
 * Time: 6:50 PM
 * To change this template use File | Settings | File Templates.
 */

class TourismFilterHelperController extends BaseFiltersController {


    const FUNCTIONAL_TABLE_NAME = "tourist_announcement";

    public function getModelByID($id){
        return TouristAnnouncement::model()->findByPk($id);
    }

    public function actionGetAllFilters(){
        $addedFilters = json_decode($_POST['data']);
        $condition = TourismFilterHelperController::getDBCriteriaByCondition($addedFilters);
        $announcements = TouristAnnouncement::model()->findAll($condition);

        $all_filters = array();

        $all_filters[] = array("filter_name" => Messages::getMessage(69), "filter_type" => "1", "filters" => DAOFilters::getTourismTransportType($condition->condition, TourismFilterHelperController::FUNCTIONAL_TABLE_NAME));
        $all_filters[] = array("filter_name" => Messages::getMessage(166), "filter_type" => "1", "filters" => DAOFilters::getTourismPlaces($condition->condition, TourismFilterHelperController::FUNCTIONAL_TABLE_NAME));
        $all_filters[] = array("filter_name" => Messages::getMessage(110), "filter_type" => "1", "filters" => DAOFilters::getTourismVendors($condition->condition, TourismFilterHelperController::FUNCTIONAL_TABLE_NAME));
        $all_filters[] = array("filter_name" => Messages::getMessage(111), "filter_type" => "1", "filters" => DAOFilters::getTourismProvided($condition->condition, TourismFilterHelperController::FUNCTIONAL_TABLE_NAME));
        $all_filters[] = array("filter_name" => Messages::getMessage(73), "filter_type" => "1", "filters" => DAOFilters::getTourismGuide($condition->condition, TourismFilterHelperController::FUNCTIONAL_TABLE_NAME));
        $all_filters[] = array("filter_name" => Messages::getMessage(17), "filter_type" => "1", "filters" => DAOFilters::getCarPrices($condition->condition, TourismFilterHelperController::FUNCTIONAL_TABLE_NAME));




        $filters = $all_filters;
        if($addedFilters){
            for($i = 0; $i < count($all_filters); ++$i){
                $filter = $all_filters[$i];
                foreach($addedFilters as $addedFilter){
                    if($addedFilter->filter_name == $filter['filter_name']){
                        unset($filters[$i]);
                    }
                }
            }
        }

        $data = array("filters"=>$filters, "announcements" => $announcements, "isAdmin" => $this->isAdmin() );
        echo CJSON::encode($data);
    }


}