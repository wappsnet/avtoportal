<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Yerem
 * Date: 2/12/14
 * Time: 8:52 PM
 * To change this template use File | Settings | File Templates.
 */

class ArcaController extends BaseFormController{



    public function actionConnectToArca(){
        $data = array(
            "hostID" => "150999",
            "mid" => "15002128",
            "tid" => "15532128",
            "additionalURL" => "/ps/arca_ret.php",
            "orderID" => "9899",
            "amount" => "1",
            "currency" => "051",
            "opaque" => "some info for our needs"
        );
        $encoded = '';
        foreach($data as $name => $value){
            $encoded .= urlencode($name).'='.urlencode($value).'&';
        }
        $ch = curl_init('https://91.199.226.106/services/authorize.php');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $encoded);
        curl_exec($ch);
        curl_close($ch);


    }

}