<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Edgar
 * Date: 9/2/13
 * Time: 11:24 PM
 * To change this template use File | Settings | File Templates.
 */

class AutoServiceController extends BaseFormController{


    public function actionInsert(){
        if(isset($_POST['data'])  && isset($_POST['uploadSave']) && isset($_POST['uploadDelete'])){
            $announcementToEdit = $_POST['announcementToEdit'];
            $this->formData = json_decode($_POST['data']);
            $this->imagesToSave = json_decode($_POST['uploadSave']);
            $this->imagesToDelete = json_decode($_POST['uploadDelete']);

            $announcementModel = $this->announcementForm;

            if($announcementToEdit != 'null'){
                $current_announcement = $announcementModel::model()->findByPk($announcementToEdit);
            }else{
                $current_announcement = new $announcementModel();
            }

            $this->fillOrdinaryData($current_announcement,true);
            //$this->fillUserData($current_announcement);
           // $this->fillPriceData($current_announcement , 176 , 177 , 178);
            $this->fillMainImage($current_announcement);
            $this->fillSpecificData($current_announcement);
            $current_announcement->save();

            $announcement_id = $current_announcement->getPrimaryKey();

            $this->savingServices($announcement_id);
            $this->processImages($announcement_id , $announcementToEdit != 'null');
            $this->processProperties($announcement_id , $announcementToEdit != 'null');


        }
    }
/*
    protected function fillUserData(&$announcement){
        $announcement->name = $this->getPropertyByID(203);
        $announcement->phone_1 = $this->getPropertyByID(206);
        $announcement->phone_2 = $this->getPropertyByID(207);
        $announcement->email = $this->getPropertyByID(208);
    }*/

    private function savingServices($announcement_id){
        foreach($this->formData->selected_services as $selected_service){
            $service = new SelectedServices();
            $service->announcement_id = $announcement_id;
            $service->service_lookup_id = $selected_service->value->id;
            $service->name_arm = $selected_service->value->name_arm;
            $service->name_eng = $selected_service->value->name_eng;
            $service->name_rus = $selected_service->value->name_rus;
            $service->name_geo = $selected_service->value->name_geo;
            $service->price = $selected_service->price;
            $service->save();

        }
    }

    protected  function  fillSpecificData(&$announcement){

        $announcement->longitude = $this->formData->longitude;
        $announcement->latitude = $this->formData->latitude;
        $announcement->email = $this->getPropertyNameByID(208);
        $announcement->web_site = $this->getPropertyNameByID(209);
        $announcement->phone_1 = $this->getPropertyNameByID(206);
        $announcement->phone_2 = $this->getPropertyNameByID(207);
        $announcement->time_from = $this->getPropertyNameByID(204);
        $announcement->time_to = $this->getPropertyNameByID(205);
        $announcement->service_id = $this->getPropertyNameByID(211);
        $announcement->name = $this->getPropertyNameByID(203);
    }



    public function init(){
        parent::init();
        $this->announcementForm = 'AutoServiceAnnouncement';
        $this->imagesForm = 'AutoServicePhotos';
        $this->propertiesForm = 'AutoServiceProperties';

    }

    public function actionGetAutoServices(){
        if($this->isAdmin()){
            $this->actionGetDraftAutoServices();
        }else{
            $this->actionGetAcceptedAutoServices();
        }
    }

    public function actionGetAutoServicesByPlace(){
        $loc_id = json_decode($_POST['loc_id']);

        $criteria = New CDbCriteria();
        $criteria->select = '*';
        $criteria->condition = 'c_location_id = :loc_id ';
        $criteria->params = array(':loc_id'=>$loc_id );
        $announcements = AutoServiceAnnouncement::model()->findAll($criteria);
        echo  CJSON::encode($announcements);
    }

    public function actionGetAutoServicesBySubPlace(){
        $subLoc_id = json_decode($_POST['subLoc_id']);

        $criteria = New CDbCriteria();
        $criteria->select = '*';
        $criteria->condition = 'c_sub_location_id = :loc_id ';
        $criteria->params = array(':loc_id'=>$subLoc_id );
        $announcements = AutoServiceAnnouncement::model()->findAll($criteria);
        echo  CJSON::encode($announcements);
    }

    public function actionGetAllServiceMarkers(){
        $services = AutoServiceAnnouncement::model()->findAll();
        echo  CJSON::encode($services);
    }

    public function actionGetAllYerevanServiceMarkers(){

        $criteria = New CDbCriteria();
        $criteria->select = '*';
        $criteria->condition = 'c_sub_location_id = 1';

        $services = AutoServiceAnnouncement::model()->findAll($criteria);
        echo  CJSON::encode($services);
    }


    public function actionGetAcceptedAutoServices(){
        echo CJSON::encode($this->findAutoServicesByCondition("t.isAccepted = 1"));
    }

    public function actionGetDraftAutoServices(){
        echo CJSON::encode($this->findAutoServicesByCondition("t.isAccepted = 0"));
    }

    private function findAutoServicesByCondition($condition = ""){
        $criteria = New CDbCriteria();
        $criteria->condition = $condition;
        $announcements = AutoServiceAnnouncement::model()->findAll($criteria);
        return $announcements;
    }

    public function actionAcceptAndRefresh(){
        if($this->actionAccept()){
            $criteria = new CDbCriteria();
            $criteria->condition = "isAccepted = 0";
            $data = array("announcements" => AutoServiceAnnouncement::model()->findAll($criteria));
            echo CJSON::encode($data);
        }else{
            if($error=Yii::app()->errorHandler->error)
            {
                if(Yii::app()->request->isAjaxRequest)
                    echo $error['message'];
                else
                    $this->render('error', $error);
            }
        }
    }

    public function actionDeleteAndRefresh(){

        if($this->actionDelete()){
            $criteria = new CDbCriteria();
            $criteria->condition = "isAccepted = 0";
            $data = array("announcements" => AutoServiceAnnouncement::model()->findAll($criteria));
            echo CJSON::encode($data);
        }else{
            if($error=Yii::app()->errorHandler->error)
            {
                if(Yii::app()->request->isAjaxRequest)
                    echo $error['message'];
                else
                    $this->render('error', $error);
            }
        }
    }

    public function actionAccept(){
        $id = json_decode($_POST['id']);
        $model = AutoServiceAnnouncement::model()->findByPk($id);
        $model->isAccepted = true;
        return $model->save();
    }

    public function actionDelete(){
        $id = json_decode($_POST['id']);
        $model = AutoServiceAnnouncement::model()->findByPk($id);
        return $model->delete();
    }

    private function sumOfLocationServices($loc_id){
        $sum = Yii::app()->db->createCommand()
            ->select('sum(c_location_id) AS sum_loc')
            ->from('auto_service_announcement')
            ->group('c_location_id')
            ->having('c_location_id = :loc_id' , array('loc_id' => $loc_id))
            ->queryAll();

        if(isset($sum) && isset($sum[0]['sum_loc'])){
            return $sum[0]['sum_loc'];
        }else{
            return 0;
        }
    }

    private function sumOfSubLocationServices($subloc_id){
        $sum = Yii::app()->db->createCommand()
            ->select('sum(c_sub_location_id) AS sum_loc')
            ->from('auto_service_announcement')
            ->group('c_sub_location_id')
            ->having('c_sub_location_id = :loc_id' , array('loc_id' => $subloc_id))
            ->queryAll();

        if(isset($sum) && isset($sum[0]['sum_loc'])){
            return $sum[0]['sum_loc'];
        }else{
            return 0;
        }
    }

    public function actionServicesByPlace(){
        $arm_locs = ArmenianLocations::model()->findAll();
        $items = array();
        $item = array();

        foreach($arm_locs as $loc){
            $item['id'] = $loc->id;
            $item['name'] = $loc->name_arm;
            $item['count'] = $this->sumOfSubLocationServices($loc->id);
            $items[] = $item;
        }

       echo  CJSON::encode($items);
    }

    public function actionServicesBySubPlace(){
        $arm_locs = ArmenianSubLocations::model()->findAll();
        $items = array();
        $item = array();

        foreach($arm_locs as $loc){
            $item['id'] = $loc->id;
            $item['name'] = $loc->name_arm;
            $item['count'] = $this->sumOfLocationServices($loc->id);
            $items[] = $item;
        }

        echo  CJSON::encode($items);
    }





}