<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Edgar
 * Date: 9/9/13
 * Time: 12:53 AM
 * To change this template use File | Settings | File Templates.
 */

class JobFormController extends BaseFormController{

    protected  function  fillSpecificData(&$announcement){
        $announcement->place = $this->getPropertyByID(188);
        $announcement->job_name = $this->getPropertyByID(189);
        $announcement->job_type = $this->getPropertyByID(190);
        $announcement->job_sort = $this->getPropertyByID(191);
        $announcement->announcement_name = $this->getPropertyNameByID(189) . ' ' . $this->getPropertyNameByID(191);
    }

    public function actionInsert(){
        if(isset($_POST['data'])  && isset($_POST['uploadSave']) && isset($_POST['uploadDelete'])){
            $announcementToEdit = $_POST['announcementToEdit'];
            $this->formData = json_decode($_POST['data']);
            $this->imagesToSave = json_decode($_POST['uploadSave']);
            $this->imagesToDelete = json_decode($_POST['uploadDelete']);

            $announcementModel = $this->announcementForm;

            if($announcementToEdit != 'null'){
                $current_announcement = $announcementModel::model()->findByPk($announcementToEdit);
            }else{
                $current_announcement = new $announcementModel();
            }

            $this->fillOrdinaryData($current_announcement,false);
            $this->fillUserData($current_announcement);
            $this->fillPriceData($current_announcement , 176 , 177 , 178);
            $this->fillMainImage($current_announcement);
            $this->fillSpecificData($current_announcement);

            $current_announcement->save();

            $announcement_id = $current_announcement->getPrimaryKey();

            $this->processImages($announcement_id , $announcementToEdit != 'null');
            $this->processProperties($announcement_id , $announcementToEdit != 'null');

            $summary = $this->getPropertiesSummary($announcement_id);

            $announcementModel::model()->updateByPk($announcement_id ,
                array(  'properties_summary_eng' => $summary['eng'] ,
                    'properties_summary_arm' => $summary['arm'] ,
                    'properties_summary_rus' => $summary['rus'] ,
                    'properties_summary_geo' => $summary['geo'])
            );
        }
    }


    public function init(){
        parent::init();
        $this->announcementForm = 'JobAnnouncement';
        $this->imagesForm = 'JobPhoto';
        $this->propertiesForm = 'JobProperties';

        $this->propertyIds = array(
            'place'    => 188,
            'time'     => 240 ,
        );

        $this->extraRightProps[] = array(
            'prop_id' => 240,
            'name_arm' => 'рабочих часов' ,
            'name_eng' => 'рабочих часов' ,
            'name_rus' => 'рабочих часов' ,
            'name_geo' => 'рабочих часов' ,
        );

    }





}