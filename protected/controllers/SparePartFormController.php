<?php
/**
 * Created by JetBrains PhpStorm.
 * User: superuser
 * Date: 5/23/13
 * Time: 12:38 AM
 * To change this template use File | Settings | File Templates.
 */

class SparePartFormController extends BaseFormController{


    public function init(){
        parent::init();
        $this->announcementForm = 'SparePartAnnouncement';
        $this->imagesForm = 'SparePartPhotos';
        $this->propertiesForm = 'SparePartProperty';

        $this->propertyIds = array(
            'type'    => 192,
            'sub-type'    => 193,
            'body'    => 194,
            'mark'    => 195,
            'model'    => 197,
            'year'    => 200,
        );
    }


    public function actionInsert(){
        if(isset($_POST['data'])  && isset($_POST['uploadSave']) && isset($_POST['uploadDelete'])){
            $announcementToEdit = $_POST['announcementToEdit'];
            $this->formData = json_decode($_POST['data']);
            $this->imagesToSave = json_decode($_POST['uploadSave']);
            $this->imagesToDelete = json_decode($_POST['uploadDelete']);

            $announcementModel = $this->announcementForm;

            if($announcementToEdit != 'null'){
                $current_announcement = $announcementModel::model()->findByPk($announcementToEdit);
            }else{
                $current_announcement = new $announcementModel();
            }

            $this->fillOrdinaryData($current_announcement,true);
            $this->fillUserData($current_announcement);
            $this->fillPriceDataOnlyRUR($current_announcement , 201 );
            $this->fillMainImage($current_announcement);
            $this->fillSpecificData($current_announcement);

            $current_announcement->save();

            $announcement_id = $current_announcement->getPrimaryKey();

            $this->processImages($announcement_id , $announcementToEdit != 'null');
            $this->processProperties($announcement_id , $announcementToEdit != 'null');

            $summary = $this->getPropertiesSummary($announcement_id);

            $announcementModel::model()->updateByPk($announcement_id ,
                array(  'properties_summary_eng' => $summary['eng'] ,
                    'properties_summary_arm' => $summary['arm'] ,
                    'properties_summary_rus' => $summary['rus'] ,
                    'properties_summary_geo' => $summary['geo'])
            );
        }
    }


    protected  function  fillSpecificData(&$announcement){
        $announcement->type_id = $this->getPropertyByID(192);
        $announcement->subtype_id = $this->getPropertyByID(193);
        $announcement->body_style_id = $this->getPropertyByID(194);
        $announcement->mark_id = $this->getPropertyByID(195);
        $announcement->model_id = $this->getPropertyByID(197);
        $announcement->state = $this->getPropertyByID(198);
        $announcement->announcement_name = $this->getPropertyNameByID(192) . ' ' . $this->getPropertyNameByID(195) . ' ' . $this->getPropertyNameByID(197);
        $announcement->phone_id = $this->getPropertyByID(245);
    }

}