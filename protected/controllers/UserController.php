<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Edgar
 * Date: 8/19/13
 * Time: 9:25 PM
 * To change this template use File | Settings | File Templates.
 */

class UserController extends Controller {
    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha'=>array(
                'class'=>'CCaptchaAction',
                'backColor'=>0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page'=>array(
                'class'=>'CViewAction',
            ),
        );
    }

    public function actionInsert(){

        if(isset($_POST['data'])){

            $userData = json_decode($_POST['data']);
            $user = new User();
            $phone_1 = new UserPhone();
//            $phone_2 = new UserPhone();

            $phone_1_id = null;
//            $phone_2_id = null;

            if($userData->isPersonal == "true"){
                $user->user_roles_id = 1;
            }else if($userData->isOfficial == "true"){
                $user->user_roles_id = 3;
            }else{
                $user->user_roles_id = 2;//universal
            }

            $user->email = $userData->email_reg;
            $user->password =  UserMailService::encryptIt($userData->password_reg);
            $user->user_name = $userData->name;
            $user->name_is_hidden = !$userData->showName;
            $user->organisation_name = $userData->organisation;
            $user->organisation_address = $userData->address;
            $user->web_site = $userData->site;

            if(isset($userData->dealer_markers) && isset($userData->dealer_markers[0])){
                $user->longitude = $userData->dealer_markers[0]->longitude;
                $user->latitude = $userData->dealer_markers[0]->latitude;
            }

            if(isset($userData->phone) && isset($userData->phoneType)){
                $phone_1->phone_number = $userData->phone;
                $phone_1->c_phone_types_id = $userData->phoneType->id;
                $phone_1_id = $phone_1->save();
            }

//            if(isset($userData->secondPhone) && isset($userData->secondPhoneType)){
//                $phone_2->phone_number = "465464";
//                $phone_2->c_phone_types_id = 56;
//                $phone_2_id = $phone_2->save();
//            }

            if($phone_1_id != null){
                $user->phone1_id = $phone_1->id;
            }
//
//            if($phone_2_id != null){
//                $user->phone2_id = $phone_2->id;
//            }
//            $user->phone2_id = '';
            $user->save();

            if(isset($_POST['images'])){
                $this->insertUpdateDealerPhoto($images = json_decode($_POST['images']), $user->id);
            }
            if($user->user_roles_id == 2 || $user->user_roles_id == 3){
                $image_title = $userData->image_title;

                $dealerLogo = new DealerLogo();
                $dealerLogo->logo_name = $image_title;
                $dealerLogo->dealer_id = $user->id;
                $dealerLogo->save();
            }


            $messageBody = "Поздравляем вы прошли первый шаг регистрации. <br/> Пожалуйста, пройдите по этой ссылке, чтобы активировать Ваш аккаунт. <br/>";
            $link = Yii::app()->request->hostInfo . Yii::app()->request->baseUrl . '/activateUser?usUi=' . urlencode(UserMailService::encryptIt($user->id));
            $messageBody.= $link;
            UserMailService::sendMail("avtomeqena.am", $user->email, $messageBody);
        }
    }


    public function actionValidateCaptcha(){
        $captcha_code = Yii::app()->session['captcha_code'];
        $userInput = json_decode($_POST['data']);
        if ($captcha_code ==  $userInput){
            echo CJSON::encode(true);
        }else{
            echo CJSON::encode(false);
        }
    }

    public function actionLoadUser(){
        $user = User::model()->findByPk(Yii::app()->user->id);
        $userPhone = UserPhone::model()->findByPk($user->phone1_id);
        $userPhoneType = UserPhoneType::model()->findByPk($userPhone->c_phone_types_id);

        $dealerLogo = DealerLogo::model()->findByAttributes(array("dealer_id"=>Yii::app()->user->id));
        $imageTitle = '';
        if($dealerLogo){
            $imageTitle = $dealerLogo->logo_name;
        }

        echo CJSON::encode(array("user"=>$user, "userPhone"=>$userPhone, "isAdmin" => $user->user_roles_id == 4 ? 1 : 0, "userPhoneType"=>$userPhoneType, "image_title"=>$imageTitle));
    }

    public function actionUpdate(){

        if(isset($_POST['data'])){

            $userData = json_decode($_POST['data']);
            $user = User::model()->findByPk(Yii::app()->user->id);
            $phone_1 = UserPhone::model()->findByPk($userData->phone1_id);

            //$phone_1_id = null;
            //$phone_2_id = null;

            $user->password =  UserMailService::encryptIt($userData->password_reg);
            $user->user_name = $userData->name;
            $user->name_is_hidden = !$userData->showName;
            if(isset($userData->organisation)){
                $user->organisation_name = $userData->organisation;
            }
            if(isset($userData->address)){
                $user->organisation_address = $userData->address;
            }
            if(isset($userData->site)){
                $user->web_site = $userData->site;
            }

            if(isset($userData->phone) && isset($userData->phoneType)){
                if(!$phone_1){
                    $phone_1 = new UserPhone();
                }
                $phone_1->phone_number = $userData->phone;
                $phone_1->c_phone_types_id = $userData->phoneType->id;
                $phone_1_id = $phone_1->save();
            }

            if($phone_1_id != null){
                $user->phone1_id = $phone_1->id;
            }

            $user->save();

            if(isset($_POST['images'])){
                $this->insertUpdateDealerPhoto($images = json_decode($_POST['images']), $user->id);
            }


            if($user->user_roles_id == 2 || $user->user_roles_id == 3){
                $image_title = $userData->image_title;
                $dealerLogo = DealerLogo::model()->findByAttributes(array("dealer_id"=>$user->id));
                $dealerLogo->logo_name = $image_title;
                $dealerLogo->dealer_id = $user->id;
                $dealerLogo->save();
            }


            $messageBody = "You have updated your account detailes. <br/> ";
            $messageBody.= "Your Password : " . $userData->password_reg;
            UserMailService::sendMail("avtomeqena.am", $user->email, $messageBody);

        }
    }

    private function insertUpdateDealerPhoto($images, $dealerId){
        DealerBannerVideo::model()->deleteAllByAttributes(array("dealer_id"=>$dealerId));
        foreach($images as $image){
            $dealerImage = new DealerBannerVideo();
            $dealerImage->dealer_id = $dealerId;
            $dealerImage->img_title = $image->title;
            $dealerImage->type_id = 0;
            $dealerImage->save();
        }
    }

    public function actionGetDealerImages(){
        $dealerId = $_POST['dealerId'];
        preg_match_all('!\d+!', $dealerId, $matches);
        $dealerId = implode(' ', $matches[0]);
        $dealerImage = DealerBannerVideo::model()->findAllByAttributes(array("dealer_id"=>$dealerId));
        echo CJSON::encode($dealerImage);
    }

    public function actionDeleteAnnouncement(){
        $id = json_decode($_POST['id']);
        $formType = json_decode($_POST['formType']);
        switch ($formType) {
            case "1":
                $this->layout_top_bar = '//layouts/top_bar_1';
                if(!$this->checkPermissions(CarAnnouncement::model(), $id)){
                    throw new CHttpException(403, 'You are not authorized to perform this action.');
                }else{
                    CarAnnouncement::model()->deleteByPk($id);
                }
                break;
            case "2":
                $this->layout_top_bar = '//layouts/top_bar_1';
                $this->layout_top_bar = '//layouts/top_bar_1';
                if(!$this->checkPermissions(CarRentAnnouncement::model(), $id)){
                    throw new CHttpException(403, 'You are not authorized to perform this action.');
                }else{
                    CarRentAnnouncement::model()->deleteByPk($id);
                }
                break;
            case "3":
                $this->layout_top_bar = '//layouts/top_bar_1';
                if(!$this->checkPermissions(WheelsAnnouncement::model(), $id)){
                    throw new CHttpException(403, 'You are not authorized to perform this action.');
                }else{
                    WheelsAnnouncement::model()->deleteByPk($id);
                }
                break;
            case "4":
                $this->layout_top_bar = '//layouts/top_bar_1';
                if(!$this->checkPermissions(SparePartAnnouncement::model(), $id)){
                    throw new CHttpException(403, 'You are not authorized to perform this action.');
                }else{
                    SparePartAnnouncement::model()->deleteByPk($id);
                }
                break;
            case "5":
                $this->layout_top_bar = '//layouts/top_bar_1';
                if(!$this->checkPermissions(RuraltechAnnouncement::model(), $id)){
                    throw new CHttpException(403, 'You are not authorized to perform this action.');
                }else{
                    RuraltechAnnouncement::model()->deleteByPk($id);
                }
                break;
            case "6":
                $this->layout_top_bar = '//layouts/top_bar_1';
                if(!$this->checkPermissions(ShippingAnnouncement::model(), $id)){
                    throw new CHttpException(403, 'You are not authorized to perform this action.');
                }else{
                    ShippingAnnouncement::model()->deleteByPk($id);
                }
                break;
            case "7":
                $this->layout_top_bar = '//layouts/top_bar_1';
                if(!$this->checkPermissions(TransportationAnnouncement::model(), $id)){
                    throw new CHttpException(403, 'You are not authorized to perform this action.');
                }else{
                    TransportationAnnouncement::model()->deleteByPk($id);
                }
                break;
            case "8":
                $this->layout_top_bar = '//layouts/top_bar_1';
                if(!$this->checkPermissions(TouristAnnouncement::model(), $id)){
                    throw new CHttpException(403, 'You are not authorized to perform this action.');
                }else{
                    TouristAnnouncement::model()->deleteByPk($id);
                }
                break;
            case "9":
                $this->layout_top_bar = '//layouts/top_bar_1';
                if(!$this->checkPermissions(EvacuatorAnnouncement::model(), $id)){
                    throw new CHttpException(403, 'You are not authorized to perform this action.');
                }else{
                    EvacuatorAnnouncement::model()->deleteByPk($id);
                }
                break;
            case "10":
                $this->layout_top_bar = '//layouts/top_bar_1';
                if(!$this->checkPermissions(JobAnnouncement::model(), $id)){
                    throw new CHttpException(403, 'You are not authorized to perform this action.');
                }else{
                    JobAnnouncement::model()->deleteByPk($id);
                }
                break;
            case "11":
                $this->layout_top_bar = '//layouts/top_bar_1';
                if(!$this->checkPermissions(TaxiAnnouncement::model(), $id)){
                    throw new CHttpException(403, 'You are not authorized to perform this action.');
                }else{
                    TaxiAnnouncement::model()->deleteByPk($id);
                }
                break;
        }
    }

    private function checkPermissions($model, $id){
        $user_id = $model->findByPk($id)->user_id;
        if($user_id === Yii::app()->user->id){
            return true;
        }
        return false;
    }

    public function actionAddToUrgent(){
        $id = json_decode($_POST['id']);
        $formType = json_decode($_POST['formType']);
        $topType = 1;
        $this->actionProcessTop($id, $formType, $topType);
    }

    public function actionAddToHomeTop(){
        $id = json_decode($_POST['id']);
        $formType = json_decode($_POST['formType']);
        $topType = 2;
        $this->actionProcessTop($id, $formType, $topType);
    }

    public function actionAddToListTop(){
        $id = json_decode($_POST['id']);
        $formType = json_decode($_POST['formType']);
        $topType = 3;
        $this->actionProcessTop($id, $formType, $topType);
    }

    public function actionValidateEmail(){
        $email = $_POST['email_reg'];
        $criteria = new CDbCriteria();
        $criteria->condition = 'email = :em';
        $criteria->params = array(':em'=>$email);
        $model = User::model()->find($criteria);

        if($model == null){
            echo 0;
        }else{
            echo 1;
        }

    }


    public function actionProcessTop($announcementId, $announcementType, $topType){

        switch ($announcementType) {
            case "1":
                if(!$this->checkPermissions(CarAnnouncement::model(), $announcementId)){
                    throw new CHttpException(403, 'You are not authorized to perform this action.');
                }else{
                    $model = CarAnnouncement::model()->findByPk($announcementId);
                    $model->is_top = $topType;
                    $model->save();
                }
                break;
            case "2":
                if(!$this->checkPermissions(CarRentAnnouncement::model(), $announcementId)){
                    throw new CHttpException(403, 'You are not authorized to perform this action.');
                }else{
                    $model = CarRentAnnouncement::model()->findByPk($announcementId);
                    $model->is_top = $topType;
                    $model->save();
                }
                break;
            case "3":
                if(!$this->checkPermissions(WheelsAnnouncement::model(), $announcementId)){
                    throw new CHttpException(403, 'You are not authorized to perform this action.');
                }else{
                    $model = WheelsAnnouncement::model()->findByPk($announcementId);
                    $model->is_top = $topType;
                    $model->save();
                }
                break;
            case "4":
                $this->layout_top_bar = '//layouts/top_bar_1';
                if(!$this->checkPermissions(SparePartAnnouncement::model(), $announcementId)){
                    throw new CHttpException(403, 'You are not authorized to perform this action.');
                }else{
                    $model = SparePartAnnouncement::model()->findByPk($announcementId);
                    $model->is_top = $topType;
                    $model->save();
                }
                break;
            case "5":
                if(!$this->checkPermissions(RuraltechAnnouncement::model(), $announcementId)){
                    throw new CHttpException(403, 'You are not authorized to perform this action.');
                }else{
                    $model = RuraltechAnnouncement::model()->findByPk($announcementId);
                    $model->is_top = $topType;
                    $model->save();
                }
                break;
            case "6":
                if(!$this->checkPermissions(ShippingAnnouncement::model(), $announcementId)){
                    throw new CHttpException(403, 'You are not authorized to perform this action.');
                }else{
                    $model = ShippingAnnouncement::model()->findByPk($announcementId);
                    $model->is_top = $topType;
                    $model->save();
                }
                break;
            case "7":
                if(!$this->checkPermissions(TransportationAnnouncement::model(), $announcementId)){
                    throw new CHttpException(403, 'You are not authorized to perform this action.');
                }else{
                    $model = TransportationAnnouncement::model()->findByPk($announcementId);
                    $model->is_top = $topType;
                    $model->save();
                }
                break;
            case "8":
                if(!$this->checkPermissions(TouristAnnouncement::model(), $announcementId)){
                    throw new CHttpException(403, 'You are not authorized to perform this action.');
                }else{
                    $model = TouristAnnouncement::model()->findByPk($announcementId);
                    $model->is_top = $topType;
                    $model->save();
                }
                break;
            case "9":
                $this->layout_top_bar = '//layouts/top_bar_1';
                if(!$this->checkPermissions(EvacuatorAnnouncement::model(), $announcementId)){
                    throw new CHttpException(403, 'You are not authorized to perform this action.');
                }else{
                    $model = EvacuatorAnnouncement::model()->findByPk($announcementId);
                    $model->is_top = $topType;
                    $model->save();
                }
                break;
            case "10":
                if(!$this->checkPermissions(JobAnnouncement::model(), $announcementId)){
                    throw new CHttpException(403, 'You are not authorized to perform this action.');
                }else{
                    $model = JobAnnouncement::model()->findByPk($announcementId);
                    $model->is_top = $topType;
                    $model->save();
                }
                break;
            case "11":
                if(!$this->checkPermissions(TaxiAnnouncement::model(), $announcementId)){
                    throw new CHttpException(403, 'You are not authorized to perform this action.');
                }else{
                    $model = TaxiAnnouncement::model()->findByPk($announcementId);
                    $model->is_top = $topType;
                    $model->save();
                }
                break;
        }
    }



}