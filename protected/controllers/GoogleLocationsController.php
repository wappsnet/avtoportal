<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Edgar
 * Date: 8/15/13
 * Time: 7:38 PM
 * To change this template use File | Settings | File Templates.
 */

class  GoogleLocationsController extends Controller {

    public function accessRules()
    {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('appaMarkers'),
                'users' => array('*'),
            ),
        );
    }

    public function actionAppaMarkers(){
        if(isset($_POST['appa_id'])){
            $appa_id = $_POST['appa_id'];

            $markers = Yii::app()->db->createCommand()->select('id ,infoWindow as infoWindow , latitude , longitude as longitude')->
                from('google_map_location')->where('location_type = 1 and belongs_to = :appa_id',array(':appa_id'=>$appa_id))->query()->readAll();

            echo  CJSON::encode($markers);
        }
    }

    public function actionAutoSchoolMarkers(){
        if(isset($_POST['autoschool_id'])){
            $autoschool_id = $_POST['autoschool_id'];

            $markers = Yii::app()->db->createCommand()->select('id ,infoWindow as infoWindow , latitude , longitude as longitude')->
                from('google_map_location')->where('location_type = 2 and belongs_to = :autoschool_id',array(':autoschool_id'=>$autoschool_id))->query()->readAll();

            echo  CJSON::encode($markers);
        }
    }

    public function actionTechObserveMarkers(){
        $markers = Yii::app()->db->createCommand()->select('id ,infoWindow as infoWindow , latitude , longitude as longitude')->
            from('google_map_location')->where('location_type = 3')->query()->readAll();

        echo  CJSON::encode($markers);

    }

    public function actionTaxServiceMarkers(){
        $markers = Yii::app()->db->createCommand()->select('id ,infoWindow as infoWindow , latitude , longitude as longitude')->
            from('google_map_location')->where('location_type = 4')->query()->readAll();

        echo  CJSON::encode($markers);

    }

    public function actionRoadPoliceMarkers(){
        $markers = Yii::app()->db->createCommand()->select('id ,infoWindow as infoWindow , latitude , longitude as longitude')->
            from('google_map_location')->where('location_type = 5')->query()->readAll();

        echo  CJSON::encode($markers);

    }

}