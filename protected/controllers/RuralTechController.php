<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Edgar
 * Date: 9/5/13
 * Time: 3:42 PM
 * To change this template use File | Settings | File Templates.
 */

class RuralTechController extends BaseFormController{

    public function init(){
        parent::init();
        $this->announcementForm = 'RuraltechAnnouncement';
        $this->imagesForm = 'RuraltechPhoto';
        $this->propertiesForm = 'RuraltechProperties';

        $this->propertyIds = array(
            'mark  '    => 171,
            'body'    => 172,
            'vendor'    => 174
        );
    }

    protected  function  fillSpecificData(&$announcement){
        $announcement->technic_id = $this->getPropertyByID(170);
        $announcement->mark_id = $this->getPropertyByID(171);
        $announcement->body_id = $this->getPropertyByID(172);
        $announcement->work_type_id = $this->getPropertyByID(173);
        $announcement->vendor = $this->getPropertyByID(174);
        $announcement->with_driver = $this->getPropertyByID(175);
        $announcement->announcement_name = $this->getPropertyNameByID(170);
        $announcement->phone_id = $this->getPropertyNameByID(248);

    }

    public function actionInsert(){
        if(isset($_POST['data'])  && isset($_POST['uploadSave']) && isset($_POST['uploadDelete'])){
            $announcementToEdit = $_POST['announcementToEdit'];
            $this->formData = json_decode($_POST['data']);
            $this->imagesToSave = json_decode($_POST['uploadSave']);
            $this->imagesToDelete = json_decode($_POST['uploadDelete']);

            $announcementModel = $this->announcementForm;

            if($announcementToEdit != 'null'){
                $current_announcement = $announcementModel::model()->findByPk($announcementToEdit);
            }else{
                $current_announcement = new $announcementModel();
            }

            $this->fillOrdinaryData($current_announcement,true);
            $this->fillUserData($current_announcement);
            $this->fillPriceDataOnlyRUR($current_announcement , 241 );
            $this->fillMainImage($current_announcement);
            $this->fillSpecificData($current_announcement);

            $current_announcement->save();

            $announcement_id = $current_announcement->getPrimaryKey();

            $this->processImages($announcement_id , $announcementToEdit != 'null');
            $this->processProperties($announcement_id , $announcementToEdit != 'null');

            $summary = $this->getPropertiesSummary($announcement_id);

            $announcementModel::model()->updateByPk($announcement_id ,
                array(  'properties_summary_eng' => $summary['eng'] ,
                    'properties_summary_arm' => $summary['arm'] ,
                    'properties_summary_rus' => $summary['rus'] ,
                    'properties_summary_geo' => $summary['geo'])
            );
        }
    }

}