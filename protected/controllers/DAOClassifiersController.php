<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Yerem
 * Date: 7/14/13
 * Time: 10:53 PM
 * To change this template use File | Settings | File Templates.
 */

class DAOClassifiersController extends Controller
{

    private function getLabel()
    {
        $lang = Messages::getLanguageId();
        $label = "name_" . $lang;
        return $label;
    }

    private function getDefaultMessageForMark()
    {
        return Messages::getMessage(5);
    }

    private function getDefaultMessageForModel()
    {
        return Messages::getMessage(5);
    }

    private function getDefaultMessageForYearFrom()
    {
        return Messages::getMessage(5);
    }

    private function getDefaultMessageForYearTo()
    {
        return Messages::getMessage(5);
    }

    private function getDefaultMessageForPriceFrom()
    {
        return Messages::getMessage(5);
    }

    private function getDefaultMessageForPriceTo()
    {
        return Messages::getMessage(5);
    }

    private function getDefaultMessageForCrashed()
    {
        return Messages::getMessage(5);
    }

    private function getDefaultMessageForYesOption()
    {
        return Messages::getMessage(5);
    }

    private function getDefaultMessageForNoOption()
    {
        return Messages::getMessage(5);
    }

    private function getDefaultMessageForColor()
    {
        return Messages::getMessage(5);
    }

    private function getDefaultMessageForGearbox()
    {
        return Messages::getMessage(5);
    }

    private function getDefaultMessageForEngine()
    {
        return Messages::getMessage(5);
    }

    private function getDefaultMessageForMilage()
    {
        return Messages::getMessage(5);
    }

    private function getDefaultMessageForTax()
    {
        return Messages::getMessage(5);
    }

    private function getDefaultMessageForSubLocation()
    {
        return Messages::getMessage(5);
    }

    private function getDefaultMessageForLocation()
    {
        return Messages::getMessage(5);
    }

    public function actionLoadCarAnnouncementFilters()
    {
        $filters = array();
        $filters["markFilters"] = $this->getCarMarks();
        $filters["fromYearFilters"] = $this->getFromYears();
        $filters["toYearFilters"] = $this->getToYears();
        $filters["fromPriceFilters"] = $this->getFromPrice();
        $filters["toPriceFilters"] = $this->getToPrice();
        $filters["crashed"] = $this->getCrashed();
        $filters["color"] = $this->getCarColors();
        $filters["gearbox"] = $this->getGearbox();
        $filters["engine"] = $this->getEngineType();
        $filters["mileage"] = $this->getMileageOptions();
        $filters["engineVolume"] = $this->getEngineVolumeOptions();
        $filters["taxOptions"] = $this->getTaxOptions();
        $filters["locations"] = $this->getLocations();
        echo CJSON::encode($filters);
    }



    private function actionGetModelsWithGroups($mark_id){
        //$mark_id = $_POST['mark_id'];
        $model_items = array();
        $prev_group_id = 0;

        $models = Yii::app()->db->createCommand()->select('c_lookup_property.id prop_id, c_lookup_property.name_eng prop_name ,c_lookup_property.group_id group_id , mg.name group_name , c_lookup_property.father_id ')->
            from(CLookupProperty::model()->tableName())->leftJoin('model_groups mg','mg.id = group_id')->
            having('father_id = :ann_id',array('ann_id'=>$mark_id))->order('group_id , prop_name')->
            query()->readAll();

        foreach($models as $item){
            if($item['group_id'] != null && $prev_group_id != $item['group_id']){
                $model_item = array();
                $model_item['id'] = '0';
                $model_item['group'] = $item['group_id'];
                $model_item['label'] = $item['group_name'];
                $prev_group_id = $item['group_id'];
                $model_items[] = $model_item;
            }

            $model_item = array();
            $model_item['id'] = $item['prop_id'];
            $model_item['group'] = '0';
            if($item['group_id'] != null){
                $model_item['label'] = '--------' . $item['prop_name'];
            }else{
                $model_item['label'] = $item['prop_name'];
            }

            $model_items[] =  $model_item;
        }

        return  $model_items;
    }




    private function getCarMarks()
    {
        $result = array();
        $defModels = array();
        $defModels[] = array("id" => "0", "label" => $this->getDefaultMessageForModel());
        $result[] = array("id" => "0", "label" => $this->getDefaultMessageForMark(), "models" => $defModels);
        $marks = Yii::app()->db->createCommand()
            ->select($this->getLabel() . ' label, id')
            ->from('c_lookup_property')
            ->where('c_property_id = 1')
            ->order('label')
            ->queryAll();
        foreach ($marks as $mark) {
            $models = array();
            $models[] = array("id" => "0", "label" => $this->getDefaultMessageForModel());

            $modelsRes = $this->actionGetModelsWithGroups($mark['id']);
            if(count($modelsRes) != 0){
                $models = array_merge($models,$modelsRes);
            }

            $result[] = array("id" => $mark['id'], "label" => $mark['label'], "models" => $models);
        }
        return $result;
    }

    private function getFromYears()
    {
        $result = Yii::app()->db->createCommand()
            ->select($this->getLabel() . ' label, id')
            ->from('c_lookup_property')
            ->where('c_property_id = 5')
            ->order('cast(label as unsigned) DESC')
            ->queryAll();
        $years = array();
        $years[] = array("id" => "0", "label" => $this->getDefaultMessageForYearFrom());
        foreach ($result as $year) {
            $years[] = array("id" => $year['id'], "label" => $year['label']);
        }
        return $years;
    }

    private function getToYears()
    {
        $result = Yii::app()->db->createCommand()
            ->select($this->getLabel() . ' label, id')
            ->from('c_lookup_property')
            ->where('c_property_id = 5')
            ->order('cast(label as unsigned) DESC')
            ->queryAll();
        $years = array();
        $years[] = array("id" => "0", "label" => $this->getDefaultMessageForYearTo());
        foreach ($result as $year) {
            $years[] = array("id" => $year['id'], "label" => $year['label']);
        }
        return $years;
    }

    private function getFromPrice()
    {
        $result = Yii::app()->db->createCommand()
            ->select('value label, id')
            ->from('c_price_options')
            ->order('cast(label as unsigned)')
            ->queryAll();
        $prices = array();
        $prices[] = array("id" => "0", "label" => $this->getDefaultMessageForPriceFrom());
        foreach ($result as $price) {
            $prices[] = array("id" => $price['id'], "label" => $price['label']);
        }
        return $prices;
    }

    private function getToPrice()
    {
        $result = Yii::app()->db->createCommand()
            ->select('value label, id')
            ->from('c_price_options')
            ->order('cast(label as unsigned)')
            ->queryAll();
        $prices = array();
        $prices[] = array("id" => "0", "label" => $this->getDefaultMessageForPriceTo());
        foreach ($result as $price) {
            $prices[] = array("id" => $price['id'], "label" => $price['label']);
        }
        return $prices;
    }

    private function getCrashed()
    {
        $crashed = array();
        $crashed[] = array("id" => "0", "label" => $this->getDefaultMessageForCrashed());
        $crashed[] = array("id" => "1", "label" => $this->getDefaultMessageForYesOption());
        $crashed[] = array("id" => "2", "label" => $this->getDefaultMessageForNoOption());

        return $crashed;
    }

    private function getCarColors()
    {
        $result = Yii::app()->db->createCommand()
            ->select($this->getLabel() . ' label, id')
            ->from('c_lookup_property')
            ->where('c_property_id = 11')
            ->order('label')
            ->queryAll();
        $prices = array();
        $prices[] = array("id" => "0", "label" => $this->getDefaultMessageForColor());
        foreach ($result as $price) {
            $prices[] = array("id" => $price['id'], "label" => $price['label']);
        }
        return $prices;
    }

    private function getGearbox()
    {
        $result = Yii::app()->db->createCommand()
            ->select($this->getLabel() . ' label, id')
            ->from('c_lookup_property')
            ->where('c_property_id = 9')
            ->order('label')
            ->queryAll();
        $gearBoxOptions = array();
        $gearBoxOptions[] = array("id" => "0", "label" => $this->getDefaultMessageForGearbox());
        foreach ($result as $price) {
            $gearBoxOptions[] = array("id" => $price['id'], "label" => $price['label']);
        }
        return $gearBoxOptions;
    }

    private function getEngineType()
    {
        $result = Yii::app()->db->createCommand()
            ->select($this->getLabel() . ' label, id')
            ->from('c_lookup_property')
            ->where('c_property_id = 13')
            ->order('label')
            ->queryAll();
        $engineOptions = array();
        $engineOptions[] = array("id" => "0", "label" => $this->getDefaultMessageForEngine());
        foreach ($result as $price) {
            $engineOptions[] = array("id" => $price['id'], "label" => $price['label']);
        }
        return $engineOptions;
    }

    private function getMileageOptions()
    {
        $result = Yii::app()->db->createCommand()
            ->select('value label, id')
            ->from('c_mileage_options')
            ->order('cast(label as unsigned)')
            ->queryAll();
        $milageOptions = array();
        $milageOptions[] = array("id" => "0", "label" => $this->getDefaultMessageForMilage());
        foreach ($result as $price) {
            $milageOptions[] = array("id" => $price['id'], "label" => $price['label']);
        }
        return $milageOptions;
    }

    private function getEngineVolumeOptions()
    {
        $result = Yii::app()->db->createCommand()
            ->select($this->getLabel() . ' label, id, start, end')
            ->from('car_engine_volume_filters')
            ->queryAll();
        $taxOptions = array();
        $taxOptions[] = array("id" => "0", "label" => $this->getDefaultMessageForTax());
        foreach ($result as $tax) {
            $taxOptions[] = array("id" => $tax['id'], "label" => $tax['label'], "start"=>$tax['start'], "end"=>$tax['end']);
        }
        return $taxOptions;
    }

    private function getTaxOptions()
    {
        $result = Yii::app()->db->createCommand()
            ->select($this->getLabel() . ' label, id')
            ->from('c_lookup_property')
            ->where('c_property_id = 26')
            ->queryAll();
        $taxOptions = array();
        $taxOptions[] = array("id" => "0", "label" => $this->getDefaultMessageForTax());
        foreach ($result as $tax) {
            $taxOptions[] = array("id" => $tax['id'], "label" => $tax['label']);
        }
        return $taxOptions;
    }

    private function getLocations()
    {
        $result = array();
        $defSubLocation = array();
        $defSubLocation[] = array("id" => "0", "label" => $this->getDefaultMessageForSubLocation());
        $result[] = array("id" => "0", "label" => $this->getDefaultMessageForLocation(), "subLocations" => $defSubLocation);
        $locations = Yii::app()->db->createCommand()
            ->select($this->getLabel() . ' label, id')
            ->from('c_location')
            ->order('label')
            ->queryAll();
        foreach ($locations as $location) {
            $subLocations = array();
            $subLocations[] = array("id" => "0", "label" => $this->getDefaultMessageForSubLocation());
            $subLocationsRes = Yii::app()->db->createCommand()
                ->select($this->getLabel() . ' label, id')
                ->from('c_sub_location')
                ->where('c_location_id =' . $location['id'])
                ->order('label')
                ->queryAll();
            if (count($subLocationsRes) > 0) {
                $subLocations[] = $subLocationsRes[0];
            }
            $result[] = array("id" => $location['id'], "label" => $location['label'], "subLocations" => $subLocations);
        }
        return $result;
    }


    public function actionGetCarFilters()
    {

        $passed_filters = json_decode($_POST['data']);
        if(is_null($passed_filters )){
            $passed_filters = array();
        }
        $where_condition = FiltersHelper::getCriteriaByFilters($passed_filters)->condition;

        $selected_filters = array();
        $available_filters = array();

        $available_filters['mark_id'] = array("filter_name" => Messages::getMessage(14), "filters" => $this->getMarks($where_condition),
            "property_name" => "mark_id", "filter_type" => "0", "property_id" => "1", "property_type" => "0");
        $available_filters['year'] = array("filter_name" => Messages::getMessage(16), "filters" => $this->getYears($where_condition),
            "property_name" => "year", "filter_type" => "0", "property_id" => "5", "property_type" => "20");
        $available_filters['price'] = array("filter_name" => Messages::getMessage(17), "filters" => $this->getPrices($where_condition),
            "property_name" => "price", "filter_type" => "3", "property_id" => "", "property_type" => "21");
        $available_filters['color_id'] = array("filter_name" => "COlor", "filters" => $this->getColors($where_condition),
            "property_name" => "color_id", "filter_type" => "0", "property_id" => "11", "property_type" => "1");
        $available_filters['gearbox_id'] = array("filter_name" => "Gear Box", "filters" => $this->getGearBoxes($where_condition),
            "property_name" => "gearbox_id", "filter_type" => "0", "property_id" => "9", "property_type" => "1");
        $available_filters['engine_id'] = array("filter_name" => "Engine", "filters" => $this->getEngines($where_condition),
            "property_name" => "engine_id", "filter_type" => "0", "property_id" => "13", "property_type" => "1");
        $available_filters['mileage'] = array("filter_name" => "Mileage", "filters" => $this->getMileages($where_condition),
            "property_name" => "mileage", "filter_type" => "3", "property_id" => "7", "property_type" => "22");
        $available_filters['engine_volume'] = array("filter_name" => "Engeane Volume", "filters" => $this->getEngineVolumes($where_condition),
            "property_name" => "engine_volume", "filter_type" => "0", "property_id" => "14", "property_type" => "1");
        $available_filters['tax_id'] = array("filter_name" => "Custom Clearance", "filters" => $this->getCustomClearances($where_condition),
            "property_name" => "tax_id", "filter_type" => "0", "property_id" => "26", "property_type" => "1");
        $available_filters['c_location_id'] = array("filter_name" => "Location", "filters" => $this->getLocationFilters($where_condition),
            "property_name" => "c_location_id", "filter_type" => "0", "property_id" => "", "property_type" => "23");
        $available_filters['body_style_id'] = array("filter_name" => "Body Style", "filters" => $this->getBodyStyles($where_condition),
            "property_name" => "body_style_id", "filter_type" => "0", "property_id" => "4", "property_type" => "1");

        $available_filters['negotiable'] = array("filter_name" => "Negotiable", "filters" => array(array("count" => "", "name" => "Negotiable", "value" => "1")),
            "property_name" => "negotiable", "filter_type" => "0", "property_id" => "24", "property_type" => "30");
        $available_filters['byExchange'] = array("filter_name" => "byExchange", "filters" => array(array("count" => "", "name" => "byExchange", "value" => "1")),
            "property_name" => "byExchange", "filter_type" => "0", "property_id" => "25", "property_type" => "30");
        $available_filters['user_role_id'] = array("filter_name" => "user_role_id", "filters" => array(array("count" => "", "name" => "Dillers", "value" => "2"), array("count" => "", "name" => "Personal", "value" => "1")),
            "property_name" => "user_role_id", "filter_type" => "4", "property_id" => "", "property_type" => "31");


        foreach ($passed_filters as $filter) {
            unset($available_filters[$filter->property_name]);

            $propertyType = $filter->property_type;
            $propertyId = $filter->property_id;

            $filterName = "";

            switch ($propertyType) {
                // year
                case 20:
                    if ($filter->filter_type == 0 || $filter->filter_type == 1 || $filter->filter_type == 2) {
                        if ($filter->filter_type == 0) {
                            $filterName = "year";
                        } else if ($filter->filter_type == 2) {
                            $filterName = "year more than";
                        } else if ($filter->filter_type == 1) {
                            $filterName = "year less    than";
                        }
                        $filterValue = $filter->value;
                        $selected_filters[] = array("filter_name" => $filterName, "filter_value" => $filterValue,
                            "property_name" => $filter->property_name, "value" => $filterValue, "filter_type" => $filter->filter_type,
                            "property_id" => $filter->property_id, "property_type" => $filter->property_type);
                    } else {
                        $filterName = "year";
                        $filterStartValue = $filter->start_value;
                        $filterEndValue = $filter->end_value;
                        $selected_filters[] = array("filter_name" => $filterName, "filter_value" => $filterStartValue . " - " . $filterEndValue,
                            "property_name" => $filter->property_name, "start_value" => $filterStartValue, "end_value" => $filterEndValue, "filter_type" => $filter->filter_type,
                            "property_id" => $filter->property_id, "property_type" => $filter->property_type);
                    }
                    break;
                //price
                case 21:
                    if ($filter->filter_type == 0 || $filter->filter_type == 1 || $filter->filter_type == 2) {
                        if ($filter->filter_type == 0) {
                            $filterName = "price";
                        } else if ($filter->filter_type == 2) {
                            $filterName = "price more than";
                        } else if ($filter->filter_type == 1) {
                            $filterName = "price less than";
                        }
                        $filterValue = $filter->value;
                        $selected_filters[] = array("filter_name" => $filterName, "filter_value" => $filterValue,
                            "property_name" => $filter->property_name, "value" => $filterValue, "filter_type" => $filter->filter_type,
                            "property_id" => $filter->property_id, "property_type" => $filter->property_type);
                    } else {
                        $filterName = "price";
                        $filterStartValue = $filter->start_value;
                        $filterEndValue = $filter->end_value;
                        $selected_filters[] = array("filter_name" => $filterName, "filter_value" => $filterStartValue . " - " . $filterEndValue,
                            "property_name" => $filter->property_name, "start_value" => $filterStartValue, "end_value" => $filterEndValue, "filter_type" => $filter->filter_type,
                            "property_id" => $filter->property_id, "property_type" => $filter->property_type);
                    }
                    break;
                //mileage
                case 22:
                    if ($filter->filter_type == 1) {
                        $filterName = "Mileage less than";
                        $filterValue = $filter->value;
                        $selected_filters[] = array("filter_name" => $filterName, "filter_value" => $filterValue,
                            "property_name" => $filter->property_name, "value" => $filterValue, "filter_type" => $filter->filter_type,
                            "property_id" => $filter->property_id, "property_type" => $filter->property_type);
                    } else {
                        $filterName = "Mileage";
                        $filterStartValue = $filter->start_value;
                        $filterEndValue = $filter->end_value;
                        $selected_filters[] = array("filter_name" => $filterName, "filter_value" => $filterStartValue . " - " . $filterEndValue,
                            "property_name" => $filter->property_name, "start_value" => $filterStartValue, "end_value" => $filterEndValue, "filter_type" => $filter->filter_type,
                            "property_id" => $filter->property_id, "property_type" => $filter->property_type);
                    }
                    break;
                //mark
                case 0:
                    $filterName = $this->getPropertyName($propertyId);
                    $filterValue = $this->getPropertyValue($filter->value);
                    $mark_id = $filter->value;
                    if ($mark_id == "") {
                        $available_filters['mark_id'] = array("filter_name" => Messages::getMessage(14), "filters" => $this->getMarks($where_condition),
                            "property_name" => "mark_id", "filter_type" => "0", "property_id" => "1", "property_type" => "0");
                    } else {
                        $available_filters['model_id'] = array("filter_name" => Messages::getMessage(15), "filters" => $this->getModels($mark_id, $where_condition),
                            "property_name" => "model_id", "filter_type" => "0", "property_id" => "2", "property_type" => "1");
                    }
                    $selected_filters[] = array("filter_name" => $filterName, "filter_value" => $filterValue,
                        "property_name" => $filter->property_name, "value" => $filterValue, "filter_type" => $filter->filter_type,
                        "property_id" => $filter->property_id, "property_type" => $filter->property_type);
                    break;
                case 1:
                    $filterName = $this->getPropertyName($propertyId);
                    $filterValue = $this->getPropertyValue($filter->value);
                    $selected_filters[] = array("filter_name" => $filterName, "filter_value" => $filterValue,
                        "property_name" => $filter->property_name, "value" => $filterValue, "filter_type" => $filter->filter_type,
                        "property_id" => $filter->property_id, "property_type" => $filter->property_type);
                    break;
                case 30:
                    $filterName = $this->getPropertyName($propertyId);
                    $filterValue = $this->getPropertyName($propertyId);
                    $selected_filters[] = array("filter_name" => $filterName, "filter_value" => $filterValue,
                        "property_name" => $filter->property_name, "value" => $filterValue, "filter_type" => $filter->filter_type,
                        "property_id" => $filter->property_id, "property_type" => $filter->property_type);
                    break;
                case 31:
                    $filterName = "User Role";
                    $filterValue = "";
                    if($filter->value == 1){
                        $filterValue = "Person";
                    }
                    if($filter->value == 2){
                        $filterValue = "Diller";
                    }
                    $selected_filters[] = array("filter_name" => $filterName, "filter_value" => $filterValue,
                        "property_name" => $filter->property_name, "value" => $filterValue, "filter_type" => $filter->filter_type,
                        "property_id" => $filter->property_id, "property_type" => $filter->property_type);
                    break;
                //location

                case 23:
                    $location_id = $filter->value;
                    $filterName = "Location";
                    $filterValue = $this->getLocationValue($filter->value);
                    $available_filters['c_sub_location_id'] = array("filter_name" => "Sub Location", "filters" => $this->getSubLocationFilters($location_id, $where_condition),
                        "property_name" => "c_sub_location_id", "filter_type" => "0", "property_id" => "", "property_type" => "24");

                    $selected_filters[] = array("filter_name" => $filterName, "filter_value" => $filterValue,
                        "property_name" => $filter->property_name, "value" => $filterValue, "filter_type" => $filter->filter_type,
                        "property_id" => $filter->property_id, "property_type" => $filter->property_type);
                    break;
                case
                24:
                    $filterName = "Sub Location";
                    $filterValue = $this->getSubLocationValue($filter->value);
                    $selected_filters[] = array("filter_name" => $filterName, "filter_value" => $filterValue,
                        "property_name" => $filter->property_name, "value" => $filterValue, "filter_type" => $filter->filter_type,
                        "property_id" => $filter->property_id, "property_type" => $filter->property_type);
                    break;
            }

        }
        $data = array("selected_filters" => $selected_filters, "available_filters" => $available_filters, "count" => $this->getAnnouncementCount($where_condition));
        echo CJSON::encode($data);
    }

    private function getAnnouncementCount($where_condition)
    {
        $result = Yii::app()->db->createCommand()
            ->select('count(car_announcement.id) as count')
            ->from('car_announcement')
            ->join("user", "user.id = car_announcement.user_id")
            ->where($where_condition)
            ->queryAll();
        return $result[0]["count"];
    }

    private
    function getPropertyName($propertyId)
    {
        $result = Yii::app()->db->createCommand()
            ->select($this->getLabel() . ' label')
            ->from('c_property')
            ->where('id = ' . $propertyId)
            ->queryAll();
        return $result[0]["label"];
    }

    private
    function getPropertyValue($propertyId)
    {
        $result = Yii::app()->db->createCommand()
            ->select($this->getLabel() . ' label')
            ->from('c_lookup_property')
            ->where('id = ' . $propertyId)
            ->queryAll();
        return $result[0]["label"];
    }

    private
    function getLocationValue($propertyId)
    {
        $result = Yii::app()->db->createCommand()
            ->select($this->getLabel() . ' label')
            ->from('c_location')
            ->where('id = ' . $propertyId)
            ->queryAll();
        return $result[0]["label"];
    }

    private
    function getSubLocationValue($propertyId)
    {
        $result = Yii::app()->db->createCommand()
            ->select($this->getLabel() . ' label')
            ->from('c_sub_location')
            ->where('id = ' . $propertyId)
            ->queryAll();
        return $result[0]["label"];
    }


    /**
     * @param $columnName
     * Method return side filter prepared json
     */
    private
    function getFilterTemplate($columnName, $sqlCondition)
    {
        return Yii::app()->db->createCommand()
            ->select("*, c_lookup_property.id value, count(car_announcement.id) count, c_lookup_property." . $this->getLabel() . " name")
            ->from("car_announcement")
            ->join("c_lookup_property", "car_announcement." . $columnName . " = c_lookup_property.id")
            ->join("user", "user.id = car_announcement.user_id")
            ->where($sqlCondition)
            ->group($columnName)
            ->queryAll();
    }


    private
    function getMarks($where_condition)
    {
        return $this->getFilterTemplate("mark_id", $where_condition);
    }

    private
    function getModels($markId, $where_condition)
    {
        return Yii::app()->db->createCommand()
            ->select("c_lookup_property.id value, c_lookup_property.father_id test, count(car_announcement.id) count, c_lookup_property." . $this->getLabel() . " name")
            ->from("car_announcement")
            ->join("c_lookup_property", "car_announcement.model_id = c_lookup_property.id")
            ->join("user", "user.id = car_announcement.user_id")
            ->where($where_condition)
            /*->where("c_lookup_property.father_id = " . $markId . " AND " . $where_condition)*/
            ->group("model_id")
            ->queryAll();
    }

    private
    function getYears($where_condition)
    {
        return Yii::app()->db->createCommand()
            ->select("*, c_lookup_property.name_eng value, count(car_announcement.id) count, c_lookup_property." . $this->getLabel() . " name")
            ->from("car_announcement")
            ->join("c_lookup_property", "car_announcement.year = c_lookup_property.name_eng")
            ->join("user", "user.id = car_announcement.user_id")
            ->where($where_condition)
            ->group("year")
            ->queryAll();


    }

    private
    function getPrices($where_condition)
    {
        return Yii::app()->db->createCommand()
            ->select("*, price_filters." . $this->getLabel() . " as name, count(price_filters.id) as count, price_filters.end as end_value, price_filters.start as start_value")
            ->from("price_filters, car_announcement")
            ->join("user", "user.id = car_announcement.user_id")
            ->where("car_announcement.price > price_filters.start and car_announcement.price <= price_filters.end " . ((is_null($where_condition) || $where_condition == "") ? "" : (" AND " . $where_condition)))
            ->group("price_filters.name_eng")
            ->queryAll();
    }

    private
    function getColors($where_condition)
    {
        return $this->getFilterTemplate("color_id", $where_condition);
    }

    private
    function getGearBoxes($where_condition)
    {
        return $this->getFilterTemplate("gearbox_id", $where_condition);
    }

    private
    function getEngines($where_condition)
    {
        return $this->getFilterTemplate("engine_id", $where_condition);
    }

    private
    function getMileages($where_condition)
    {
        return Yii::app()->db->createCommand()
            ->select("*, mileage_filters." . $this->getLabel() . " as name, count(mileage_filters.id) as count, mileage_filters.end as end_value, mileage_filters.start as start_value")
            ->from("mileage_filters, car_announcement")
            ->join("user", "user.id = car_announcement.user_id")
            ->where("car_announcement.mileage > mileage_filters.start and car_announcement.mileage <= mileage_filters.end " . ((is_null($where_condition) || $where_condition == "") ? "" : (" AND " . $where_condition)))
            ->group("mileage_filters.name_eng")
            ->queryAll();
        return $this->getFilterTemplate("mileage", $where_condition);
    }

    private
    function getEngineVolumes($where_condition)
    {
        return $this->getFilterTemplate("engine_volume", $where_condition);
    }

    private
    function getCustomClearances($where_condition)
    {
        return $this->getFilterTemplate("tax_id", $where_condition);
    }


    private
    function getLocationFilters($where_condition)
    {
        return Yii::app()->db->createCommand()
            ->select("*, c_location." . $this->getLabel() . " as name, count(c_location.id) as count, c_location.id as value")
            ->from("c_location")
            ->join("car_announcement", "car_announcement.c_location_id = c_location.id")
            ->join("user", "user.id = car_announcement.user_id")
            ->where($where_condition)
            ->group("c_location.id")
            ->queryAll();
    }

    private
    function getSubLocationFilters($locationId, $where_condition)
    {
        $where_condition = str_replace("c_location_id", "car_announcement.c_location_id", $where_condition);

        return Yii::app()->db->createCommand()
            ->select("*,  subloc." . $this->getLabel() . " as name, count(subloc.id) as count, subloc.id as value")
            ->from("c_sub_location as subloc")
            ->join("car_announcement", "car_announcement.c_sub_location_id = subloc.id")
            ->join("user", "user.id = car_announcement.user_id")
            /*->where("car_announcement.c_sub_location_id = " . $locationId . (is_null($where_condition) ? "" : (" AND " . $where_condition)))*/
            ->where($where_condition)
            ->group("subloc.id")
            ->queryAll();
    }

    private
    function getBodyStyles($where_condition)
    {
        return $this->getFilterTemplate("body_style_id", $where_condition);
    }

    public function actionInitMessageMap(){
        $messages = array();
        $messages['14'] = Messages::getMessage(14);
        $messages['15'] = Messages::getMessage(15);
        $messages['16'] = Messages::getMessage(16);
        $messages['17'] = Messages::getMessage(17);
        $messages['54'] = Messages::getMessage(54);
        $messages['49'] = Messages::getMessage(49);
        $messages['51'] = Messages::getMessage(51);
        $messages['48'] = Messages::getMessage(48);
        $messages['50'] = Messages::getMessage(50);
        $messages['47'] = Messages::getMessage(47);
        $messages['57'] = Messages::getMessage(57);
        $messages['58'] = Messages::getMessage(58);
        $messages['55'] = Messages::getMessage(55);
        $messages['76'] = Messages::getMessage(76);
        $messages['77'] = Messages::getMessage(77);
        $messages['56'] = Messages::getMessage(56);
        $messages['6'] = Messages::getMessage(6);
        $messages['78'] = Messages::getMessage(78);
        $messages['79'] = Messages::getMessage(79);
        $messages['80'] = Messages::getMessage(80);
        $messages['81'] = Messages::getMessage(81);
        $messages['53'] = Messages::getMessage(53);
        $messages['133'] = Messages::getMessage(133);




        echo CJSON::encode($messages);
    }


}