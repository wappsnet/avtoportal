<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Yerem
 * Date: 9/26/13
 * Time: 12:05 AM
 * To change this template use File | Settings | File Templates.
 */

class CarFilterHelperController extends BaseFiltersController{

    public function getDBCriteriaByCondition($addedFilters){
        $criteria = New CDbCriteria();
        $conditionAND = array();
        $conditionOR = array();
        $condition = "";
        if($addedFilters){
            foreach($addedFilters as $filter){
                if($filter->filter_type == BaseFiltersController::CONDITION_AND){
                    $conditionAND[] = $filter->filter_condition;
                }else if($filter->filter_type == BaseFiltersController::CONDITION_OR){
                    $conditionOR[] = $filter->filter_condition;
                }
            }
        }
        $conditionAND = implode(' AND ', $conditionAND);
        $conditionOR = implode(' OR ', $conditionOR);
        if($conditionAND && $conditionOR){
            $condition = $conditionAND ." AND (" . $conditionOR . ")";
        }else if($conditionAND){
            $condition = $conditionAND;
        }else if($conditionOR){
            $condition = $conditionOR;
        }

        if($this->isAdmin()){
            if($condition){
                $condition = "(".$condition.") AND t.isAccepted = 0";
            }else{
                $condition = "t.isAccepted = 0";
            }
        }else{
            if($condition){
                $condition = "(".$condition.") AND t.isAccepted = 1";
            }else{
                $condition = "t.isAccepted = 1";
            }
        }
        $criteria->order = "FLOOR((t.is_top+1)*3/12) DESC, t.created DESC";

        $criteria->condition = $condition;
        $criteria->join = "JOIN user ON user.id = t.user_id JOIN user_phone ON user.phone1_id = user_phone.id left join unregistered_users on unregistered_users.id = t.unregistered_user_id";

        return $criteria;
    }



    const FUNCTIONAL_TABLE_NAME = "car_announcement";

    public function getModelByID($id){
        return CarAnnouncement::model()->findByPk($id);
    }


    private static function markIsSelected($addedFilters){
        if($addedFilters){
            foreach($addedFilters as $filter){
                if($filter->filter_name == Messages::getMessage(14)){
                    return true;
                }
            }
        }
        return false;
    }

    private static function locationIsSelected($addedFilters){
        if($addedFilters){
            foreach($addedFilters as $filter){
                if($filter->filter_name == Messages::getMessage(57)){
                    return true;
                }
            }
        }
        return false;
    }


    public function actionGetAllFilters(){
        $addedFilters = json_decode($_POST['data']);
        $condition = $this->getDBCriteriaByCondition($addedFilters);
        $announcements = CarAnnouncement::model()->findAll($condition);
        if(!$announcements){
            $announcements = array();
        }

        $all_filters = array();
        if(CarFilterHelperController::markIsSelected($addedFilters)){
            $all_filters[] = array("filter_name" => Messages::getMessage(15), "filter_type" => "1", "filters" => DAOFilters::getModels($condition->condition, CarFilterHelperController::FUNCTIONAL_TABLE_NAME));
        }else{
            $all_filters[] = array("filter_name" => Messages::getMessage(14), "child_name" => Messages::getMessage(15), "filter_type" => "1", "filters" => DAOFilters::getMarks($condition->condition, CarFilterHelperController::FUNCTIONAL_TABLE_NAME));
        }

        $all_filters[] = array("filter_name" => Messages::getMessage(16), "filter_type" => "1", "filters" => DAOFilters::getCarYears($condition->condition, CarFilterHelperController::FUNCTIONAL_TABLE_NAME));
        $all_filters[] = array("filter_name" => Messages::getMessage(17), "filter_type" => "1", "filters" => DAOFilters::getCarPrices($condition->condition, CarFilterHelperController::FUNCTIONAL_TABLE_NAME));
        $all_filters[] = array("filter_name" => Messages::getMessage(47), "filter_type" => "1", "filters" => DAOFilters::getCustomClearances($condition->condition, CarFilterHelperController::FUNCTIONAL_TABLE_NAME));
        $all_filters[] = array("filter_name" => Messages::getMessage(48), "filter_type" => "1", "filters" => DAOFilters::getMileages($condition->condition, CarFilterHelperController::FUNCTIONAL_TABLE_NAME));
        $all_filters[] = array("filter_name" => Messages::getMessage(49), "filter_type" => "1", "filters" => DAOFilters::getGearBoxes($condition->condition, CarFilterHelperController::FUNCTIONAL_TABLE_NAME));
        $all_filters[] = array("filter_name" => Messages::getMessage(50), "filter_type" => "1", "filters" => DAOFilters::getEngineVolumes($condition->condition, CarFilterHelperController::FUNCTIONAL_TABLE_NAME));
        $all_filters[] = array("filter_name" => Messages::getMessage(134), "filter_type" => "1", "filters" => DAOFilters::getEngines($condition->condition, CarFilterHelperController::FUNCTIONAL_TABLE_NAME));
        $all_filters[] = array("filter_name" => Messages::getMessage(135), "filter_type" => "1", "filters" => DAOFilters::getEnginePowers($condition->condition, CarFilterHelperController::FUNCTIONAL_TABLE_NAME));
        $all_filters[] = array("filter_name" => Messages::getMessage(53), "filter_type" => "1", "filters" => DAOFilters::getBodyTypes($condition->condition, CarFilterHelperController::FUNCTIONAL_TABLE_NAME));
        $all_filters[] = array("filter_name" => Messages::getMessage(54), "filter_type" => "1", "filters" => DAOFilters::getColors($condition->condition, CarFilterHelperController::FUNCTIONAL_TABLE_NAME));

        $all_filters[] = array("filter_name" => Messages::getMessage(82), "filter_type" => "1", "filters" => DAOFilters::getCarRudders($condition->condition, CarFilterHelperController::FUNCTIONAL_TABLE_NAME));

        if(CarFilterHelperController::locationIsSelected($addedFilters)){
            $all_filters[] = array("filter_name" => Messages::getMessage(58), "filter_type" => "1", "filters" => DAOFilters::getSubLocations($condition->condition, CarFilterHelperController::FUNCTIONAL_TABLE_NAME));
        }else{
            $all_filters[] = array("filter_name" => Messages::getMessage(57), "child_name" => Messages::getMessage(58), "filter_type" => "1", "filters" => DAOFilters::getLocations($condition->condition, CarFilterHelperController::FUNCTIONAL_TABLE_NAME));
        }
        //TODO:$all_filters[] = array("filter_name" => "Hand Drive", "filter_type" => "1", "filters" => DAOFilters::getBodyTypes($condition->condition, CarFilterHelperController::FUNCTIONAL_TABLE_NAME));
        $all_filters[] = array("filter_name" => Messages::getMessage(56), "filter_type" => "1", "filters" => DAOFilters::getByExchange($condition->condition, CarFilterHelperController::FUNCTIONAL_TABLE_NAME));
        $all_filters[] = array("filter_name" => Messages::getMessage(55), "filter_type" => "1", "filters" => DAOFilters::getUserRoles($condition->condition, CarFilterHelperController::FUNCTIONAL_TABLE_NAME));


        $filters = $all_filters;
        if($addedFilters){
            for($i = 0; $i < count($all_filters); ++$i){
                $filter = $all_filters[$i];
                foreach($addedFilters as $addedFilter){
                    if($addedFilter->filter_name == $filter['filter_name']){
                        unset($filters[$i]);
                    }
                }
            }
        }
        $data = array("filters"=>$filters, "announcements" => $announcements, "isAdmin" => $this->isAdmin() );
        echo CJSON::encode($data);
    }



}