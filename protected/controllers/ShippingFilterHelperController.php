<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Yerem
 * Date: 9/26/13
 * Time: 12:05 AM
 * To change this template use File | Settings | File Templates.
 */

class ShippingFilterHelperController extends BaseFiltersController{
    const FUNCTIONAL_TABLE_NAME = "shipping_announcement";

    public function getModelByID($id){
        return ShippingAnnouncement::model()->findByPk($id);
    }



    public function actionGetAllFilters(){
        $addedFilters = json_decode($_POST['data']);
        $condition = ShippingFilterHelperController::getDBCriteriaByCondition($addedFilters);
        $announcements = ShippingAnnouncement::model()->findAll($condition);

        $all_filters = array();
        $all_filters[] = array("filter_name" => Messages::getMessage(63), "filter_type" => "1", "filters" => DAOFilters::getShippingTechnics($condition->condition, ShippingFilterHelperController::FUNCTIONAL_TABLE_NAME));
        $all_filters[] = array("filter_name" => Messages::getMessage(14), "filter_type" => "1", "filters" => DAOFilters::getShippingMarks($condition->condition, ShippingFilterHelperController::FUNCTIONAL_TABLE_NAME));
        $all_filters[] = array("filter_name" => Messages::getMessage(53), "filter_type" => "1", "filters" => DAOFilters::getShippingBodyTypes($condition->condition, ShippingFilterHelperController::FUNCTIONAL_TABLE_NAME));
        $all_filters[] = array("filter_name" => Messages::getMessage(64), "filter_type" => "1", "filters" => DAOFilters::getShippingJobTypes($condition->condition, ShippingFilterHelperController::FUNCTIONAL_TABLE_NAME));
        $all_filters[] = array("filter_name" => Messages::getMessage(17), "filter_type" => "1", "filters" => DAOFilters::getCarPrices($condition->condition, ShippingFilterHelperController::FUNCTIONAL_TABLE_NAME));
        $all_filters[] = array("filter_name" => Messages::getMessage(65), "filter_type" => "1", "filters" => DAOFilters::getTourismVendors($condition->condition, ShippingFilterHelperController::FUNCTIONAL_TABLE_NAME));


        $filters = $all_filters;
        if($addedFilters){
            for($i = 0; $i < count($all_filters); ++$i){
                $filter = $all_filters[$i];
                foreach($addedFilters as $addedFilter){
                    if($addedFilter->filter_name == $filter['filter_name']){
                        unset($filters[$i]);
                    }
                }
            }
        }

        $data = array("filters"=>$filters, "announcements" => $announcements, "isAdmin" => $this->isAdmin() );
        echo CJSON::encode($data);
    }

}