<?php

class UploadController extends Controller
{
    private $error = "";
    private $msg = "";
    private $fileElementName = 'qqfile';

    public function accessRules()
    {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('upload', 'cleanUpOnExit'),
                'users' => array('*'),
            ),
        );
    }

    public function actionUpload()
    {
        $sizeLimit = 80 * 1024 * 1024; // maximum file size in bytes
        /*  if(@filesize($_FILES[$this->fileElementName]['tmp_name']) > $sizeLimit){
              $this->error = 'Max file size exceeded';
          }*/
        if (!empty($_FILES[$this->fileElementName]['error'])) {
            switch ($_FILES[$this->fileElementName]['error']) {
                case '1':
                    $this->error = 'The uploaded file exceeds the upload_max_filesize directive in php.ini';
                    break;
                case '2':
                    $this->error = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
                    break;
                case '3':
                    $this->error = 'The uploaded file was only partially uploaded';
                    break;
                case '4':
                    $this->error = 'No file was uploaded.';
                    break;
                case '6':
                    $this->error = 'Missing a temporary folder';
                    break;
                case '7':
                    $this->error = 'Failed to write file to disk';
                    break;
                case '8':
                    $this->error = 'File upload stopped by extension';
                    break;
                case '999':
                default:
                    $this->error = 'No error code avaiable';
            }
        } elseif (empty($_FILES[$this->fileElementName]['tmp_name']) || $_FILES[$this->fileElementName]['tmp_name'] == 'none') {
            $this->error = 'No file was uploaded..';
        } else {
            $this->msg .= " File Name: " . $_FILES[$this->fileElementName]['name'] . ", ";
            $this->msg .= " File Size: " . @filesize($_FILES[$this->fileElementName]['tmp_name']);
            //for security reason, we force to remove all uploaded file
            // @unlink($_FILES[$this->fileElementName]);
        }
        $fileName = '';
        if (!$this->error) {
            Yii::import("ext.EAjaxUpload.qqFileUploader");
            Yii::import("application.components.SimpleImage");
            $folder = Yii::app()->getBasePath() . "/../uploads/"; // folder for uploaded files
            $allowedExtensions = array("png", "jpg", "jpeg", "gif", "mp4"); //array("jpg","jpeg","gif","exe","mov" and etc...

            $uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
            $result = $uploader->handleUpload($folder);
            if (!isset($result['error'])) {
                $fileName = $result['filename']; //GETTING FILE NAME
                $image = new SimpleImage();
                $image->load($folder . $fileName);
                $image->resizeWithRestriction(800, 600);


                $watermark = imagecreatefrompng(Yii::app()->getBasePath() . "/../images/watermark.png");
                // getting dimensions of watermark image
                $watermark_width = imagesx($watermark);
                $watermark_height = imagesy($watermark);

                $dest_x = $image->getWidth() - $watermark_width - 10;
                $dest_y = $image->getHeight() - $watermark_height - 10;
                // blending the images together
                imagealphablending($image->image, true);
                imagealphablending($watermark, true);

                // creating the new image
                imagecopy($image->image, $watermark, $dest_x, $dest_y, 0, 0, $watermark_width, $watermark_height);
                //imagejpeg($image->image);


                $image->save($folder . $fileName);

                $image->resizeWithRestriction(376, 282);
                $image->save($folder . $fileName);

                /*  $image->resizeWithRestriction(120, 100);
                  $image->save($folder ."thumb2.". $fileName);*/


                imagedestroy($watermark);
            }else{
                $this->error = $result['error'];
            }
        }


        //$tmpImage = new TmpImageTable;
        //$tmpImage->title = $fileName;
        //$tmpImage->save();

        //$result['imageId'] = $tmpImage->getPrimaryKey();
        $result['imagePath'] = Yii::app()->request->baseUrl . "/uploads/" . $fileName;
        $result['title'] = $fileName;
        $result['error'] = $this->error;
        $result['msg'] = $this->msg;
        $result['id'] = -1;


        $return = htmlspecialchars(json_encode($result), ENT_NOQUOTES);
        echo $return; // it's array
    }

    public function actionCleanUpOnExit()
    {
        $array = Yii::app()->request->getPost('ids');
        if (is_array($array)) {
            foreach ($array as $src) {
                unlink(Yii::app()->getBasePath() . "/../uploads/" . $src);
                unlink(Yii::app()->getBasePath() . "/../uploads/" . $src);
                /*unlink(Yii::app()->getBasePath() . "/../uploads/" . "thumb2.".$src);*/
            }
        }
    }

}