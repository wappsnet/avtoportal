<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Edgar
 * Date: 9/23/13
 * Time: 3:23 PM
 * To change this template use File | Settings | File Templates.
 */

class EvacuatorFormController extends BaseFormController{

    protected  function  fillSpecificData(&$announcement){
        $announcement->mark_id = $this->getPropertyByID(179);
        $announcement->district = $this->getPropertyByID(180);
        $announcement->city_block = $this->getPropertyByID(181);
        $announcement->payment_type = $this->getPropertyByID(182);
        $announcement->announcement_name = $this->getPropertyNameByID(179) . ' ' . $this->getPropertyNameByID(180);
        $announcement->phone_id = $this->getPropertyByID(248);
    }

    public function actionInsert(){
        if(isset($_POST['data'])  && isset($_POST['uploadSave']) && isset($_POST['uploadDelete'])){
            $announcementToEdit = $_POST['announcementToEdit'];
            $this->formData = json_decode($_POST['data']);
            $this->imagesToSave = json_decode($_POST['uploadSave']);
            $this->imagesToDelete = json_decode($_POST['uploadDelete']);

            $announcementModel = $this->announcementForm;

            if($announcementToEdit != 'null'){
                $current_announcement = $announcementModel::model()->findByPk($announcementToEdit);
            }else{
                $current_announcement = new $announcementModel();
            }

            $this->fillOrdinaryData($current_announcement,false);
            $this->fillUserData($current_announcement);
            $this->fillPriceDataOnlyRUR($current_announcement , 241);
            $this->fillMainImage($current_announcement);
            $this->fillSpecificData($current_announcement);

            $current_announcement->save();

            $announcement_id = $current_announcement->getPrimaryKey();

            $this->processImages($announcement_id , $announcementToEdit != 'null');
            $this->processProperties($announcement_id , $announcementToEdit != 'null');

            $summary = $this->getPropertiesSummary($announcement_id);

            $announcementModel::model()->updateByPk($announcement_id ,
                array(  'properties_summary_eng' => $summary['eng'] ,
                    'properties_summary_arm' => $summary['arm'] ,
                    'properties_summary_rus' => $summary['rus'] ,
                    'properties_summary_geo' => $summary['geo'])
            );
        }
    }

    public function init(){
        parent::init();
        $this->announcementForm = 'EvacuatorAnnouncement';
        $this->imagesForm = 'EvacuatorPhotos';
        $this->propertiesForm = 'EvacuatorProperties';

        $this->propertyIds = array(
//            'millage'    => 7,
//            'body'       => 4,
//            'color'      => 11,
//            'interior'   => 12,
//            'doors'      => 18
        );
    }



}