<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Edgar
 * Date: 5/5/14
 * Time: 12:03 AM
 * To change this template use File | Settings | File Templates.
 */

class UtilityController extends Controller {

    public function actionGroupByID(){
        $group_id = $_GET['group_id'];
        $result = array();

        $properties = $this->getPropsByGroupID($group_id);
        $result['properties'] = $properties;
        $result['id'] = $group_id;

        echo CJSON::encode($result);
    }

    public function actionAnswerGroupID() // $model is a property model
    {
        $groupID = $_POST['group_id'];
        $announcement_id = $_POST['ann_id'];
        $model = $_POST['model'];
        $result = array();

        $properties = $this->getPropAnswersByGroupID($groupID, $announcement_id, $model);
        $result['properties'] = $properties;
        $result['id'] = $groupID;

        echo CJSON::encode($result);
    }

    private function getPropAnswersByGroupID($groupID , $announcement_id , $model){
        $properties = Yii::app()->db->createCommand()->select('cp.id id , c_property_type_id , c_property_group_id , name_'.Messages::getLanguageId().' name,
        validation , show_info , label_style, controller_style , property_resolver , child_id , style , value as temp_value, announcement_id')->
            from('c_property cp')->join($model::model()->tableName() ,'cp.id = c_property_id')->where('c_property_group_id = :gr_id and announcement_id = :ann_id ',
                array('gr_id' => $groupID , 'ann_id' => $announcement_id ))->query()->readAll();

        foreach($properties as $k => &$item){
            if($item['c_property_type_id'] == 2){
                $item['lookup_items'] = Utility::getLookupByIDForAnswers($item['id'] , $groupID, $item['temp_value']);
            }
        }

        return $properties;
    }

    private function getPropsByGroupID($groupID){
        $properties = Yii::app()->db->createCommand()->select('cp.id id , c_property_type_id , c_property_group_id , name_'.Messages::getLanguageId().' name,
        validation , show_info , label_style, controller_style , property_resolver , child_id , style')->
            from('c_property cp')->where('c_property_group_id = :gr_id',array('gr_id' => $groupID ))->query()->readAll();

        foreach($properties as $k => &$item){
            if($item['c_property_type_id'] == 2){
                $item['lookup_items'] = Utility::getLookupByID($item['id'] , $groupID);
            }
        }

        return $properties;
    }

    public function actionLoadItemByID(){
        $announcement_id = $_POST['ann_id'];
        $model = $_POST['model'];
        $imageModel = $_POST['imageModel'];

        $item = $model::model()->findByPk($announcement_id);

        $item->images = $this->imageProcessing($imageModel , $announcement_id);

        echo CJSON::encode($item);
    }

    private function imageProcessing ($imageModel , $announcement_id){

        $criteria = new CDbCriteria();
        $criteria->condition = 'announcement_id = :ann_id';

        $criteria->params = array(':ann_id'=>$announcement_id);
        $images = $imageModel::model()->findAll($criteria);

        return $images;

    }


}