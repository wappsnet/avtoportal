<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Edgar
 * Date: 9/22/13
 * Time: 11:06 PM
 * To change this template use File | Settings | File Templates.
 */
class ShippingFormController extends BaseFormController{

    public function init(){
        parent::init();
        $this->announcementForm = 'ShippingAnnouncement';
        $this->imagesForm = 'ShippingPhotos';
        $this->propertiesForm = 'ShippingProperties';

        $this->propertyIds = array(
            'tech'       => 225,
            'body'       => 227,
            'type'       => 228,
            'vendor'       => 229
        );
    }


    protected  function  fillSpecificData(&$announcement){
        $announcement->techique_id = $this->getPropertyByID(225);
        $announcement->mark_id = $this->getPropertyByID(226);
        $announcement->body_id = $this->getPropertyByID(227);
        $announcement->job_type_id = $this->getPropertyByID(228);
        $announcement->vendor = $this->getPropertyByID(229);

        $announcement->announcement_name = $this->getPropertyNameByID(226) . ' ' . $this->getPropertyNameByID(225);

    }

    public function actionInsert(){
        if(isset($_POST['data'])  && isset($_POST['uploadSave']) && isset($_POST['uploadDelete'])){
            $announcementToEdit = $_POST['announcementToEdit'];
            $this->formData = json_decode($_POST['data']);
            $this->imagesToSave = json_decode($_POST['uploadSave']);
            $this->imagesToDelete = json_decode($_POST['uploadDelete']);

            $announcementModel = $this->announcementForm;

            if($announcementToEdit != 'null'){
                $current_announcement = $announcementModel::model()->findByPk($announcementToEdit);
            }else{
                $current_announcement = new $announcementModel();
            }

            $this->fillOrdinaryData($current_announcement,false);
            $this->fillUserData($current_announcement);
            $this->fillPriceDataOnlyRUR($current_announcement , 241);
            $this->fillMainImage($current_announcement);
            $this->fillSpecificData($current_announcement);

            $current_announcement->save();

            $announcement_id = $current_announcement->getPrimaryKey();

            $this->processImages($announcement_id , $announcementToEdit != 'null');
            $this->processProperties($announcement_id , $announcementToEdit != 'null');

            $summary = $this->getPropertiesSummary($announcement_id);

            $announcementModel::model()->updateByPk($announcement_id ,
                array(  'properties_summary_eng' => $summary['eng'] ,
                    'properties_summary_arm' => $summary['arm'] ,
                    'properties_summary_rus' => $summary['rus'] ,
                    'properties_summary_geo' => $summary['geo'])
            );
        }
    }

}