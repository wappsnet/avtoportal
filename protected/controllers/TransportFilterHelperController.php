<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Yerem
 * Date: 9/26/13
 * Time: 12:05 AM
 * To change this template use File | Settings | File Templates.
 */

class TransportFilterHelperController extends BaseFiltersController
{
    const FUNCTIONAL_TABLE_NAME = "transportation_announcement";

    public function getModelByID($id){
        return TransportationAnnouncement::model()->findByPk($id);
    }

    private static function countryIsSelected($addedFilters)
    {
        if ($addedFilters) {
            foreach ($addedFilters as $filter) {
                if ($filter->filter_name == Messages::getMessage(74)) {
                    return true;
                }
            }
        }
        return false;
    }



    public function actionGetAllFilters()
    {
        $addedFilters = json_decode($_POST['data']);
        $condition = TransportFilterHelperController::getDBCriteriaByCondition($addedFilters);
        $announcements = TransportationAnnouncement::model()->findAll($condition);

        $all_filters = array();

        $all_filters[] = array("filter_name" => Messages::getMessage(69), "filter_type" => "1", "filters" => DAOFilters::getTransportType($condition->condition, TransportFilterHelperController::FUNCTIONAL_TABLE_NAME));
        if (TransportFilterHelperController::countryIsSelected($addedFilters)) {
            $all_filters[] = array("filter_name" => Messages::getMessage(68), "filter_type" => "1", "filters" => DAOFilters::getTransportCity($condition->condition, TransportFilterHelperController::FUNCTIONAL_TABLE_NAME));
        } else {
            $all_filters[] = array("filter_name" => Messages::getMessage(74), "child_name" => Messages::getMessage(68), "filter_type" => "1", "filters" => DAOFilters::getTransportCountry($condition->condition, TransportFilterHelperController::FUNCTIONAL_TABLE_NAME));
        }

        $all_filters[] = array("filter_name" => Messages::getMessage(17), "filter_type" => "1", "filters" => DAOFilters::getCarPrices($condition->condition, TransportFilterHelperController::FUNCTIONAL_TABLE_NAME));
        $all_filters[] = array("filter_name" => Messages::getMessage(65), "filter_type" => "1", "filters" => DAOFilters::getTourismVendors($condition->condition, TransportFilterHelperController::FUNCTIONAL_TABLE_NAME));

        $filters = $all_filters;
        if ($addedFilters) {
            for ($i = 0; $i < count($all_filters); ++$i) {
                $filter = $all_filters[$i];
                foreach ($addedFilters as $addedFilter) {
                    if ($addedFilter->filter_name == $filter['filter_name']) {
                        unset($filters[$i]);
                    }
                }
            }
        }

        $data = array("filters" => $filters, "announcements" => $announcements, "isAdmin" => $this->isAdmin() );
        echo CJSON::encode($data);
    }

}