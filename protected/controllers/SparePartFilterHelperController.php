<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Yerem
 * Date: 9/25/13
 * Time: 9:59 PM
 * To change this template use File | Settings | File Templates.
 */

class SparePartFilterHelperController extends BaseFiltersController{

    public function getModelByID($id){
        return SparePartAnnouncement::model()->findByPk($id);
    }

    private static function markIsSelected($addedFilters){
        if($addedFilters){
            foreach($addedFilters as $filter){
                if($filter->filter_name == Messages::getMessage(14)){
                    return true;
                }
            }
        }
        return false;
    }

    private static function locationIsSelected($addedFilters){
        if($addedFilters){
            foreach($addedFilters as $filter){
                if($filter->filter_name == Messages::getMessage(57)){
                    return true;
                }
            }
        }
        return false;
    }

    private static function typeIsSelected($addedFilters){
        if($addedFilters){
            foreach($addedFilters as $filter){
                if($filter->filter_name == Messages::getMessage(113)){
                    return true;
                }
            }
        }
        return false;
    }


    public function actionGetAllFilters(){
        $addedFilters = json_decode($_POST['data']);
        $condition = SparePartFilterHelperController::getDBCriteriaByCondition($addedFilters);
        $announcements = SparePartAnnouncement::model()->findAll($condition);

        $all_filters = array();
        $all_filters[] = array("filter_name" => Messages::getMessage(53), "filter_type" => "1", "filters" => DAOFilters::getBodyTypes($condition->condition, "spare_part_announcement"));
        if(SparePartFilterHelperController::markIsSelected($addedFilters)){
            $all_filters[] = array("filter_name" => Messages::getMessage(15), "filter_type" => "1", "filters" => DAOFilters::getModels($condition->condition, "spare_part_announcement"));
        }else{
            $all_filters[] = array("filter_name" => Messages::getMessage(14), "child_name" => Messages::getMessage(15), "filter_type" => "1", "filters" => DAOFilters::getMarks($condition->condition, "spare_part_announcement"));
        }
        if(SparePartFilterHelperController::typeIsSelected($addedFilters)){
            $all_filters[] = array("filter_name" => Messages::getMessage(114), "filter_type" => "1", "filters" => DAOFilters::getSparePartSubTypes($condition->condition, "spare_part_announcement"));
        }else{
            $all_filters[] = array("filter_name" => Messages::getMessage(113), "child_name" => Messages::getMessage(114), "filter_type" => "1", "filters" => DAOFilters::getSparePartTypes($condition->condition, "spare_part_announcement"));
        }
        $all_filters[] = array("filter_name" => Messages::getMessage(112), "filter_type" => "1", "filters" => DAOFilters::getSparePartCondition($condition->condition, "spare_part_announcement"));
        $all_filters[] = array("filter_name" => Messages::getMessage(17), "filter_type" => "1", "filters" => DAOFilters::getCarPrices($condition->condition, "spare_part_announcement"));
        if(SparePartFilterHelperController::locationIsSelected($addedFilters)){
            $all_filters[] = array("filter_name" => Messages::getMessage(58), "filter_type" => "1", "filters" => DAOFilters::getSubLocations($condition->condition, "spare_part_announcement"));
        }else{
            $all_filters[] = array("filter_name" => Messages::getMessage(57), "child_name" => Messages::getMessage(58), "filter_type" => "1", "filters" => DAOFilters::getLocations($condition->condition, "spare_part_announcement"));
        }

        /*$all_filters[] = array("filter_name" => Messages::getMessage(55), "filter_type" => "1", "filters" => DAOFilters::getUserRoles($condition->condition, "spare_part_announcement"));*/


        $filters = $all_filters;
        if($addedFilters){
            for($i = 0; $i < count($all_filters); ++$i){
                $filter = $all_filters[$i];
                foreach($addedFilters as $addedFilter){
                    if($addedFilter->filter_name == $filter['filter_name']){
                        unset($filters[$i]);
                    }
                }
            }
        }

        $data = array("filters"=>$filters, "announcements" => $announcements, "isAdmin" => $this->isAdmin() );
        echo CJSON::encode($data);
    }

}