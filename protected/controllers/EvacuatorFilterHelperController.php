<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Yerem
 * Date: 9/26/13
 * Time: 12:05 AM
 * To change this template use File | Settings | File Templates.
 */

class EvacuatorFilterHelperController extends BaseFiltersController
{

    public function getModelByID($id){
        return EvacuatorAnnouncement::model()->findByPk($id);
    }

    private static function districtIsSelected($addedFilters)
    {
        if ($addedFilters) {
            foreach ($addedFilters as $filter) {
                if ($filter->filter_name == Messages::getMessage(68)) {
                    return true;
                }
            }
        }
        return false;
    }


    public function actionGetAllFilters()
    {
        $addedFilters = json_decode($_POST['data']);
        $condition = EvacuatorFilterHelperController::getDBCriteriaByCondition($addedFilters);
        $announcements = EvacuatorAnnouncement::model()->findAll($condition);

        $all_filters = array();

        if (EvacuatorFilterHelperController::districtIsSelected($addedFilters)) {
            $all_filters[] = array("filter_name" => Messages::getMessage(60), "filter_type" => "1", "filters" => DAOFilters::getEvacuatorCity($condition->condition, "evacuator_announcement"));
        } else {
            $all_filters[] = array("filter_name" => Messages::getMessage(68), "child_name" => Messages::getMessage(60), "filter_type" => "1", "filters" => DAOFilters::getEvacuatorDistricts($condition->condition, "evacuator_announcement"));
        }
        $all_filters[] = array("filter_name" => Messages::getMessage(109), "filter_type" => "1", "filters" => DAOFilters::getEvacuatorPerKMPrices($condition->condition, "evacuator_announcement"));
        $all_filters[] = array("filter_name" => Messages::getMessage(59), "filter_type" => "1", "filters" => DAOFilters::getEvacuatorPrices($condition->condition, "evacuator_announcement"));

        $filters = $all_filters;
        if ($addedFilters) {
            for ($i = 0; $i < count($all_filters); ++$i) {
                $filter = $all_filters[$i];
                foreach ($addedFilters as $addedFilter) {
                    if ($addedFilter->filter_name == $filter['filter_name']) {
                        unset($filters[$i]);
                    }
                }
            }
        }

        $data = array("filters" => $filters, "announcements" => $announcements, "isAdmin" => $this->isAdmin() );
        echo CJSON::encode($data);
    }

}