<?php

class PaymentController extends Controller
{

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
				array('allow',  // allow all users to perform 'index' and 'view' actions
						'actions'=>array('payment'),
						'users'=>array('*'),
				),
				array('allow', // allow admin user to perform 'admin' and 'delete' actions
						'actions'=>array('admin','delete'),
						'users'=>array('admin'),
				),
				array('deny',  // deny all users
						'users'=>array('*'),
				),
		);
	}


	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
	// return the filter configuration for this controller, e.g.:
	return array(
			'inlineFilterName',
			array(
					'class'=>'path.to.FilterClass',
					'propertyName'=>'propertyValue',
			),
	);
	}

	public function actions()
	{
	// return external action classes, e.g.:
	return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
					'class'=>'path.to.AnotherActionClass',
					'propertyName'=>'propertyValue',
			),
	);
	}
	*/

	public function actionPayment(){
		if(isset($_POST['payment_subbmition'])){
			$paymentType= $_POST['payment_type'];
			if($paymentType === 'arca'){
				$this->redirect(array('announcement/index'));
				
				$data_string = $this->getOrderDataForArca();
				// inti cURL
				$ch = curl_init('http://localhost/vacharq/');
				//type post
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
				//json as body
				curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
				//return parameters as result
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				//for https enabling
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
				//timeout
				curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
				//headers
				curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				'Content-Type: application/json',
				'Content-Length: ' . strlen($data_string))
				);
				$result = curl_exec($ch);
				curl_close($ch);
			}
			if($paymentType === 'paypal'){
				$this->redirect(array('announcement/index'));
				$this->redirect(array('paypal/buy'));

			}
		}
	}


	public function getOrderDataForArca(){
		$data = array("hostID" => "1",
				"mid" => "1",
				"tid" => "1",
				"additionalURL" => "someURL",
				"orderID" => "1",
				"amount" => "555.50",
				"currency" => "051",
				"opaque" => "some info for our needs"
		);
		return $data;
	}

	public function getOrderDataForArcaForMerchantCheck(){
		$data_string = '{
				"jsonrpc": "2.0",
				"method": "merchant_check",
				"params": {
				"hostID": "4321",
				"orderID": "1234",
				"amount": "555.50",
				"currency": "051",
				"mid": "1",
				"tid": "1",
				"mtpass": "agenti gaxtnabar",
				"trxnDetails": "Информация о сделке"
				},
				"id": "1"
		}';
		return $data_string;
	}

	public function actionOrder(){
		$orderId = Yii::app()->request->getPost('orderId');
		$respcode = Yii::app()->request->getPost('respcode');
		$opaque = Yii::app()->request->getPost('opaque');
		//$orderInfo;
		if($respcode === "00"){
			$orderInfo = $this->merchantCheck();
		}else{
			//TODO : addd error page with message
			$this->redirect('ERROR PAGE');
		}
	}

	public function merchantCheck(){
		$data_string = $this->getOrderDataForArcaForMerchantCheck();
		// inti cURL
		$ch = curl_init('http://localhost/vacharq/');
		//type post
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		//json as body
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		//return parameters as result
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		//for https enabling
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		//timeout
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
		//headers
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		'Content-Type: application/json',
		'Content-Length: ' . strlen($data_string))
		);
		$result = curl_exec($ch);
		
		curl_close($ch);
		
	}

}