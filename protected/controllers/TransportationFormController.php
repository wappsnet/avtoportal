<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Edgar
 * Date: 9/26/13
 * Time: 12:29 AM
 * To change this template use File | Settings | File Templates.
 */


class TransportationFormController extends BaseFormController{

    public function init(){
        parent::init();
        $this->announcementForm = 'TransportationAnnouncement';
        $this->imagesForm = 'TransportationPhotos';
        $this->propertiesForm = 'TransportationProperties';

        $this->propertyIds = array(
            'type'       => 213,
            'country'    => 214,
            'color'      => 216,
        );
    }

    protected  function  fillSpecificData(&$announcement){
        $announcement->transport_type = $this->getPropertyByID(213);
        $announcement->coutry = $this->getPropertyByID(214);
        $announcement->place = $this->getPropertyByID(215);
        $announcement->vendor = $this->getPropertyByID(216);
        $announcement->with_driver = $this->getPropertyByID(218);
        $announcement->announcement_name =  $this->getPropertyNameByID(215);
        $announcement->phone_id =  $this->getPropertyNameByID(248);
    }


    public function actionInsert(){
        if(isset($_POST['data'])  && isset($_POST['uploadSave']) && isset($_POST['uploadDelete'])){
            $announcementToEdit = $_POST['announcementToEdit'];
            $this->formData = json_decode($_POST['data']);
            $this->imagesToSave = json_decode($_POST['uploadSave']);
            $this->imagesToDelete = json_decode($_POST['uploadDelete']);

            $announcementModel = $this->announcementForm;

            if($announcementToEdit != 'null'){
                $current_announcement = $announcementModel::model()->findByPk($announcementToEdit);
            }else{
                $current_announcement = new $announcementModel();
            }

            $this->fillOrdinaryData($current_announcement,false);
            $this->fillUserData($current_announcement);
            $this->fillPriceDataOnlyRUR($current_announcement , 241);
            $this->fillMainImage($current_announcement);
            $this->fillSpecificData($current_announcement);

            $current_announcement->save();

            $announcement_id = $current_announcement->getPrimaryKey();

            $this->processImages($announcement_id , $announcementToEdit != 'null');
            $this->processProperties($announcement_id , $announcementToEdit != 'null');

            $summary = $this->getPropertiesSummary($announcement_id);

            $announcementModel::model()->updateByPk($announcement_id ,
                array(  'properties_summary_eng' => $summary['eng'] ,
                    'properties_summary_arm' => $summary['arm'] ,
                    'properties_summary_rus' => $summary['rus'] ,
                    'properties_summary_geo' => $summary['geo'])
            );
        }
    }

}