<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Edgar
 * Date: 7/13/13
 * Time: 4:17 PM
 * To change this template use File | Settings | File Templates.
 */

class SparePartWheelController extends BaseFormController {

    protected  function  fillSpecificData(&$announcement){
        $announcement->height_id = $this->getPropertyByID(153);
        $announcement->width_id = $this->getPropertyByID(152);
        $announcement->inches_id = $this->getPropertyByID(154);
        $announcement->production_id = $this->getPropertyByID(155);
        $announcement->type_id = $this->getPropertyByID(151);
        $announcement->spare_part_id = $this->getPropertyByID(145);
        $announcement->state = $this->getPropertyByID(224);
        $announcement->phone_id = $this->getPropertyByID(244);


        $announcement->announcement_name =  $this->getPropertyNameByID(155) . " " . "Шина" ;
    }



    public function actionInsert(){
        if(isset($_POST['data'])  && isset($_POST['uploadSave']) && isset($_POST['uploadDelete'])){
            $announcementToEdit = $_POST['announcementToEdit'];
            $this->formData = json_decode($_POST['data']);
            $this->imagesToSave = json_decode($_POST['uploadSave']);
            $this->imagesToDelete = json_decode($_POST['uploadDelete']);

            $announcementModel = $this->announcementForm;

            if($announcementToEdit != 'null'){
                $current_announcement = $announcementModel::model()->findByPk($announcementToEdit);
            }else{
                $current_announcement = new $announcementModel();
            }

            $this->fillOrdinaryData($current_announcement,true);
            $this->fillUserData($current_announcement);
            $this->fillPriceDataOnlyRUR($current_announcement , 157 );
            $this->fillMainImage($current_announcement);
            $this->fillSpecificData($current_announcement);

            $current_announcement->save();

            $announcement_id = $current_announcement->getPrimaryKey();

            $this->processImages($announcement_id , $announcementToEdit != 'null');
            $this->processProperties($announcement_id , $announcementToEdit != 'null');

            $summary = $this->getPropertiesSummary($announcement_id);

            $announcementModel::model()->updateByPk($announcement_id ,
                array(  'properties_summary_eng' => $summary['eng'] ,
                    'properties_summary_arm' => $summary['arm'] ,
                    'properties_summary_rus' => $summary['rus'] ,
                    'properties_summary_geo' => $summary['geo'])
            );
        }
    }

    public function init(){
        parent::init();
        $this->announcementForm = 'WheelsAnnouncement';
        $this->imagesForm = 'WheelsPhoto';
        $this->propertiesForm = 'WheelsProperties';

        $this->propertyIds = array(
            'type'    => 151,
            'width  '    => 152,
            'height'    => 153,
            'inch'    => 154,
            'prod'    => 155,
            'year'    => 200,
        );

        $this->extraProps[] = array(
            'prop_id' => 152,
            'name_arm' => 'Ширина' ,
            'name_eng' => 'Width' ,
            'name_rus' => 'Ширина' ,
            'name_geo' => 'Width' ,
        );

        $this->extraProps[] = array(
            'prop_id' => 153,
            'name_arm' => 'Высота' ,
            'name_eng' => 'Height' ,
            'name_rus' => 'Высота' ,
            'name_geo' => 'Height' ,
        );

        $this->extraProps[] = array(
            'prop_id' => 154,
            'name_arm' => 'Дьюм' ,
            'name_eng' => 'Inch' ,
            'name_rus' => 'Дьюм' ,
            'name_geo' => 'Inch' ,
        );
    }




}