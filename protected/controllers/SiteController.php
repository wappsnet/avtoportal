<?php

class SiteController extends Controller
{
    public $layout = '//layouts/column1';
    public $showAll = false;

    /**
     * Declares class-based actions.
     */
    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
                //'testLimit' => 0,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'successfullyAdded', 'dealerCarAnnouncements', 'personalPage', 'forgotPassword', 'officialDealers', 'universalDealers', 'activateForgotPassword', 'getRates', 'loadMyAnnouncements', 'myAnnouncements', 'authenticateUser', 'activateUser', 'succeedRegistered', 'auto', 'sparePart', 'autoRent', 'ruralTech', 'taxi', 'shipping', 'addAnnouncement', 'jobList', 'carRepair', 'login', 'needRegister',
                    'registration', 'news', 'loadUploads', 'view', 'select', 'create', 'captcha', 'edit', 'test2', 'trainingCar', 'evacuator', 'jobForm', 'addUser', 'listTaxiAnnouncements',
                    'changeLanguage', 'listCarAnnouncements', 'carDetailed', 'searchByFilters', 'getCarAnnouncementsByFilters', 'listCarAnnouncementsAll', 'listJobAnnouncementsAll',
                    'newsDetailed', 'appa', 'customs', 'tech', 'roadPoliceAddresses', 'autoSchool', 'generateCaptcha', 'captcha', 'listCarWheelAnnouncementsAll', 'listRuralAnnouncementsAll',
                    'listSparePartAnnouncementsAll', 'listSparePartAnnouncements', 'listRentCarAnnouncements', 'autoService', 'listEvacuatorAnnouncementsAll', 'listEvacuatorAnnouncements', 'listShippingAnnouncements', 'tourism', 'listTransportationAnnouncements',
                    'transportation', 'listTourismAnnouncements', 'wheelDetailed', 'sparePartDetailed', 'carRentDetailed', 'taxiDetailed', 'listSpecCarAnnouncements',
                    'evacuatorDetailed', 'shippingDetailed', 'transportationDetailed', 'tourismDetailed', 'ruralTechDetailed', 'jobDetailed', 'specCarDetailed', 'editForm',
                    'listAutoServices', 'autoServiceDetailed', 'getDealerCarAnnouncements', 'roadPolicePenalty', 'roadSigns', 'trafficRules', 'selectedServices',
                    'listServicesByPlace', 'listAutoServicesFromPlace', 'listAutoServicesFromSubPlace', 'getDealerMarker', 'siteRules', 'addValidDate'),
                'users' => array('*'),
            ),
        );
    }

    /** =========================================== START VIEW ACTION ===================================================== */
    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex()
    {
        $this->layout_top_bar = '//layouts/top_bar_1';
        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'
        $this->render('index', array('detail_url' => 'site/carDetailed'));
    }

    public function actionSuccessfullyAdded()
    {
        //$this->layout_top_bar = '//layouts/top_bar_1';
        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'
        $this->render('successfullyAdded', array('detail_url' => 'site/carDetailed'));
    }


    public function actionForgotPassword()
    {
        //$this->layout_top_bar = '//layouts/top_bar_1';
        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'
        $this->render('forgotPassword');
    }


    public function actionAuto()
    {
        $this->layout_top_bar = '//layouts/top_bar_1';
        $captcha = new MyCaptcha();
        $this->render('auto_form', array("editMode" => 0,  'captcha' => $captcha, 'editMode2' => false, "announcementId" => 0, "formCaption" => Messages::getMessage(18)));
    }

    private function checkPermissions($model, $id)
    {
        $user_id = $model->findByPk($id)->user_id;
        if ($user_id === Yii::app()->user->id) {
            return true;
        }
        return false;
    }

    private function expandValidDate(&$announcement, $term_id)
    {
        $currentDate = date('Y-m-d');
        if ($term_id == 57) { //1 week
            if ($announcement->validDate == null || $announcement->validDate == 'null' || $announcement->validDate == "") {
                $announcement->validDate = date('Y-m-d', strtotime($currentDate) + (24 * 3600 * 7));
                $announcement->save();
            } else {
                $announcement->validDate = date('Y-m-d', strtotime($announcement->validDate) + (24 * 3600 * 7));
                $announcement->save();
            }
        } else if ($term_id == 58) { //2 week
            if ($announcement->validDate == null || $announcement->validDate == 'null' || $announcement->validDate == "") {
                $announcement->validDate = date('Y-m-d', strtotime($currentDate) + (24 * 3600 * 14));
                $announcement->save();
            } else {
                $announcement->validDate = date('Y-m-d', strtotime($announcement->validDate) + (24 * 3600 * 14));
                $announcement->save();
            }
        } else if ($term_id == 59) { //3 week
            if ($announcement->validDate == null || $announcement->validDate == 'null' || $announcement->validDate == "") {
                $announcement->validDate = date('Y-m-d', strtotime($currentDate) + (24 * 3600 * 21));
                $announcement->save();
            } else {
                $announcement->validDate = date('Y-m-d', strtotime($announcement->validDate) + (24 * 3600 * 21));
                $announcement->save();
            }
        } else if ($term_id == 3372) { //30 days
            if ($announcement->validDate == null || $announcement->validDate == 'null' || $announcement->validDate == "") {
                $announcement->validDate = date('Y-m-d', strtotime($currentDate) + (24 * 3600 * 30));
                $announcement->save();
            } else {
                $announcement->validDate = date('Y-m-d', strtotime($announcement->validDate) + (24 * 3600 * 30));
                $announcement->save();
            }
        } else if ($term_id == 3373) { //60 days
            if ($announcement->validDate == null || $announcement->validDate == 'null' || $announcement->validDate == "") {
                $announcement->validDate = date('Y-m-d', strtotime($currentDate) + (24 * 3600 * 60));
                $announcement->save();
            } else {
                $announcement->validDate = date('Y-m-d', strtotime($announcement->validDate) + (24 * 3600 * 60));
                $announcement->save();
            }
        } else if ($term_id == 3374) { //120 days
            if ($announcement->validDate == null || $announcement->validDate == 'null' || $announcement->validDate == "") {
                $announcement->validDate = date('Y-m-d', strtotime($currentDate) + (24 * 3600 * 120));
                $announcement->save();
            } else {
                $announcement->validDate = date('Y-m-d', strtotime($announcement->validDate) + (24 * 3600 * 120));
                $announcement->save();
            }
        }
    }

    public function  actionAddValidDate()
    {
        $id = $_POST['id'];
        $formType = $_POST['formType'];
        $term_id = $_POST['term_id'];

        switch ($formType) {
            case "1":
                $announcement = CarAnnouncement::model()->findByPk($id);
                $this->expandValidDate($announcement, $term_id);
                break;
            case "2":
                $announcement = CarRentAnnouncement::model()->findByPk($id);
                $this->expandValidDate($announcement, $term_id);
                break;
            case "3":
                $announcement = WheelsAnnouncement::model()->findByPk($id);
                $this->expandValidDate($announcement, $term_id);
                break;
            case "4":
                $announcement = SparePartAnnouncement::model()->findByPk($id);
                $this->expandValidDate($announcement, $term_id);
                break;
            case "5":
                $announcement = RuraltechAnnouncement::model()->findByPk($id);
                $this->expandValidDate($announcement, $term_id);
                break;
            case "6":
                $announcement = ShippingAnnouncement::model()->findByPk($id);
                $this->expandValidDate($announcement, $term_id);
                break;
            case "7":
                $announcement = TransportationAnnouncement::model()->findByPk($id);
                $this->expandValidDate($announcement, $term_id);
                break;
            case "8":
                $announcement = TouristAnnouncement::model()->findByPk($id);
                $this->expandValidDate($announcement, $term_id);
                break;
            case "9":
                $announcement = EvacuatorAnnouncement::model()->findByPk($id);
                $this->expandValidDate($announcement, $term_id);
                break;
            case "10":
                $announcement = JobAnnouncement::model()->findByPk($id);
                $this->expandValidDate($announcement, $term_id);
                break;
            case "11":
                $announcement = TaxiAnnouncement::model()->findByPk($id);
                $this->expandValidDate($announcement, $term_id);
                break;
            case "12":
                $announcement = SpecCarAnnouncement::model()->findByPk($id);
                $this->expandValidDate($announcement, $term_id);
                break;
        }

    }

    public function actionEditForm($id, $formType)
    {
        switch ($formType) {
            case "1":
                $this->layout_top_bar = '//layouts/top_bar_1';
                if (!$this->checkPermissions(CarAnnouncement::model(), $id)) {
                    throw new CHttpException(403, 'You are not authorized to perform this action.');
                } else {
                    $captcha = new MyCaptcha();
                    $this->render('auto_form', array("editMode" => 1, 'captcha' => $captcha, 'editMode2' => false, "announcementId" => $id, "formCaption" => Messages::getMessage(18)));
                }
                break;
            case "2":
                $this->layout_top_bar = '//layouts/top_bar_1';
                if (!$this->checkPermissions(CarRentAnnouncement::model(), $id)) {
                    throw new CHttpException(403, 'You are not authorized to perform this action.');
                } else {
                    $this->render('_autoRentForm', array("editMode" => 1, "announcementId" => $id, "formCaption" => Messages::getMessage(21)));
                }
                break;
            case "3":
                $this->layout_top_bar = '//layouts/top_bar_1';
                if (!$this->checkPermissions(WheelsAnnouncement::model(), $id)) {
                    throw new CHttpException(403, 'You are not authorized to perform this action.');
                } else {
                    $this->render('_spareWheelForm', array("editMode" => 1, "announcementId" => $id, "formCaption" => Messages::getMessage(19)));
                }
                break;
            case "4":
                $this->layout_top_bar = '//layouts/top_bar_1';
                if (!$this->checkPermissions(SparePartAnnouncement::model(), $id)) {
                    throw new CHttpException(403, 'You are not authorized to perform this action.');
                } else {
                    $this->render('_spareForm', array("editMode" => 1, "announcementId" => $id, "formCaption" => Messages::getMessage(20)));
                }
                break;
            case "5":
                $this->layout_top_bar = '//layouts/top_bar_1';
                if (!$this->checkPermissions(RuraltechAnnouncement::model(), $id)) {
                    throw new CHttpException(403, 'You are not authorized to perform this action.');
                } else {
                    $this->render('_ruralTechForm', array("editMode" => 1, "announcementId" => $id, "formCaption" => Messages::getMessage(23)));
                }
                break;
            case "6":
                $this->layout_top_bar = '//layouts/top_bar_1';
                if (!$this->checkPermissions(ShippingAnnouncement::model(), $id)) {
                    throw new CHttpException(403, 'You are not authorized to perform this action.');
                } else {
                    $this->render('_shippingForm', array("editMode" => 1, "announcementId" => $id, "formCaption" => Messages::getMessage(25)));
                }
                break;
            case "7":
                $this->layout_top_bar = '//layouts/top_bar_1';
                if (!$this->checkPermissions(TransportationAnnouncement::model(), $id)) {
                    throw new CHttpException(403, 'You are not authorized to perform this action.');
                } else {
                    $this->render('_transportationForm', array("editMode" => 1, "announcementId" => $id, "formCaption" => Messages::getMessage(24)));
                }
                break;
            case "8":
                $this->layout_top_bar = '//layouts/top_bar_1';
                if (!$this->checkPermissions(TouristAnnouncement::model(), $id)) {
                    throw new CHttpException(403, 'You are not authorized to perform this action.');
                } else {
                    $this->render('_tourismForm', array("editMode" => 1, "announcementId" => $id, "formCaption" => Messages::getMessage(26)));
                }
                break;
            case "9":
                $this->layout_top_bar = '//layouts/top_bar_1';
                if (!$this->checkPermissions(EvacuatorAnnouncement::model(), $id)) {
                    throw new CHttpException(403, 'You are not authorized to perform this action.');
                } else {
                    $this->render('_evacuatorForm', array("editMode" => 1, "announcementId" => $id, "formCaption" => Messages::getMessage(27)));
                }
                break;
            case "10":
                $this->layout_top_bar = '//layouts/top_bar_1';
                if (!$this->checkPermissions(JobAnnouncement::model(), $id)) {
                    throw new CHttpException(403, 'You are not authorized to perform this action.');
                } else {
                    $this->render('_jobForm', array("editMode" => 1, "announcementId" => $id, "formCaption" => Messages::getMessage(30)));
                }
                break;
            case "11":
                $this->layout_top_bar = '//layouts/top_bar_1';
                if (!$this->checkPermissions(TaxiAnnouncement::model(), $id)) {
                    throw new CHttpException(403, 'You are not authorized to perform this action.');
                }
                else
                {
                    $this->render('_taxiForm', array("editMode" => 1, "announcementId" => $id, "formCaption" => Messages::getMessage(28)));
                }
                break;
            case "12":
                $this->layout_top_bar = '//layouts/top_bar_1';
                if (!$this->checkPermissions(SpecCarAnnouncement::model(), $id)) {
                    throw new CHttpException(403, 'You are not authorized to perform this action.');
                } else {
                    $this->render('_trainingCarForm', array("editMode" => 1, "announcementId" => $id, "formCaption" => Messages::getMessage(28)));
                }
                break;
        }
    }
    public function actionSparePartWheels()
    {
        $this->layout_top_bar = '//layouts/top_bar_1';
        $this->render('_spareWheelForm', array("editMode" => 0, "announcementId" => 0, "formCaption" => Messages::getMessage(19)));
    }
    public function actionSparePart()
    {
        $this->layout_top_bar = '//layouts/top_bar_1';
        $this->render('_spareForm', array("editMode" => 0, "announcementId" => 0, "formCaption" => Messages::getMessage(20)));
    }
    public function actionAddAnnouncement()
    {
        $this->layout_top_bar = '//layouts/top_bar_1';
//        if (Yii::app()->user->isGuest) {
//            $this->render('need_registration');
//        }
//        else
//        {
            $this->render('add_announcement', array("editMode" => 0, "announcementId" => 0));
//        }
    }
    public function actionAutoRent()
    {
        $this->layout_top_bar = '//layouts/top_bar_1';
        $this->render('_autoRentForm', array("editMode" => 0, "announcementId" => 0, "formCaption" => Messages::getMessage(21)));
    }

    public function actionAutoService()
    {
        $this->layout_top_bar = '//layouts/top_bar_1';
        $this->render('_autoServiceForm', array("editMode" => 0, "announcementId" => 0, "formCaption" => Messages::getMessage(22)));
    }

    public function actionAppa($id)
    {
        $this->layout_top_bar = '//layouts/top_bar_1';
        // $this->renderLeftPart = false;
        $this->render('_appa', array('appa_id' => $id));
    }

    public function actionCustoms()
    {
        $this->layout_top_bar = '//layouts/top_bar_1';
        $this->render('_customs');
    }

    public function actionRoadPolicePenalty()
    {
        $this->layout_top_bar = '//layouts/top_bar_1';
        $model = RoadPolicePenalty::model()->find();
        $this->render('road_police_penalties', array('model' => $model));
    }

    public function actionRoadSigns($road_sign)
    {
        $this->layout_top_bar = '//layouts/top_bar_1';

        $model = RoadPoliceRoadSigns::model()->findByPk($road_sign);
        $this->render('_roadSigns', array('model' => $model));
    }

    public function actionTrafficRules($rule_id)
    {
        $this->layout_top_bar = '//layouts/top_bar_1';

        $model = RoadTrafficRules::model()->findByPk($rule_id);
        $this->render('_trafficRules', array('model' => $model));
    }


    public function actionRuralTech()
    {
        $this->layout_top_bar = '//layouts/top_bar_1';
        $this->render('_ruralTechForm', array("editMode" => 0, "announcementId" => 0, "formCaption" => Messages::getMessage(23)));
    }

    public function actionTaxi()
    {
        $this->layout_top_bar = '//layouts/top_bar_1';
        $this->render('_taxiForm', array("editMode" => 0, "announcementId" => 0, "formCaption" => Messages::getMessage(28)));
    }

    public function actionShipping()
    {
        $this->layout_top_bar = '//layouts/top_bar_1';
        $this->render('_shippingForm', array("editMode" => 0, "announcementId" => 0, "formCaption" => Messages::getMessage(24)));
    }

    public function actionTrainingCar()
    {
        $this->layout_top_bar = '//layouts/top_bar_1';
        $this->render('_trainingCarForm', array("editMode" => 0, "announcementId" => 0, "formCaption" => Messages::getMessage(29)));
    }

    public function actionEvacuator()
    {
        $this->layout_top_bar = '//layouts/top_bar_1';
        $this->render('_evacuatorForm', array("editMode" => 0, "announcementId" => 0, "formCaption" => Messages::getMessage(27)));
    }

    public function actionJobForm()
    {
        $this->layout_top_bar = '//layouts/top_bar_1';
        $this->render('_jobForm', array("editMode" => 0, "announcementId" => 0, "formCaption" => Messages::getMessage(30)));
    }

    public function actionTourism()
    {
        $this->layout_top_bar = '//layouts/top_bar_1';
        $this->render('_tourismForm', array("editMode" => 0, "announcementId" => 0, "formCaption" => Messages::getMessage(26)));
    }

    public function actionTransportation()
    {
        $this->layout_top_bar = '//layouts/top_bar_1';
        $this->render('_transportationForm', array("editMode" => 0, "announcementId" => 0, "formCaption" => Messages::getMessage(25)));
    }

    public function actionJobList()
    {
        $this->layout_filter = '//layouts/filters';
        $this->layout_top_bar = '//layouts/top_bar_1';
        $this->render('jobList', array("editMode" => 0, "announcementId" => 0));
    }

    public function actionCarRepair()
    {
        $this->layout_top_bar = '//layouts/top_bar_1';
        $this->render('carRepair', array("editMode" => 0, "announcementId" => 0));
    }

    public function actionRegister()
    {
        $this->layout_top_bar = '//layouts/top_bar_1';
        $captcha = new MyCaptcha();
        $this->render('_registration', array('captcha' => $captcha, 'editMode' => false));
    }

    public function actionNeedRegister()
    {
        $this->layout_top_bar = '//layouts/top_bar_1';
        $this->render('need_registration');
    }

    public function actionNews()
    {
        $this->layout_top_bar = '//layouts/top_bar_1';
        $this->render('_newsList', array("editMode" => 0, "announcementId" => 0));
    }

    public function actionTech()
    {
        $this->layout_top_bar = '//layouts/top_bar_1';
        $this->render('_tech', array("editMode" => 0, "announcementId" => 0));
    }

    public function actionAutoSchool()
    {
        $this->layout_top_bar = '//layouts/top_bar_1';
        $this->render('_autoSchool', array('autoSchool_id' => 1));
    }

    public function actionRoadPoliceAddresses()
    {
        $this->layout_top_bar = '//layouts/top_bar_1';
        $model = RoadPoliceAddress::model()->find();
        $this->render('road_police_addresses', array('model' => $model));
    }


    public function actionNewsDetailed($id)
    {
        $this->layout_top_bar = '//layouts/top_bar_1';
        $this->render('news', array('news_id' => $id));
    }

    public function actionListCarAnnouncements()
    {
        $this->layout_filter = '//layouts/carFilters';
        $this->render('_listCars', array('title' => 'Автомобиль', 'detail_url' => 'site/carDetailed', 'isCar' => 1));
    }

    public function actionDealerCarAnnouncements()
    {
        $dealerId = $_GET['dealerId'];
        $this->layout_filter = '';
        $dealerImage = DealerBannerVideo::model()->findAllByAttributes(array("dealer_id" => $dealerId));
        $this->render('_listDealerAnnouncements', array('title' => 'Автомобиль', 'dealer_id' => $dealerId, 'detail_url' => 'site/carDetailed', 'dealerImages' => $dealerImage, 'isCar' => 1));
    }

    public function actionListAutoServices()
    {
        if ($this->isAdmin()) {
            $this->showAll = true;
            //$this->layout_filter = '//layouts/carFilters';
            $this->render('_listAutoServicesAdmin', array('title' => 'Авторемонтный Салон,', 'detail_url' => 'site/autoServiceDetailed', 'isCar' => 1));

        } else {
            //$this->layout_filter = '//layouts/carFilters';
            $this->render('_listAutoServices', array('detail_url' => 'site/autoServiceDetailed'));
        }
    }

    public function actionListAutoServicesFromPlace($loc_id)
    {
        $isYerevan = null;
        if ($loc_id == 1) {
            $isYerevan = true;
        } else {
            $isYerevan = false;
        }
        if ($isYerevan) {
            $this->render('_listAutoServiceByPlace', array('detail_url' => 'site/listAutoServicesFromSubPlace', 'isYerevan' => 1));
        } else {
            $this->render('_listAutoServices', array('detail_url' => 'site/autoServiceDetailed', 'loc_id' => $loc_id, 'location' => 1));
        }

    }

    public function actionListAutoServicesFromSubPlace($loc_id)
    {
        $this->render('_listAutoServices', array('detail_url' => 'site/autoServiceDetailed', 'loc_id' => $loc_id, 'location' => 0));
    }

    public function actionListServicesByPlace()
    {
        $this->render('_listAutoServiceByPlace', array('detail_url' => 'site/listAutoServicesFromPlace', 'isYerevan' => 0));
    }

    public function actionListCarAnnouncementsAll()
    {
        $this->showAll = true;
        $this->layout_filter = '//layouts/carFilters';
        $this->render('_listCars', array('title' => 'Автомобиль', 'detail_url' => 'site/carDetailed', 'isCar' => 1));
    }

    public function actionListRentCarAnnouncements()
    {
        $this->showAll = true;
        $this->layout_filter = '//layouts/carRentFilters';
        $this->render('_listRentCars', array('title' => 'Машины', 'detail_url' => 'site/carRentDetailed', 'isCar' => 0));
    }

    public function actionListCarWheelAnnouncementsAll()
    {
        $this->showAll = true;
        $this->layout_filter = '//layouts/wheelFilters';
        $this->render('_listCarWheels', array('title' => 'Колеса', 'detail_url' => 'site/wheelDetailed', 'isCar' => 0));
    }

    public function actionListSpecCarAnnouncements()
    {
        $this->showAll = true;
        $this->layout_filter = '//layouts/specCarFilters';
        $this->render('_listSpecCars', array('title' => 'Учебные машины', 'detail_url' => 'site/specCarDetailed', 'isCar' => 0));
    }

    public function actionListSparePartAnnouncements()
    {
        $this->layout_filter = '//layouts/sparePartFilters';
        $this->render('_listSpareParts', array('title' => 'Запчасти', 'detail_url' => 'site/sparePartDetailed', 'isCar' => 0));
    }

    public function actionListSparePartAnnouncementsAll()
    {
        $this->showAll = true;
        $this->layout_filter = '//layouts/sparePartFilters';
        $this->render('_listSpareParts', array('title' => 'Запчасти', 'detail_url' => 'site/sparePartDetailed', 'isCar' => 0));
    }

    public function actionListRuralAnnouncementsAll()
    {
        $this->showAll = true;
        $this->layout_filter = '//layouts/ruralFilters';
        $this->render('_listRural', array('title' => 'Строительная техника', 'detail_url' => 'site/ruralTechDetailed', 'isCar' => 0));

    }

    public function actionListJobAnnouncementsAll()
    {
        $this->showAll = true;
        $this->layout_filter = '//layouts/jobFilters';
        $this->render('_listJob', array('title' => 'Работа', 'detail_url' => 'site/jobDetailed', 'isCar' => 0));
    }

    public function actionListTaxiAnnouncements()
    {
        $this->showAll = true;
        $this->layout_filter = '//layouts/taxiFilters';
        $this->render('_listTaxi', array('title' => 'Такси', 'detail_url' => 'site/taxiDetailed', 'isCar' => 0));
    }

    public function actionListTransportationAnnouncements()
    {
        $this->showAll = true;
        $this->layout_filter = '//layouts/transportFilters';
        $this->render('_listTransportation', array('title' => 'Пассажирские перевозки', 'detail_url' => 'site/transportationDetailed', 'isCar' => 0));
    }

    public function actionListTourismAnnouncements()
    {
        $this->showAll = true;
        $this->layout_filter = '//layouts/tourizmFilters';
        $this->render('_listTourism', array('title' => 'Туризм ', 'detail_url' => 'site/tourismDetailed', 'isCar' => 0));
    }

    public function actionListEvacuatorAnnouncementsAll()
    {
        $this->showAll = true;
        $this->layout_filter = '//layouts/evacuatorFilters';
        $this->render('_listEvacuator', array('title' => 'Эвакуатор', 'detail_url' => 'site/evacuatorDetailed', 'isCar' => 0));
    }

    public function actionListEvacuatorAnnouncements()
    {
        $this->layout_filter = '//layouts/evacuatorFilters';
        $this->render('_listEvacuator', array('title' => 'Эвакуатор', 'detail_url' => 'site/evacuatorDetailed', 'isCar' => 0));
    }

    public function actionListShippingAnnouncements()
    {
        $this->showAll = true;
        $this->layout_filter = '//layouts/shippingFilters';
        $this->render('_listShipping', array('title' => 'Перевозка грузов', 'detail_url' => 'site/shippingDetailed', 'isCar' => 0));
    }

    public function actionCarDetailed($id)
    {
        $this->layout_top_bar = '//layouts/top_bar_1';
        $this->renderLeftPart = false;


        $model = CarAnnouncement::model()->findByPk($id);
        $model->view_count = $model->view_count + 1;
        $model->save();

        $criteria = new CDbCriteria();
        $criteria->limit = 30;
        $criteria->order = 'id DESC';
        $criteria->condition = "mark_id = " . $model->mark_id;
        $models = CarAnnouncement::model()->findAll($criteria);
        while (count($models) < 6) {
            $models = array_merge((array)$models, (array)$models);
        }

        $this->render('_carDetailed', array('car_id' => $id, 'models' => $models));
    }

    public function actionSpecCarDetailed($id)
    {
        $this->layout_top_bar = '//layouts/top_bar_1';
        $this->renderLeftPart = false;

        $model = SpecCarAnnouncement::model()->findByPk($id);
        $model->view_count = $model->view_count + 1;
        $model->save();

        $this->render('_detailedSpecCar', array('car_id' => $id));
    }

    public function actionWheelDetailed($id)
    {
        $this->layout_top_bar = '//layouts/top_bar_1';
        $this->renderLeftPart = false;

        $model = TransportationAnnouncement::model()->findByPk($id);
        $model->view_count = $model->view_count + 1;
        $model->save();

        $this->render('_detailedWheel', array('car_id' => $id));
    }

    public function actionSparePartDetailed($id)
    {
        $this->layout_top_bar = '//layouts/top_bar_1';
        $this->renderLeftPart = false;

        $model = SparePartAnnouncement::model()->findByPk($id);
        $model->view_count = $model->view_count + 1;
        $model->save();

        $this->render('_detailedSparePart', array('car_id' => $id));
    }

    public function actionCarRentDetailed($id)
    {
        $this->layout_top_bar = '//layouts/top_bar_1';
        $this->renderLeftPart = false;

        $model = CarRentAnnouncement::model()->findByPk($id);
        $model->view_count = $model->view_count + 1;
        $model->save();

        $this->render('_detailedCarRent', array('car_id' => $id));
    }

    public function actionAutoServiceDetailed($id)
    {
        $this->layout_top_bar = '//layouts/top_bar_1';
        $this->renderLeftPart = false;
        $this->render('_detailedAutoService', array('car_id' => $id));
    }

    public function actionSelectedServices()
    {
        $ann_id = $_POST['announcement_id'];
        $criteria = new CDbCriteria;
        $criteria->condition = 'announcement_id = :ann_id';
        $criteria->params = array(':ann_id' => $ann_id);
        $selected_services = SelectedServices::model()->findAll($criteria);

        echo CJSON::encode($selected_services);

    }

    public function actionTaxiDetailed($id)
    {
        $this->layout_top_bar = '//layouts/top_bar_1';
        $this->renderLeftPart = false;

        $model = TaxiAnnouncement::model()->findByPk($id);
        $model->view_count = $model->view_count + 1;
        $model->save();

        $this->render('_detailedTaxi', array('car_id' => $id));
    }

    public function actionEvacuatorDetailed($id)
    {
        $this->layout_top_bar = '//layouts/top_bar_1';
        $this->renderLeftPart = false;

        $model = EvacuatorAnnouncement::model()->findByPk($id);
        $model->view_count = $model->view_count + 1;
        $model->save();

        $this->render('_detailedEvacuator', array('car_id' => $id));
    }

    public function actionShippingDetailed($id)
    {
        $this->layout_top_bar = '//layouts/top_bar_1';
        $this->renderLeftPart = false;

        $model = ShippingAnnouncement::model()->findByPk($id);
        $model->view_count = $model->view_count + 1;
        $model->save();

        $this->render('_detailedShipping', array('car_id' => $id));
    }

    public function actionTransportationDetailed($id)
    {
        $this->layout_top_bar = '//layouts/top_bar_1';
        $this->renderLeftPart = false;

        $model = TransportationAnnouncement::model()->findByPk($id);
        $model->view_count = $model->view_count + 1;
        $model->save();

        $this->render('_detailedTransportation', array('car_id' => $id));
    }

    public function actionTourismDetailed($id)
    {
        $this->layout_top_bar = '//layouts/top_bar_1';
        $this->renderLeftPart = false;

        $model = TouristAnnouncement::model()->findByPk($id);
        $model->view_count = $model->view_count + 1;
        $model->save();

        $this->render('_detailedTourism', array('car_id' => $id));
    }

    public function actionRuralTechDetailed($id)
    {
        $this->layout_top_bar = '//layouts/top_bar_1';
        $this->renderLeftPart = false;

        $model = RuraltechAnnouncement::model()->findByPk($id);
        $model->view_count = $model->view_count + 1;
        $model->save();

        $this->render('_detailedRuralTech', array('car_id' => $id));
    }

    public function actionJobDetailed($id)
    {
        $this->layout_top_bar = '//layouts/top_bar_1';
        $this->renderLeftPart = false;

        $model = JobAnnouncement::model()->findByPk($id);
        $model->view_count = $model->view_count + 1;
        $model->save();


        $this->render('_detailedJob', array('car_id' => $id));
    }

    public function actionMyAnnouncements()
    {
        //$this->layout_top_bar = '//layouts/top_bar_2';
        $this->renderLeftPart = false;
        $this->render('my_announcements');
    }

    public function actionOfficialDealers()
    {
        //$this->layout_top_bar = '//layouts/top_bar_2';
        $this->renderLeftPart = true;
        $this->render('officialDealers', array('isOfficial' => true));
    }

    public function actionGetOfficialDealers()
    {
        $dealers = Yii::app()->db->createCommand()
            ->select('user.id as id, user.organisation_name as organisation_name, dealer_logo.logo_name as logo_name ,longitude , latitude ')
            ->from('user')
            ->join("dealer_logo", "user.id = dealer_logo.dealer_id")
            ->where("user.user_roles_id = 3")
            ->queryAll();
        echo CJSON::encode($dealers);
    }

    public function actionUniversalDealers()
    {
        //$this->layout_top_bar = '//layouts/top_bar_2';
        $this->renderLeftPart = true;
        $this->render('officialDealers', array('isOfficial' => false));
    }

    public function actionGetUniversalDealers()
    {
        $dealers = Yii::app()->db->createCommand()
            ->select('user.id as id, user.organisation_name as organisation_name, dealer_logo.logo_name as logo_name ,longitude , latitude ')
            ->from('user')
            ->join("dealer_logo", "user.id = dealer_logo.dealer_id")
            ->where("user.user_roles_id = 2")
            ->queryAll();
        echo CJSON::encode($dealers);
    }

    public function actionPersonalPage()
    {
        $this->layout_top_bar = '//layouts/top_bar_1';
        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'
        $this->render('_registration', array("editMode2" => true));
    }

    /** =========================================== END VIEW ACTION ===================================================== */

    /** =========================================== Filters  ===================================================== */
    public function actionSearchByFilters()
    {
        if (isset($_POST['data'])) {
            $filters = json_decode($_POST['data']);
            $criteria = FiltersHelper::getCriteriaByFilters($filters);
            $announcements = CarAnnouncement::model()->findAll($criteria);
            $announcementsJson = CJSON::encode($announcements);
            echo $announcementsJson;

        }
    }


    /** =========================================== Filters  ===================================================== */


    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    public function actionSiteRules()
    {
        $model = SiteRules::model()->findByPk(1);
        $this->render('site_rules', array('model' => $model));
    }

    /**
     * Displays the contact page
     */
    public function actionContact()
    {
        $model = new ContactForm;
        if (isset($_POST['ContactForm'])) {
            $model->attributes = $_POST['ContactForm'];
            if ($model->validate()) {
                $name = '=?UTF-8?B?' . base64_encode($model->name) . '?=';
                $subject = '=?UTF-8?B?' . base64_encode($model->subject) . '?=';
                $headers = "From: $name <{$model->email}>\r\n" .
                    "Reply-To: {$model->email}\r\n" .
                    "MIME-Version: 1.0\r\n" .
                    "Content-type: text/plain; charset=UTF-8";

                mail(Yii::app()->params['adminEmail'], $subject, $model->body, $headers);
                Yii::app()->user->setFlash('contact', 'Thank you for contacting us. We will respond to you as soon as possible.');
                $this->refresh();
            }
        }
        $this->render('contact', array('model' => $model));
    }

    public function actionGenerateCaptcha()
    {
        $width = 100; //Ширина изображения
        $height = 60; //Высота изображения
        $font_size = 16; //Размер шрифта
        $let_amount = 4; //Количество символов, которые нужно набрать
        $fon_let_amount = 30; //Количество символов на фоне
        $font = realpath('.') . "/css/fonts/uh.ttf"; //Путь к шрифту
        $letters = array("a", "b", "c", "d", "e", "f", "g");
        $colors = array("90", "110", "130", "150", "170", "190", "210");
        $src = imagecreatetruecolor($width, $height); //создаем изображение
        $fon = imagecolorallocate($src, 255, 255, 255); //создаем фон
        imagefill($src, 0, 0, $fon); //заливаем изображение фоном
        for ($i = 0; $i < $fon_let_amount; $i++) { //добавляем на фон буковки
            $color = imagecolorallocatealpha($src, rand(0, 255), rand(0, 255), rand(0, 255), 100);
            $letter = $letters[rand(0, sizeof($letters) - 1)];
            $size = rand($font_size - 2, $font_size + 2);
            imagettftext($src, $size, rand(0, 45),
                rand($width * 0.1, $width - $width * 0.1),
                rand($height * 0.2, $height), $color, $font, $letter);
        }

        for ($i = 0; $i < $let_amount; $i++) { //то же самое для основных букв
            $color = imagecolorallocatealpha($src, $colors[rand(0, sizeof($colors) - 1)],
                $colors[rand(0, sizeof($colors) - 1)],
                $colors[rand(0, sizeof($colors) - 1)], rand(20, 40));
            $letter = $letters[rand(0, sizeof($letters) - 1)];
            $size = rand($font_size * 2 - 2, $font_size * 2 + 2);
            $x = ($i + 1) * $font_size + rand(1, 5); //даем каждому символу случайное смещение
            $y = (($height * 2) / 3) + rand(0, 5);
            $cod[] = $letter; //запоминаем код
            imagettftext($src, $size, rand(0, 15), $x, $y, $color, $font, $letter);
        }

        $cod = implode("", $cod); //переводим код в строку
        Yii::app()->session['captcha_code'] = $cod;
        header("Content-type: image/gif"); //выводим готовую картинку
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        imagegif($src);
        imagedestroy($src);

    }

    public function actionSucceedRegistered()
    {
        $this->render('_registration_succeed', array('registered' => false));
    }

    public function actionLogin()
    {
        $userJSON = json_decode(file_get_contents("php://input"));
        $userId = new UserIdentity($userJSON->email, $userJSON->password);
        if ($userId->authenticate()) {
            Yii::app()->user->login($userId);
            echo CJSON::encode($userId);
            //echo 'success'; //TODO change
        } else {
            echo 'error';
        }
    }

    public function actionAuthenticateUser()
    {
        if (!Yii::app()->user->isGuest) {
            echo CJSON::encode(array("user_name" => Yii::app()->user->name, "registered" => true));
        } else {
            echo CJSON::encode("");
        }
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout()
    {
        Yii::app()->user->logout();
        //$this->redirect(Yii::app()->homeUrl);
    }

    public function actionActivateUser($usUi)
    {
        $userId = UserMailService::decryptIt($usUi);
        $user = User::model()->findByPk($userId);
        if (isset($user)) {
            if ($user->user_status == 1) {
                $this->actionIndex();
            } else {
                $user->user_status = 1;
                $user->save();
                $messageBody = Messages::getMessage(38);
                $messageBody .= "<br/>";
                $messageBody .= Messages::getMessage(39);
                $messageBody .= "<br/>";
                $messageBody .= Messages::getMessage(40) . " : " . $user->email;
                $messageBody .= "<br/>";
                $messageBody .= Messages::getMessage(41) . " : " . UserMailService::decryptIt($user->password);
                UserMailService::sendMail("avtomeqena.am", $user->email, $messageBody);

                $this->render('_registration_succeed', array('registered' => true));
            }

        }
    }

    //public function action

    public function actionAddUser()
    {
        $fileName = $this->addUpload();
        if (!is_null($fileName)) {
            $user = new User();
            $user->is_diller = self::returnCheckingAvailability($_POST, 'personal') ? 0 : 1;
            $user->is_phone1_mobile = self::returnCheckingAvailability($_POST, 'is_phone1_mobile') ? 0 : 1;
            $user->is_official = ($user->is_diller && self::returnCheckingAvailability($_POST, 'official')) ? 1 : 0;
            $user->email = self::returnCheckingAvailability($_POST, 'email');
            $user->password = crypt(self::returnCheckingAvailability($_POST, 'password'));
            $user->first_name = self::returnCheckingAvailability($_POST, 'first_name');
            $user->show_name = self::returnCheckingAvailability($_POST, 'show_name');
            $user->organisation = self::returnCheckingAvailability($_POST, 'organisation');
            $user->address = self::returnCheckingAvailability($_POST, 'address');
            $user->site = self::returnCheckingAvailability($_POST, 'site');
            $user->phone1 = self::returnCheckingAvailability($_POST, 'phone1');
            $user->logo_name = $fileName;
            if ($user->save()) {
                $this->actionIndex();
            }
        }
    }

    private function addUpload()
    {
        if ($_FILES["image"]["error"] > 0) {
            return null;
        } else {
            $folder = 'uploads/';
            $file_name = time() . $_FILES["image"]["name"];
            move_uploaded_file($_FILES["image"]["tmp_name"], $folder . $file_name);
            Yii::import("application.components.SimpleImage");
            $image = new SimpleImage();
            $image->load($folder . $file_name);
            $image->resizeWithRestriction(1024, 1024);
            $image->save($folder . $file_name);
            $image->resizeWithRestriction(300, 300);
            $image->save($folder . 'thumbs/' . 'thumb.' . $file_name);
            return $file_name;
        }
    }

    /**
     * @param array $array
     * @param string $key
     * @return string $value
     */
    private static function returnCheckingAvailability($array, $key)
    {
        return isset($array[$key]) ? $array[$key] : null;
    }

    public function actionLoadUploads($formType)
    {
        //:TODO add load logic
        if ($formType == 'auto') {

        } else if ($formType == 'sparePart') {

        }

        $uploads = array();
        return CJSON::encode($uploads);
    }


    public function actionLoadMyAnnouncements()
    {
        $user_id = Yii::app()->user->id;
        $list = Yii::app()->db->createCommand("(select announcement_name,  " . "'" . Messages::getMessage(83) . "'" . " as announcement_type, 1 as announcement_type_id,  id, announcement_name as description, price as price, term_id, created as date_created , validDate
            from car_announcement
            where user_id = :user_id)
            union
            (select announcement_name,  " . "'" . Messages::getMessage(86) . "'" . " as announcement_type, 2 as announcement_type_id, id, announcement_name as description, price as price,term_id, created as date_created , validDate
            from car_rent_announcement
            where user_id = :user_id)
            union
            (select announcement_name, " . "'" . Messages::getMessage(84) . "'" . " as announcement_type, 3 as announcement_type_id, id, announcement_name as description, price as price,term_id, created as date_created , validDate
            from wheels_announcement
            where user_id = :user_id)
            union
            (select announcement_name, " . "'" . Messages::getMessage(85) . "'" . " as announcement_type, 4 as announcement_type_id, id, announcement_name as description, price as price,term_id, created as date_created , validDate
            from spare_part_announcement
            where user_id = :user_id)
            union
            (select announcement_name, " . "'" . Messages::getMessage(88) . "'" . " as announcement_type, 5 as announcement_type_id, id, announcement_name as description, price as price,term_id, created as date_created , validDate
            from ruraltech_announcement
            where user_id = :user_id)
            union
            (select announcement_name, " . "'" . Messages::getMessage(89) . "'" . " as announcement_type, 6 as announcement_type_id, id, announcement_name as description, price as price,term_id, created as date_created , validDate
            from shipping_announcement
            where user_id = :user_id)
            union
            (select announcement_name, " . "'" . Messages::getMessage(90) . "'" . " as announcement_type, 7 as announcement_type_id, id, announcement_name as description, price as price,term_id, created as date_created , validDate
            from transportation_announcement
            where user_id = :user_id)
            union
            (select announcement_name, " . "'" . Messages::getMessage(91) . "'" . " as announcement_type,8 as announcement_type_id, id, announcement_name as description, price as price,term_id, created as date_created , validDate
            from tourist_announcement
            where user_id = :user_id)
            union
            (select announcement_name, " . "'" . Messages::getMessage(92) . "'" . " as announcement_type,9 as announcement_type_id, id, announcement_name as description, price as price,term_id, created as date_created ,validDate
            from evacuator_announcement
            where user_id = :user_id)
            union
            (select announcement_name, " . "'" . Messages::getMessage(94) . "'" . " as announcement_type,12 as announcement_type_id, id, announcement_name as description, price as price,term_id, created as date_created , validDate
            from spec_car_announcement
            where user_id = :user_id)
            union
            (select announcement_name, " . "'" . Messages::getMessage(95) . "'" . " as announcement_type,10 as announcement_type_id, id, announcement_name as description, price as price,term_id, created as date_created , validDate
            from job_announcement
            where user_id = :user_id)
            union
            (select announcement_name, " . "'" . Messages::getMessage(93) . "'" . " as announcement_type,11 as announcement_type_id, term_id, taxi_announcement.id as id, c_lookup_property.name_eng as description, price_per_km as price, created as date_created , validDate
            from taxi_announcement join c_lookup_property on c_lookup_property.id = taxi_announcement.mark_id
            where taxi_announcement.user_id = :user_id)")->bindValue("user_id", $user_id)->queryAll();
        echo CJSON::encode($list);
    }

    public function actionGetRates()
    {
        $list = Yii::app()->db->createCommand("select rate, value from rates_from_cba")->queryAll();
        echo CJSON::encode($list);
    }

    public function actionGetTerms()
    {
        $list = Yii::app()->db->createCommand("select 'выбирать' as label, -55 as value union (select name_" . Messages::getLanguageId() . " as label, id as value from c_lookup_property where c_property_id = 212)")->queryAll();
        echo CJSON::encode($list);
    }


    public function actionRequestNewPassword()
    {
        $email = json_decode($_POST['data']);
        $user = User::model()->findByAttributes(array('email' => $email));
        if (isset($user)) {
            $user->password = UserMailService::encryptIt($this->getRandomString());
            //$user->user_status = 0;
            $user->save();

            $messageBody = "<b>";
            $messageBody .= Messages::getMessage(184);
            $messageBody .= "</b>";
            $messageBody .= "<br/>";
            $messageBody .= "<br/>";
            //$link = Yii::app()->request->hostInfo . Yii::app()->request->baseUrl . '/activateForgotPassword?usUi=' . urlencode(UserMailService::encryptIt($user->id));
            //$messageBody.= $link;
            $messageBody .= "<br/>";
            $messageBody .= "<b>";
            $messageBody .= Messages::getMessage(185);
            $messageBody .= "</b>";
            $messageBody .= "<br/>";
            $messageBody .= Messages::getMessage(40) . " : " . $user->email;
            $messageBody .= "<br/>";
            $messageBody .= Messages::getMessage(41) . " : " . UserMailService::decryptIt($user->password);
            $messageBody .= "<br/>";
            $messageBody .= "<br/>";
            $messageBody .= Messages::getMessage(186);
            $messageBody .= "<br/>";
            $messageBody .= Messages::getMessage(187);
            UserMailService::sendMail("avtomeqena.am", $user->email, $messageBody);
        }
    }

    public function actionActivateForgotPassword($usUi)
    {
        $userId = UserMailService::decryptIt($usUi);
        $user = User::model()->findByPk($userId);
        if (isset($user)) {
            $user->user_status = 1;
            $user->save();
            $this->actionIndex();
        }
    }


    public function getRandomString($length = 8)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $string = '';

        for ($i = 0; $i < $length; $i++) {
            $string .= $characters[mt_rand(0, strlen($characters) - 1)];
        }

        return $string;
    }

    public function actionGetImediateAutos()
    {
        $amendments = CarAnnouncement::model()->findAllByAttributes(array('is_top' => 2));
        $autos = array();
        $currentAuto = array();

        foreach ($amendments as $auto) {
            $currentAuto['id'] = $auto->id;
            $currentAuto['name'] = CLookupProperty::model()->findByPk($auto->mark_id)->name_eng . ' ' . CLookupProperty::model()->findByPk($auto->model_id)->name_eng;
            $currentAuto['year'] = $auto->year;
            $currentAuto['main_image'] = $auto->main_image;
            $currentAuto['price'] = $auto->price;

            if ($auto->rate_id == 1) {
                $currentAuto['price'] = $currentAuto['price'] . ' ' . 'RUR';
            } else if ($auto->rate_id == 2) {
                $currentAuto['price'] = $currentAuto['price'] . ' ' . 'USD';
            } else if ($auto->rate_id == 3) {
                $currentAuto['price'] = $currentAuto['price'] . ' ' . 'EUR';
            }
            $autos[] = $currentAuto;

        }
        echo CJSON::encode($autos);
    }

    public function actionGetDealerCarAnnouncements()
    {
        $dealerId = $_POST['dealerId'];

        $criteria = New CDbCriteria();

        if ($this->isAdmin()) {
            $condition = "isAccepted = 0";
        } else {
            $condition = "isAccepted = 1";
        }
        $condition = $condition . " AND user.id = :user_id";
        $criteria->condition = $condition;
        $criteria->params = array(':user_id' => $dealerId);
        $criteria->join = "JOIN user ON user.id = user_id JOIN user_phone ON user.phone1_id = user_phone.id";

        echo CJSON::encode(CarAnnouncement::model()->findAll($criteria));


        /* $announcements = Yii::app()->db->createCommand()->select('count(car_photos.announcement_id) as imageCount, main_image, rate_id, car_announcement.id as id, user.id  us_id , user_roles_id , price ,
          properties_summary_eng , properties_summary_rus , properties_summary_arm , properties_summary_geo , announcement_name')->
             from(CarAnnouncement::model()->tableName())->
             join('user','user.id = car_announcement.user_id')->
             join('car_photos','car_announcement.id = car_photos.announcement_id')->
             group('car_photos.announcement_id')->
             where('user.id = :us_id',array('us_id'=>(int)$dealerId ))->
             query()->readAll();


         echo CJSON::encode($announcements);*/
    }

    /* public function actionNewsByIndex(){
         $index = $_POST['currentIndex'];
         $news = Yii::app()->db->createCommand('SELECT t.* FROM news t JOIN ( SELECT l.created FROM news l GROUP BY l.created ORDER BY l.created DESC LIMIT '.$index.',1) m ON m.created = t.created')->queryRow();
         $size = Yii::app()->db->createCommand('select count(id) as count from news')->queryRow();
         echo CJSON::encode(array('news'=>$news, 'size'=>$size));
     }*/

    public function actionTopNews()
    {
        $criteria = new CDbCriteria();
        $criteria->limit = 30;
        $news = News::model()->findAll($criteria);
        echo CJSON::encode($news);
    }


    public function actionSendMailToSeller()
    {
        if (isset($_POST['data']) && isset($_POST['userMail'])) {
            $userData = json_decode($_POST['data']);
            $userMail = $_POST['userMail'];
            $messageBody = "Sender Contacts. <br/>";
            $messageBody .= "Name :" . $userData->name;
            $messageBody .= "<br/>";
            $messageBody .= "Email :" . $userData->email;
            $messageBody .= "<br/>";
            $messageBody .= "Phone Number :" . $userData->phoneNumber;
            $messageBody .= "<br/>";
            $messageBody .= "<br/>";
            $messageBody .= "<h3>Message</h3>";
            $messageBody .= $userData->message;
            UserMailService::sendMail("avtomeqena.am", $userMail, $messageBody);
            echo true;
        }
    }

    public function actionGetDealerMarker()
    {
        $dealerId = $_POST['dealerId'];
        $marker = array();
        $dealer = User::model()->findByPK($dealerId);

        $marker['longitude'] = $dealer->longitude;
        $marker['latitude'] = $dealer->latitude;

        echo CJSON::encode($marker);


    }

    public function actionSortingOptions()
    {
        $criteria = new CDbCriteria();
        $criteria->condition = 'id <> 1 AND id <> 2';
        echo CJSON::encode(SortingOptions::model()->findAll($criteria));
    }


}