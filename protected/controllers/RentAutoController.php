<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Edgar
 * Date: 7/14/13
 * Time: 10:52 PM
 * To change this template use File | Settings | File Templates.
 */

class RentAutoController extends BaseFormController {

    public function init(){
        parent::init();
        $this->announcementForm = 'CarRentAnnouncement';
        $this->imagesForm = 'CarRentPhotos';
        $this->propertiesForm = 'CarRentProperties';

        $this->propertyIds = array(
            'millage'    => 7,
            'body'       => 4,
            'color'      => 11,
            'interior'   => 12,
            'doors'      => 18,
            'hand drive' => 10,
            'gearbox'    => 9,
            'engine'     => 13,
            'power'      => 15,
            'cylinders'  => 16,
            'Train'      => 17,
            'wheels'     => 19
        );
    }

    protected  function  fillSpecificData(&$announcement){

        $announcement->mark_id = $this->getPropertyByID(1);
        $announcement->model_id = $this->getPropertyByID(2);
        $announcement->body_style_id = $this->getPropertyByID(4);
        $announcement->year = $this->getPropertyNameByID(5);
        $announcement->mileage = $this->getPropertyByID(7);
        $announcement->gearbox_id = $this->getPropertyByID(9);
        $announcement->color_id = $this->getPropertyByID(11);
        $announcement->engine_id = $this->getPropertyByID(13);
        $announcement->engine_volume = $this->getPropertyByID(14);
        $announcement->announcement_name = $this->getPropertyNameByID(5) .' '. $this->getPropertyNameByID(1) . ' ' .  $this->getPropertyNameByID(2);
        $announcement->engine_power = $this->getPropertyByID(15);
        $announcement->payment_type_id = $this->getPropertyByID(242);
        $announcement->phone_id = $this->getPropertyByID(243);
    }

    public function actionInsert(){
        if(isset($_POST['data'])  && isset($_POST['uploadSave']) && isset($_POST['uploadDelete'])){
            $announcementToEdit = $_POST['announcementToEdit'];
            $this->formData = json_decode($_POST['data']);
            $this->imagesToSave = json_decode($_POST['uploadSave']);
            $this->imagesToDelete = json_decode($_POST['uploadDelete']);

            $announcementModel = $this->announcementForm;

            if($announcementToEdit != 'null'){
                $current_announcement = $announcementModel::model()->findByPk($announcementToEdit);
            }else{
                $current_announcement = new $announcementModel();
            }

            $this->fillOrdinaryData($current_announcement,true);
            $this->fillUserData($current_announcement);
            $this->fillPriceData($current_announcement , 175 , 176 , 177);
            $this->fillMainImage($current_announcement);
            $this->fillSpecificData($current_announcement);

            $current_announcement->save();

            $announcement_id = $current_announcement->getPrimaryKey();
            $this->processImages($announcement_id , $announcementToEdit != 'null');
            $this->processProperties($announcement_id , $announcementToEdit != 'null');

            $summary = $this->getPropertiesSummary($announcement_id);
            $announcementModel::model()->updateByPk($announcement_id ,
                array(  'properties_summary_eng' => $summary['eng'] ,
                    'properties_summary_arm' => $summary['arm'] ,
                    'properties_summary_rus' => $summary['rus'] ,
                    'properties_summary_geo' => $summary['geo'])
            );
        }
    }
}