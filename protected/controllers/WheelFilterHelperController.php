<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Yerem
 * Date: 9/22/13
 * Time: 6:50 PM
 * To change this template use File | Settings | File Templates.
 */

class WheelFilterHelperController extends BaseFiltersController {


    public function getModelByID($id){
        return WheelsAnnouncement::model()->findByPk($id);
    }


    private static function locationIsSelected($addedFilters){
        if($addedFilters){
            foreach($addedFilters as $filter){
                if($filter->filter_name == Messages::getMessage(57)){
                    return true;
                }
            }
        }
        return false;
    }

    private static function typeIsSelected($addedFilters){
        if($addedFilters){
            foreach($addedFilters as $filter){
                if($filter->filter_name == Messages::getMessage(136)){
                    return true;
                }
            }
        }
        return false;
    }


    public function actionGetAllFilters(){
        $addedFilters = json_decode($_POST['data']);
        $condition = $this->getDBCriteriaByCondition($addedFilters);
        $announcements = WheelsAnnouncement::model()->findAll($condition);

        $all_filters = array();

        if(WheelFilterHelperController::typeIsSelected($addedFilters)){
            $all_filters[] = array("filter_name" => Messages::getMessage(137), "filter_type" => "1", "filters" => DAOFilters::getWheelSubType($condition->condition, "wheels_announcement"));
        }else{
            $all_filters[] = array("filter_name" => Messages::getMessage(136), "child_name" => Messages::getMessage(137), "filter_type" => "1", "filters" => DAOFilters::getWheelType($condition->condition, "wheels_announcement"));
        }

        $all_filters[] = array("filter_name" => Messages::getMessage(138), "filter_type" => "1", "filters" => DAOFilters::getWheelInches($condition->condition, "wheels_announcement"));
        $all_filters[] = array("filter_name" => Messages::getMessage(139), "filter_type" => "1", "filters" => DAOFilters::getWheelWidth($condition->condition, "wheels_announcement"));
        $all_filters[] = array("filter_name" => Messages::getMessage(140), "filter_type" => "1", "filters" => DAOFilters::getWheelHeight($condition->condition, "wheels_announcement"));
        $all_filters[] = array("filter_name" => Messages::getMessage(75), "filter_type" => "1", "filters" => DAOFilters::getProductions($condition->condition, "wheels_announcement"));
        $all_filters[] = array("filter_name" => Messages::getMessage(17), "filter_type" => "1", "filters" => DAOFilters::getCarPrices($condition->condition, "wheels_announcement"));

        $all_filters[] = array("filter_name" => Messages::getMessage(112), "filter_type" => "1", "filters" => DAOFilters::getWheelCondition($condition->condition, "wheels_announcement"));


        //TODO: CHECK THIS
        $all_filters[] = array("filter_name" => Messages::getMessage(55), "filter_type" => "1", "filters" => DAOFilters::getUserRoles($condition->condition, "wheels_announcement"));


        if(WheelFilterHelperController::locationIsSelected($addedFilters)){
            $all_filters[] = array("filter_name" => Messages::getMessage(58), "filter_type" => "1", "filters" => DAOFilters::getSubLocations($condition->condition, "wheels_announcement"));
        }else{
            $all_filters[] = array("filter_name" => Messages::getMessage(57), "child_name" => Messages::getMessage(58), "filter_type" => "1", "filters" => DAOFilters::getLocations($condition->condition, "wheels_announcement"));
        }





        $filters = $all_filters;
        if($addedFilters){
            for($i = 0; $i < count($all_filters); ++$i){
                $filter = $all_filters[$i];
                foreach($addedFilters as $addedFilter){
                    if($addedFilter->filter_name == $filter['filter_name']){
                        unset($filters[$i]);
                    }
                }
            }
        }

        $data = array("filters"=>$filters, "announcements" => $announcements, "isAdmin" => $this->isAdmin() );
        echo CJSON::encode($data);
    }


}