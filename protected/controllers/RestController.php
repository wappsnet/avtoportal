<?php

class RestController extends Controller
{
    // Members
    /**
     * Key which has to be in HTTP USERNAME and PASSWORD headers
     */
    Const APPLICATION_ID = 'ASCCPE';

    /**
     * Default response format
     * either 'json' or 'xml'
     */
    private $format = 'json';

    protected $models = array(  'groups'=> 'CPropertyGroup',
                                'mark'=>'CCarMark',
                                'location'=>'CLocation',
                                'arm_location' => 'ArmenianLocations',
                                'car_announcements'=>'CarAnnouncement',
                                'news'=>'News',
                                'appa'=>'AppaItems',
                                'autoschool'=>'Autoschool',
                                'techObserve'=>'TechObserve',
                                'phoneType'=>'UserPhoneType',
                                'spareParts' => 'SparePartAnnouncement',
                                'wheels'=>'WheelsAnnouncement',
                                'car_rent_announcements'=>'CarRentAnnouncement',
                                'banks' => 'BanksAutoLoan',
                                'rural_announcements' =>'RuraltechAnnouncement',
                                'job_announcements' => 'JobAnnouncement',
                                'taxi_announcements'=>'TaxiAnnouncement',
                                'specCar_announcements'=>'SpecCarAnnouncement',
                                'evacuator_announcements'=> 'EvacuatorAnnouncement',
                                'shipping_announcements'=> 'ShippingAnnouncement',
                                'transportation_announcements'=> 'TransportationAnnouncement',
                                'tourism_announcements'=> 'TouristAnnouncement',
                                'appaAutoTypes' => 'AppaAutoType',
                                'appaAutoUsages' => 'AppaAutoUsage',
                                'appaContractPeriods' => 'AppaContractPeriods',
                                'sortingOptions' => 'SortingOptions',
                                'spec_car_announcements' => 'SpecCarAnnouncement',
                                'auto_service' => 'AutoServiceAnnouncement',
                                'services' =>'Services' ,
                                'taxService' => 'TaxService',
                                'ModelGroups' => 'ModelGroups',
                                'RoadPoliceAddresses' => 'RoadPoliceAddress',
                             );

    /**
     * @param string $model_id
     * @param bool $create supposed to use in future Armen
     * @return CActiveRecord
     */
    private function findModel($model_id, $create = false){
        if(isset($this->models[$model_id])){
                return new $this->models[$model_id]();
        }else{
            $this->_sendResponse(501, sprintf(
                'Error: Mode is not implemented for model <b>%s</b>',
                $model_id));
            Yii::app()->end();
        }
    }
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array();
    }

    // Actions
    public function actionList()
    {
        $models = $this->findModel($_GET['model'])->findAll();
        // Did we get some results?
//        if (empty($models)) {
//            // No
//            $this->_sendResponse(200,
//                sprintf('No items where found for model <b>%s</b>', $_GET['model']));
//        } else {
            // Prepare response
            $rows = array();
            foreach ($models as $model)
                $rows[] = $model->attributes;
            // Send the response
            $this->_sendResponse(200, CJSON::encode($rows));
 //       }
    }

    public function actionView()
    {
        // Check if id was submitted via GET
        if (!isset($_GET['id']))
            $this->_sendResponse(500, 'Error: Parameter <b>id</b> is missing');

        if(isset($_GET['ann_id'])){
           $model = GroupAnswers::findByPk($_GET['id'] ,$_GET['ann_id'],$_GET['model']);
        }else{
            $model = $this->findModel($_GET['model'])->findByPk($_GET['id']);
        }
        // Did we find the requested model? If not, raise an error
        if (is_null($model))
            $this->_sendResponse(404, 'No Item found with id ' . $_GET['id']);
        else
            $this->_sendResponse(200, CJSON::encode($model));

    }

    public function actionCreate()
    {
        $model = $this->findModel($_GET['model'], true);
        $json = file_get_contents('php://input'); //$GLOBALS['HTTP_RAW_POST_DATA'] is not preferred: http://www.php.net/manual/en/ini.core.php#ini.always-populate-raw-post-data
        $put_vars = CJSON::decode($json, true); //true means use associative array
        foreach ($put_vars as $var => $value) {
            // Does the model have this attribute? If not raise an error
            if ($model->hasAttribute($var))
                $model->$var = $value;
            else
                $this->_sendResponse(500,
                    sprintf('Parameter <b>%s</b> is not allowed for model <b>%s</b>', $var,
                        $_GET['model']));
        }
        // Try to save the model
        if ($model->save())
            $this->_sendResponse(200, CJSON::encode($model));
        else {
            // Errors occurred
            $msg = "<h1>Error</h1>";
            $msg .= sprintf("Couldn't create model <b>%s</b>", $_GET['model']);
            $msg .= "<ul>";
            foreach ($model->errors as $attribute => $attr_errors) {
                $msg .= "<li>Attribute: $attribute</li>";
                $msg .= "<ul>";
                foreach ($attr_errors as $attr_error)
                    $msg .= "<li>$attr_error</li>";
                $msg .= "</ul>";
            }
            $msg .= "</ul>";
            $this->_sendResponse(500, $msg);
        }
    }

    public function actionUpdate()
    {
        // Parse the PUT parameters. This didn't work: parse_str(file_get_contents('php://input'), $put_vars);
        $json = file_get_contents('php://input'); //$GLOBALS['HTTP_RAW_POST_DATA'] is not preferred: http://www.php.net/manual/en/ini.core.php#ini.always-populate-raw-post-data
        $put_vars = CJSON::decode($json, true); //true means use associative array

        $model = $this->findModel($_GET['model'])->findByPk($_GET['id']);
        // Did we find the requested model? If not, raise an error
        if ($model === null)
            $this->_sendResponse(400,
                sprintf("Error: Didn't find any model <b>%s</b> with ID <b>%s</b>.",
                    $_GET['model'], $_GET['id']));

        // Try to assign PUT parameters to attributes
        foreach ($put_vars as $var => $value) {
            // Does model have this attribute? If not, raise an error
            if ($model->hasAttribute($var))
                $model->$var = $value;
            else {
                $this->_sendResponse(500,
                    sprintf('Parameter <b>%s</b> is not allowed for model <b>%s</b>',
                        $var, $_GET['model']));
            }
        }
        // Try to save the model
        if ($model->save())
            $this->_sendResponse(200, CJSON::encode($model));
        else
            $msg = "<h1>Error</h1>";
        $msg .= sprintf("Couldn't create model <b>%s</b>", $_GET['model']);
        $msg .= "<ul>";
        foreach ($model->errors as $attribute => $attr_errors) {
            $msg .= "<li>Attribute: $attribute</li>";
            $msg .= "<ul>";
            foreach ($attr_errors as $attr_error)
                $msg .= "<li>$attr_error</li>";
            $msg .= "</ul>";
        }
        $msg .= "</ul>";
        $this->_sendResponse(500, $msg);
    }

    public function actionDelete()
    {
        $model = $this->findModel($_GET['model'])->findByPk($_GET['id']);
        // Was a model found? If not, raise an error
        if ($model === null)
            $this->_sendResponse(400,
                sprintf("Error: Didn't find any model <b>%s</b> with ID <b>%s</b>.",
                    $_GET['model'], $_GET['id']));

        // Delete the model
        $num = $model->delete();
        if ($num > 0)
            $this->_sendResponse(200, $num); //this is the only way to work with backbone
        else
            $this->_sendResponse(500,
                sprintf("Error: Couldn't delete model <b>%s</b> with ID <b>%s</b>.",
                    $_GET['model'], $_GET['id']));
    }

    private function _sendResponse($status = 200, $body = '', $content_type = 'text/html')
    {
        // set the status
/*        $status_header = 'HTTP/1.1 ' . $status . ' ' . $this->_getStatusCodeMessage($status);
        header($status_header);
        // and the content type
        header('Content-type: ' . $content_type);*/

        // pages with body are easy
        if ($body != '') {
            // send the body
            echo $body;
        } // we need to create the body if none is passed
        else {
            // create some body messages
            $message = '';

            // this is purely optional, but makes the pages a little nicer to read
            // for your users.  Since you won't likely send a lot of different status codes,
            // this also shouldn't be too ponderous to maintain
            switch ($status) {
                case 401:
                    $message = 'You must be authorized to view this page.';
                    break;
                case 404:
                    $message = 'The requested URL ' . $_SERVER['REQUEST_URI'] . ' was not found.';
                    break;
                case 500:
                    $message = 'The server encountered an error processing your request.';
                    break;
                case 501:
                    $message = 'The requested method is not implemented.';
                    break;
            }

            // servers don't always have a signature turned on
            // (this is an apache directive "ServerSignature On")
            $signature = ($_SERVER['SERVER_SIGNATURE'] == '') ? $_SERVER['SERVER_SOFTWARE'] . ' Server at ' . $_SERVER['SERVER_NAME'] . ' Port ' . $_SERVER['SERVER_PORT'] : $_SERVER['SERVER_SIGNATURE'];

            // this should be templated in a real-world solution
            $body = '
                <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
                <html>
                <head>
                    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
                    <title>' . $status . ' ' . $this->_getStatusCodeMessage($status) . '</title>
                </head>
                <body>
                    <h1>' . $this->_getStatusCodeMessage($status) . '</h1>
                    <p>' . $message . '</p>
                    <hr />
                    <address>' . $signature . '</address>
                </body>
                </html>';

            echo $body;
        }
        Yii::app()->end();
    }

    private function _getStatusCodeMessage($status)
    {
        // these could be stored in a .ini file and loaded
        // via parse_ini_file()... however, this will suffice
        // for an example
        $codes = Array(
            200 => 'OK',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
        );
        return (isset($codes[$status])) ? $codes[$status] : '';
    }
}