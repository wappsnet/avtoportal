    <?php
/**
 * Created by JetBrains PhpStorm.
 * User: superuser
 * Date: 5/29/13
 * Time: 1:18 AM
 * To change this template use File | Settings | File Templates.
 */

class AutoFormController extends BaseFormController {

    public function init(){
        Yii:: app () ->cache->flush();
        parent::init();
        $this->announcementForm = 'CarAnnouncement';
        $this->imagesForm = 'CarPhotos';
        $this->propertiesForm = 'CarProperty';

        $this->propertyIds = array(
            'mileage'    => 7,
            'body'       => 4,
            'color'      => 11,
            'interior'   => 12,
            'doors'      => 18,
            'hand drive' => 10,
            'gearbox'    => 9,
            'engine'     => 13,
            'power'      => 15,
            'cylinders'  => 16,
            'Train'      => 17,
            'wheels'     => 19
        );

    }

    private function  checkForMileage(){
        $temp = array();
        $temp['prop_id'] = 7;
        foreach($this->formData->groups as $group ){
            foreach($group->properties as $property){
                if($property->id == 8){
                    $lookup_property = CLookupProperty::model()->findByPk($property->value->id);
                    $temp['name_arm'] = $lookup_property->name_arm;
                    $temp['name_eng'] = $lookup_property->name_eng;
                    $temp['name_rus'] = $lookup_property->name_rus;
                    $temp['name_geo'] = $lookup_property->name_geo;
                }
            }
        }
        $this->extraRightProps[] = $temp;

    }

    protected  function  fillSpecificData(&$announcement){


        $announcement->mark_id = $this->getPropertyByID(1);
        $announcement->model_id = $this->getPropertyByID(2);
        $announcement->body_style_id = $this->getPropertyByID(4);
        $announcement->year = $this->getPropertyNameByID(5);
        $announcement->mileage = $this->getPropertyByID(7);
        $announcement->gearbox_id = $this->getPropertyByID(9);
        $announcement->color_id = $this->getPropertyByID(11);
        $announcement->engine_id = $this->getPropertyByID(13);
        $announcement->engine_volume = $this->getPropertyByID(14);
        $announcement->tax_id = $this->getPropertyByID(26);
        $announcement->announcement_name = $this->getPropertyNameByID(5) .' '. $this->getPropertyNameByID(1) . ' ' .  $this->getPropertyNameByID(2) . ' ' . $this->getPropertyNameByID(3);
        $announcement->negotiable = $this->getPropertyByID(24) == 1 ? 1 : 0;
        $announcement->byExchange = $this->getPropertyByID(25) == 1 ? 1 : 0;
        $announcement->engine_power = $this->getPropertyByID(15);
        $announcement->is_crashed = $this->getPropertyByID(238);
        $announcement->rudder_id = $this->getPropertyByID(10);
        $announcement->phone_id = $this->getPropertyByID(243);
//        $announcement->background_1 = $this->getPropertyByID(250) == 1 ? "#E5DEFF" : 0;
//        $announcement->background_2 = $this->getPropertyByID(256) == 1 ? "#f1f1f1" : 0;
//        $announcement->background_3 = $this->getPropertyByID(257) == 1 ? "#f4f3b3" : 0;
//        $announcement->background_4 = $this->getPropertyByID(258) == 1 ? "#DBF4C1" : 0;
//        $announcement->background_5 = $this->getPropertyByID(259) == 1 ? "#FFFFFF" : 0;


    }

    public function actionInsert(){
        if(isset($_POST['data'])  && isset($_POST['uploadSave']) && isset($_POST['uploadDelete'])){
            $announcementToEdit = $_POST['announcementToEdit'];
            $this->formData = json_decode($_POST['data']);
            $this->imagesToSave = json_decode($_POST['uploadSave']);
            $this->imagesToDelete = json_decode($_POST['uploadDelete']);

            $announcementModel = $this->announcementForm;

            if($announcementToEdit != 'null'){
                $current_announcement = $announcementModel::model()->findByPk($announcementToEdit);
            }else{
                $current_announcement = new $announcementModel();
            }

            $this->fillOrdinaryData($current_announcement,true);
            $this->fillUserData($current_announcement);
            $this->fillPriceData($current_announcement , 21 , 22 , 23);
            $this->fillMainImage($current_announcement);
            $this->fillSpecificData($current_announcement);
            $current_announcement->save();

            $announcement_id = $current_announcement->getPrimaryKey();

            $this->processImages($announcement_id , $announcementToEdit != 'null');
            $this->processProperties($announcement_id , $announcementToEdit != 'null');
            $this->checkForMileage();

            $summary = $this->getPropertiesSummary($announcement_id);

            $announcementModel::model()->updateByPk($announcement_id ,
                array(  'properties_summary_eng' => $summary['eng'] ,
                    'properties_summary_arm' => $summary['arm'] ,
                    'properties_summary_rus' => $summary['rus'] ,
                    'properties_summary_geo' => $summary['geo'])
            );
        }
    }


    public function actionGetChildren(){
        $child_id = $_GET["child_id"];
        $father_item_id = $_GET["father_item_id"];
        $childItems = Utility::getLookupByID_father($child_id , $father_item_id);

        echo CJSON::encode($childItems);
    }


    public function actionCalculateAppa(){
        $data = json_decode($_POST['data']);

        $carEnginePower = $this->getCarPowerId($data->enginePower);
        $isMoreThanOneDriver = $data->moreThanOneDriver;
        $autoUsageId = $data->autoUsage;
        $autoTypeId = $data->autoType;
        $isLessThan23YearsOld = $this->lessThan23($data->birthDate);
        $isLess3YearsExperience = $this->lessThan3($data->cardDate);

        $contractPeriodId = $data->coefficientId;

        $resSet = Yii::app()->db->createCommand()
            ->select('amount')
            ->from('appa_calculator')
            ->where('appa_auto_type='.$autoTypeId.' AND '.'appa_auto_usage='.$autoUsageId.
            ' AND '.'car_power='.$carEnginePower.' AND '.'less_than_23_years_old='.($isLessThan23YearsOld?'1':'0').
            ' AND '.'less_than_3_years_experience='.($isLess3YearsExperience?'1':'0').
            ' AND '.'more_than_one_driver='.($isMoreThanOneDriver?'1':'0'))
            ->queryColumn();
        $amount = $resSet[0];

        $resSet =  Yii::app()->db->createCommand()
            ->select('coefficient')
            ->from('appa_calculator_coefficients')
            ->where('contract_period_id ='.$contractPeriodId)
            ->queryColumn();
        $coefficient = $resSet[0];

        echo $coefficient*$amount;

    }

    private function lessThan23($birthDay){
        $d1 = new DateTime();
        $d2 = new DateTime($birthDay);
        $diff = $d2->diff($d1);
        return $diff->y < 23;
    }

    private function lessThan3($birthDay){
        $d1 = new DateTime();
        $d2 = new DateTime($birthDay);
        $diff = $d2->diff($d1);
        return $diff->y < 3;
    }

    private function getCarPowerId($power){
        $resSet =  Yii::app()->db->createCommand()
            ->select('id')
            ->from('car_power_filters')
            ->where('start <='.$power.' AND '.'end >'.$power)
            ->queryColumn();
        return $resSet[0];
    }

    public function actionGetTopAutos(){
        $criteria = new CDbCriteria();
        $criteria->limit = 30;
        $criteria->order = 'id DESC';
        $models = CarAnnouncement::model()->findAll($criteria);
        echo CJSON::encode($models);
    }
}