<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Yerem
 * Date: 9/26/13
 * Time: 12:05 AM
 * To change this template use File | Settings | File Templates.
 */

class TaxiFilterHelperController extends BaseFiltersController{

    public function getModelByID($id){
        return TaxiAnnouncement::model()->findByPk($id);
    }

    private static function districtIsSelected($addedFilters)
    {
        if ($addedFilters) {
            foreach ($addedFilters as $filter) {
                if ($filter->filter_name == Messages::getMessage(68)) {
                    return true;
                }
            }
        }
        return false;
    }




    public function actionGetAllFilters(){
        $addedFilters = json_decode($_POST['data']);
        $condition = TaxiFilterHelperController::getDBCriteriaByCondition($addedFilters);
        $announcements = TaxiAnnouncement::model()->findAll($condition);

        $all_filters = array();
        if (TaxiFilterHelperController::districtIsSelected($addedFilters)) {
            $all_filters[] = array("filter_name" => Messages::getMessage(60), "filter_type" => "1", "filters" => DAOFilters::getTaxiCity($condition->condition, "taxi_announcement"));
        } else {
            $all_filters[] = array("filter_name" => Messages::getMessage(68), "child_name" => Messages::getMessage(60), "filter_type" => "1", "filters" => DAOFilters::getTaxiDistricts($condition->condition, "taxi_announcement"));
        }

        $all_filters[] = array("filter_name" => Messages::getMessage(59), "filter_type" => "1", "filters" => DAOFilters::getTaxiMinimalPrices($condition->condition, "taxi_announcement"));
        $all_filters[] = array("filter_name" => Messages::getMessage(109), "filter_type" => "1", "filters" => DAOFilters::getTaxiPerKMPrices($condition->condition, "taxi_announcement"));

        $filters = $all_filters;
        if($addedFilters){
            for($i = 0; $i < count($all_filters); ++$i){
                $filter = $all_filters[$i];
                foreach($addedFilters as $addedFilter){
                    if($addedFilter->filter_name == $filter['filter_name']){
                        unset($filters[$i]);
                    }
                }
            }
        }

        $data = array("filters"=>$filters, "announcements" => $announcements, "isAdmin" => $this->isAdmin() );
        echo CJSON::encode($data);
    }

}