<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Yerem
 * Date: 2/12/14
 * Time: 8:52 PM
 * To change this template use File | Settings | File Templates.
 */

class DetailedController extends BaseFormController{
    private $allPropIDsToShow = array(
                "autoPropIDs" => array(
                        'millage' => 7,
                        'body' => 4,
                        'color' => 11,
                        'interior' => 12,
                        'doors' => 18,
                        'hand drive' => 10,
                        'gearbox' => 9,
                        'engine' => 13,
                        'power' => 15,
                        'cylinders' => 16,
                        'Train' => 17,
                        'wheels' => 19
                ),
                "carRent" => array(
                    'millage' => 7,
                    'body' => 4,
                    'color' => 11,
                    'interior' => 12,
                    'doors' => 18,
                    'hand drive' => 10,
                    'gearbox' => 9,
                    'engine' => 13,
                    'power' => 15,
                    'cylinders' => 16,
                    'Train' => 17,
                    'wheels' => 19
                ),
                "job" => array(
                    'place' => 188,
                    'type'  => 190,
                    'time'  => 240
                ),
                "shipping" => array(
                    'tech'       => 225,
                    'vendor'       => 229
                ),
                "evac" => array(
                    'price'       => 182,
                    'block'       => 181,
                    'district'       => 180,
                ),
                "agro" => array(
                    'mark'    => 171,
                    'body' => 172,
                    'vendor'    => 174,
                    'driver'       => 175
                ),
                "taxi" => array(
                    'mark'       => 179,
                    'price'       => 182,
                    'block'       => 181,
                    'district'       => 180,
                ),
                "trans" => array(
                    'tech'       => 213,
                    'vendor'       => 216,
                    'country'       => 214,
                    'place'       => 215,
                ),
                "tourism" => array(
                    'type'      => 219,
                    'vendor'    => 221,
                    'driver'    => 222
                ),
                "wheels" => array(
                    'type'    => 151,
                    'width'       => 152,
                    'height'      => 153,
                    'inch'    => 154,
                    'production'       => 155
                ),
                "spare_part" => array(
                    'type'    => 192,
                    'subtype'       => 193,
                    'body'      => 194,
                    'mark'    => 195,
                    'model'       => 197,
                    'state'       => 198
                ),
                "spec_car" => array(
                    'vendor'      => 235,
                ),

    );


    private function getAllProps($properties , $typeOfIDs){
        $result = array();
        $prop = array();

        foreach($this->allPropIDsToShow[$typeOfIDs] as $prop_id){

            if($prop_id == 7 || $prop_id == 18 || $prop_id == 16){
                foreach($properties as $raw_prop){
                    if($raw_prop['prop_id'] == $prop_id){
                        if($raw_prop['value'] != "" && $raw_prop['value'] != "null" && $raw_prop['value'] != null){
                            $prop['prop_name'] =  $raw_prop['prop_name'];
                            $prop['text'] =  $raw_prop['value'];
                            $prop['prop_type'] =  1;
                            $result[] = $prop;
                        }
                    }
                }
            }else{
                foreach($properties as $raw_prop){
                    if($raw_prop['prop_id'] == $prop_id){
                        if($raw_prop['text'] != "" && $raw_prop['text'] != "null" && $raw_prop['text'] != null){
                            $prop['prop_name'] =  $raw_prop['prop_name'];
                            $prop['text'] =  $raw_prop['text'];
                            $prop['prop_type'] =  2;
                            $result[] = $prop;
                        }
                    }
                }
            }
        }

        return $result;
    }

    private function getAdditionalOptions($properties){
        $options = array();
        $option = array();

        foreach ($properties as $prop){
            if($prop['group_id'] == 5 || $prop['group_id'] == 6 || $prop['group_id'] == 7){
                if($prop['value'] == 'true'){
                    $option['prop_name'] = $prop['prop_name'];
                    $option['prop_type'] =  3;
                    $option['text'] = "";
                    $options[] = $option;
                }
            }
            else if($prop['prop_id'] == 223){
                $option['prop_name'] = $prop['prop_name'];
                $option['prop_type'] =  3;
                $option['text'] = "";
                $options[] = $option;
            }
        }

        return $options;
    }

    private function leftAndRightSides($props , $options){
        $result = array();

        $propsForLeft = array_slice ( $props,  0 , round( sizeof($props) / 2 ));
        $propsForRight = array_slice ( $props, round(sizeof($props) / 2));

        if($options != null){
            $optionsForLeft = array_slice ( $options,  0 , round( sizeof($options) / 2 ));
            $optionsForRight = array_slice ( $options,  round(sizeof($options) / 2));

            $result["left"] = array_merge($propsForLeft ,$optionsForLeft);
            $result["right"] = array_merge($propsForRight ,$optionsForRight);
        }else{
            $result["left"] = $propsForLeft;
            $result["right"] = $propsForRight;
        }

        return $result;
    }

    private function imageProcessing ($imageModel , $item_id){

        $criteria = new CDbCriteria();
        $criteria->condition = 'announcement_id = :ann_id';

        $criteria->params = array(':ann_id'=>$item_id);
        $images = $imageModel::model()->findAll($criteria);

        return $images;

    }

    public function actionCarDetailed(){
        $car_id = $_POST['car_id'];
        $item = CarAnnouncement::model()->findByPk($car_id);

        $properties = Yii::app()->db->createCommand()->select('value, text_'.Messages::getLanguageId().'  text, cp.name_'.Messages::getLanguageId().' prop_name,
        cp.c_property_type_id prop_type , cp.id prop_id , cp.c_property_group_id group_id')->
            from('car_property')->join('c_property cp','cp.id = c_property_id')->where('announcement_id = :ann_id',array('ann_id'=>$item->id))->query()->readAll();

        $allProps = $this->getAllProps($properties , "autoPropIDs");
        $allOptions = $this->getAdditionalOptions($properties);

        $twoSideProps = $this->leftAndRightSides($allProps , $allOptions);

        $item->propertiesLeft  = $twoSideProps['left'];
        $item->propertiesRight = $twoSideProps['right'];

        $item->images = $this->imageProcessing("CarPhotos" , $car_id);

        BaseFormController::setOwnerData($item);

        echo CJSON::encode($item);

    }


    public function actionCarRentDetailed(){
        $car_id = $_POST['car_id'];
        $item = CarRentAnnouncement::model()->findByPk($car_id);

        $properties = Yii::app()->db->createCommand()->select('value, text_'.Messages::getLanguageId().'  text, cp.name_'.Messages::getLanguageId().' prop_name,
        cp.c_property_type_id prop_type , cp.id prop_id , cp.c_property_group_id group_id')->
            from('car_rent_properties')->join('c_property cp','cp.id = c_property_id')->where('announcement_id = :ann_id',array('ann_id'=>$car_id))->query()->readAll();

        $allProps = $this->getAllProps($properties , "carRent");
        $allOptions = $this->getAdditionalOptions($properties);

        $twoSideProps = $this->leftAndRightSides($allProps , $allOptions);

        $item->propertiesLeft  = $twoSideProps['left'];
        $item->propertiesRight = $twoSideProps['right'];

        $item->images = $this->imageProcessing("CarRentPhotos" , $car_id);



        $criteria = new CDbCriteria;
        $criteria->select = '*';
        $criteria->condition = 'c_property_id = 242';
        $payment_types = CLookupProperty::model()->findAll($criteria);

        if($payment_types != null){
            foreach($payment_types as $type){
                if($type->id == $item->payment_type_id){
                    $item->payment_type = $type->name_arm;
                }
            }
        }

        BaseFormController::setOwnerData($item);

        echo CJSON::encode($item);

    }

    public function actionJobDetailed(){
        $car_id = $_POST['car_id'];
        $item = JobAnnouncement::model()->findByPk($car_id);

        $properties = Yii::app()->db->createCommand()->select('value, text_'.Messages::getLanguageId().'  text, cp.name_'.Messages::getLanguageId().' prop_name,
        cp.c_property_type_id prop_type , cp.id prop_id , cp.c_property_group_id group_id')->
            from('job_properties')->join('c_property cp','cp.id = c_property_id')->where('announcement_id = :ann_id',array('ann_id'=>$item->id))->query()->readAll();

        $allProps = $this->getAllProps($properties , "job");

        $twoSideProps = $this->leftAndRightSides($allProps , null);


        $item->propertiesLeft  = $twoSideProps['left'];
        $item->propertiesRight = $twoSideProps['right'];

        $item->images = $this->imageProcessing("JobPhoto" , $car_id);

        BaseFormController::setOwnerData($item);

        echo CJSON::encode($item);
    }

    public function actionShippingDetailed(){
        $car_id = $_POST['car_id'];
        $item = ShippingAnnouncement::model()->findByPk($car_id);

        $properties = Yii::app()->db->createCommand()->select('value, text_'.Messages::getLanguageId().'  text, cp.name_'.Messages::getLanguageId().' prop_name,
        cp.c_property_type_id prop_type , cp.id prop_id , cp.c_property_group_id group_id')->
            from('shipping_properties')->join('c_property cp','cp.id = c_property_id')->where('announcement_id = :ann_id',array('ann_id'=>$item->id))->query()->readAll();

        $allProps = $this->getAllProps($properties , "shipping");

        $twoSideProps = $this->leftAndRightSides($allProps , null);


        $item->propertiesLeft  = $twoSideProps['left'];
        $item->propertiesRight = $twoSideProps['right'];

        $item->images = $this->imageProcessing("ShippingPhotos" , $car_id);

        BaseFormController::setOwnerData($item);

        echo CJSON::encode($item);
    }

    public function actionEvacDetailed(){
        $car_id = $_POST['car_id'];
        $item = EvacuatorAnnouncement::model()->findByPk($car_id);

        $properties = Yii::app()->db->createCommand()->select('value, text_'.Messages::getLanguageId().'  text, cp.name_'.Messages::getLanguageId().' prop_name,
        cp.c_property_type_id prop_type , cp.id prop_id , cp.c_property_group_id group_id')->
            from('evacuator_properties')->join('c_property cp','cp.id = c_property_id')->where('announcement_id = :ann_id',array('ann_id'=>$item->id))->query()->readAll();

        $allProps = $this->getAllProps($properties , "evac");

        $twoSideProps = $this->leftAndRightSides($allProps , null);


        $item->propertiesLeft  = $twoSideProps['left'];
        $item->propertiesRight = $twoSideProps['right'];

        $item->images = $this->imageProcessing("EvacuatorPhotos" , $car_id);

        $item->payment_type = Messages::getMessage(100);

        BaseFormController::setOwnerData($item);

        echo CJSON::encode($item);
    }

    public function actionAgroDetailed(){
        $car_id = $_POST['car_id'];
        $item = RuraltechAnnouncement::model()->findByPk($car_id);

        $properties = Yii::app()->db->createCommand()->select('value, text_'.Messages::getLanguageId().'  text, cp.name_'.Messages::getLanguageId().' prop_name,
        cp.c_property_type_id prop_type , cp.id prop_id , cp.c_property_group_id group_id')->
            from('ruraltech_properties')->join('c_property cp','cp.id = c_property_id')->where('announcement_id = :ann_id',array('ann_id'=>$item->id))->query()->readAll();

        $allProps = $this->getAllProps($properties , "agro");

        $twoSideProps = $this->leftAndRightSides($allProps , null);


        $item->propertiesLeft  = $twoSideProps['left'];
        $item->propertiesRight = $twoSideProps['right'];

        $item->images = $this->imageProcessing("RuraltechPhoto" , $car_id);

        $criteria = new CDbCriteria;
        $criteria->select = '*';
        $criteria->condition = 'c_property_id = 173';
        $payment_types = CLookupProperty::model()->findAll($criteria);

        if($payment_types != null){
            foreach($payment_types as $type){
                if($type->id == $item->payment_type_id){
                    $item->payment_type = $type->name_arm;
                }
            }
        }


        BaseFormController::setOwnerData($item);

        echo CJSON::encode($item);
    }

    public function actionTaxiDetailed(){
        $car_id = $_POST['car_id'];
        $item = TaxiAnnouncement::model()->findByPk($car_id);

        $properties = Yii::app()->db->createCommand()->select('value, text_'.Messages::getLanguageId().'  text, cp.name_'.Messages::getLanguageId().' prop_name,
        cp.c_property_type_id prop_type , cp.id prop_id , cp.c_property_group_id group_id')->
            from('taxi_properties')->join('c_property cp','cp.id = c_property_id')->where('announcement_id = :ann_id',array('ann_id'=>$item->id))->query()->readAll();

        $allProps = $this->getAllProps($properties , "taxi");

        $twoSideProps = $this->leftAndRightSides($allProps , null);


        $item->propertiesLeft  = $twoSideProps['left'];
        $item->propertiesRight = $twoSideProps['right'];

        $item->images = $this->imageProcessing("TaxiPhotos" , $car_id);

        $item->payment_type = Messages::getMessage(100);

        BaseFormController::setOwnerData($item);

        echo CJSON::encode($item);
    }

    public function actionTransDetailed(){
        $car_id = $_POST['car_id'];
        $item = TransportationAnnouncement::model()->findByPk($car_id);

        $properties = Yii::app()->db->createCommand()->select('value, text_'.Messages::getLanguageId().'  text, cp.name_'.Messages::getLanguageId().' prop_name,
        cp.c_property_type_id prop_type , cp.id prop_id , cp.c_property_group_id group_id')->
            from('transportation_properties')->join('c_property cp','cp.id = c_property_id')->where('announcement_id = :ann_id',array('ann_id'=>$item->id))->query()->readAll();

        $allProps = $this->getAllProps($properties , "trans");

        $twoSideProps = $this->leftAndRightSides($allProps , null);


        $item->propertiesLeft  = $twoSideProps['left'];
        $item->propertiesRight = $twoSideProps['right'];

        $item->images = $this->imageProcessing("TransportationPhotos" , $car_id);

        BaseFormController::setOwnerData($item);

        echo CJSON::encode($item);
    }

    public function actionTourismDetailed(){
        $car_id = $_POST['car_id'];
        $item = TouristAnnouncement::model()->findByPk($car_id);

        $properties = Yii::app()->db->createCommand()->select('value, text_'.Messages::getLanguageId().'  text, cp.name_'.Messages::getLanguageId().' prop_name,
        cp.c_property_type_id prop_type , cp.id prop_id , cp.c_property_group_id group_id')->
            from('tourism_properties')->join('c_property cp','cp.id = c_property_id')->where('announcement_id = :ann_id',array('ann_id'=>$item->id))->query()->readAll();

        $allProps = $this->getAllProps($properties , "tourism");
        $allOptions = $this->getAdditionalOptions($properties);

        $twoSideProps = $this->leftAndRightSides($allProps , $allOptions);


        $item->propertiesLeft  = $twoSideProps['left'];
        $item->propertiesRight = $twoSideProps['right'];

        $item->images = $this->imageProcessing("TourismPhotos" , $car_id);

        BaseFormController::setOwnerData($item);

        echo CJSON::encode($item);
    }

    public function actionWheelDetailed(){
        $car_id = $_POST['car_id'];
        $item = WheelsAnnouncement::model()->findByPk($car_id);

        $properties = Yii::app()->db->createCommand()->select('value, text_'.Messages::getLanguageId().'  text, cp.name_'.Messages::getLanguageId().' prop_name,
        cp.c_property_type_id prop_type , cp.id prop_id , cp.c_property_group_id group_id')->
            from('wheels_properties')->join('c_property cp','cp.id = c_property_id')->where('announcement_id = :ann_id',array('ann_id'=>$item->id))->query()->readAll();

        $allProps = $this->getAllProps($properties , "wheels");

        $twoSideProps = $this->leftAndRightSides($allProps , null);


        $item->propertiesLeft  = $twoSideProps['left'];
        $item->propertiesRight = $twoSideProps['right'];

        $item->images = $this->imageProcessing("WheelsPhoto" , $car_id);

        BaseFormController::setOwnerData($item);

        echo CJSON::encode($item);
    }

    public function actionSparePartDetailed(){
        $car_id = $_POST['car_id'];
        $item = SparePartAnnouncement::model()->findByPk($car_id);

        $properties = Yii::app()->db->createCommand()->select('value, text_'.Messages::getLanguageId().'  text, cp.name_'.Messages::getLanguageId().' prop_name,
        cp.c_property_type_id prop_type , cp.id prop_id , cp.c_property_group_id group_id')->
            from('spare_part_property')->join('c_property cp','cp.id = c_property_id')->where('announcement_id = :ann_id',array('ann_id'=>$item->id))->query()->readAll();

        $allProps = $this->getAllProps($properties , "spare_part");

        $twoSideProps = $this->leftAndRightSides($allProps , null);


        $item->propertiesLeft  = $twoSideProps['left'];
        $item->propertiesRight = $twoSideProps['right'];

        $item->images = $this->imageProcessing("SparePartPhotos" , $car_id);

        BaseFormController::setOwnerData($item);

        echo CJSON::encode($item);
    }

    public function actionSpecCarDetailed(){
        $car_id = $_POST['car_id'];
        $item = SpecCarAnnouncement::model()->findByPk($car_id);

        $properties = Yii::app()->db->createCommand()->select('value, text_'.Messages::getLanguageId().'  text, cp.name_'.Messages::getLanguageId().' prop_name,
        cp.c_property_type_id prop_type , cp.id prop_id , cp.c_property_group_id group_id')->
            from('spec_car_properties')->join('c_property cp','cp.id = c_property_id')->where('announcement_id = :ann_id',array('ann_id'=>$item->id))->query()->readAll();

        $allProps = $this->getAllProps($properties , "spec_car");

        $twoSideProps = $this->leftAndRightSides($allProps , null);


        $item->propertiesLeft  = $twoSideProps['left'];
        $item->propertiesRight = $twoSideProps['right'];

        $item->images = $this->imageProcessing("SpecCarPhotos" , $car_id);

        $item->payment_type = Messages::getMessage(320);

        BaseFormController::setOwnerData($item);

        echo CJSON::encode($item);
    }





}