<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Yerem
 * Date: 9/26/13
 * Time: 12:05 AM
 * To change this template use File | Settings | File Templates.
 */

class JobFilterHelperController extends BaseFiltersController{
    const FUNCTIONAL_TABLE_NAME = "job_announcement";

    public function getModelByID($id){
        return JobAnnouncement::model()->findByPk($id);
    }

    private static function jobTypeIsSelected($addedFilters)
    {
        if ($addedFilters) {
            foreach ($addedFilters as $filter) {
                if ($filter->filter_name == Messages::getMessage(102)) {
                    return true;
                }
            }
        }
        return false;
    }



    public function actionGetAllFilters(){
        $addedFilters = json_decode($_POST['data']);
        $condition = JobFilterHelperController::getDBCriteriaByCondition($addedFilters);
        $announcements = JobAnnouncement::model()->findAll($condition);

        $all_filters = array();

        $all_filters[] = array("filter_name" => Messages::getMessage(95), "filter_type" => "1", "filters" => DAOFilters::getJobs($condition->condition, JobFilterHelperController::FUNCTIONAL_TABLE_NAME));
        if (JobFilterHelperController::jobTypeIsSelected($addedFilters)) {
            $all_filters[] = array("filter_name" => Messages::getMessage(103), "filter_type" => "1", "filters" => DAOFilters::getJobSort($condition->condition, JobFilterHelperController::FUNCTIONAL_TABLE_NAME));
        } else {
            $all_filters[] = array("filter_name" => Messages::getMessage(102),"child_name" => Messages::getMessage(103), "filter_type" => "1", "filters" => DAOFilters::getJobTypes($condition->condition, JobFilterHelperController::FUNCTIONAL_TABLE_NAME));
        }

        $all_filters[] = array("filter_name" => Messages::getMessage(62), "filter_type" => "1", "filters" => DAOFilters::getSalaryPrices($condition->condition, JobFilterHelperController::FUNCTIONAL_TABLE_NAME));
        $all_filters[] = array("filter_name" => Messages::getMessage(166), "filter_type" => "1", "filters" => DAOFilters::getJobPlaces($condition->condition, JobFilterHelperController::FUNCTIONAL_TABLE_NAME));


        $filters = $all_filters;
        if($addedFilters){
            for($i = 0; $i < count($all_filters); ++$i){
                $filter = $all_filters[$i];
                foreach($addedFilters as $addedFilter){
                    if($addedFilter->filter_name == $filter['filter_name']){
                        unset($filters[$i]);
                    }
                }
            }
        }

        $data = array("filters"=>$filters, "announcements" => $announcements, "isAdmin" => $this->isAdmin() );
        echo CJSON::encode($data);
    }

}