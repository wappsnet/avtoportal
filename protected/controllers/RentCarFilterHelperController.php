<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Yerem
 * Date: 9/26/13
 * Time: 12:05 AM
 * To change this template use File | Settings | File Templates.
 */

class RentCarFilterHelperController extends BaseFiltersController{
    const FUNCTIONAL_TABLE_NAME = "car_rent_announcement";

    public function getModelByID($id){
        return CarRentAnnouncement::model()->findByPk($id);
    }


    private static function markIsSelected($addedFilters){
        if($addedFilters){
            foreach($addedFilters as $filter){
                if($filter->filter_name == Messages::getMessage(14)){
                    return true;
                }
            }
        }
        return false;
    }

    public function actionGetAllFilters(){
        $addedFilters = json_decode($_POST['data']);
        $condition = RentCarFilterHelperController::getDBCriteriaByCondition($addedFilters);
        $announcements = CarRentAnnouncement::model()->findAll($condition);

        $all_filters = array();
        if(RentCarFilterHelperController::markIsSelected($addedFilters)){
            $all_filters[] = array("filter_name" => Messages::getMessage(15), "filter_type" => "1", "filters" => DAOFilters::getModels($condition->condition, RentCarFilterHelperController::FUNCTIONAL_TABLE_NAME));
        }else{
            $all_filters[] = array("filter_name" => Messages::getMessage(14), "child_name" => Messages::getMessage(15), "filter_type" => "1", "filters" => DAOFilters::getMarks($condition->condition, RentCarFilterHelperController::FUNCTIONAL_TABLE_NAME));
        }
        $all_filters[] = array("filter_name" => Messages::getMessage(53), "filter_type" => "1", "filters" => DAOFilters::getBodyTypes($condition->condition, RentCarFilterHelperController::FUNCTIONAL_TABLE_NAME));
        $all_filters[] = array("filter_name" => Messages::getMessage(16), "filter_type" => "1", "filters" => DAOFilters::getCarYears($condition->condition, RentCarFilterHelperController::FUNCTIONAL_TABLE_NAME));
        $all_filters[] = array("filter_name" => Messages::getMessage(62), "filter_type" => "1", "filters" => DAOFilters::getSalaryPrices($condition->condition, RentCarFilterHelperController::FUNCTIONAL_TABLE_NAME));


        $filters = $all_filters;
        if($addedFilters){
            for($i = 0; $i < count($all_filters); ++$i){
                $filter = $all_filters[$i];
                foreach($addedFilters as $addedFilter){
                    if($addedFilter->filter_name == $filter['filter_name']){
                        unset($filters[$i]);
                    }
                }
            }
        }

        $data = array("filters"=>$filters, "announcements" => $announcements, "isAdmin" => $this->isAdmin() );
        echo CJSON::encode($data);
    }

}