<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Yerem
 * Date: 9/26/13
 * Time: 12:05 AM
 * To change this template use File | Settings | File Templates.
 */

class RuralFilterHelperController extends BaseFiltersController
{
    const FUNCTIONAL_TABLE_NAME = "ruraltech_announcement";

    public function getModelByID($id){
        return RuraltechAnnouncement::model()->findByPk($id);
    }

    public function actionGetAllFilters()
    {
        $addedFilters = json_decode($_POST['data']);
        $condition = RuralFilterHelperController::getDBCriteriaByCondition($addedFilters);
        $announcements = RuraltechAnnouncement::model()->findAll($condition);

        $all_filters = array();
        $all_filters[] = array("filter_name" => Messages::getMessage(14), "filter_type" => "1", "filters" => DAOFilters::getMarks($condition->condition, RuralFilterHelperController::FUNCTIONAL_TABLE_NAME));
        $all_filters[] = array("filter_name" => Messages::getMessage(63), "filter_type" => "1", "filters" => DAOFilters::getRuralTechnics($condition->condition, RuralFilterHelperController::FUNCTIONAL_TABLE_NAME));
        $all_filters[] = array("filter_name" => Messages::getMessage(64), "filter_type" => "1", "filters" => DAOFilters::getRuralWorkType($condition->condition, RuralFilterHelperController::FUNCTIONAL_TABLE_NAME));
        $all_filters[] = array("filter_name" => Messages::getMessage(17), "filter_type" => "1", "filters" => DAOFilters::getCarPrices($condition->condition, RuralFilterHelperController::FUNCTIONAL_TABLE_NAME));

        $all_filters[] = array("filter_name" => Messages::getMessage(53), "filter_type" => "1", "filters" => DAOFilters::getShippingBodyTypes($condition->condition, RuralFilterHelperController::FUNCTIONAL_TABLE_NAME));
        $all_filters[] = array("filter_name" => Messages::getMessage(110), "filter_type" => "1", "filters" => DAOFilters::getTourismVendors($condition->condition, RuralFilterHelperController::FUNCTIONAL_TABLE_NAME));
        /*$all_filters[] = array("filter_name" => Messages::getMessage(62), "filter_type" => "1", "filters" => DAOFilters::getSalaryPrices($condition->condition, RuralFilterHelperController::FUNCTIONAL_TABLE_NAME));*/

        $filters = $all_filters;
        if ($addedFilters) {
            for ($i = 0; $i < count($all_filters); ++$i) {
                $filter = $all_filters[$i];
                foreach ($addedFilters as $addedFilter) {
                    if ($addedFilter->filter_name == $filter['filter_name']) {
                        unset($filters[$i]);
                    }
                }
            }
        }

        $data = array("filters" => $filters, "announcements" => $announcements, "isAdmin" => $this->isAdmin() );
        echo CJSON::encode($data);
    }

}